/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.operations.Revision;

import ogm.operations.Revision;
import ogm.test.fixtures;
import vibe.data.json;
import vibe.http.client;
import crate.error;

import crate.ctfe;

import vibe.data.json;
import fluent.asserts;
import trial.discovery.spec;

alias suite = Spec!({
  describe("Revision operation", {
    RevisionOperation!Page operation;
    Json page;

    beforeEach({
      setupTestData();
      operation = new RevisionOperation!Page(crates, crates.page);
    });

    describe("when there are no changesets", {
      beforeEach({
        page = `{
          "_id": "000000000000000000000001" ,
          "info": { "changeIndex": 0 }
        }`.parseJsonString;

        crates.page.addItem(page);
      });

      it("returns an error when the revision is requested", {
        operation.getModelRevision(page, "000000000000000000000002")
          .should.throwAnyException.withMessage("The revsion does not exist.");
      });
    });

    describe("when there are is an add changeset", {
      beforeEach({
        page = `{
          "_id": "000000000000000000000001",
          "name": "name",
          "slug": "slug",
          "info": { "changeIndex": 1 }
        }`.parseJsonString;

        crates.page.addItem(page);

        auto changeSet = `{
          "model": "Page",
          "author": "000000000000000000000001",
          "itemId": "000000000000000000000001",
          "time": "2010-04-05T12:33:22Z",
          "type": "add",
          "added": {
            "_id": "000000000000000000000001",
            "name": "page name",
            "slug": "page slug"
          }
        }`.parseJsonString;

        crates.changeSet.addItem(changeSet);
      });

      it("returns the original item when the changeset has the type add", {
        auto result = operation.getModelRevision(page, "000000000000000000000001");

        result.should.equal(`{
          "_id": "000000000000000000000001",
          "name": "page name",
          "slug": "page slug",
        }`.parseJsonString);
      });
    });

    describe("when there are is a update with removed fields", {
      beforeEach({
        page = `{
          "_id": "000000000000000000000001",
          "name": "name",
          "slug": "slug",
          "info": { "changeIndex": 1 }
        }`.parseJsonString;

        crates.page.addItem(page);

        auto changeSet = `{
          "model": "Page",
          "author": "000000000000000000000001",
          "itemId": "000000000000000000000001",
          "time": "2010-04-05T12:33:22Z",
          "type": "update",
          "removed": {
            "attributes.a": 1,
            "attributes.b": 2
          }
        }`.parseJsonString;

        crates.changeSet.addItem(changeSet);
      });

      it("returns the model with the removed fields", {
        auto result = operation.getModelRevision(page, "000000000000000000000001");

        result.should.equal(`{
          "_id": "000000000000000000000001",
          "name": "name",
          "slug": "slug",
          "attributes": { "a": 1, "b": 2 },
          "info": { "changeIndex": 1 },
        }`.parseJsonString);
      });
    });

    describe("when there are is a update with added fields", {
      beforeEach({
        page = `{
          "_id": "000000000000000000000001",
          "name": "name",
          "slug": "slug",
          "info": { "changeIndex": 1 },
        }`.parseJsonString;

        crates.page.addItem(page);

        auto changeSet = `{
          "model": "Page",
          "author": "000000000000000000000001",
          "itemId": "000000000000000000000001",
          "time": "2010-04-05T12:33:22Z",
          "type": "update",
          "added": {
            "name": "name",
          }
        }`.parseJsonString;

        crates.changeSet.addItem(changeSet);
      });

      it("returns the model without added fields", {
        auto result = operation.getModelRevision(page, "000000000000000000000001");

        result.remove("name");

        result.should.equal(`{
          "_id": "000000000000000000000001",
          "slug": "slug",
          "info": { "changeIndex": 1 },
        }`.parseJsonString);
      });
    });

    describe("when there are multiple update changesets", {
      beforeEach({
        page = `{
          "_id": "000000000000000000000001",
          "name": "name",
          "slug": "slug",
          "info": { "changeIndex": 1 },
        }`.parseJsonString;

        crates.page.addItem(page);

        auto changeSet = `{
          "model": "Page",
          "author": "000000000000000000000001",
          "itemId": "000000000000000000000001",
          "time": "2010-04-05T12:33:22Z",
          "type": "update",
          "added": {
            "name": "name",
          },
          "removed": {
            "name": "inintial name",
          }
        }`.parseJsonString;
        crates.changeSet.addItem(changeSet);

        changeSet = `{
          "model": "Page",
          "author": "000000000000000000000001",
          "itemId": "000000000000000000000001",
          "time": "2010-04-05T12:33:22Z",
          "type": "update",
          "added": {
            "slug": "slug",
          }
        }`.parseJsonString;
        crates.changeSet.addItem(changeSet);
      });

      it("returns the model without added fields", {
        auto result = operation.getModelRevision(page, "000000000000000000000001");

        result.should.equal(`{
          "_id": "000000000000000000000001",
          "name": "inintial name",
          "slug": "slug",
          "info": { "changeIndex": 1 },
        }`.parseJsonString);
      });
    });
  });
});

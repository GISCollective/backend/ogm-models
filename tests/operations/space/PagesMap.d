/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.operations.space.PagesMap;

import ogm.operations.space.PagesMap;
import ogm.test.fixtures;
import vibe.data.json;
import vibe.http.client;
import crate.error;

import crate.ctfe;

import vibe.data.json;
import fluent.asserts;
import trial.discovery.spec;

alias suite = Spec!({
  describe("PagesMap operation", {
    PagesMapOperation operation;
    Json page;

    beforeEach({
      setupTestData();
      operation = new PagesMapOperation(crates);

      auto page = `{
        "_id": "000000000000000000000001",
        "space": "000000000000000000000001",
        "name": "name",
        "slug": "slug1",
        "info": { "changeIndex": 1 }
      }`.parseJsonString;

      crates.page.addItem(page);

      page = `{
        "_id": "000000000000000000000002",
        "space": "000000000000000000000001",
        "name": "name",
        "slug": "slug2",
        "info": { "changeIndex": 1 }
      }`.parseJsonString;

      crates.page.addItem(page);

      page = `{
        "_id": "000000000000000000000003",
        "space": "000000000000000000000002",
        "name": "name",
        "slug": "slug2",
        "info": { "changeIndex": 1 }
      }`.parseJsonString;

      crates.page.addItem(page);
    });

    it("only returns a map with the requested space", {
      auto pageMap = operation.getPagesMap("000000000000000000000001");

      pageMap.should.equal(`{
        "slug1": "000000000000000000000001",
        "slug2": "000000000000000000000002"
      }`.parseJsonString);
    });
  });
});

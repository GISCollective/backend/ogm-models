/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.operations.RevisionHistory;

import ogm.operations.RevisionHistory;
import ogm.test.fixtures;
import vibe.data.json;
import vibe.http.client;
import crate.error;

import crate.ctfe;

import vibe.data.json;
import fluent.asserts;
import trial.discovery.spec;

alias suite = Spec!({
  describe("RevisionHistory operation", {
    RevisionHistoryOperation!Page operation;

    beforeEach({
      setupTestData();
      operation = new RevisionHistoryOperation!Page(crates, crates.page, "http://localhost");
    });

    describe("when there are no changesets", {
      beforeEach({
        auto page = `{
          "_id": "000000000000000000000001" ,
          "info": { "changeIndex": 0 }
        }`.parseJsonString;

        crates.page.addItem(page);
      });

      it("returns an empty list", {
        auto result = operation.getRevisions("000000000000000000000001");

        result.length.should.equal(0);
      });
    });

    describe("when there are is an add changeset", {
      beforeEach({
        auto page = `{
          "_id": "000000000000000000000001",
          "name": "page name",
          "slug": "page slug",
          "info": { "changeIndex": 1 }
        }`.parseJsonString;

        crates.page.addItem(page);

        auto changeSet = `{
          "model": "Page",
          "author": "000000000000000000000001",
          "itemId": "000000000000000000000001",
          "time": "2010-04-05T12:33:22Z",
          "type": "add",
          "added": {
            "name": "page name",
            "slug": "page slug"
          }
        }`.parseJsonString;

        crates.changeSet.addItem(changeSet);
      });

      it("returns a list with the initial value change", {
        auto result = operation.getRevisions("000000000000000000000001");
        result.length.should.equal(1);
        result.should.equal([ Revision(SysTime.fromISOExtString("2010-04-05T12:33:22Z"), "http://localhost/pages/000000000000000000000001/revisions/000000000000000000000001", "000000000000000000000001") ]);
      });
    });
  });
});

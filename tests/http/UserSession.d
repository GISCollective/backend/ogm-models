/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.filter.UserSession;

import fluent.asserts;
import trial.discovery.spec;

import vibe.service.base;
import vibe.inet.url;
import vibe.stream.memory;
import vibe.inet.message;

import std.path;
import std.exception;
import std.process;
import std.file;
import std.conv;
import std.string;
import fluent.asserts;
import vibe.http.server;
import ogm.http.request;

import crate.http.request;
import crate.test.mock_query;
import crate.error;

import ogm.filter.indirectvisibility;
import ogm.test.fixtures;
import ogm.models.userProfile;

alias suite = Spec!({
  describe("The user session class", {
    RequestUserData userData;

    beforeEach({
      setupTestData();

      auto context = new VibeContext();
      (*context)["user_id"] = "1";

      userData = RequestUserData(context, VibeParams());
    });

    it("should be able to get the user profile", {
      auto profile = UserProfile().serializeToJson;
      profile["picture"] = "000000000000000000000001";


      crates.userProfile.addItem(profile);
      auto session = UserSession(userData, crates);

      session.profile._id.toString.should.equal("000000000000000000000001");
    });
  });
});

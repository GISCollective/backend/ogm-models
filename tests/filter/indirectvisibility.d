/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.filter.indirectvisibility;

import trial.discovery.spec;

import vibe.service.base;
import vibe.inet.url;
import vibe.stream.memory;
import vibe.inet.message;

import std.path;
import std.exception;
import std.process;
import std.file;
import std.conv;
import std.string;
import fluent.asserts;
import vibe.http.server;

import crate.http.request;
import crate.test.mock_query;
import crate.error;

import ogm.filter.indirectvisibility;
import ogm.test.fixtures;
import ogm.http.request;

alias suite = Spec!({
  describe("The indirect visibility filter", {
    IndirectVisibility visibility;
    MockQuery mockQuery;
    IndirectVisibility.Parameters params;
    HTTPServerRequest req;
    Json map;

    beforeEach({
      setupTestData();

      visibility = new IndirectVisibility(crates, crates.map);
      mockQuery = new MockQuery;
      params = IndirectVisibility.Parameters();
      map = LazyMap(Json.emptyObject).toJson();
    });

    describe("with GET requests", {
      beforeEach({
        req = createTestHTTPServerRequest(URL("http://localhost/items"));
      });

      it("should return the public items by default", {
        visibility.get(mockQuery, params, req);
        mockQuery.log.join(" ").should.equal("or where visibility equal 1 where maps anyOf [000000000000000000000001, 000000000000000000000003]");
      });

      it("should raise an exception when there is no user token", {
        params.visibility = "0";

        ({
          visibility.get(mockQuery, params, req);
        }).should.throwException!ForbiddenException.withMessage("You can't use the `visibility` parameter, without an auth token");
      });

      describe("with a leader token", {
        beforeEach({
          req.context["user_id"] = userId["owner"];
          req.context["email"] = userEmail["owner"];
        });

        it("should return the private items when visibility is 0", {
          params.visibility = "0";
          visibility.get(mockQuery, params, req);
          mockQuery.log.join(" ").should.equal("or where visibility equal 1 where maps anyOf [000000000000000000000001, 000000000000000000000003] or where maps anyOf [000000000000000000000001, 000000000000000000000002] where visibility equal 0 and");
        });

        it("should return the public items when visibility is 1", {
          params.visibility = "1";
          visibility.get(mockQuery, params, req);
          mockQuery.log.join(" ").should.equal("or where visibility equal 1 where maps anyOf [000000000000000000000001, 000000000000000000000003] or where maps anyOf [000000000000000000000001, 000000000000000000000002] where visibility equal 1 where maps anyOf [000000000000000000000001, 000000000000000000000003, 000000000000000000000002]");
        });

        it("should return the pedding items when visibility is -1", {
          params.visibility = "-1";
          visibility.get(mockQuery, params, req);
          mockQuery.log.join(" ").should.equal("or where visibility equal 1 where maps anyOf [000000000000000000000001, 000000000000000000000003] or where maps anyOf [000000000000000000000001, 000000000000000000000002] where visibility equal -1 and");
        });

        it("should return the public and pending items when visibility is 1 and pending mode is true", {
          params.visibility = "1";

          visibility.pendingMode = true;
          visibility.get(mockQuery, params, req);
          mockQuery.log.join(" ").should.equal("or where visibility anyOf [-1, 1] where maps anyOf [000000000000000000000001, 000000000000000000000003] or where maps anyOf [000000000000000000000001, 000000000000000000000002] where visibility anyOf [-1, 1] where maps anyOf [000000000000000000000001, 000000000000000000000003, 000000000000000000000002]");
        });
      });

      describe("with an admin token without all token", {
        beforeEach({
          req.context["user_id"] = userId["administrator"];
          req.context["email"] = userEmail["administrator"];
        });

        it("should return the private items when visibility is 0", {
          params.visibility = "0";
          visibility.get(mockQuery, params, req);
          mockQuery.log.join(" ").should.equal("or where visibility equal 1 where maps anyOf [000000000000000000000001, 000000000000000000000003] where visibility equal 0 and");
        });

        it("should return the public items when visibility is 1", {
          params.visibility = "1";
          visibility.get(mockQuery, params, req);
          mockQuery.log.join(" ").should.equal("or where visibility equal 1 where maps anyOf [000000000000000000000001, 000000000000000000000003] where visibility equal 1 where maps anyOf [000000000000000000000001, 000000000000000000000003]");
        });

        it("should return the pedding items when visibility is -1", {
          params.visibility = "-1";
          visibility.get(mockQuery, params, req);
          mockQuery.log.join(" ").should.equal("or where visibility equal 1 where maps anyOf [000000000000000000000001, 000000000000000000000003] where visibility equal -1 and");
        });

        it("should return the public and pending items when visibility is 1 and pending mode is true", {
          params.visibility = "1";

          visibility.pendingMode = true;
          visibility.get(mockQuery, params, req);
          mockQuery.log.join(" ").should.equal("or where visibility anyOf [-1, 1] where maps anyOf [000000000000000000000001, 000000000000000000000003] where visibility anyOf [-1, 1] where maps anyOf [000000000000000000000001, 000000000000000000000003]");
        });
      });

      describe("with an admin token and the all parameter", {
        beforeEach({
          req.context["isAdmin"] = "true";
          req.context["user_id"] = userId["administrator"];
          req.context["email"] = userEmail["administrator"];
          params.all = true;
        });

        it("should return the private items when visibility is 0", {
          params.visibility = "0";
          visibility.get(mockQuery, params, req);
          mockQuery.log.join(" ").should.equal("where visibility equal 0 and");
        });

        it("should return the public items when visibility is 1", {
          params.visibility = "1";
          visibility.get(mockQuery, params, req);
          mockQuery.log.join(" ").should.equal("where visibility equal 1");
        });

        it("should return the pedding items when visibility is -1", {
          params.visibility = "-1";
          visibility.get(mockQuery, params, req);
          mockQuery.log.join(" ").should.equal("where visibility equal -1 and");
        });

        it("should return pending an publisc in pending mode", {
          params.visibility = "1";

          visibility.pendingMode = true;
          visibility.get(mockQuery, params, req);
          mockQuery.log.join(" ").should.equal("where visibility anyOf [-1, 1]");
        });
      });
    });

    describe("with GET admin requests", {
      RequestUserData requestUserData;

      beforeEach({
        req = createTestHTTPServerRequest(URL("http://localhost/items"));
        requestUserData = RequestUserData(req);
      });

      it("should query features only on the right maps", {
        params.onMainMap = true;

        visibility.readableAdminItems(mockQuery, params, requestUserData);
        mockQuery.log.join(" ").should.equal("where maps anyOf [000000000000000000000001, 000000000000000000000002, 000000000000000000000003, 000000000000000000000004]");
      });

      it("should query features on the requested map", {
        params.map = "000000000000000000000001";

        visibility.readableAdminItems(mockQuery, params, requestUserData);
        mockQuery.log.join(" ").should.equal("where maps arrayContains 000000000000000000000001");
      });

      it("should query index features on the requested map", {
        map["isIndex"] = true;
        crates.map.addItem(map);

        params.onlyIndexes = true;

        visibility.readableAdminItems(mockQuery, params, requestUserData);
        mockQuery.log.join(" ").should.equal("where maps anyOf [000000000000000000000005]");
      });
    });

    describe("the visibleOnMainMap query", {
      beforeEach({
        visibility = new IndirectVisibility(crates, crates.map);
      });

      it("should not return a map that has hideOnMainMap = true", {
        map["hideOnMainMap"] = true;
        auto storedMap = crates.map.addItem(map);

        auto result = visibility.visibleOnMainMap.compute;

        result.should.not.contain(ObjectId(storedMap["_id"]));
      });

      it("should not return a map that has isIndex = true", {
        map["isIndex"] = true;
        auto storedMap = crates.map.addItem(map);

        auto result = visibility.visibleOnMainMap.compute;

        result.should.not.contain(ObjectId(storedMap["_id"]));
      });

      it("should return a map that has isIndex and hideOnMainMap = false", {
        auto storedMap = crates.map.addItem(map);
        auto result = visibility.visibleOnMainMap.compute;

        result.should.contain(ObjectId(storedMap["_id"]));
      });
    });

    describe("the hiddenOnMainMap query", {
      beforeEach({
        visibility = new IndirectVisibility(crates, crates.map);
      });

      it("should return a map that has hideOnMainMap = true", {
        map["hideOnMainMap"] = true;
        auto storedMap = crates.map.addItem(map);

        auto result = visibility.hiddenOnMainMap.compute;

        result.should.contain(ObjectId(storedMap["_id"]));
      });

      it("should return a map that has isIndex = true", {
        map["isIndex"] = true;
        auto storedMap = crates.map.addItem(map);

        auto result = visibility.hiddenOnMainMap.compute;

        result.should.contain(ObjectId(storedMap["_id"]));
      });

      it("should not return a map that has isIndex and hideOnMainMap = false", {
        auto storedMap = crates.map.addItem(map);
        auto result = visibility.hiddenOnMainMap.compute;

        result.should.not.contain(ObjectId(storedMap["_id"]));
      });
    });

    describe("the member maps canEdit rule", {
      it("is ignored when the list of maps ie empty", {
        auto feature = Json.emptyObject;
        string[] maps;

        visibility.canEditAsMember(feature, "me", maps);

        feature.should.equal(Json.emptyObject);
      });

      it("can be edited when the map is in the list and the user is a contibutor", {
        auto feature = Json.emptyObject;
        feature["contributors"] = ["other", "me"].serializeToJson;
        feature["maps"] = ["other", "map"].serializeToJson;

        string[] maps = ["map"];

        visibility.canEditAsMember(feature, "me", maps);

        feature["canEdit"].should.equal(true);
      });

      it("does not change canEdit when the user is a member on a map but not a contributor", {
        auto feature = Json.emptyObject;
        feature["contributors"] = ["other", "me"].serializeToJson;
        feature["maps"] = ["other"].serializeToJson;
        feature["canEdit"] = false;

        string[] maps = ["map"];

        visibility.canEditAsMember(feature, "me", maps);
        feature["canEdit"].should.equal(false);
      });

      it("keeps the canEdit = true when is not a member and not a contributor", {
        auto feature = Json.emptyObject;
        feature["contributors"] = ["other"].serializeToJson;
        feature["maps"] = ["other"].serializeToJson;
        feature["canEdit"] = true;

        string[] maps = ["map"];

        visibility.canEditAsMember(feature, "me", maps);
        feature["canEdit"].should.equal(true);
      });
    });
  });
});

/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.filter.visibility;

import fluent.asserts;
import trial.discovery.spec;

import vibe.service.base;
import vibe.inet.url;
import vibe.stream.memory;
import vibe.inet.message;

import std.path;
import std.exception;
import std.process;
import std.file;
import std.conv;
import std.string;
import fluent.asserts;
import vibe.http.server;

import crate.http.request;
import crate.test.mock_query;
import crate.error;

import ogm.filter.visibility;
import ogm.test.fixtures;

alias suite = Spec!({
  describe("The visibility filter", {
    VisibilityFilter!"maps" visibility;
    MockQuery mockQuery;
    VisibilityParameters params;
    HTTPServerRequest req;

    beforeEach({
      setupTestData();

      visibility = new VisibilityFilter!"maps"(crates, crates.map);
      mockQuery = new MockQuery;
      params = VisibilityParameters();
    });

    describe("with GET requests", {
      beforeEach({
        req = createTestHTTPServerRequest(URL("http://localhost/items"));
      });

      it("should return only public items by default", {
        visibility.get(mockQuery, params, req);
        mockQuery.log.join(" ").should.equal("or where visibility.isPublic equal true");
      });

      it("should return all items for admins", {
        req.context["email"] = "admin@gmail.com";
        req.context["user_id"] = "000000000000000000000005";
        req.context["isAdmin"] = true;

        visibility.get(mockQuery, params, req);
        mockQuery.log.join(" ").should.equal("or");
      });

      describe("when the `isDefault` parameter is present", {
        it("should select the public default items when no user is present", {
          params.default_ = true;

          visibility.get(mockQuery, params, req);
          mockQuery.log.join(" ").should.equal("or where visibility.isDefault equal true and where visibility.isPublic equal true");
        });

        it("should select the private default items for an owner", {
          params.default_ = true;

          req.context["email"] = "owner@gmail.com";
          req.context["user_id"] = "000000000000000000000004";

          visibility.get(mockQuery, params, req);
          mockQuery.log.join(" ").should.equal("or where visibility.isDefault equal true and where visibility.isPublic equal true " ~
            "or where visibility.team containsAny [000000000000000000000001, 000000000000000000000002] where visibility.isDefault equal true");
        });

        it("should select all default items for admins", {
          params.default_ = true;

          req.context["email"] = "admin@gmail.com";
          req.context["user_id"] = "000000000000000000000005";
          req.context["isAdmin"] = true;

          visibility.get(mockQuery, params, req);
          mockQuery.log.join(" ").should.equal("or where visibility.isDefault equal true and");
        });
      });

      describe("when the `team` parameter is present", {
        it("should throw an exception when the team does not exist", {
          params.team = "1";

          visibility.get(mockQuery, params, req)
            .should
            .throwException!CrateNotFoundException
            .withMessage("There is no item with id `1`");
        });

        it("should throw an exception when the team is private", {
          params.team = "000000000000000000000004";

          req.context["email"] = "owner@gmail.com";
          req.context["user_id"] = "000000000000000000000004";

          visibility.get(mockQuery, params, req)
            .should
            .throwException!CrateNotFoundException
            .withMessage("Can't find the team `000000000000000000000004`.");
        });

        it("should select a private team if the user has access", {
          params.team = "000000000000000000000002";

          req.context["email"] = "owner@gmail.com";
          req.context["user_id"] = "000000000000000000000004";

          visibility.get(mockQuery, params, req);
          mockQuery.log.join(" ").should.equal("or where visibility.team anyOf [000000000000000000000002] and");
        });

        it("should throw when there a private team without access is selected", {
          params.team = "000000000000000000000002,000000000000000000000004";

          req.context["email"] = "owner@gmail.com";
          req.context["user_id"] = "000000000000000000000004";

          visibility.get(mockQuery, params, req).should
            .throwException!CrateNotFoundException
            .withMessage("Can't find the team `000000000000000000000004`.");
        });

        it("should select only public items when the team is public", {
          params.team = "000000000000000000000001";

          visibility.get(mockQuery, params, req);
          mockQuery.log.join(" ").should.equal("or where visibility.team anyOf [000000000000000000000001] and where visibility.isPublic equal true");
        });

        it("should select only public items when the user is not in at least one team", {
          params.team = "000000000000000000000001,000000000000000000000003";
          req.context["email"] = "owner@gmail.com";
          req.context["user_id"] = "000000000000000000000004";

          visibility.get(mockQuery, params, req);
          mockQuery.log.join(" ").should.equal("or where visibility.team anyOf [000000000000000000000001, 000000000000000000000003] and where visibility.isPublic equal true");
        });

        it("should select only public items when the user is not in at least one team", {
          params.team = "000000000000000000000001,000000000000000000000003";
          params.published = "false";
          req.context["email"] = "owner@gmail.com";
          req.context["user_id"] = "000000000000000000000004";

          visibility.get(mockQuery, params, req);
          mockQuery.log.join(" ").should.equal("or where visibility.team anyOf [000000000000000000000001, 000000000000000000000003] and where visibility.isPublic equal __ignore");
        });
      });

      describe("when the `usableByTeam` parameter is present", {
        it("should throw an exception when the team does not exist", {
          params.usableByTeam = "1";

          visibility.get(mockQuery, params, req)
            .should
            .throwException!CrateNotFoundException
            .withMessage("There is no item with id `1`");
        });

        it("should throw an exception when the team is private", {
          params.usableByTeam = "000000000000000000000004";

          req.context["email"] = "owner@gmail.com";
          req.context["user_id"] = "000000000000000000000004";

          visibility.get(mockQuery, params, req)
            .should
            .throwException!CrateNotFoundException
            .withMessage("Can't find the team `000000000000000000000004`.");
        });

        it("should select a private team if the user has access", {
          params.usableByTeam = "000000000000000000000002";

          req.context["email"] = "owner@gmail.com";
          req.context["user_id"] = "000000000000000000000004";

          visibility.get(mockQuery, params, req);
          mockQuery.log.join(" ").should.equal("or where visibility.isDefault equal true and where visibility.isPublic equal true or or where visibility.team anyOf [000000000000000000000002] and");
        });

        it("should select only public items when the team is public", {
          params.usableByTeam = "000000000000000000000001";

          visibility.get(mockQuery, params, req);
          mockQuery.log.join(" ").should.equal("or where visibility.isDefault equal true and where visibility.isPublic equal true or or where visibility.team anyOf [000000000000000000000001] and where visibility.isPublic equal true");
        });
      });

      describe("when the `edit` parameter is present", {
        it("should throw an exception when there is no user", {
          params.edit = true;

          visibility.get(mockQuery, params, req)
            .should
            .throwException!ForbiddenException
            .withMessage("You can't edit items.");
        });

        it("should select all items if is admin and all is true", {
          params.edit = true;
          params.all = true;

          req.context["email"] = "admin@gmail.com";
          req.context["user_id"] = "000000000000000000000005";
          req.context["isAdmin"] = true;

          visibility.get(mockQuery, params, req);
          mockQuery.log.join(" ").should.equal("or");
        });

        it("should select the team items if is admin and all is false", {
          params.edit = true;
          params.all = false;

          req.context["email"] = "admin@gmail.com";
          req.context["user_id"] = "000000000000000000000005";
          req.context["isAdmin"] = true;

          visibility.get(mockQuery, params, req);
          mockQuery.log.join(" ").should.equal("or where visibility.team containsAny []");
        });

        it("should select the team items where the user is an owner", {
          params.edit = true;
          params.all = false;

          req.context["email"] = "owner@gmail.com";
          req.context["user_id"] = "000000000000000000000004";

          visibility.get(mockQuery, params, req);
          mockQuery.log.join(" ").should.equal("or where visibility.team containsAny [000000000000000000000001, 000000000000000000000002]");
        });
      });
    });

    describe("with create requests", {
      HTTPServerResponse res;

      describe("when there is no json body", {
        it("should throw a validation error", {
          req = createTestHTTPServerRequest(URL("http://localhost/items"), HTTPMethod.POST);

          visibility.create(req)
            .should
            .throwException!CrateValidationException
            .withMessage("You must pass an JSON object.");
        });
      });

      describe("when there is an empty json object", {
        it("should throw a validation error", {
          InetHeaderMap headers;
          headers["Content-Type"] = "application/json";
          req = createTestHTTPServerRequest(URL("http://localhost/items"), HTTPMethod.POST, headers, createMemoryStream(cast(ubyte[])"{}"));

          visibility.create(req)
            .should
            .throwException!CrateValidationException
            .withMessage("There are no keys in your object.");
        });
      });

      describe("when there are more than one key", {
        it("should throw a validation error", {
          InetHeaderMap headers;
          headers["Content-Type"] = "application/json";
          req = createTestHTTPServerRequest(URL("http://localhost/items"), HTTPMethod.POST, headers, createMemoryStream(cast(ubyte[])`{ "a":{}, "b":{}}`));

          visibility.create(req)
            .should
            .throwException!CrateValidationException
            .withMessage("There are more than one key in your object.");
        });
      });

      describe("when there is one key", {
        it("should throw a validation error when the value is not an object", {
          InetHeaderMap headers;
          headers["Content-Type"] = "application/json";
          req = createTestHTTPServerRequest(URL("http://localhost/items"), HTTPMethod.POST, headers, createMemoryStream(cast(ubyte[])`{ "a": 5 }`));

          visibility.create(req)
            .should
            .throwException!CrateValidationException
            .withMessage("You provided an invalid object.");
        });

        it("should throw a validation error when the visibility is missing", {
          InetHeaderMap headers;
          headers["Content-Type"] = "application/json";
          req = createTestHTTPServerRequest(URL("http://localhost/items"), HTTPMethod.POST, headers, createMemoryStream(cast(ubyte[])`{ "a": {} }`));

          visibility.create(req)
            .should
            .throwException!CrateValidationException
            .withMessage("The `visibility` object is missing.");
        });

        it("should throw a validation error when the visibility is missing", {
          InetHeaderMap headers;
          headers["Content-Type"] = "application/json";
          req = createTestHTTPServerRequest(URL("http://localhost/items"), HTTPMethod.POST, headers, createMemoryStream(cast(ubyte[])`{ "a": {
            "visibility": {}
          }}`));

          visibility.create(req)
            .should
            .throwException!CrateValidationException
            .withMessage("The `visibility.team` value is missing.");
        });

        it("should add the `isDefault` and `isPublic` values when they are not set", {
          InetHeaderMap headers;
          headers["Content-Type"] = "application/json";
          req = createTestHTTPServerRequest(URL("http://localhost/items"), HTTPMethod.POST, headers, createMemoryStream(cast(ubyte[])`{ "a": {
            "visibility": { "team": "000000000000000000000002" }
          }}`));

          req.context["email"] = "owner@gmail.com";
          req.context["user_id"] = "000000000000000000000004";

          visibility.create(req);
          req.json.should.equal(`{"a": {"visibility": {
            "isDefault": false,
            "isPublic": false,
            "team": "000000000000000000000002"
          }}}`.parseJsonString);
        });

        it("should not allow default items when they are private", {
          InetHeaderMap headers;
          headers["Content-Type"] = "application/json";
          req = createTestHTTPServerRequest(URL("http://localhost/items"), HTTPMethod.POST, headers, createMemoryStream(cast(ubyte[])`{ "a": {
            "visibility": { "team": "000000000000000000000001", "isDefault": true, "isPrivate": false }
          }}`));

          req.context["email"] = "admin@gmail.com";
          req.context["user_id"] = "000000000000000000000005";
          req.context["isAdmin"] = true;

          visibility.create(req).should
            .throwException!CrateValidationException
            .withMessage("You can't create private default items.");
        });

        it("should not allow regular users to create default items", {
          InetHeaderMap headers;
          headers["Content-Type"] = "application/json";
          req = createTestHTTPServerRequest(URL("http://localhost/items"), HTTPMethod.POST, headers, createMemoryStream(cast(ubyte[])`{ "a": {
            "visibility": { "team": "000000000000000000000002", "isDefault": true }
          }}`));

          req.context["email"] = "owner@gmail.com";
          req.context["user_id"] = "000000000000000000000004";

          visibility.create(req)
            .should
            .throwException!CrateValidationException
            .withMessage("You can't create private default items.");
        });

        it("should allow admin users to create default items", {
          InetHeaderMap headers;
          headers["Content-Type"] = "application/json";
          req = createTestHTTPServerRequest(URL("http://localhost/items"), HTTPMethod.POST, headers, createMemoryStream(cast(ubyte[])`{ "a": {
            "visibility": { "team": "000000000000000000000002", "isDefault": true, "isPublic": true }
          }}`));

          req.context["email"] = "admin@gmail.com";
          req.context["user_id"] = "000000000000000000000005";
          req.context["isAdmin"] = true;

          visibility.create(req)
            .should
            .not
            .throwAnyException;
        });

        it("should not allow users to create items for other teams", {
          InetHeaderMap headers;
          headers["Content-Type"] = "application/json";
          req = createTestHTTPServerRequest(URL("http://localhost/items"), HTTPMethod.POST, headers, createMemoryStream(cast(ubyte[])`{ "a": {
            "visibility": { "team": "000000000000000000000003" }
          }}`));

          req.context["email"] = "owner@gmail.com";
          req.context["user_id"] = "000000000000000000000004";

          visibility.create(req)
            .should
            .throwException!ForbiddenException
            .withMessage("You can't add an item for the team `000000000000000000000003`.");
        });

        it("should not allow guests to create items", {
          InetHeaderMap headers;
          headers["Content-Type"] = "application/json";
          req = createTestHTTPServerRequest(URL("http://localhost/items"), HTTPMethod.POST, headers, createMemoryStream(cast(ubyte[])`{ "a": {
            "visibility": { "team": "000000000000000000000001" }
          }}`));

          req.context["email"] = "guest@gmail.com";
          req.context["user_id"] = "000000000000000000000003";

          visibility.create(req)
            .should
            .throwException!ForbiddenException
            .withMessage("You don't have enough rights to add an item.");
        });

        it("should not allow members to create items", {
          InetHeaderMap headers;
          headers["Content-Type"] = "application/json";
          req = createTestHTTPServerRequest(URL("http://localhost/items"), HTTPMethod.POST, headers, createMemoryStream(cast(ubyte[])`{ "a": {
            "visibility": { "team": "000000000000000000000001" }
          }}`));

          req.context["email"] = "member@gmail.com";
          req.context["user_id"] = "000000000000000000000002";

          visibility.create(req)
            .should
            .throwException!ForbiddenException
            .withMessage("You don't have enough rights to add an item.");
        });

        it("should not allow leaders to create items", {
          InetHeaderMap headers;
          headers["Content-Type"] = "application/json";
          req = createTestHTTPServerRequest(URL("http://localhost/items"), HTTPMethod.POST, headers, createMemoryStream(cast(ubyte[])`{ "a": {
            "visibility": { "team": "000000000000000000000001" }
          }}`));

          req.context["email"] = "leader@gmail.com";
          req.context["user_id"] = "000000000000000000000001";

          visibility.create(req)
            .should
            .throwException!ForbiddenException
            .withMessage("You don't have enough rights to add an item.");
        });

        it("should allow owners to create items", {
          InetHeaderMap headers;
          headers["Content-Type"] = "application/json";
          req = createTestHTTPServerRequest(URL("http://localhost/items"), HTTPMethod.POST, headers, createMemoryStream(cast(ubyte[])`{ "a": {
            "visibility": { "team": "000000000000000000000001" }
          }}`));

          req.context["email"] = "owner@gmail.com";
          req.context["user_id"] = "000000000000000000000004";

          visibility.create(req)
            .should.not
            .throwAnyException;
        });
      });
    });

    describe("with update requests", {
      describe("with no payload", {
        it("should throw when there is no user", {
          auto req = createTestHTTPServerRequest(URL("http://localhost/items"), HTTPMethod.DELETE);
          req.params["id"] = "000000000000000000000001";

          visibility.update(req)
            .should
            .throwException!CrateNotFoundException
            .withMessage("Item `000000000000000000000001` not found.");
        });

        it("should not throw for admins", {
          auto req = createTestHTTPServerRequest(URL("http://localhost/items"), HTTPMethod.DELETE);
          req.params["id"] = "000000000000000000000001";
          req.context["email"] = "admin@gmail.com";
          req.context["user_id"] = "000000000000000000000005";
          req.context["isAdmin"] = true;

          visibility.update(req)
            .should.not
            .throwAnyException;
        });

        it("should not throw for an owner", {
          auto req = createTestHTTPServerRequest(URL("http://localhost/items"), HTTPMethod.DELETE);
          req.params["id"] = "000000000000000000000001";
          req.context["email"] = "owner@gmail.com";
          req.context["user_id"] = "000000000000000000000004";

          visibility.update(req)
            .should.not
            .throwAnyException;
        });

        it("should throw for an leader", {
          auto req = createTestHTTPServerRequest(URL("http://localhost/items"), HTTPMethod.DELETE);
          req.params["id"] = "000000000000000000000001";
          req.context["email"] = "leader@gmail.com";
          req.context["user_id"] = "000000000000000000000001";

          visibility.update(req)
            .should
            .throwException!ForbiddenException
            .withMessage("You don't have enough rights to remove `000000000000000000000001`.");
        });

        it("should throw for a member", {
          auto req = createTestHTTPServerRequest(URL("http://localhost/items"), HTTPMethod.DELETE);
          req.params["id"] = "000000000000000000000001";
          req.context["email"] = "member@gmail.com";
          req.context["user_id"] = "000000000000000000000002";

          visibility.update(req)
            .should
            .throwException!ForbiddenException
            .withMessage("You don't have enough rights to remove `000000000000000000000001`.");
        });

        it("should throw for a guest", {
          auto req = createTestHTTPServerRequest(URL("http://localhost/items"), HTTPMethod.DELETE);
          req.params["id"] = "000000000000000000000001";
          req.context["email"] = "guest@gmail.com";
          req.context["user_id"] = "000000000000000000000003";

          visibility.update(req)
            .should
            .throwException!ForbiddenException
            .withMessage("You don't have enough rights to remove `000000000000000000000001`.");
        });

        it("should throw for an owner deleting an item that can't be seen", {
          auto req = createTestHTTPServerRequest(URL("http://localhost/items"), HTTPMethod.DELETE);
          req.params["id"] = "000000000000000000000003";
          req.context["email"] = "owner@gmail.com";
          req.context["user_id"] = "000000000000000000000004";

          visibility.update(req)
            .should
            .throwException!CrateNotFoundException
            .withMessage("Item `000000000000000000000003` not found.");
        });

        it("should throw for a guest deleting an item that can't be seen", {
          auto req = createTestHTTPServerRequest(URL("http://localhost/items"), HTTPMethod.DELETE);
          req.params["id"] = "000000000000000000000003";
          req.context["email"] = "guest@gmail.com";
          req.context["user_id"] = "000000000000000000000003";

          visibility.update(req)
            .should
            .throwException!CrateNotFoundException
            .withMessage("Item `000000000000000000000003` not found.");
        });
      });

      describe("with a valid payload", {
        it("should fill the existing visibility value if it's missing", {
          InetHeaderMap headers;
          headers["Content-Type"] = "application/json";
          req = createTestHTTPServerRequest(URL("http://localhost/items"), HTTPMethod.PUT, headers, createMemoryStream(cast(ubyte[])`{ "a": {}}`));

          req.params["id"] = "000000000000000000000001";
          req.context["email"] = "admin@gmail.com";
          req.context["user_id"] = "000000000000000000000005";
          req.context["isAdmin"] = true;

          visibility.update(req)
            .should.not
            .throwAnyException;

          req.json.should.equal(`{"a": {"visibility": {
            "isDefault": false,
            "isPublic": true,
            "team": "000000000000000000000001" }}}`.parseJsonString);
        });

        it("should fill the existing visibility.isPublic value if it's missing", {
          InetHeaderMap headers;
          headers["Content-Type"] = "application/json";
          req = createTestHTTPServerRequest(URL("http://localhost/items"), HTTPMethod.PUT, headers, createMemoryStream(cast(ubyte[])`{ "a":{"visibility": {
            "isDefault": false,
            "team": "000000000000000000000001"
          }}}`));

          req.params["id"] = "000000000000000000000001";
          req.context["email"] = "admin@gmail.com";
          req.context["user_id"] = "000000000000000000000005";
          req.context["isAdmin"] = true;

          visibility.update(req)
            .should.not
            .throwAnyException;

          req.json.should.equal(`{"a": {"visibility": {
            "isDefault": false,
            "isPublic": true,
            "team": "000000000000000000000001" }}}`.parseJsonString);
        });

        it("should fill the existing visibility.isDefault value if it's missing", {
          InetHeaderMap headers;
          headers["Content-Type"] = "application/json";
          req = createTestHTTPServerRequest(URL("http://localhost/items"), HTTPMethod.PUT, headers, createMemoryStream(cast(ubyte[])`{ "a":{"visibility": {
            "isPublic": true,
            "team": "000000000000000000000001"
          }}}`));

          req.params["id"] = "000000000000000000000001";
          req.context["email"] = "admin@gmail.com";
          req.context["user_id"] = "000000000000000000000005";
          req.context["isAdmin"] = true;

          visibility.update(req)
            .should.not
            .throwAnyException;

          req.json.should.equal(`{"a": {"visibility": {
            "isDefault": false,
            "isPublic": true,
            "team": "000000000000000000000001" }}}`.parseJsonString);
        });

        it("should fill the existing visibility.team value if it's missing", {
          InetHeaderMap headers;
          headers["Content-Type"] = "application/json";
          req = createTestHTTPServerRequest(URL("http://localhost/items"), HTTPMethod.PUT, headers, createMemoryStream(cast(ubyte[])`{ "a":{"visibility": {
            "isDefault": false,
            "isPublic": true
          }}}`));

          req.params["id"] = "000000000000000000000001";
          req.context["email"] = "admin@gmail.com";
          req.context["user_id"] = "000000000000000000000005";
          req.context["isAdmin"] = true;

          visibility.update(req)
            .should.not
            .throwAnyException;

          req.json.should.equal(`{"a": {"visibility": {
            "isDefault": false,
            "isPublic": true,
            "team": "000000000000000000000001" }}}`.parseJsonString);
        });

        it("should not allow an owner to change the is default flag", {
          InetHeaderMap headers;
          headers["Content-Type"] = "application/json";
          req = createTestHTTPServerRequest(URL("http://localhost/items"), HTTPMethod.PUT, headers, createMemoryStream(cast(ubyte[])`{ "a":{"visibility": {
            "isDefault": true
          }}}`));

          req.params["id"] = "000000000000000000000001";
          req.context["email"] = "owner@gmail.com";
          req.context["user_id"] = "000000000000000000000004";

          visibility.update(req)
            .should
            .throwException!ForbiddenException
            .withMessage("You don't have enough rights to change the `isDefault` flag.");
        });
      });
    });

    describe("with the mapper", {
      it("should not mark the item as editable if there is no user", {
        req = createTestHTTPServerRequest(URL("http://localhost/items"), HTTPMethod.GET);

        Json val = Json.emptyObject;
        visibility.mapper(req, val);
        val.should.equal(`{"visibility": {}}`.parseJsonString);
      });

      it("should mark the item as editable for admins", {
        req = createTestHTTPServerRequest(URL("http://localhost/items"), HTTPMethod.GET);

        req.context["email"] = "admin@gmail.com";
        req.context["user_id"] = "000000000000000000000005";
        req.context["isAdmin"] = true;

        Json val = Json.emptyObject;
        visibility.mapper(req, val);
        val.should.equal(`{ "visibility": {}, "canEdit": true }`.parseJsonString);
      });

      it("should mark the item as editable for owners", {
        req = createTestHTTPServerRequest(URL("http://localhost/items"), HTTPMethod.GET);

        req.params["id"] = "000000000000000000000001";
        req.context["email"] = "owner@gmail.com";
        req.context["user_id"] = "000000000000000000000004";

        Json val = Json.emptyObject;
        val["visibility"] = Json.emptyObject;
        val["visibility"]["team"] = "000000000000000000000001";

        visibility.mapper(req, val);
        val.should.equal(`{ "visibility": {
            "team": "000000000000000000000001"
          }, "canEdit": true }`.parseJsonString);
      });

      it("should not mark the item as editable for leaders", {
        req = createTestHTTPServerRequest(URL("http://localhost/items"), HTTPMethod.GET);

        req.params["id"] = "000000000000000000000001";
        req.context["email"] = "leader@gmail.com";
        req.context["user_id"] = "000000000000000000000001";

        Json val = Json.emptyObject;
        val["visibility"] = Json.emptyObject;
        val["visibility"]["team"] = "000000000000000000000001";

        visibility.mapper(req, val);
        val.should.equal(`{ "visibility": {
            "team": "000000000000000000000001"
          }, "canEdit": false }`.parseJsonString);
      });

      it("should not mark the item as editable for members", {
        req = createTestHTTPServerRequest(URL("http://localhost/items"), HTTPMethod.GET);

        req.params["id"] = "000000000000000000000001";
        req.context["email"] = "member@gmail.com";
        req.context["user_id"] = "000000000000000000000002";

        Json val = Json.emptyObject;
        val["visibility"] = Json.emptyObject;
        val["visibility"]["team"] = "000000000000000000000001";

        visibility.mapper(req, val);
        val.should.equal(`{ "visibility": {
            "team": "000000000000000000000001"
          }, "canEdit": false }`.parseJsonString);
      });

      it("should not mark the item as editable for guests", {
        req = createTestHTTPServerRequest(URL("http://localhost/items"), HTTPMethod.GET);

        req.params["id"] = "000000000000000000000001";
        req.context["email"] = "guest@gmail.com";
        req.context["user_id"] = "000000000000000000000003";

        Json val = Json.emptyObject;
        val["visibility"] = Json.emptyObject;
        val["visibility"]["team"] = "000000000000000000000001";

        visibility.mapper(req, val);
        val.should.equal(`{ "visibility": {
            "team": "000000000000000000000001"
          }, "canEdit": false }`.parseJsonString);
      });
    });
  });
});

/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.middleware.map.mapListFieldValidationMiddleware;

import ogm.middleware.map.mapListFieldValidationMiddleware;
import ogm.test.fixtures;
import vibe.data.json;
import vibe.http.client;
import crate.error;

import crate.ctfe;

import vibe.data.json;
import fluent.asserts;
import trial.discovery.spec;

alias suite = Spec!({
  describe("mapValidation middleware", {
    MapListFieldValidationMiddleware!("iconSets", "iconset") mapValidationMiddleware;

    beforeEach({
      setupTestData();

      mapValidationMiddleware = new MapListFieldValidationMiddleware!("iconSets", "iconset")(crates, crates.iconSet);
    });

    it("throws an error when the map uses an iconset owned by other teams", {
      auto map = `{
        "visibility": {
          "team":"000000000000000000000002",
          "isPublic": true,
          "isDefault": false
        },
        "iconSets": {
          "useCustomList": true,
          "list": ["000000000000000000000003"]
        }
      }`.parseJsonString;

      mapValidationMiddleware.validateMap(map).should.throwException!CrateValidationException
        .withMessage("You can't use the iconset `set3` with id `000000000000000000000003` because it belongs to another team");
    });

    it("throws an error when the useCustomList is not set", {
      auto map = `{
        "visibility": {
          "team":"000000000000000000000002",
          "isPublic": true,
          "isDefault": false
        },
        "iconSets": {
          "list": ["000000000000000000000003"]
        }
      }`.parseJsonString;

      mapValidationMiddleware.validateMap(map).should.throwException!CrateValidationException
        .withMessage("You can't use the iconset `set3` with id `000000000000000000000003` because it belongs to another team");
    });

    it("does not throw an exception when the map uses the default sets", {
      auto map = `{
        "visibility": {
          "team":"000000000000000000000002",
          "isPublic": true,
          "isDefault": false
        },
        "iconSets": {
          "useCustomList": false,
          "list": ["000000000000000000000003"]
        }
      }`.parseJsonString;

      mapValidationMiddleware.validateMap(map).should.not.throwAnyException;
    });

    it("does not throw an exception when the iconsets belong to the map team", {
      auto map = `{
        "visibility": {
          "team":"000000000000000000000002",
          "isPublic": true,
          "isDefault": false
        },
        "iconSets": {
          "useCustomList": true,
          "list": ["000000000000000000000002"]
        }
      }`.parseJsonString;

      mapValidationMiddleware.validateMap(map).should.not.throwAnyException;
    });

    it("does not throw an exception when it has a default set of aanother team", {
      auto map = `{
        "visibility": {
          "team":"000000000000000000000002",
          "isPublic": true,
          "isDefault": false
        },
        "iconSets": {
          "useCustomList": true,
          "list": ["000000000000000000000001"]
        }
      }`.parseJsonString;

      mapValidationMiddleware.validateMap(map).should.not.throwAnyException;
    });

    it("does not throw an exception when the icon sets do not exist", {
      auto map = `{
        "visibility": {
          "team":"000000000000000000000002",
          "isPublic": true,
          "isDefault": false
        },
        "iconSets": {
          "useCustomList": true,
          "list": ["000000000000000000000090"]
        }
      }`.parseJsonString;

      mapValidationMiddleware.validateMap(map).should.not.throwAnyException;
    });
  });
});
/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.middleware.TimedCache;

import ogm.TimedCache;

import ogm.middleware.attributes.IconAttributesMapper;

import vibe.data.json;
import fluent.asserts;
import trial.discovery.spec;
import std.datetime;

alias suite = Spec!({
  describe("The timed cache", {
    it("should retreive a missing item", {
      Json getItem(string id) {
        return Json(id);
      }

      auto cache = new TimedCache!Json(&getItem);

      cache.get("1").should.equal(`"1"`.parseJsonString);
    });

    it("should query once the same id", {
      int count;

      Json getItem(string id) {
        count++;
        return Json(id);
      }

      auto cache = new TimedCache!Json(&getItem);

      cache.get("1");
      cache.get("1");

      count.should.equal(1);
    });

    it("should not remove an item on sequential access", {
      int count;

      Json getItem(string id) {
        count++;
        return Json(id);
      }

      auto cache = new TimedCache!Json(&getItem);

      cache.get("1");
      cache.reindex;
      cache.get("1");

      count.should.equal(1);
    });

    it("should not remove an item on sequential access when the expire duration is 0 seconds", {
      int count;

      Json getItem(string id) {
        count++;
        return Json(id);
      }

      auto cache = new TimedCache!Json(&getItem, 0.seconds);

      cache.get("1");
      cache.reindex;
      cache.get("1");

      count.should.equal(2);
    });
  });
});
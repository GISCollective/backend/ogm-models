/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.middleware.translation;


import ogm.middleware.translation;
import crate.ctfe;

import vibe.data.json;
import fluent.asserts;
import trial.discovery.spec;

struct TestModel {
  string name;
  @translate string localName;
}

struct TestModelWithList {
  string name;
  @translate string localName;

  TestModel[] items;
}

struct TestModelWithHashMap {
  string name;
  @translate string localName;

  TestModel[string] items;
}

alias suite = Spec!({
  describe("Translation middleware", {
    it("should extract only the adnotated fields", {
      auto description = describe!TestModel;

      Json icon = `{ "name": "new name", "localName": "localName" }`.parseJsonString;

      auto result = extractTranslations(description.fields, icon);

      result.to!string.should.equal(`{"localName":"localName"}`);
    });

    it("should ignore empty strings", {
      auto description = describe!TestModel;

      Json icon = `{ "name": "new name", "localName": "" }`.parseJsonString;

      auto result = extractTranslations(description.fields, icon);

      result.to!string.should.equal(`{}`);
    });

    it("should ignore ws strings", {
      auto description = describe!TestModel;

      Json icon = `{ "name": "new name", "localName": " " }`.parseJsonString;

      auto result = extractTranslations(description.fields, icon);

      result.to!string.should.equal(`{}`);
    });

    it("should extract adnotated fields from list items", {
      auto description = describe!TestModelWithList;

      Json icon = `{ "name": "new name", "localName": "localName",
        "items": [
          {"name": "item 1", "localName": "local 1" },
          {"name": "item 2", "localName": "local 2" }
        ]}`.parseJsonString;

      auto result = extractTranslations(description.fields, icon);

      result.to!string.should.equal(`{"localName":"localName","items":[{"localName":"local 1"},{"localName":"local 2"}]}`);
    });

    it("should ignore empty lists", {
      auto description = describe!TestModelWithList;

      Json icon = `{ "name": "new name", "localName": "localName","items": []}`.parseJsonString;

      auto result = extractTranslations(description.fields, icon);

      result.to!string.should.equal(`{"localName":"localName"}`);
    });

    it("should extract adnotated fields from hash map items", {
      auto description = describe!TestModelWithHashMap;

      Json icon = `{ "name": "new name", "localName": "localName", "items":
        {
          "a": {"name": "item 1", "localName": "local 1" },
          "b": {"name": "item 2", "localName": "local 2" }
        }}`.parseJsonString;

      auto result = extractTranslations(description.fields, icon);

      result.to!string.should.equal(`{"localName":"localName","items":{"a":{"localName":"local 1"},"b":{"localName":"local 2"}}}`);
    });

    it("should ignore empty hash maps", {
      auto description = describe!TestModelWithHashMap;

      Json icon = `{ "name": "new name", "localName": "localName", "items":{}}`.parseJsonString;

      auto result = extractTranslations(description.fields, icon);

      result.to!string.should.equal(`{"localName":"localName"}`);
    });
  });
});

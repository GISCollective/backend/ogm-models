/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.middleware.icons.IconFilter;

import ogm.middleware.icons.IconFilter;
import ogm.test.fixtures;
import vibe.data.json;
import vibe.http.client;
import crate.error;

import crate.ctfe;

import vibe.data.json;
import fluent.asserts;
import trial.discovery.spec;

alias suite = Spec!({
  describe("IconFilter middleware", {
    beforeEach({
      setupTestData();
    });

    describe("the getMapIconSets method", {
      IconFilter middleware;

      beforeEach({
        middleware = new IconFilter(crates, null, null, null);
      });

      it("returns the public map icon sets", {
        auto result = middleware.getMapIconSets("000000000000000000000001", [], false);
        result.should.equal(["000000000000000000000001"]);
      });

      it("returns an empty list for a private map", {
        try {
          auto result = middleware.getMapIconSets("000000000000000000000002", [], false);
          result.should.equal([]);
        } catch(Throwable e) {
          import std.stdio;

          writeln("============================================================");
          writeln(e);
        }
      });

      it("returns the default icon sets when a map does not have a custom list", {
        auto map = crates.map.getItem("000000000000000000000003").exec.front;
        map["iconSets"]["useCustomList"] = false;
        map["iconSets"]["list"] = Json.emptyArray;

        crates.map.updateItem(map);


        auto result = middleware.getMapIconSets("000000000000000000000003", [], false);
        result.should.equal(["000000000000000000000001"]);
      });
    });
  });
});

/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.middleware.attributes.IconAttributesMapper;

import ogm.middleware.attributes.IconAttributesMapper;
import crate.ctfe;
import crate.base;
import crate.collection.memory;

import vibe.data.json;
import fluent.asserts;
import trial.discovery.spec;

Json[string] icons;
Json[string] iconSets;
IconAttributesMapper iconAttributes;

alias suite = Spec!({
  describe("Icon attributes middleware", {

    describe("when all icon sets are public", function() {
      beforeEach({
        icons = null;

        IQuery mockIconGetter() {
          foreach(id, icon; icons) {
            icons[id]["_id"] = id;
            icons[id]["iconSet"] = "000000000000000000000001";
          }

          return new MemoryQuery(icons.byValue);
        }

        IQuery mockIconSetGetter(string id) {
          auto item = `{
            "visibility": {
              "team": "000000000000000000000001",
              "isPublic": true
            }
          }`.parseJsonString;
          return MemoryQuery([ item ]);
        }

        iconAttributes = new IconAttributesMapper(&mockIconGetter, &mockIconSetGetter);
      });

      it("should append the attributes from the parent icon", {
        icons["000000000000000000000001"] = parseJsonString(`{
          "_id": "000000000000000000000001",
          "parent": "000000000000000000000002",
          "attributes": [{
            "name": "level1",
            "type": "short text"
          }]
        }`);

        icons["000000000000000000000002"] = parseJsonString(`{
          "_id": "000000000000000000000002",
          "parent": "",
          "attributes": [{
            "name": "root",
            "type": "short text"
          }]
        }`);

        iconAttributes.fill(icons["000000000000000000000001"]);

        icons["000000000000000000000001"].should.equal(`{
          "_id": "000000000000000000000001",
          "parent": "000000000000000000000002",
          "iconSet": "000000000000000000000001",
          "styles": {
            "hasCustomStyle": false,
            "types": {}
          },
          "attributes": [{
            "name": "root",
            "type": "short text",
            "isInherited": true,
            "from": {
              "icon": "000000000000000000000002",
              "iconSet": "000000000000000000000001"
            }
          },{
            "type": "short text",
            "name": "level1",
            "from": {
              "icon": "000000000000000000000001",
              "iconSet": "000000000000000000000001"
            }
          }]
        }`.parseJsonString);
      });

      it("should append the attributes from the 2 parent icons", {
        icons["000000000000000000000001"] = parseJsonString(`{
          "_id": "000000000000000000000001",
          "parent": "000000000000000000000002",
          "attributes": [{
            "name": "level2",
            "type": "short text"
          }]
        }`);

        icons["000000000000000000000002"] = parseJsonString(`{
          "_id": "000000000000000000000002",
          "parent": "000000000000000000000003",
          "attributes": [{
            "name": "level1",
            "type": "short text"
          }]
        }`);

        icons["000000000000000000000003"] = parseJsonString(`{
          "_id": "000000000000000000000003",
          "parent": "",
          "attributes": [{
            "name": "root",
            "type": "short text"
          }]
        }`);

        iconAttributes.fill(icons["000000000000000000000001"]);

        icons["000000000000000000000001"].should.equal(`{
          "_id": "000000000000000000000001",
          "parent": "000000000000000000000002",
          "iconSet": "000000000000000000000001",
          "styles": {
            "hasCustomStyle": false,
            "types": {}
          },
          "attributes": [{
            "type": "short text",
            "isInherited": true,
            "from": {
              "icon": "000000000000000000000003",
              "iconSet": "000000000000000000000001"
            },
            "name": "root"
          },{
            "type": "short text",
            "isInherited": true,
            "from": {
              "icon": "000000000000000000000002",
              "iconSet": "000000000000000000000001"
            },
            "name": "level1"
          },{
            "type": "short text",
            "from": {
              "icon": "000000000000000000000001",
              "iconSet": "000000000000000000000001"
            },
            "name": "level2"
          }]
        }`.parseJsonString);
      });

      it("should stop on cyclic dependencies", {
        icons["000000000000000000000001"] = parseJsonString(`{
          "_id": "000000000000000000000001",
          "name": "name",
          "parent": "000000000000000000000002",
          "attributes": [{
            "name": "level2",
            "type": "short text"
          }]
        }`);

        icons["000000000000000000000002"] = parseJsonString(`{
          "_id": "000000000000000000000002",
          "parent": "000000000000000000000001",
          "attributes": [{
            "name": "level1",
            "type": "short text"
          }]
        }`);

        iconAttributes.fill(icons["000000000000000000000001"]);

        icons["000000000000000000000001"].should.equal(`{
          "_id": "000000000000000000000001",
          "name": "name",
          "parent": "000000000000000000000002",
          "iconSet": "000000000000000000000001",
          "styles": {
            "hasCustomStyle": false,
            "types": {}
          },
          "attributes": [{
            "type": "short text",
            "isInherited": true,
            "from": {
              "icon": "000000000000000000000002",
              "iconSet": "000000000000000000000001"
            },
            "name": "level1"
          },{
            "type": "short text",
            "from": {
              "icon": "000000000000000000000001",
              "iconSet": "000000000000000000000001"
            },
            "name": "level2"
          }]
        }`.parseJsonString);
      });

      it("should add the attributes from the parent icon", {
        icons["000000000000000000000001"] = parseJsonString(`{
          "_id": "000000000000000000000001",
          "parent": "000000000000000000000002"
        }`);

        icons["000000000000000000000002"] = parseJsonString(`{
          "_id": "000000000000000000000002",
          "parent": "",
          "attributes": [{
            "name": "root",
            "type": "short text"
          }]
        }`);

        iconAttributes.fill(icons["000000000000000000000001"]);

        icons["000000000000000000000001"].should.equal(`{
          "_id": "000000000000000000000001",
          "parent": "000000000000000000000002",
          "iconSet": "000000000000000000000001",
          "styles": {
            "hasCustomStyle": false,
            "types": {}
          },
          "attributes": [{
            "type": "short text",
            "isInherited": true,
            "from": {
              "icon": "000000000000000000000002",
              "iconSet": "000000000000000000000001"
            },
            "name": "root"
          }]
        }`.parseJsonString);
      });

      it("should not replace an existing attribute", {
        icons["000000000000000000000001"] = parseJsonString(`{
          "_id": "000000000000000000000001",
          "parent": "000000000000000000000002",
          "attributes": [{
            "name": "root",
            "type": "long text"
          }]
        }`);

        icons["000000000000000000000002"] = parseJsonString(`{
          "_id": "000000000000000000000002",
          "parent": "",
          "attributes": [{
            "name": "root",
            "type": "short text"
          }]
        }`);

        iconAttributes.fill(icons["000000000000000000000001"]);

        icons["000000000000000000000001"].should.equal(`{
          "_id": "000000000000000000000001",
          "parent": "000000000000000000000002",
          "iconSet": "000000000000000000000001",
          "styles": {
            "hasCustomStyle": false,
            "types": {}
          },
          "attributes": [{ "type": "long text", "isInherited": true, "name": "root", "from": {
            "icon": "000000000000000000000002",
            "iconSet": "000000000000000000000001"
          }}]
        }`.parseJsonString);
      });

      it("should ignore a missing parent", {
        icons["000000000000000000000001"] = parseJsonString(`{
          "_id": "000000000000000000000001",
          "parent": "000000000000000000000003",
          "attributes": [{
            "name": "root",
            "type": "long text"
          }]
        }`);

        icons["000000000000000000000001"].should.equal(`{
          "_id": "000000000000000000000001",
          "parent": "000000000000000000000003",
          "attributes": [{ "type": "long text", "name": "root" }]
        }`.parseJsonString);
      });

      it("should ignore a parent attribute with no name", {
        icons["000000000000000000000001"] = parseJsonString(`{
          "_id": "000000000000000000000001",
          "parent": "000000000000000000000002",
          "attributes": [{
            "name": "root",
            "type": "long text"
          }]
        }`);

        icons["000000000000000000000002"] = parseJsonString(`{
          "_id": "000000000000000000000002",
          "parent": "",
          "attributes": [{
            "type": "short text"
          }]
        }`);

        iconAttributes.fill(icons["000000000000000000000001"]);

        icons["000000000000000000000001"].should.equal(`{
          "_id": "000000000000000000000001",
          "parent": "000000000000000000000002",
          "iconSet": "000000000000000000000001",
          "styles": {
            "hasCustomStyle": false,
            "types": {}
          },
          "attributes": [{
            "type": "long text",
            "from": {
              "icon": "000000000000000000000001",
              "iconSet": "000000000000000000000001"
            },
            "name": "root"
          }]
        }`.parseJsonString);
      });
    });

    describe("when the parent icon is in a private set", function() {
      beforeEach({
        icons = null;
        iconSets = null;

        IQuery mockIconGetter() {

          foreach(id, icon; icons) {
            icons[id]["_id"] = id;
          }

          return new MemoryQuery(icons.byValue);
        }

        iconSets["000000000000000000000001"] = `{
          "visibility": {
            "team": "000000000000000000000001",
            "isPublic": true
          }
        }`.parseJsonString;

        iconSets["000000000000000000000002"] = `{
          "visibility": {
            "team": "000000000000000000000002",
            "isPublic": false
          }
        }`.parseJsonString;

        IQuery mockIconSetGetter(string id) {
          if(id !in iconSets) {
            return MemoryQuery([]);
          }

          iconSets[id]["_id"] = id;
          return MemoryQuery([ iconSets[id] ]);
        }

        iconAttributes = new IconAttributesMapper(&mockIconGetter, &mockIconSetGetter);
      });

      it("should not append the attributes from the private parent icon when the team does not match", {
        icons["000000000000000000000001"] = parseJsonString(`{
          "_id": "000000000000000000000001",
          "parent": "000000000000000000000002",
          "iconSet": "000000000000000000000001",
          "attributes": [{
            "name": "level1",
            "type": "short text"
          }]
        }`);

        icons["000000000000000000000002"] = parseJsonString(`{
          "_id": "000000000000000000000002",
          "parent": "",
          "iconSet": "000000000000000000000002",
          "styles": {
            "hasCustomStyle": false
          },
          "attributes": [{
            "name": "root",
            "type": "short text"
          }]
        }`);

        iconAttributes.fill(icons["000000000000000000000001"]);

        icons["000000000000000000000001"].should.equal(`{
          "_id": "000000000000000000000001",
          "parent": "000000000000000000000002",
          "iconSet": "000000000000000000000001",
          "styles": {
            "hasCustomStyle": false,
            "types": {}
          },
          "attributes": [{
            "type": "short text",
            "from": {
              "icon": "000000000000000000000001",
              "iconSet": "000000000000000000000001"
            },
            "name": "level1"
          }]
        }`.parseJsonString);
      });
    });
  });
});

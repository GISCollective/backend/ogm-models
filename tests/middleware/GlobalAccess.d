/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.middleware.GlobalAccess;

import ogm.middleware.GlobalAccess;
import ogm.test.fixtures;
import vibe.data.json;
import vibe.http.client;
import crate.error;

import crate.ctfe;

import vibe.data.json;
import fluent.asserts;
import trial.discovery.spec;

alias suite = Spec!({
  describe("GlobalAccess middleware", {
    GlobalAccessMiddleware middleware;

    beforeEach({
      setupTestData();

      middleware = new GlobalAccessMiddleware(crates);
    });

    describe("when the access.allowManageWithoutTeams preference is false as string", {
      beforeEach({
        auto preference = Json.emptyObject;
        preference["name"] = "access.allowManageWithoutTeams";
        preference["value"] = "false";
        crates.preference.addItem(preference);
      });

      it("should extract only the annotated fields", {
        middleware.allowManageWithoutTeams.compute.should.equal(false);
      });

      it("should not throw when the user is admin", {
        auto req = createTestHTTPServerRequest(URL("http://localhost/items"));
        req.context["user_id"] = userId["administrator"];
        req.context["email"] = userEmail["administrator"];
        req.context["isAdmin"] = true;

        middleware.handler(req);
      });

      it("should throw for an user without teams", {
        UserModel user;
        user._id = ObjectId.fromString("6").toString;
        user.username = "test";
        user.email = "other@gmail.com";
        userCollection.createUser(user, "password");

        auto req = createTestHTTPServerRequest(URL("http://localhost/items"));
        req.context["user_id"] = user._id;
        req.context["email"] = user.email;

        ({
          middleware.handler(req);
        }).should.throwException!CrateValidationException.withMessage("You need to be part of a team to perform this request.");
      });

      it("should not throw when the user has teams", {
        auto req = createTestHTTPServerRequest(URL("http://localhost/items"));
        req.context["user_id"] = userId["owner"];
        req.context["email"] = userEmail["owner"];
        req.context["isAdmin"] = true;

        middleware.handler(req);
      });
    });

    describe("when the access.allowManageWithoutTeams preference is false as boolean", {
      beforeEach({
        auto preference = Json.emptyObject;
        preference["name"] = "access.allowManageWithoutTeams";
        preference["value"] = false;
        crates.preference.addItem(preference);
      });

      it("should extract only the annotated fields", {
        middleware.allowManageWithoutTeams.compute.should.equal(false);
      });
    });

    describe("when the access.allowManageWithoutTeams preference is true", {
      beforeEach({
        auto preference = Json.emptyObject;
        preference["name"] = "access.allowManageWithoutTeams";
        preference["value"] = "true";

        crates.preference.addItem(preference);
      });

      it("should extract only the annotated fields", {
        middleware.allowManageWithoutTeams.compute.should.equal(true);
      });


      it("should not throw for an user without teams", {
        UserModel user;
        user._id = ObjectId.fromString("6").toString;
        user.username = "test";
        user.email = "other@gmail.com";
        userCollection.createUser(user, "password");

        auto req = createTestHTTPServerRequest(URL("http://localhost/items"));
        req.context["user_id"] = user._id;
        req.context["email"] = user.email;

        ({
          middleware.handler(req);
        }).should.not.throwException!CrateValidationException;
      });
    });

    describe("when the access.allowManageWithoutTeams preference is true as boolean", {
      beforeEach({
        auto preference = Json.emptyObject;
        preference["name"] = "access.allowManageWithoutTeams";
        preference["value"] = true;
        crates.preference.addItem(preference);
      });

      it("should extract only the annotated fields", {
        middleware.allowManageWithoutTeams.compute.should.equal(true);
      });
    });
  });
});

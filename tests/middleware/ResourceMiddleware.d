/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.middleware.ResourceMiddleware;

import ogm.middleware.ResourceMiddleware;
import ogm.test.fixtures;
import vibe.data.json;
import vibe.http.client;
import crate.error;

import crate.ctfe;

import vibe.data.json;
import fluent.asserts;
import trial.discovery.spec;
import vibe.http.server;
import vibe.inet.message;
import vibe.stream.memory;

alias suite = Spec!({
  describe("ResourceMiddleware middleware", {
    ResourceMiddleware!("feature", "sounds") middleware;

    beforeEach({
      setupTestData();
      middleware = new ResourceMiddleware!("feature", "sounds")(crates.feature, crates.sound);
    });

    describe("the replace operation", {
      it("does nothing when the listKey value is null", {
        auto soundCount = crates.sound.get.size;
        InetHeaderMap headers;
        headers["Content-Type"] = "application/json";
        auto req = createTestHTTPServerRequest(URL("http://localhost/items"), HTTPMethod.PUT, headers,
          createMemoryStream(cast(ubyte[])`{ "feature":{ "sounds": null }}`));

        req.params["id"] = "000000000000000000000001";
        middleware.replace(req);

        crates.sound.get.size.should.equal(soundCount);
      });
    });

    describe("the delete operation", {
      it("does nothing when the listKey value is null", {
        auto soundCount = crates.sound.get.size;
        InetHeaderMap headers;
        headers["Content-Type"] = "application/json";
        auto req = createTestHTTPServerRequest(URL("http://localhost/items"), HTTPMethod.PUT, headers,
          createMemoryStream(cast(ubyte[])`{ "feature":{ "sounds": null }}`));

        req.params["id"] = "000000000000000000000001";
        middleware.delete_(req);

        crates.sound.get.size.should.equal(soundCount);
      });
    });

    describe("when the stored features has the sounds list set as null", {
      HTTPServerRequest req;

      beforeEach({
        auto feature = crates.feature.getItem("000000000000000000000001").exec.front;
        feature["sounds"] = null;
        crates.feature.updateItem(feature);

        InetHeaderMap headers;
        headers["Content-Type"] = "application/json";
        req = createTestHTTPServerRequest(URL("http://localhost/items"), HTTPMethod.PUT, headers,
          createMemoryStream(cast(ubyte[])`{ "feature":{ "sounds": [] }}`));

        req.params["id"] = "000000000000000000000001";
      });

      it("does nothing on replace", {
        auto soundCount = crates.sound.get.size;
        middleware.replace(req);

        crates.sound.get.size.should.equal(soundCount);
      });

      it("does nothing on delete", {
        auto soundCount = crates.sound.get.size;
        middleware.delete_(req);

        crates.sound.get.size.should.equal(soundCount);
      });
    });
  });
});

/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.geocoding.IndexMaps;

import fluent.asserts;
import trial.discovery.spec;

import crate.error;
import ogm.crates.geocoding;
import ogm.models.geocoding;
import ogm.models.icon;
import vibe.data.json;
import geo.json;
import crate.base;
import ogm.geocoding.IndexMaps;
import ogm.test.fixtures;

alias suite = Spec!({
  describe("The IndexMap resolver", {
    IndexMapsResolver resolver;
    Json response;
    string requestUrl;
    string indexMapId;

    beforeEach({
      setupTestData();

      resolver = new IndexMapsResolver(crates);
      crates.map.addItem(`{
        "name": "Index map",
        "visibility": {
          "isPublic": true
        },
        "isIndex": true,
        "license": {
          "name": "Data © OpenStreetMap contributors, ODbL 1.0",
          "url": "https://osm.org/copyright"
        }
      }`.parseJsonString);
      indexMapId = crates.map.get
        .where("name").equal("Index map").and.exec.front["_id"].to!string;
    });

    it("returns nothing when there is no stored feature", {
      auto result = resolver.resolve("some query");
      result.length.should.equal(0);
    });

    describe("when there is a feature", {
      beforeEach({
        crates.feature.addItem((`{
          "name": "22, Some Straße, Luisenstadt",
          "maps": ["` ~ indexMapId ~ `"],
          "icons": ["1"],
          "position": { "type": "Point", "coordinates": [1.6, 1.6] }
        }`).parseJsonString);
      });

      it("should match a feature by substring", {
        auto result = resolver.resolve("22, Some Stra");

        result.length.should.equal(1);
        result[0].name.should.equal("22, Some Straße, Luisenstadt");
        result[0].geometry.toJson.should.equal(`{ "type": "Point", "coordinates": [1.6, 1.6] }`.parseJsonString);
        result[0].icons.should.equal(["1"]);
        result[0].license.name.should.equal("Data © OpenStreetMap contributors, ODbL 1.0");
        result[0].license.url.should.equal("https://osm.org/copyright");
      });

      it("should return an empty list when the feature is not matched", {
        auto result = resolver.resolve("not");
        result.length.should.equal(0);
      });

      it("should return an empty list when the levenshtein distance is too big", {
        auto result = resolver.resolve("Some");
        result.length.should.equal(0);
      });
    });
  });
});


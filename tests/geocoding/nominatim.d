/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.geocoding.nominatim;

import fluent.asserts;
import trial.discovery.spec;

import crate.error;
import ogm.crates.geocoding;
import ogm.models.geocoding;
import ogm.models.icon;
import vibe.data.json;
import geo.json;
import crate.base;
import ogm.geocoding.nominatim;
import ogm.test.fixtures;

alias suite = Spec!({
  describe("The nominatim resolver", {
    NominatimResolver resolver;
    Json response;
    string requestUrl;

    beforeEach({
      setupTestData();

      requestUrl = "";
      Json requestHandler(string url) {
        requestUrl = url;
        return response;
      }

      resolver = new NominatimResolver(crates, &requestHandler);
      crates.map.addItem(`{
        "name": "Nominatim cache"
      }`.parseJsonString);
    });

    it("does not send the request query when the api is not enabled", {
      resolver.resolve("some query");
      requestUrl.should.equal("");
    });

    describe("when the api is enabled", {
      beforeEach({
        response = Json();
        Json apiKey = Json.emptyObject;
        apiKey["name"] = "integrations.nominatim";
        apiKey["value"] = "https://nominatim.openstreetmap.org/search";

        crates.preference.addItem(apiKey);
      });

      it("sends a request", {
        resolver.resolve("some query");
        requestUrl.should.equal("https://nominatim.openstreetmap.org/search?q=some+query&format=geojson&addressdetails=1&extratags=1&namedetails=1&polygon_threshold=0.001&polygon_geojson=1");
      });

      it("converts the result to a geocoding structure", {
        response = `{
          "type": "FeatureCollection",
          "licence": "Data © OpenStreetMap contributors, ODbL 1.0. https://osm.org/copyright",
          "features": [{
            "type": "Feature",
            "properties": {
              "place_id": 574401,
              "osm_type": "node",
              "osm_id": 240109189,
              "display_name": "Berlin, 10117, Germany",
              "place_rank": 15,
              "category": "place",
              "type": "city",
              "importance": 0.8875390282491362,
              "address": { },
              "extratags": { },
              "namedetails": { }
            },
            "geometry": { "type": "Point", "coordinates": [13.3888599, 52.5170365] }
          },{
            "type": "Feature",
            "properties": {
              "place_id": 256375666,
              "osm_type": "relation",
              "osm_id": 62422,
              "display_name": "Berlin, Germany",
              "place_rank": 8,
              "category": "boundary",
              "type": "administrative",
              "importance": 0.8875390282491362,
              "address": { },
              "extratags": { },
              "namedetails": { }
            },
            "geometry": {
              "type": "MultiPolygon",
              "coordinates": [[[[13.088345, 52.4196325],[13.6477328, 52.3382448],[13.7611609, 52.4377097],[13.4794873, 52.6755087],[13.088345, 52.4196325]]]]
        }}]}`.parseJsonString;

        auto result = resolver.resolve("some query");

        result.length.should.equal(2);
        result[0].name.should.equal("Berlin, 10117, Germany");
        result[0].geometry.serializeToJson.should.equal(`{ "type": "Point", "coordinates": [13.3888599, 52.5170365] }`.parseJsonString);
        result[1].name.should.equal("Berlin, Germany");
      });

      it("returns nothing when there is no returned feature", {
        response = `{
          "type": "FeatureCollection",
          "licence": "Data © OpenStreetMap contributors, ODbL 1.0. https://osm.org/copyright",
        }`.parseJsonString;

        auto result = resolver.resolve("some query");

        result.length.should.equal(0);
      });

      it("returns nothing when there is an invalid feature", {
        response = `{
          "type": "FeatureCollection",
          "licence": "Data © OpenStreetMap contributors, ODbL 1.0. https://osm.org/copyright",
          "features": [{}]
        }`.parseJsonString;

        auto result = resolver.resolve("some query");

        result.length.should.equal(0);
      });
    });

    describe("converting a feature", {
      it("extracts all fields", {
        Json feature = `{
          "type": "Feature",
          "properties": {
            "place_id": 256375666,
            "osm_type": "relation",
            "osm_id": 62422,
            "display_name": "Berlin, Germany",
            "place_rank": 8,
            "category": "boundary",
            "type": "administrative",
            "importance": 0.8875390282491362,
            "address": { },
            "extratags": { },
            "namedetails": { }
          },
          "geometry": {
            "type": "MultiPolygon",
            "coordinates": [[[
              [13.088345, 52.4196325],
              [13.6477328, 52.3382448],
              [13.7611609, 52.4377097],
              [13.4794873, 52.6755087],
              [13.088345, 52.4196325]
            ]]]
          }
        }`.parseJsonString;

        auto result = resolver.toGeocoding(feature);
        result.name.should.equal("Berlin, Germany");
        result.license.name.should.equal("Data © OpenStreetMap contributors, ODbL 1.0");
        result.license.url.should.equal("https://osm.org/copyright");
        result.geometry.toJson.should.equal(`{
          "type": "MultiPolygon",
          "coordinates": [[[
            [13.088345, 52.4196325],
            [13.6477328, 52.3382448],
            [13.7611609, 52.4377097],
            [13.4794873, 52.6755087],
            [13.088345, 52.4196325]
          ]]]
        }`.parseJsonString);
      });

      it("fills in the matched icons", {
        crates.icon.addItem(`{
          "name": "",
          "otherNames": ["boundary"]
        }`.parseJsonString);

        crates.icon.addItem(`{
          "name": "",
          "otherNames": ["administrative"]
        }`.parseJsonString);

        crates.icon.addItem(`{
          "name": "",
          "otherNames": ["boundary.administrative"]
        }`.parseJsonString);

        Json feature = `{
          "type": "Feature",
          "properties": {
            "place_id": 256375666,
            "osm_type": "relation",
            "osm_id": 62422,
            "display_name": "Berlin, Germany",
            "place_rank": 8,
            "category": "boundary",
            "type": "administrative",
            "importance": 0.8875390282491362,
            "address": { },
            "extratags": { },
            "namedetails": { }
          },
          "geometry": {
            "type": "MultiPolygon",
            "coordinates": [[[
              [13.088345, 52.4196325],
              [13.6477328, 52.3382448],
              [13.7611609, 52.4377097],
              [13.4794873, 52.6755087],
              [13.088345, 52.4196325]
            ]]]
          }
        }`.parseJsonString;

        auto result = resolver.toGeocoding(feature);
        result.name.should.equal("Berlin, Germany");
        result.icons.length.should.equal(3);
      });

      it("caches the result as a feature", {
        crates.icon.addItem(`{
          "name": "",
          "otherNames": ["boundary"]
        }`.parseJsonString);

        Json feature = `{
          "type": "Feature",
          "properties": {
            "place_id": 256375666,
            "osm_type": "relation",
            "osm_id": 62422,
            "display_name": "Berlin, Germany",
            "place_rank": 8,
            "category": "boundary",
            "type": "administrative",
            "importance": 0.8875390282491362,
            "address": { "a": 1 },
            "extratags": { "b": 2 },
            "namedetails": { "c": 3 }
          },
          "geometry": {
            "type": "MultiPolygon",
            "coordinates": [[[
              [13.088345, 52.4196325],
              [13.6477328, 52.3382448],
              [13.7611609, 52.4377097],
              [13.4794873, 52.6755087],
              [13.088345, 52.4196325]
            ]]]
          }
        }`.parseJsonString;

        resolver.toGeocoding(feature);

        auto range = crates.feature.get.where("name").equal("Berlin, Germany").and.exec;

        range.empty.should.equal(false);
        auto item = range.front;
        item["name"].to!string.should.equal("Berlin, Germany");
        item["attributes"].should.equal(`{
          "geocoding": {
            "a": 1
          },
          "other": {
            "b": 2
          },
          "osm": {
            "osm_type": "relation",
            "place_rank": 8,
            "display_name": "Berlin, Germany",
            "osm_id": 62422,
            "category": "boundary",
            "importance": 0.8875390282491362,
            "type": "administrative",
            "place_id": 256375666
          },
          "names": {
            "c": 3
          }
        }`.parseJsonString);
      });
    });
  });
});


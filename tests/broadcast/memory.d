/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.broadcast.memory;

import ogm.models.changeset;
import gis_collective.hmq.broadcast.memory;

import vibe.data.json;
import fluent.asserts;
import trial.discovery.spec;

import crate.collection.notifications;

alias suite = Spec!({
  describe("Changeset broadcast with memory broadcast", {
    MemoryBroadcast broadcast;
    int called;

    beforeEach({
      broadcast = new MemoryBroadcast;
      called = 0;
    });

    it("should be called when an item is added", {
      void testHandler(const Json value) {
        called++;
        auto changeSet = value.deserializeJson!ChangeSet;

        changeSet.model.should.equal("Test");
        changeSet.type.should.equal(CrateChangeType.add);
        changeSet.itemId.toString.should.equal("000000000000000000000001");

        changeSet.removed.length.should.equal(0);
        changeSet.added.length.should.equal(2);
        changeSet.added["_id"].should.equal("000000000000000000000001");
        changeSet.added["field"].should.equal(1);
      }

      broadcast.register("Test.field", &testHandler);

      CrateChange change;
      change.modelName = "Test";
      change.after = `{
        "_id": "000000000000000000000001",
        "field": 1
      }`.parseJsonString;

      broadcast.push("Test.field", change.serializeToJson);

      called.should.equal(1);
    });
  });
});

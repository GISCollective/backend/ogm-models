/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.features.visibility;

import fluent.asserts;
import trial.discovery.spec;
import vibe.data.json;
import ogm.features.visibility;
import ogm.test.fixtures;

alias suite = Spec!({
  describe("getComputedVisibility", {
    beforeEach({
      setupTestData();
    });

    it("returns a private visibility when the feature and map are private", {
      auto feature = crates.feature.getItem("000000000000000000000002").and.exec.front;
      feature["visibility"] = 0;

      auto result = getComputedVisibility(crates, feature);

      expect(result).to.equal(`{
        "isDefault": false,
        "isPublic": false,
        "team": "000000000000000000000002"
      }`.parseJsonString);
    });

    it("returns a public visibility when the feature and map are public", {
      auto feature = crates.feature.getItem("000000000000000000000001").and.exec.front;
      auto result = getComputedVisibility(crates, feature);

      expect(result).to.equal(`{
        "isDefault": false,
        "isPublic": true,
        "team": "000000000000000000000001"
      }`.parseJsonString);
    });

    it("returns a public visibility when the feature is pending and the map public", {
      auto feature = crates.feature.getItem("000000000000000000000001").and.exec.front;
      feature["visibility"] = -1;

      auto result = getComputedVisibility(crates, feature);

      expect(result).to.equal(`{
        "isDefault": false,
        "isPublic": true,
        "team": "000000000000000000000001"
      }`.parseJsonString);
    });

    it("returns a public visibility when the feature is public and one map is public and one is private", {
      auto feature = crates.feature.getItem("000000000000000000000001").and.exec.front;
      feature["visibility"] = -1;
      feature["maps"] = ["000000000000000000000002", "000000000000000000000001"].serializeToJson;

      auto result = getComputedVisibility(crates, feature);

      expect(result).to.equal(`{
        "isDefault": false,
        "isPublic": true,
        "team": "000000000000000000000001"
      }`.parseJsonString);
    });

    it("returns a private visibility when the feature is private and the map public", {
      auto feature = crates.feature.getItem("000000000000000000000001").and.exec.front;
      feature["visibility"] = 0;

      auto result = getComputedVisibility(crates, feature);

      expect(result).to.equal(`{
        "isDefault": false,
        "isPublic": false,
        "team": "000000000000000000000001"
      }`.parseJsonString);
    });

    it("returns a private visibility when the feature is public and the map private", {
      auto map = crates.map.getItem("000000000000000000000001").and.exec.front;
      map["visibility"]["isPublic"] = false;
      crates.map.updateItem(map);

      auto feature = crates.feature.getItem("000000000000000000000001").and.exec.front;
      feature["visibility"] = 1;

      auto result = getComputedVisibility(crates, feature);

      expect(result).to.equal(`{
        "isDefault": false,
        "isPublic": false,
        "team": "000000000000000000000001"
      }`.parseJsonString);
    });

    it("generates a visibility when it is not set and it has no maps", {
      auto map = crates.map.getItem("000000000000000000000001").and.exec.front;
      map["visibility"]["isPublic"] = false;
      crates.map.updateItem(map);

      auto feature = crates.feature.getItem("000000000000000000000001").and.exec.front;
      feature["visibility"] = 1;
      feature.remove("computedVisibility");
      feature["maps"] = Json.emptyArray;

      auto result = getComputedVisibility(crates, feature);

      expect(result).to.equal(`{
        "isDefault": false,
        "isPublic": false,
        "team": "000000000000000000000001"
      }`.parseJsonString);
    });
  });
});
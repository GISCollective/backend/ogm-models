/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.crates.geocoding;

import fluent.asserts;
import trial.discovery.spec;

import crate.error;
import ogm.crates.geocoding;
import ogm.models.geocoding;
import ogm.models.icon;
import vibe.data.json;
import geo.json;
import crate.base;
import std.array;


class MockGeocodingResolver : GeocodingResolver {
  string resolvedName;
  Geocoding[] resolvedItems;

  @trusted Geocoding[] resolve(string name) {
    this.resolvedName = name;

    return resolvedItems;
  }
}

alias suite = Spec!({
  describe("The GeocodingCrate", {
    GeocodingCrate crate;
    MockGeocodingResolver resolver;

    beforeEach({
      resolver = new MockGeocodingResolver();
      crate = new GeocodingCrate([resolver]);
    });

    it("does not accept delete operations", {
      crate.deleteItem("").should.throwException!CrateException;
    });

    it("does not accept update operations", {
      crate.updateItem(Json.emptyObject).should.throwException!CrateException;
    });

    it("does not accept add operations", {
      crate.addItem(Json.emptyObject).should.throwException!CrateException;
    });

    it("does not accept add operations", {
      crate.getItem("").should.throwException!CrateException;
    });

    it("calls the resolver with the name value", {
      crate.get
        .where("name")
        .equal("some query")
        .and
        .exec;
      resolver.resolvedName.should.equal("some query");
    });

    describe("when the resolver returns an empty list", {
      beforeEach({
        auto icon = Icon(ObjectId.fromString("2"));
        icon.image = OptionalIconImage(false, new IconFile());

        resolver.resolvedItems = [];
      });

      it("returns an empty list", {
        auto items = crate.get
          .where("name")
          .equal("some query")
          .and
          .exec
          .array;

        items.length.should.equal(0);
      });
    });

    describe("when the resolver returns an item", {
      beforeEach({
        auto icon = Icon(ObjectId.fromString("2"));
        icon.image = OptionalIconImage(false, new IconFile());

        resolver.resolvedItems = [Geocoding(
          ObjectId.fromString("1"),
          "some query",
          GeoJsonGeometry.nullPoint,
          [ "2" ],
          1
        )];
      });

      it("returns the resolved geometry", {
        auto items = crate.get
          .where("name")
          .equal("some query")
          .and
          .exec
          .array;

        items.length.should.equal(1);
        items[0].should.equal(`{
          "_id": "000000000000000000000001",
          "geometry": { "type": "Point", "coordinates": [ 0., 0. ] },
          "icons": ["2"],
          "score": 1.,
          "license": {
            "url": "",
            "name": ""
          },
          "name": "some query"
        }`.parseJsonString);
      });
    });

    describe("when the resolver returns two items", {
      beforeEach({
        resolver.resolvedItems = [Geocoding(
          ObjectId.fromString("1"),
          "some query",
          GeoJsonGeometry.nullPoint,
          [ "2" ],
          1.
        ), Geocoding(
          ObjectId.fromString("2"),
          "some query",
          GeoJsonGeometry.nullPoint,
          [ "2" ],
          1.
        )];
      });

      it("returns two resolved geometries", {
        auto items = crate.get
          .where("name")
          .equal("some query")
          .and
          .exec
          .array;

        items.length.should.equal(2);
        items[0].should.equal(`{
          "_id": "000000000000000000000001",
          "geometry": { "type": "Point", "coordinates": [ 0., 0. ] },
          "icons": ["2"],
          "score": 1.,
          "license": {
            "url": "",
            "name": ""
          },
          "name": "some query"
        }`.parseJsonString);
        items[1].should.equal(`{
          "_id": "000000000000000000000002",
          "geometry": { "type": "Point", "coordinates": [ 0., 0. ] },
          "icons": ["2"],
          "score": 1.,
          "license": {
            "url": "",
            "name": ""
          },
          "name": "some query"
        }`.parseJsonString);
      });
    });

    describe("when there are two resolvers", {
      MockGeocodingResolver resolver2;

      beforeEach({
        auto icon = Icon(ObjectId.fromString("2"));
        icon.image = OptionalIconImage(false, new IconFile());

        resolver.resolvedItems = [Geocoding(
          ObjectId.fromString("1"),
          "some query",
          GeoJsonGeometry.nullPoint,
          [ "2" ],
          1.
        )];

        resolver2 = new MockGeocodingResolver();
      });

      it("queries only the first resolver if returns", {
        crate = new GeocodingCrate([resolver, resolver2]);
        auto items = crate.get
          .where("name")
          .equal("some query")
          .and
          .exec
          .array;

        resolver.resolvedName.should.equal("some query");
        resolver2.resolvedName.should.equal("");

        items.length.should.equal(1);
        items[0].should.equal(`{
          "_id": "000000000000000000000001",
          "geometry": { "type": "Point", "coordinates": [ 0., 0. ] },
          "icons": ["2"],
          "score": 1.,
          "license": {
            "url": "",
            "name": ""
          },
          "name": "some query"
        }`.parseJsonString);
      });

      it("queries both resolvers if the first one returns an empty list", {
        crate = new GeocodingCrate([resolver2, resolver]);
        auto items = crate.get
          .where("name")
          .equal("some query")
          .and
          .exec
          .array;

        resolver.resolvedName.should.equal("some query");
        resolver2.resolvedName.should.equal("some query");

        items.length.should.equal(1);
        items[0].should.equal(`{
          "_id": "000000000000000000000001",
          "geometry": { "type": "Point", "coordinates": [ 0., 0. ] },
          "icons": ["2"],
          "score": 1.,
          "license": {
            "url": "",
            "name": ""
          },
          "name": "some query"
        }`.parseJsonString);
      });
    });
  });
});

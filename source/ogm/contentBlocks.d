/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.contentBlocks;

import vibe.data.json;
import std.algorithm;
import std.array;
import ogm.markdown;

string toTextContent(Json source, bool ignoreTitle = false) {
    if(source.type == Json.Type.string) {
        return source.to!string;
    }

    if(source.type != Json.Type.object) {
        return "The message is not redacted yet.";
    }

    auto blocks = cast(Json[]) source["blocks"];

    if(ignoreTitle) {
        blocks = blocks.filter!(a => a["type"] != "header" && a["data"]["level"] != 1).array;
    }

    string result;

    foreach(block; blocks) {
        switch(block["type"].to!string) {
            case "paragraph":
                result ~= block["data"]["text"].to!string ~ "\n\n";
                break;
            default:
        }
    }

    return result;
}

string toHtmlContent(Json source, bool ignoreTitle = false) {
    if(source.type == Json.Type.string) {
        source = source.to!string.blocksFromMarkdown;
    }

    if(source.type != Json.Type.object) {
        return "The message is not redacted yet.";
    }

    auto blocks = cast(Json[]) source["blocks"];

    if(ignoreTitle) {
        blocks = blocks.filter!(a => a["type"] != "header" && a["data"]["level"] != 1).array;
    }

    string result;

    foreach(block; blocks) {
        switch(block["type"].to!string) {
            case "paragraph":
                result ~= "<p>" ~ block["data"]["text"].to!string ~ "</p>\n\n";
                break;
            default:
        }
    }

    return result;
}
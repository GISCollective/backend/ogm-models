/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.operations.BulkOperation;

import crate.base;
import crate.http.wrapper;
import crate.http.operations.base;
import crate.auth.middleware;
import crate.error;

import ogm.rights;
import ogm.crates.all;
import ogm.http.request;
import ogm.middleware.ResourceMiddleware;

import std.algorithm;
import std.array;
import std.string;

import vibe.http.server;
import vibe.data.json;

class BulkOperation(string requiredField = "team", bool ignoreTeam = false) : CrateOperation!DefaultStorage {
  protected {
    PrivateDataMiddleware privateData;
    OgmCrates crates;
  }

  this(OgmCrates crates, CrateAccess crate, PrivateDataMiddleware privateData, CrateRule rule) {
    this.crates = crates;
    this.privateData = privateData;

    rule.response.statusCode = 204;
    rule.response.mime = "";
    rule.response.description = "Returns a void body.";

    super(crate, rule);

    auto wrappedCalls = new MiddlewareWrapper!(DefaultStorage, PrivateDataMiddleware)(privateData);
    super.addMiddlewares(wrappedCalls);
  }

  override void addMiddlewares(IMiddlewareWrapper!DefaultStorage middlewares) {
    super.addMiddlewares(middlewares);
    this.updateRule!"getList";
  }

  void performOperation(ref Json item, RequestUserData request, DefaultStorage storage, bool isOwner, bool isAuthor) {
  }

  override void handle(DefaultStorage storage) {
    this.prepareListOperation!"getList"(storage);

    static if(!ignoreTeam) {
      enforce!CrateValidationException("team" in storage.request.query, "You must provide the `team` query parameter");
      enforce!CrateValidationException(storage.request.query["team"].strip != "", "You must provide the `team` query parameter");
    }

    static if(requiredField != "team") {
      enforce!CrateValidationException(requiredField in storage.request.query, "You must provide the `"~ requiredField ~"` query parameter");
      enforce!CrateValidationException(storage.request.query[requiredField].strip != "", "You must provide the `"~ requiredField ~"` query parameter");
    }

    if(storage.response.headerWritten) {
      return;
    }

    this.applyRule(storage);

    auto request = RequestUserData(storage.request);
    auto session = request.session(crates);

    static if(!ignoreTeam) {
      scope teams = session.all.teams;
      scope isAdmin = request.isAdmin;

      auto team = storage.request.query["team"].strip;

      if(!isAdmin && !teams.canFind(team)) {
        throw new CrateNotFoundException("The team can not be found.");
      }
    }

    foreach(item; storage.query.exec) {
      string teamId;

      if(item["visibility"].type == Json.Type.object) {
        teamId = item["visibility"]["team"].to!string;
      }

      if(item["computedVisibility"].type == Json.Type.object) {
        teamId = item["computedVisibility"]["team"].to!string;
      }

      auto isGuest = session.guest.teams.canFind(teamId);

      if(isGuest) {
        continue;
      }

      auto isOwner = session.owner.teams.canFind(teamId);
      auto isAuthor = item["info"]["originalAuthor"] == request.userId;

      string itemId = item["_id"].to!string;

      storage.request.params["id"] = itemId;
      request.params["id"] = itemId;

      performOperation(item, request, storage, isOwner, isAuthor);
    }

    storage.response.statusCode = rule.response.statusCode;
    storage.response.writeVoidBody;
  }
}
/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.operations.BulkPublishOperation;

import crate.base;
import crate.http.wrapper;
import crate.http.operations.base;
import crate.auth.middleware;
import crate.error;

import ogm.rights;
import ogm.crates.all;
import ogm.http.request;
import ogm.middleware.ResourceMiddleware;
import ogm.operations.BulkOperation;

import std.algorithm;
import std.array;

import vibe.data.json;
import vibe.http.server;

class BulkPublishOperation(string modelName, string requiredField) : BulkOperation!requiredField {
  private {
    IResourceMiddleware[] resourceMiddlewares;
  }

  this(OgmCrates crates, PrivateDataMiddleware privateData, IResourceMiddleware[] resourceMiddlewares) {
    this.resourceMiddlewares = resourceMiddlewares;

    CrateRule rule;

    rule.request.path = "/" ~ modelName ~ "s/publish";
    rule.request.method = HTTPMethod.POST;

    mixin("auto crate = crates." ~ modelName ~ ";");

    super(crates, crate, privateData, rule);
  }

  override void performOperation(ref Json item, RequestUserData request, DefaultStorage storage, bool isOwner, bool isAuthor) {
    bool canUpdate = request.isAdmin || isOwner || isAuthor;

    if(canUpdate && item["visibility"]["isPublic"] == false) {
      item["visibility"]["isPublic"] = true;
      crate.updateItem(item);
    }
  }
}
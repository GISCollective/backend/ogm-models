/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.operations.Revision;

import std.datetime;
import std.path;
import std.string;
import std.range;
import std.algorithm;

import ogm.crates.all;
import crate.ctfe;
import crate.http.operations.base;
import crate.base;
import crate.error;
import crate.json;
import vibe.http.router;
import vibe.data.json;
import crate.collection.memory;
import crate.api.rest.serializer;

class RevisionOperation(T) : CrateOperation!DefaultStorage {
  private {
    OgmCrates crates;
    ModelDescription definition;
    string host;
  }

  this(OgmCrates crates, CrateAccess crate) {
    this.definition = describe!T;
    this.crates = crates;
    this.host = host;

    CrateRule rule;
    rule.request.path = "/" ~ definition.plural.toLower ~ "/:id/revisions/:revisionId";
    rule.request.method = HTTPMethod.GET;
    rule.response.mime = "application/json";
    rule.response.statusCode = 200;
    rule.response.serializer = new RestApiSerializer(definition);

    super(crate, rule);
  }

  Json getModelRevision(Json item, string revisionId) {
    string itemId = item["_id"].to!string;

    auto range = crates.changeSet.get
      .where("itemId").equal(itemId).and
      .sort("time", -1).and
      .exec;

    enforce!CrateNotFoundException(!range.empty, "The revsion does not exist.");

    auto result = item.clone;

    foreach (item; range) {
      if(item["type"] == "add") {
        result = item["added"].toNestedJson;
      }

      if(item["type"] == "update") {
        if("added" in item) {
          foreach(string key, Json value; item["added"]) {
            result.set(key, Json());
          }
        }

        if("removed" in item) {
          foreach(string key, Json value; item["removed"]) {
            result.set(key, value);
          }
        }
      }

      if(item["_id"] == revisionId) {
        break;
      }
    }

    return result;
  }

  override void handle(DefaultStorage storage) {
    storage.item = this.prepareItemOperation!"getItem"(storage);

    if(storage.response.headerWritten) {
      return;
    }

    string revisionId = getPathVar!"revisionId"(storage.request.requestPath, rule.request.path);

    storage.item = getModelRevision(storage.item, revisionId);
    storage.query = new MemoryQuery([ storage.item ]);

    this.applyRule(storage);

    auto responseData = rule.response.serializer.denormalise(storage.item, this.mapper(storage));
    storage.response.writeRangeBody(responseData, rule.response.statusCode, rule.response.mime);
  }
}
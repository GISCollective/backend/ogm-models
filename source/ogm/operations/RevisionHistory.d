/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.operations.RevisionHistory;

import std.datetime;
import std.path;
import std.string;
import std.range;
import std.algorithm;

import ogm.crates.all;
import crate.ctfe;
import crate.http.operations.base;
import crate.base;
import vibe.http.router;
import vibe.data.json;

struct Revision {
  SysTime time;
  string url;
  string id;
}

class RevisionHistoryOperation(T) : CrateOperation!DefaultStorage {
  private {
    OgmCrates crates;
    ModelDescription definition;
    string host;
  }

  this(OgmCrates crates, CrateAccess crate, string host) {
    this.definition = describe!T;
    this.crates = crates;
    this.host = host;

    CrateRule rule;
    rule.request.path = "/" ~ definition.plural.toLower ~ "/:id/revisions";
    rule.request.method = HTTPMethod.GET;
    rule.response.mime = "application/json";
    rule.response.statusCode = 200;

    super(crate, rule);
  }

  Revision[] getRevisions(string itemId) {
    Revision[] list;

    auto range = crates.changeSet.get
      .where("itemId").equal(itemId).and
      .sort("time", -1).and
      .exec;

    foreach (change; range) {
      auto time = SysTime.fromISOExtString(change["time"].to!string);
      auto id = change["_id"].to!string;
      auto url = host ~ "/" ~ buildPath(definition.plural.toLower, itemId, "revisions", id);
      list ~= Revision(time, url, id);
    }

    return list;
  }

  override void handle(DefaultStorage storage) {
    this.prepareItemOperation!"getItem"(storage);

    if(storage.response.headerWritten) {
      return;
    }
    this.applyRule(storage);

    auto revisions = getRevisions(storage.request.params["id"]);
    storage.response.statusCode = rule.response.statusCode;
    storage.response.contentType = rule.response.mime;
    storage.response.statusCode = rule.response.statusCode;

    struct ResponseBody {
      Revision[] revisions;
    }

    storage.response.writeJsonBody(ResponseBody(revisions));
  }
}
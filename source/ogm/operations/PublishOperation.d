/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.operations.PublishOperation;

import crate.base;
import crate.http.wrapper;
import crate.http.operations.base;
import crate.auth.middleware;
import crate.error;

import ogm.rights;
import ogm.crates.all;
import ogm.http.request;
import ogm.middleware.ResourceMiddleware;
import ogm.operations.BulkOperation;

import std.algorithm;
import std.array;

import vibe.data.json;
import vibe.http.server;

class PublishOperation(string modelName) : CrateOperation!DefaultStorage {
  private {
    IResourceMiddleware[] resourceMiddlewares;
    OgmCrates crates;
  }

  this(OgmCrates crates, PrivateDataMiddleware privateData, IResourceMiddleware[] resourceMiddlewares) {
    this.resourceMiddlewares = resourceMiddlewares;

    CrateRule rule;

    rule.request.path = "/" ~ modelName ~ "s/:id/publish";
    rule.request.method = HTTPMethod.POST;

    mixin("auto crate = crates." ~ modelName ~ ";");
    this.crates = crates;

    super(crate, rule);
  }

  override void handle(DefaultStorage storage) {
    auto item = this.prepareItemOperation!"getItem"(storage);

    auto res = storage.response;

    if(storage.response.headerWritten) {
      return;
    }

    this.applyRule(storage);

    scope request = RequestUserData(storage.request);
    if(!request.isAuthenticated) {
      res.writeJsonBody(["error": "Authorization required"], 401);

      return;
    }

    auto session = request.session(crates);

    pragma(msg, "session.owner." ~ modelName ~ "s;");
    mixin("auto ids = session.owner." ~ modelName ~ "s ~ session.leader." ~ modelName ~ "s ~ session.member." ~ modelName ~ "s;");

    enforce!ForbiddenException(request.isAdmin || ids.canFind(item["_id"].to!string), "You can't publish this record.");

    item["visibility"]["isPublic"] = true;
    this.crate.updateItem(item);

    res.statusCode = 204;
    res.writeVoidBody;
  }
}
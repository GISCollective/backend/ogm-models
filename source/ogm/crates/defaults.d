/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.crates.defaults;

import ogm.crates.all;
import ogm.models.picture;
import ogm.models.team;
import ogm.models.attribution;

import crate.base;

import vibe.core.log;
import vibe.core.file;
import vibe.stream.wrapper;
import vibe.data.json;

import std.array;
import std.file;
import std.functional;

version(unittest) {
  import fluent.asserts;
  import ogm.test.fixtures;
  import crate.collection.memory;
}

/// add a default picture
Picture addDefault(Crate!Picture pictures, string contentType, string name, string path) {
  if(path == "") {
    return Picture();
  }

  try {
    return pictures.getDefault(name);
  } catch(Exception e) {
    logError(e.message);
  }

  Picture item;
  item.name = name;
  item.owner = "@system";
  item.picture = new PictureFile();

  auto stream = openFile(path, FileMode.read).createProxyStream;
  item.picture.contentType = contentType;
  item.picture.read(stream);

  auto result = pictures.addItem(item.serializeToJson);

  item._id = ObjectId.fromJson(result["_id"]);

  return item;
}

/// It should not create a picture if the path is not set
unittest {
  auto pictures = new MemoryCrate!Picture;

  pictures.addDefault("", "", "");

  pictures.get.size.should.equal(0);
}

/// It should not create a picture if a valid one already exists
unittest {
  PictureFileSettings.files = new MockGridFsFiles;
  PictureFileSettings.chunks = new MockGridFsChunks;

  Picture tmp;
  tmp.name = "test";
  tmp.picture = PictureFile.fromString("data:text/plain;base64,dGhpcyBpcyBhIHRleHQgZmlsZQ==");


  auto pictures = new MemoryCrate!Picture;
  pictures.addItem(tmp.serializeToJson);

  pictures.addDefault("test", "test", "test");

  pictures.get.size.should.equal(1);
}

/// It should create a picture if a valid one does not exists
unittest {
  PictureFileSettings.files = new MockGridFsFiles;
  PictureFileSettings.chunks = new MockGridFsChunks;

  Picture tmp;
  tmp.name = "test";
  tmp.owner = "other";

  auto pictures = new MemoryCrate!Picture;
  pictures.addItem(tmp.serializeToJson);

  pictures.addDefault("test", "test", "dub.json");

  pictures.get.size.should.equal(2);
  pictures.get.exec.front["name"].should.equal("test");
  pictures.get.exec.front["owner"].should.equal("other");
}

Picture getDefault(Crate!Picture pictures, string name) {
  auto range = pictures.get.where("name").equal(name).and.limit(1).exec;
  scope(exit) destroy(range);

  try {
    if(!range.empty) {
      return LazyPicture(range.front).toType;
    }
  } catch(Exception e) {
    logError(e.message);
  }

  throw new Exception("The picture `" ~ name ~ "` was not found.");
}

/// Add a default team
void addDefaultTeam(OgmCrates crates, string name, bool isPublisher = false, string about = "This team was created automatically.") {
  scope range = crates.team.get.where("name").equal(name).and.limit(1).exec;

  if(!range.empty) {
    destroy(range);
    return;
  }

  Team item;
  item.name = name;
  item.about = about;
  item.isPublic = false;
  item.isDefault = true;
  item.isPublisher = isPublisher;
  item.logo = crates.picture.getDefault("cover");

  auto lazyTeam = LazyTeam.fromModel(item);

  crates.team.addItem(lazyTeam.toJson);
}

Team getDefault(Crate!Team teams) {
  auto range = teams.get.where("isDefault").equal(true).and.limit(1).exec;
  scope(exit) destroy(range);

  try {
    if(!range.empty) {
      return LazyTeam(range.front, (&itemResolver).toDelegate).toType;
    }
  } catch(Exception e) {
    logError(e.message);
  }

  throw new Exception("The default team was not found.");
}

void addDefaultBaseMap(OgmCrates crates, string name, string icon, MapLayer[] layers, int order, Attribution[] attributions = []) {
  auto range = crates.baseMap.get.where("name").equal(name).and.limit(1).exec;
  scope(exit) destroy(range);

  if(!range.empty) {
    return;
  }

  logInfo("Adding `%s` base map.", name);

  BaseMap item;
  item.name = name;
  item.icon = icon;
  item.layers = layers;
  item.defaultOrder = order;

  item.visibility.team = crates.team.getDefault;
  item.visibility.isPublic = true;
  item.visibility.isDefault = true;

  item.attributions = attributions;

  if(exists(name ~ ".png")) {
    item.cover = crates.picture.addDefault("image/png", name, name ~ ".png");
  }

  if(exists("deploy/" ~ name ~ ".png")) {
    item.cover = crates.picture.addDefault("image/png", name, "deploy/" ~ name ~ ".png");
  }

  if(exists("../../deploy/" ~ name ~ ".png")) {
    item.cover = crates.picture.addDefault("image/png", name, "../../deploy/" ~ name ~ ".png");
  }

  auto lazyBaseMap = LazyBaseMap.fromModel(item, (&itemResolver).toDelegate);

  crates.baseMap.addItem(lazyBaseMap.toJson);
}

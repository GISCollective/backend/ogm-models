/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.crates.all;

public import ogm.models.all;
public import ogm.crates.geocoding;
public import ogm.crates.legend;

import std.exception;
import std.file;
import std.conv;
import std.algorithm;
import std.array;
import std.meta;

import vibe.service.configuration.db;

import vibe.data.bson;
import vibe.data.json;
import vibe.db.mongo.client;
import vibe.http.router;
import vibe.core.log;
import vibe.core.net;
import vibe.core.file;
import vibeauth.data.usermodel;

import crate.base;
import crate.auth.usercollection;
import crate.lazydata.base;
import crate.resource.file;
import crate.collection.mongo;
import crate.collection.cache;
import crate.collection.notifications;
import crate.resource.gridfs;

alias LazyUserData = Lazy!UserModel;

alias CratesWithNotification = AliasSeq!(Article, Campaign, CampaignAnswer, Feature, Map, MapFile,
  Picture, Sound, BaseMap, Icon, IconSet, Preference, Team, Issue, UserProfile, Page, DataBinding, Space, Event, Calendar);

struct OgmCrates {
  Crate!Article article;
  Crate!BaseMap baseMap;
  Crate!BatchJob batchJob;
  Crate!Campaign campaign;
  Crate!CampaignAnswer campaignAnswer;
  Crate!ChangeSet changeSet;
  Crate!DataBinding dataBinding;
  Crate!Feature feature;
  Crate!SimpleFeature simpleFeature;
  Crate!Geocoding geocoding;
  Crate!Icon icon;
  Crate!IconSet iconSet;
  Crate!Issue issue;
  Crate!Legend legend;
  Crate!LogEntry log;
  Crate!Map map;
  Crate!MapFile mapFile;
  Crate!Meta meta;
  Crate!Metric metric;
  Crate!Model model;
  Crate!Page page;
  Crate!Picture picture;
  Crate!Preference preference;
  Crate!Presentation presentation;
  Crate!SiteSubset siteSubset;
  Crate!Sound sound;
  Crate!Space space;
  Crate!Team team;
  Crate!TileLayer tileLayer;
  Crate!User user;
  Crate!UserProfile userProfile;
  Crate!Stat stat;
  Crate!Newsletter newsletter;
  Crate!NewsletterEmail newsletterEmail;
  Crate!ArticleLink articleLink;
  Crate!SearchMeta searchMeta;
  Crate!Event event;
  Crate!Calendar calendar;
  Crate!Message message;

  Crate!Translation translation;
  Crate!FieldTranslation fieldTranslation;

  /// files
  IGridFsCollection iconFiles;
  IGridFsCollection iconChunks;

  IGridFsCollection pictureFiles;
  IGridFsCollection pictureChunks;

  IGridFsCollection soundFiles;
  IGridFsCollection soundChunks;

  IGridFsCollection issueFiles;
  IGridFsCollection issueChunks;

  IGridFsCollection mapFiles;
  IGridFsCollection mapChunks;

  IGridFsCollection translationFiles;
  IGridFsCollection translationChunks;
}

Crate!Model notificationCrate(Model)(MongoClient client, string collection, void delegate(CrateChange change) @safe notification) {
  auto crate = new MongoCrate!Model(client, collection);

  if(notification is null) {
    logWarn("Setting up `%s` crate without notifications", Model.stringof);
    return crate;
  }

  return crate.notification!Model(notification);
}

OgmCrates setupOgmCrates(DbConfiguration[] configurations, void delegate(CrateChange change) @safe notification = null) {
  logInfo("Setting up crates...");

  auto client = configurations.getMongoClient;
  string dbName = configurations.getMongoConfig.configuration["database"].to!string;

  logInfo("Connected to mongo db.");

  OgmCrates crates;
  crates.user = new MongoCrate!User(client, dbName ~ ".users");

  crates.article = notificationCrate!Article(client, dbName ~ ".articles", notification);
  crates.baseMap = notificationCrate!BaseMap(client, dbName ~ ".basemaps", notification);
  crates.campaign = notificationCrate!Campaign(client, dbName ~ ".campaigns", notification);
  crates.campaignAnswer = notificationCrate!CampaignAnswer(client, dbName ~ ".campaignAnswers", notification);
  crates.dataBinding = notificationCrate!DataBinding(client, dbName ~ ".dataBinding", notification);
  crates.feature = notificationCrate!Feature(client, dbName ~ ".sites", notification);
  crates.simpleFeature = new MongoCrate!SimpleFeature(client, dbName ~ ".simpleFeature");
  crates.icon = notificationCrate!Icon(client, dbName ~ ".icons", notification);
  crates.iconSet = notificationCrate!IconSet(client, dbName ~ ".iconsets", notification);
  crates.issue = notificationCrate!Issue(client, dbName ~ ".issue", notification);
  crates.map = notificationCrate!Map(client, dbName ~ ".maps", notification);
  crates.mapFile = notificationCrate!MapFile(client, dbName ~ ".mapFile", notification);
  crates.page = notificationCrate!Page(client, dbName ~ ".page", notification);
  crates.picture = notificationCrate!Picture(client, dbName ~ ".pictures", notification);
  crates.preference = notificationCrate!Preference(client, dbName ~ ".preference", notification);
  crates.presentation = notificationCrate!Presentation(client, dbName ~ ".presentation", notification);
  crates.sound = notificationCrate!Sound(client, dbName ~ ".sounds", notification);
  crates.space = notificationCrate!Space(client, dbName ~ ".space", notification);
  crates.team = notificationCrate!Team(client, dbName ~ ".teams", notification);
  crates.userProfile = notificationCrate!UserProfile(client, dbName ~ ".userProfile", notification);
  crates.event = notificationCrate!Event(client, dbName ~ ".events", notification);
  crates.calendar = notificationCrate!Calendar(client, dbName ~ ".calendar", notification);

  crates.meta = new MongoCrate!Meta(client, dbName ~ ".meta");
  crates.message = new MongoCrate!Message(client, dbName ~ ".message");
  crates.batchJob = new MongoCrate!BatchJob(client, dbName ~ ".batchjobs");
  crates.translation = new MongoCrate!Translation(client, dbName ~ ".translation");
  crates.fieldTranslation = notificationCrate!FieldTranslation(client, dbName ~ ".fieldTranslation", notification);
  crates.tileLayer = new MongoCrate!TileLayer(client, dbName ~ ".tileLayer");
  crates.log = new MongoCrate!LogEntry(client, dbName ~ ".logs");
  crates.stat = new MongoCrate!Stat(client, dbName ~ ".stats");
  crates.newsletter = new MongoCrate!Newsletter(client, dbName ~ ".newsletter");
  crates.newsletterEmail = new MongoCrate!NewsletterEmail(client, dbName ~ ".newsletterEmail");
  crates.articleLink = new MongoCrate!ArticleLink(client, dbName ~ ".articleLink");

  crates.siteSubset = new MongoCrate!SiteSubset(client, dbName ~ ".sites");
  crates.changeSet = new MongoCrate!ChangeSet(client, dbName ~ ".changesets");
  crates.metric = new MongoCrate!Metric(client, dbName ~ ".metrics");
  crates.searchMeta = new MongoCrate!SearchMeta(client, dbName ~ ".searchMeta");

  crates.legend = new LegendCrate(crates.feature, crates.icon);
  crates.model = new ModelCrate();

  crates.iconFiles = new MongoGridFsCollection(client.getCollection(dbName ~ ".icons.files"));
  crates.iconChunks = new MongoGridFsCollection(client.getCollection(dbName ~ ".icons.chunks"));

  crates.pictureFiles = new MongoGridFsCollection(client.getCollection(dbName ~ ".pictures.files"));
  crates.pictureChunks = new MongoGridFsCollection(client.getCollection(dbName ~ ".pictures.chunks"));

  crates.soundFiles = new MongoGridFsCollection(client.getCollection(dbName ~ ".sounds.files"));
  crates.soundChunks = new MongoGridFsCollection(client.getCollection(dbName ~ ".sounds.chunks"));

  crates.issueFiles = new MongoGridFsCollection(client.getCollection(dbName ~ ".issues.files"));
  crates.issueChunks = new MongoGridFsCollection(client.getCollection(dbName ~ ".issues.chunks"));

  crates.mapFiles = new MongoGridFsCollection(client.getCollection(dbName ~ ".map.files"));
  crates.mapChunks = new MongoGridFsCollection(client.getCollection(dbName ~ ".map.chunks"));

  crates.translationFiles = new MongoGridFsCollection(client.getCollection(dbName ~ ".translation.files"));
  crates.translationChunks = new MongoGridFsCollection(client.getCollection(dbName ~ ".translation.chunks"));

  GeocodingCrate.instance = new GeocodingCrate([]);
  crates.geocoding = GeocodingCrate.instance;

  auto db = client.getDatabase(dbName);

  /// article
  ensureIndex(db, "articles", [ "title", "content"], "text");
  ensureIndex(db, "articles", [ "visibility", "team", "categories"], 1);

  /// page
  ensureIndex(db, "page", [ "space"], 1);

  /// space
  ensureIndex(db, "space", [ "visibility.isDefault"], 1);

  /// picture files
  ensureIndex(db, "pictures.files",  [ "filename" ], "text");
  ensureIndex(db, "pictures.chunks", [ "files_id", "n"], 1);

  /// icon files
  ensureIndex(db, "icons.files", [ "filename" ], "text");
  ensureIndex(db, "icons.chunks", [ "files_id", "n"], 1);

  /// sound files
  ensureIndex(db, "sounds.files", [ "filename" ], "text");
  ensureIndex(db, "sounds.chunks", [ "files_id", "n"], 1);

  /// user indexes
  ensureIndex(db, "users", "email", 1);
  ensureIndex(db, "users", "isActive", 1);
  ensureIndex(db, "users", "tokens.name", 1);
  ensureIndex(db, "users", "tokens.scopes", 1);
  ensureIndex(db, "users", "tokens.type", 1);
  ensureIndex(db, "users", "tokens.expire", 1);

  /// site indexes
  ensureIndex(db, "sites", [ "name", "description"], "text");
  ensureIndex(db, "sites", "position", "2dsphere");
  ensureIndex(db, "sites", "positionBox", "2dsphere");
  ensureIndex(db, "sites", "unmasked", "2dsphere");
  ensureIndex(db, "sites", "positionBox", "2dsphere");
  ensureIndex(db, "sites", "unmaskedBox", "2dsphere");
  ensureIndex(db, "sites", `{
    "computedVisibility.isPublic": 1,
    "maps": 1,
    "position.type": 1,
    "positionBox": "2dsphere",
    "isLarge": 1
  }`.parseJsonString);

  /// simple features indexes
  ensureIndex(db, "simpleFeature", ["featureId", "changeIndex", "maps", "computedVisibility.isPublic", "z", "x", "y", "_ver"], 1);
  ensureIndex(db, "simpleFeature", ["maps", "z", "x", "y"], 1);

  /// map indexes
  ensureIndex(db, "maps", "area", "2dsphere");
  ensureIndex(db, "maps", "visibility.team", 1);
  ensureIndex(db, "maps", "visibility.isPublic", 1);
  ensureIndex(db, "maps", "iconSet", 1);

  /// picture indexes
  ensureIndex(db, "pictures", [ "name", "owner", "picture"], "text");
  ensureIndex(db, "pictures", "owner", 1);

  /// sound indexes
  ensureIndex(db, "sounds", [ "name", "owner", "picture"], "text");
  ensureIndex(db, "sounds", "owner", 1);

  /// icon indexes
  ensureIndex(db, "icons", [ "category", "subcategory", "name",
    "description", "parent", "image"], "text");
  ensureIndex(db, "icons", "attributes", 1);
  ensureIndex(db, "icons", "iconSet", 1);

  /// icon sets indexes
  ensureIndex(db, "iconsets", ["name", "description"], "text");
  ensureIndex(db, "iconsets", "isPublic", 1);
  ensureIndex(db, "iconsets", "visibility.isDefault", 1);
  ensureIndex(db, "iconsets", "visibility.team", 1);

  /// preferences indexes
  ensureIndex(db, "preference", [ "name", "value"], "text");

  /// teams indexes
  ensureIndex(db, "teams", ["name", "owners", "leaders", "members", "guests"], "text");
  ensureIndex(db, "teams", "owners", 1);
  ensureIndex(db, "teams", "leaders", 1);
  ensureIndex(db, "teams", "members", 1);
  ensureIndex(db, "teams", "guests", 1);
  ensureIndex(db, "teams", "isPublic", 1);

  /// issues indexes
  ensureIndex(db, "issue", ["site", "author", "title", "type", "description"], "text");
  ensureIndex(db, "issue", "site", 1);
  ensureIndex(db, "issue", "author", 1);
  ensureIndex(db, "issue", "creationDate", 1);

  /// tileLayer indexes
  ensureIndex(db, "stat", "name", 1);

  /// tileLayer indexes
  ensureIndex(db, "tileLayer", "path.x", 1);
  ensureIndex(db, "tileLayer", "path.y", 1);
  ensureIndex(db, "tileLayer", "path.z", 1);
  ensureIndex(db, "tileLayer", "changeIndex", 1);
  ensureIndex(db, "tileLayer", "isPublic", 1);
  ensureIndex(db, "tileLayer", "isReady", 1);
  ensureIndex(db, "tileLayer", ["layerName", "mapId", "teamId"], "text");

  /// search meta index
  ensureIndex(db, "searchMeta", ["keywords", "relatedModel", "relatedId", "type"], "text");
  ensureIndex(db, "searchMeta", ["itemId", "type"], -1);
  ensureIndex(db, "searchMeta", "feature.centroid", "2dsphere");

  return crates;
}

Json publishersId(Crate!Team crate) {
  auto range = crate.get.where("isPublisher").equal(true).and.limit(1).and.exec;

  enforce(!range.empty, "There is no Publisher team!");

  return range.front["_id"];
}

auto getWelcomeMessageRange(OgmCrates crates, string newsletterId) {
  return crates.article.get
    .where("type").equal(ArticleType.newsletterWelcomeMessage).and
    .where("relatedId").equal(newsletterId).and
    .exec;
}

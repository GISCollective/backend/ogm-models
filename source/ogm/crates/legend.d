/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.crates.legend;

import crate.base;
import crate.error;
import crate.lazydata.base;
import crate.collection.proxy;
import core.memory : GC;

import ogm.models.legend;
import ogm.models.feature;
import ogm.models.icon;

import vibe.data.json;
import vibe.core.log;

import std.conv;
import std.array;
import std.range;
import std.algorithm;
import std.functional;

version(unittest) {
  import crate.collection.memory;
  import fluent.asserts;
}

class LegendCrate : Crate!Legend {

  private {
    Crate!Feature feature;
    Crate!Icon icon;
  }

  this(Crate!Feature feature, Crate!Icon icon) {
    this.feature = feature;
    this.icon = icon;
  }

  @safe:
    /// Get items from the crate
    IQuery get() {
      return new LegendSelector(feature.get, icon);
    }

    /// Get one item by id
    IQuery getItem(const string) {
      throw new CrateException("Not supported");
    }

    /// Add a new item
    Json addItem(const Json) {
      throw new CrateException("Not supported");
    }

    /// Update an existing item
    Json updateItem(const Json) {
      throw new CrateException("Not supported");
    }

    /// Delete an existing item
    void deleteItem(const string) {
      throw new CrateException("Not supported");
    }
}

///
class LegendSelector : ProxySelector {
  private {
    Json[string] icons;
    Crate!Icon iconCrate;

    size_t[string] categories;
    size_t[string][string] subcategories;
    size_t[string][string][string] presentIcons;
    Legend[] legends;
  }

  @safe:
    this(IQuery siteSelector, Crate!Icon iconCrate) {
      this.iconCrate = iconCrate;

      super(siteSelector);
    }

    private {
      Json getIcon(string id) @trusted {
        if(id !in icons) {
          enforce(iconCrate !is null, "the icon crate is not set");

          auto result = iconCrate.get.where("_id").equal(ObjectId.fromString(id)).and.exec;
          scope(exit) destroy(result);

          if(result.empty) {
            icons[id] = Json.undefined;
          } else {
            icons[id] = result.front;
          }
        }

        return icons[id];
      }

      void collect(Json[] icons) @trusted {
        string[] countedCategories;
        string[] countedSubcategories;

        foreach(icon; icons) {
          string category = icon["category"].to!string;
          string subcategory = icon["subcategory"].to!string;
          string key = category ~ "." ~ subcategory;

          string iconId = icon["_id"].to!string;

          if(category !in categories) {
            Legend legend;
            legend._id = legends.length + 1;
            legend.name = category;

            categories[category] = legends.length;

            ulong[string] emptySubcategories;
            ulong[string][string] emptyIcons;

            subcategories[category] = emptySubcategories;
            presentIcons[category] = emptyIcons;

            legends ~= legend;
          }

          auto categoryIndex = categories[category];

          if(subcategory !in subcategories[category]) {
            subcategories[category][subcategory] = legends[categoryIndex].subcategories.length;
            legends[categoryIndex].subcategories ~= LegendSubcategory(subcategory, key);

            ulong[string] emptyIcons;
            presentIcons[category][subcategory] = emptyIcons;
          }

          auto subcategoryIndex = subcategories[category][subcategory];

          if(iconId !in presentIcons[category][subcategory]) {
            presentIcons[category][subcategory][iconId] = legends[categoryIndex].subcategories[subcategoryIndex].icons.length;
            auto iconValue = Lazy!Icon.fromJson(icon, (&itemResolver).toDelegate).toType;
            auto legendIcon = LegendIcon(iconValue, icon["_id"].to!string, 0);

            legends[categoryIndex].subcategories[subcategoryIndex].icons ~= legendIcon;
          }

          auto iconIndex = presentIcons[category][subcategory][iconId];

          if(!countedCategories.canFind(category)) {
            legends[categoryIndex].count = legends[categoryIndex].count + 1;
            countedCategories ~= category;
          }

          if(!countedSubcategories.canFind(key)) {
            legends[categoryIndex].subcategories[subcategoryIndex].count = legends[categoryIndex].subcategories[subcategoryIndex].count + 1;
            countedSubcategories ~= key;
          }

          legends[categoryIndex].subcategories[subcategoryIndex].icons[iconIndex].count = legends[categoryIndex].subcategories[subcategoryIndex].icons[iconIndex].count + 1;
        }
      }
    }

    void consumeAll(InputRange!Json range) @trusted {
      if(range is null) {
        return;
      }

      while(!range.empty) {
        range.popFront;
      }

      destroy(range);
      GC.collect;
    }

    override InputRange!Json exec() @trusted {
      InputRange!Json range;

      try {
        range = super.exec;
      } catch(Exception e) {
        logError(e.toString);
        consumeAll(range);
        return (cast(Json[])[]).inputRangeObject;
      }

      foreach(site; range.filter!(a => "icons" in a)) {
        Json[] icons = (cast(Json[]) site["icons"])
          .map!(a => getIcon(a.to!string))
          .filter!(a => a.type == Json.Type.object)
          .array;
        collect(icons);
      }

      consumeAll(range);

      return legends.map!(a => Lazy!Legend.fromModel(a).toJson).inputRangeObject;
    }
}

/// It should return an empty list when there is no data
unittest {
  auto sites = new MemoryCrate!Feature();
  auto icons = new MemoryCrate!Icon();

  auto crate = new LegendCrate(sites, icons);

  crate.get.exec.array.should.equal([]);
}

/// It should return no legend if a site has no icons
unittest {
  auto sites = new MemoryCrate!Feature();
  auto icons = new MemoryCrate!Icon();

  auto crate = new LegendCrate(sites, icons);

  auto site = `{
      "map": 1,
      "name": "test",
      "description": "description",
      "position": {
        "type": "point",
        "coordinates": [1,1]
      }
  }`.parseJsonString;

  sites.addItem(site);

  crate.get.exec.array.should.equal([]);
}

/// It should count one icon if a site has one
/*unittest {
  auto sites = new MemoryCrate!Site();
  auto icons = new MemoryCrate!Icon();

  auto crate = new LegendCrate(sites, icons);

  auto icon = `{
    "category": "category",
    "description": "description",
    "subcategory": "subcategory",
    "name": "name",
    "parent": "",
    "attributes": [],
    "iconSet": "",
    "image": ""
  }`.parseJsonString;

  auto site = `{
      "map": "000000000000000000000001",
      "name": "test",
      "description": "description",
      "position": {
        "type": "point",
        "coordinates": [1,1]
      },
      "icons": [ "000000000000000000000001" ]
  }`.parseJsonString;

  sites.addItem(site);
  icons.addItem(icon);

  crate.get.exec.array.should.equal([`{
    "_id": 1,
    "name": "category",
    "count": 1,

    "subcategories": [
      {
        "name": "subcategory",
        "uid": "category.subcategory",
        "count": 1,
        "icons": [
          {
            "icon": "000000000000000000000001",
            "uid": "000000000000000000000001",
            "count": 1
          }
        ]
      }
    ]
  }`.parseJsonString]);
}*/

/// It should count one icon twice if two sites has the same icon
/*unittest {
  auto sites = new MemoryCrate!Site();
  auto icons = new MemoryCrate!Icon();

  auto crate = new LegendCrate(sites, icons);

  auto icon = `{
    "category": "category",
    "subcategory": "subcategory",
    "name": "name",
    "description": "description",
    "parent": "",
    "attributes": [],
    "iconSet": "",
    "image": ""
  }`.parseJsonString;

  auto site1 = `{
      "map": 1,
      "name": "test1",
      "description": "description1",
      "position": {
        "type": "point",
        "coordinates": [1,1]
      },
      "icons": [ "000000000000000000000001" ]
  }`.parseJsonString;

 auto site2 = `{
      "map": 1,
      "name": "test2",
      "description": "description2",
      "position": {
        "type": "point",
        "coordinates": [1,1]
      },
      "icons": [ "000000000000000000000001" ]
  }`.parseJsonString;

  sites.addItem(site1);
  sites.addItem(site2);
  icons.addItem(icon);

  crate.get.exec.front.should.equal(`{
    "_id": 1,
    "name": "category",
    "count": 2,

    "subcategories": [
      {
        "name": "subcategory",
        "uid": "category.subcategory",
        "count": 2,
        "icons": [
          {
            "icon": "000000000000000000000001",
            "uid": "000000000000000000000001",
            "count": 2
          }
        ]
      }
    ]
  }`.parseJsonString);
}*/

/// It should count a subcategory twice if a site has two icons from the same subcategory
/*unittest {
  auto sites = new MemoryCrate!Site();
  auto icons = new MemoryCrate!Icon();

  auto crate = new LegendCrate(sites, icons);

  auto icon1 = `{
    "category": "category",
    "subcategory": "subcategory",
    "name": "name1",
    "description": "description",
    "parent": "",
    "iconSet": "",
    "attributes": [],
    "image": ""
  }`.parseJsonString;

  auto icon2 = `{
    "category": "category",
    "subcategory": "subcategory",
    "name": "name2",
    "description": "description",
    "parent": "",
    "iconSet": "",
    "attributes": [],
    "image": ""
  }`.parseJsonString;

  auto site = `{
      "map": 1,
      "name": "test",
      "description": "description",
      "position": {
        "type": "point",
        "coordinates": [1,1]
      },
      "icons": [ "000000000000000000000001", "000000000000000000000002" ]
  }`.parseJsonString;

  sites.addItem(site);
  icons.addItem(icon1);
  icons.addItem(icon2);

  crate.get.exec.front.should.equal(`{
    "_id": 1,
    "name": "category",
    "count": 1,

    "subcategories": [
      {
        "name": "subcategory",
        "uid": "category.subcategory",
        "count": 1,
        "icons": [
          {
            "icon": "000000000000000000000001",
            "uid": "000000000000000000000001",
            "count": 1
          },
          {
            "icon": "000000000000000000000002",
            "uid": "000000000000000000000002",
            "count": 1
          }
        ]
      }
    ]
  }`.parseJsonString);
}*/

/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.crates.geocoding;

import ogm.models.geocoding;
import crate.base;
import crate.error;
import vibe.data.json;
import std.range.interfaces;
import std.typecons;

interface GeocodingResolver {
  @safe Geocoding[] resolve(string);
}

class GeocodingCrate : Crate!Geocoding {
  static {
    GeocodingCrate instance;
    size_t breakSize;
  }
  GeocodingResolver[] resolvers;

  @safe:
    this(GeocodingResolver[] resolvers) {
      this.resolvers = resolvers;
    }

    /// Get items from the crate
    IQuery get() {
      return new GeocodingResolverQuery(this.resolvers);
    }

    /// Get one item by id
    IQuery getItem(const string) {
      throw new CrateException("Operation not supported");
    }

    /// Add a new item
    Json addItem(const Json) {
      throw new CrateException("Operation not supported");
    }

    /// Update an existing item
    Json updateItem(const Json) {
      throw new CrateException("Operation not supported");
    }

    void deleteItem(const string) {
      throw new CrateException("Operation not supported");
    }
}

class GeocodingResolverQuery : WhiteHole!IQuery {
  private {
    GeocodingResolver[] resolvers;
    GeocodingResolverFieldQuery query;
  }

  @safe:
    this(GeocodingResolver[] resolvers) {
      this.resolvers = resolvers;
      query = new GeocodingResolverFieldQuery(this);
    }

    override IFieldQuery where(string field) {
      return query;
    }

    override InputRange!Json exec() {
      Json[] list;

      foreach(resolver; resolvers) {
        auto resolved = resolver.resolve(query.value);

        foreach(item; resolved) {
          list ~= item.serializeToJson;
        }

        if(list.length > GeocodingCrate.breakSize) break;
      }

      return list.inputRangeObject;
    }

    override size_t size() {
      return 0;
    }
}

class GeocodingResolverFieldQuery : WhiteHole!IFieldQuery {
  alias equal = WhiteHole!IFieldQuery.equal;

  private IQuery parent;
  string value;

  @safe:
    this(IQuery parent) {
      this.parent = parent;
    }

    override IFieldQuery equal(string value) {
      this.value = value;
      return this;
    }

    override IQuery and() {
      return parent;
    }
}

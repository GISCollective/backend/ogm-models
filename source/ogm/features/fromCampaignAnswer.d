/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.features.fromCampaignAnswer;

import std.exception;
import std.algorithm;
import std.string;
import std.datetime;
import std.array;
import geo.json;
import vibe.data.json;
import ogm.crates.all;
import gis_collective.hmq.broadcast.base;
import crate.base;
import ogm.models.modelinfo;
import ogm.calendar;

bool hasFeature(Json answer, OgmCrates crates) {
  auto matches = crates.feature.get
    .where("source.type").equal("Campaign").and
    .where("source.remoteId").equal(answer["_id"].to!string).and
    .size;

  if(matches > 0) {
    return true;
  }

  if(!isObjectId(answer["featureId"])) {
    return false;
  }

  matches = crates.feature.get
    .where("_id").equal(ObjectId(answer["featureId"])).and
    .size;

  return matches > 0;
}

Json fromCampaignAnswer(Json answer, OgmCrates crates, IBroadcast broadcast) {
  auto campaign = crates.campaign.getItem(answer["campaign"].to!string).and.exec.front;

  enforce(campaign["map"]["isEnabled"].to!bool, "The answer will not be copied, because the campaign `" ~ campaign["_id"].to!string ~ "` has no map.");

  auto mapId = campaign["map"]["map"].to!string;
  auto map = crates.map.getItem(mapId).exec.front;

  auto attributes = answer["attributes"].deserializeJson!(Json[string]);

  Feature feature;
  auto prefix = "Answer";

  string featureNamePrefix = campaign["options"]["featureNamePrefix"].to!string.strip;

  if(featureNamePrefix != "" && featureNamePrefix != "null") {
    prefix = featureNamePrefix;
  }

  feature.maps = [ Map(ObjectId.fromString(mapId)) ];
  feature.name = prefix ~ " " ~ answer["_id"].to!string;
  feature.info = answer["info"].deserializeJson!ModelInfo;
  feature.icons = answer["icons"].byValue.map!(a => Icon(ObjectId.fromJson(a))).array;
  feature.pictures = answer["pictures"].byValue.map!(a => Picture(ObjectId.fromJson(a))).array;
  feature.position = answer["position"].deserializeJson!GeoJsonGeometry;
  feature.contributors = [ feature.info.originalAuthor ];
  feature.description = "";

  feature.position.normalizeWgs84;

  if(map["addFeaturesAsPending"] == true) {
    feature.visibility = VisibilityEnum.Pending;
  }

  foreach(key, group; attributes) {
    if(key == "about") {
      if(group["name"].type == Json.Type.string && group["name"].to!string.strip != "") {
        feature.name = group["name"].to!string.strip;
      }

      if(group["description"].type == Json.Type.string && group["description"].to!string.strip != "") {
        feature.description = group["description"].to!string.strip;
      }

      if(group["description"].type == Json.Type.object) {
        feature.description = group["description"];
      }

      continue;
    }

    feature.attributes[key] = group;
  }

  feature.source.type = "Campaign";
  feature.source.modelId = campaign["_id"].to!string;
  feature.source.remoteId = answer["_id"].to!string;
  feature.source.syncAt = SysCalendar.instance.now;

  auto serializedFeature = LazyFeature.fromModel(feature).toJson;

  CrateChange change;
  change.modelName = "Feature";
  change.type = CrateChangeType.add;
  change.after = crates.feature.addItem(serializedFeature);

  broadcast.push("Feature.change", change.serializeToJson);

  return change.after;
}
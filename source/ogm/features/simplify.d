/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.features.simplify;

import vibe.data.json;
import std.math.traits;
import std.math.algebraic;
import ogm.models.feature;
import geo.json;
import geo.algorithm;
import geo.xyz;
import geo.conv;
import geo.epsg;
import geo.geometries;
import geo.algo.simplify;
import geo.algo.cut;
import std.algorithm.mutation;

version(unittest) {
  import std.file;
  import fluent.asserts;
}

void findMinMax(ref double lon1, ref double lon2, ref double lat1, ref double lat2, const ref Json list) {
  if(isNaN(lon1)) {
    lon1 = list[0][0].to!double;
    lon2 = lon1;
    lat1 = list[0][1].to!double;
    lat2 = lat1;
  }

  foreach(const ref point; list) {
    auto lon = point[0].to!double;
    auto lat = point[1].to!double;

    if(lon < lon1) {
      lon1 = lon;
    }

    if(lon > lon2) {
      lon2 = lon;
    }

    if(lat < lat1) {
      lat1 = lat;
    }

    if(lat > lat2) {
      lat2 = lat;
    }
  }
}

///
Json featureBox(const ref Json geometry) {
  if(geometry["type"] == "Point") {
    return geometry;
  }

  double lon1, lon2, lat1, lat2;

  if(geometry["type"] == "MultiPoint" || geometry["type"] == "LineString") {
    auto coordinates = geometry["coordinates"];
    findMinMax(lon1, lon2, lat1, lat2, coordinates);
  }

  if(geometry["type"] == "MultiLineString" || geometry["type"] == "Polygon") {
    foreach(const ref coordinates; geometry["coordinates"]) {
      findMinMax(lon1, lon2, lat1, lat2, coordinates);
    }
  }

  if(geometry["type"] == "MultiPolygon") {
    foreach(const ref polygon; geometry["coordinates"]) {
      foreach(const ref coordinates; polygon) {
        findMinMax(lon1, lon2, lat1, lat2, coordinates);
      }
    }
  }

  if(isNaN(lon1)) {
    return Json();
  }

  auto box = Json.emptyObject;
  box["type"] = "Polygon";
  box["coordinates"] = [[[lon1, lat1],
  [lon2, lat1],[lon2, lat2],[lon1, lat2],
  [lon1, lat1]]].serializeToJson;

  return box;
}

/// it returns the point when the feature has a point
unittest {
  auto point = `{
    "type": "Point",
    "coordinates": [30.1, 10.1]
  }`.parseJsonString;

  point.featureBox.should.equal(point);
}

/// it returns a polygon for a multi point
unittest {
  auto multiPoint = `{
    "type": "MultiPoint",
    "coordinates": [
      [1, 2],
      [30.1, 10.1]
    ]}`.parseJsonString;

  multiPoint.featureBox.should.equal(`{
    "coordinates": [[[1, 2],
    [30.1, 2], [30.1, 10.1], [1, 10.1], [1, 2]]],
    "type": "Polygon"
  }`.parseJsonString);
}

/// it returns a polygon for a line string
unittest {
  auto lineString = `{
    "type": "LineString",
    "coordinates": [[1.0, 1.0], [1.0, 2.0], [2.0, 3.0]]
  }`.parseJsonString;

  lineString.featureBox.should.equal(`{
    "coordinates": [[[1, 1], [2, 1], [2, 3], [1, 3], [1, 1]]],
    "type": "Polygon"
  }`.parseJsonString);
}

/// it returns a polygon for a multi line string
unittest {
  auto lineString = `{
    "type":"MultiLineString",
    "coordinates":[[[-73.979966,40.768902],[-73.97984,40.76865000000001],[-73.97975,40.76853],[-73.97974000000001,40.76956000000001],[-73.97978999999999,40.76949999999999],[-73.97996999999999,40.76914999999997],[-73.97996000000001,40.76893999999999]]]
  }`.parseJsonString;

  lineString.featureBox.should.equal(`{
    "coordinates": [[[-73.97997, 40.76853], [-73.97974, 40.76853], [-73.97974, 40.76956], [-73.97997, 40.76956], [-73.97997, 40.76853]]],
    "type": "Polygon"
  }`.parseJsonString);
}

/// it returns a polygon for a polygon
unittest {
  auto polygon = `{
    "type":"Polygon",
    "coordinates":[[[-73.979966,40.768902],[-73.97984,40.76865000000001],[-73.97975,40.76853],[-73.97974000000001,40.76956000000001],[-73.97978999999999,40.76949999999999],[-73.97996999999999,40.76914999999997],[-73.97996000000001,40.76893999999999]]]
  }`.parseJsonString;

  polygon.featureBox.should.equal(`{
    "coordinates": [[[-73.97997, 40.76853], [-73.97974, 40.76853], [-73.97974, 40.76956], [-73.97997, 40.76956], [-73.97997, 40.76853]]],
    "type": "Polygon"
  }`.parseJsonString);
}

/// it returns a polygon for a multipolygon
unittest {
  auto polygon = `{
    "type":"MultiPolygon",
    "coordinates":[[[[-73.979966,40.768902],[-73.97984,40.76865000000001],[-73.97975,40.76853],[-73.97974000000001,40.76956000000001],[-73.97978999999999,40.76949999999999],[-73.97996999999999,40.76914999999997],[-73.97996000000001,40.76893999999999]]]]
  }`.parseJsonString;

  polygon.featureBox.should.equal(`{
    "coordinates": [[[-73.97997, 40.76853], [-73.97974, 40.76853], [-73.97974, 40.76956], [-73.97997, 40.76956], [-73.97997, 40.76853]]],
    "type": "Polygon"
  }`.parseJsonString);
}


/// it can simplify a polygon with intersecting edges - 1
unittest {
  auto intersectingEdge = readText("tests/fixtures/intersectingEdge.json").parseJsonString;
  auto validEdges = readText("tests/fixtures/validEdges.json").parseJsonString;
  auto polygon = GeoJsonGeometry.fromJson(intersectingEdge).to!Polygon;

  auto result = simplifyLine(polygon.coordinates[0].toEPSG3857, 1, 4, true).toEPSG4326.toList;

  result.should.equal(GeoJsonGeometry.fromJson(validEdges).to!Polygon.coordinates[0].toList);
}

/// it can simplify a polygon with intersecting edges - 2
unittest {
  auto intersectingEdge = readText("tests/fixtures/intersectingEdge2.json").parseJsonString;
  auto polygon = GeoJsonGeometry.fromJson(intersectingEdge).to!Polygon;

  auto result = simplifyLine(polygon.coordinates[0].toEPSG3857, 20, 4, true);

  result.toEPSG4326.toList.should.equal([
    [34.5116, 28.5188],
    [34.5198, 28.5063],
    [34.5123, 28.5183],
    [34.5169, 28.5368],
    [34.5195, 28.5531],
    [34.5358, 28.5697],
    [34.5189, 28.553],
    [34.5116, 28.5188]
  ]);
}

///
Json simplifyGeometry(T)(uint z, uint x, uint y, T geometry, FeatureDecorators decorators) if(is(T == LineString) || is(T == MultiLineString)) {
  auto result = Json.emptyObject;

  auto tilePolygon = getTilePolygon(z, x, y, 0.1);
  auto tilePoints = tilePolygon.toEPSG3857;
  const tileBox = BBox!long(tilePoints);

  EPSG3857[][] intersections;
  EPSG3857[][] simpleIntersections;

  auto coordinates3857 = geometry.coordinates.toEPSG3857;

  static if(is(T == LineString)) {
    auto l = coordinates3857.cutAndSplitWith(tileBox);

    if(l.length > 1) {
      intersections ~= l;
    }
  }

  static if(is(T == MultiLineString)) {
    foreach(coordinates; coordinates3857) {
      auto l = coordinates.cutAndSplitWith(tileBox);

      if(l.length > 1) {
        intersections ~= l;
      }
    }
  }

  if(intersections.length == 0) {
    return Json();
  }

  auto maxDistance = tileBox.dx / 8_000;

  foreach(i; 0..intersections.length) {
    auto tmp = simplifyLineString(intersections[i], maxDistance);

    if(tmp.length > 0) {
      simpleIntersections ~= tmp;
    }
  }

  result["type"] = "MultiLineString";
  result["coordinates"] = simpleIntersections.toEPSG4326.toList.serializeToJson;

  return result;
}

///
Json simplifyGeometry(T)(uint z, uint x, uint y, T geometry, FeatureDecorators decorators) if(is(T == Polygon) || is(T == MultiPolygon)) {
  auto result = Json.emptyObject;

  EPSG3857[][][] intersections;
  EPSG3857[][][] simpleIntersections;

  static if(is(T == Polygon)) {
    EPSG3857[][] polygon = simplifyPolygon!EPSG3857(z, x, y, geometry.coordinates);

    if(polygon.length > 3) {
      simpleIntersections ~= polygon;
    }
  }

  static if(is(T == MultiPolygon)) {
    foreach(polygon; geometry.coordinates) {
      EPSG3857[][] newPolygon = simplifyPolygon!EPSG3857(z, x, y, polygon);

      if(newPolygon.length > 3) {
        simpleIntersections ~= newPolygon;
      }
    }
  }

  if(simpleIntersections.length == 0) {
    return Json();
  }

  auto transformed = simpleIntersections.toEPSG4326.toList;

  foreach(ref intersection; transformed) {
    fixTileBorders(z, x, y, intersection);
  }

  result["type"] = "MultiPolygon";
  result["coordinates"] = transformed.serializeToJson;

  if(result["coordinates"].length == 0) {
    return Json();
  }

  return result;
}

/// it simplifies a line
unittest {
  auto line = `{
  "type": "LineString",
  "coordinates":
    [[13.6237312643, 52.5312462605],
    [13.6594368307, 52.5262335005],
    [13.6223579733, 52.494472733],
    [13.6656166403, 52.4744014733],
    [13.6992622702, 52.4564131924],
    [13.7528206198, 52.4484625405],
    [13.7294746725, 52.4191582785],
    [13.7390877097, 52.4061744507]]
  }`.parseJsonString;

  auto result = simplifyGeometry(0, 0, 0, GeoJsonGeometry.fromJson(line).to!LineString, FeatureDecorators());

  result.should.equal(`{
    "coordinates": [[
      [13.6237238367, 52.5312434384],
      [13.6594318693, 52.5262320135],
      [13.6223494143, 52.4944724162],
      [13.6656122784, 52.4744010325],
      [13.6992541858, 52.456411614],
      [13.7528117431, 52.4484571486],
      [13.729473512, 52.4191561263],
      [13.7390854855, 52.4061698201]
    ]],
    "type": "MultiLineString"
  }`.parseJsonString);

  result = simplifyGeometry(10, 550, 335, GeoJsonGeometry.fromJson(line).to!LineString, FeatureDecorators());

  result.should.equal(`{
    "coordinates": [[
      [13.6237238367, 52.5312434384], [13.6594318693, 52.5262320135], [13.6223494143, 52.4944724162], [13.6656122784, 52.4744010325], [13.6898847574, 52.4614200548]]],
    "type": "MultiLineString"
  }`.parseJsonString);
}

/// it returns nothing when the tile does not contain any point from the line
unittest {
  auto line = `{
  "type": "LineString",
  "coordinates": [
    [13.62373126433985,52.53124626046215],
    [13.63265765594141,52.53124626046215],
    [13.65737689422266,52.52999312411043],
    [13.6594368307461,52.52623350050391],
    [13.6319710104336,52.51286334335055],
    [13.62235797332423,52.49447273299751],
    [13.61343158172266,52.47231019040689],
    [13.62373126433985,52.46645406955813],
    [13.62441790984766,52.47481971796529],
    [13.64570392058985,52.47942014681055],
    [13.66561664031642,52.47440147331025],
    [13.68209613250391,52.46645406955813],
    [13.69376910613673,52.46394406495224],
    [13.69926227019923,52.45641319239218],
    [13.70612872527735,52.45515792172779],
    [13.70269549773829,52.46812732645168],
    [13.72878802703517,52.45181035834793],
    [13.75282061980861,52.44846254050103],
    [13.76037372039455,52.43590595709392],
    [13.74389422820705,52.43841756007956],
    [13.73977435516017,52.43130097980264],
    [13.72947467254298,52.41915827852645],
    [13.73908770965236,52.40617445072439]
  ]}`.parseJsonString;

  auto result = simplifyGeometry(10, 540, 330, GeoJsonGeometry.fromJson(line).to!LineString, FeatureDecorators());

  result.type.should.equal(Json.Type.undefined);
}

/// it simplifies a multi line
unittest {
  auto line = `{
  "type": "MultiLineString",
  "coordinates": [[
      [13.62373126433985,52.53124626046215],
      [13.73908770965236,52.40617445072439]
    ],[
      [13.555585420411063,52.53035563595981],
      [13.600730261219752,52.50856887044594],
      [13.688684864864399,52.490562957879575],
      [13.624081040948198,52.44219482841112],
      [13.757180485402472,52.37999699269295]
    ]]}`.parseJsonString;

  auto result = simplifyGeometry(0, 0, 0, GeoJsonGeometry.fromJson(line).to!MultiLineString, FeatureDecorators());
  result.should.equal(`{
    "coordinates": [[[13.6237238367, 52.5312434384],
    [13.7390854855, 52.4061698201]],
    [[13.5555776393, 52.5303526817],
    [13.6007269654, 52.5085645037],
    [13.6886810149, 52.4905617039],
    [13.6240741797, 52.4421932965],
    [13.7571775553, 52.3799938126]]],
    "type": "MultiLineString"
  }`.parseJsonString);

  result = simplifyGeometry(10, 550, 335, GeoJsonGeometry.fromJson(line).to!MultiLineString, FeatureDecorators());
  result.should.equal(`{
    "coordinates": [[
      [13.6237238367, 52.5312434384],
      [13.6881599921, 52.4614200548]],
      [[13.5555776393, 52.5303526817],
      [13.6007269654, 52.5085645037],
      [13.6886810149, 52.4905617039],
      [13.6497390473, 52.4614200548]
    ]],
    "type": "MultiLineString"
  }`.parseJsonString);
}

/// it simplifies a polygon
unittest {
  auto line = `{
   "coordinates": [[
            [13.319090168893752,52.53391645377815],
            [13.311278376034949,52.49747049939961],
            [13.308674445082318,52.46734016715624],
            [13.366828903027368,52.40172218467271],
            [13.548236092734953,52.3969557856407],
            [13.620278182427683,52.45411859825532],
            [13.622014136396729,52.530748440364675],
            [13.556915862578336,52.58615568398193],
            [13.398076074459823,52.58721038098207],
            [13.319090168893752,52.53391645377815]
          ],[
            [13.485741749869192,52.52863630444935],
            [13.417171568113616,52.538140116149634],
            [13.386792373664775,52.47209896001283],
            [13.420643476050799,52.43613089218505],
            [13.518724875270635,52.45570539616972],
            [13.564727655436144,52.48954346528927],
            [13.485741749869192,52.52863630444935]
          ]
        ],
        "type": "Polygon"
      }`.parseJsonString;

  auto result = simplifyGeometry(0, 0, 0, GeoJsonGeometry.fromJson(line).to!Polygon, FeatureDecorators());

  result.should.equal(`{
    "coordinates": [[
      [[13.3190871575, 52.5339156003], [13.3112718145, 52.497469482], [13.3086667002, 52.4673363967], [13.3668236317, 52.4017196239], [13.5482294202, 52.3969510584], [13.620274306, 52.454117941], [13.6220080545, 52.530746147], [13.5569071459, 52.5861512684], [13.3980760205, 52.5872100833], [13.3190871575, 52.5339156003]],
      [[13.4857336259, 52.5286312294], [13.5647224888, 52.4895388449], [13.5187197631, 52.455699986], [13.4206417004, 52.4361257094], [13.3867841973, 52.4720973525], [13.4171652203, 52.5381393608], [13.4857336259, 52.5286312294]]
    ]],
    "type": "MultiPolygon"
  }`.parseJsonString);

  result = simplifyGeometry(10, 550, 336, GeoJsonGeometry.fromJson(line).to!Polygon, FeatureDecorators());

  result.should.equal(`{
    "coordinates": [[
      [[13.3242165378, 52.4497985047], [13.3668236317, 52.4017196239], [13.5482294202, 52.3969510584], [13.620274306, 52.454117941], [13.6214061833, 52.5042394734], [13.3242165378, 52.5042394734], [13.3242165378, 52.4497985047]],
      [[13.4015614838, 52.5042394734], [13.5350241855, 52.5042394734], [13.5647224888, 52.4895388449], [13.5187197631, 52.455699986], [13.4206417004, 52.4361257094], [13.3867841973, 52.4720973525], [13.4015614838, 52.5042394734]]
    ]],
    "type": "MultiPolygon"
  }`.parseJsonString);
}

/// it simplifies a large polygon on all zoom levels
unittest {
  auto largePolygon = readText("tests/fixtures/bandon.geojson").parseJsonString;
  auto result = simplifyGeometry(0, 0, 0, GeoJsonGeometry.fromJson(largePolygon).to!MultiPolygon, FeatureDecorators());

  result.to!string.should.equal("null");

  result = simplifyGeometry(8, 121, 84, GeoJsonGeometry.fromJson(largePolygon).to!MultiPolygon, FeatureDecorators());

  result["type"].should.equal("MultiPolygon");
  result["coordinates"].length.should.equal(1);
  result["coordinates"][0].length.should.equal(1);
}

/// it simplifies a multi polygon
unittest {
  auto multiPolygon = `{
   "coordinates": [[[
            [13.319090168893752,52.53391645377815],
            [13.311278376034949,52.49747049939961],
            [13.308674445082318,52.46734016715624],
            [13.366828903027368,52.40172218467271],
            [13.548236092734953,52.3969557856407],
            [13.620278182427683,52.45411859825532],
            [13.622014136396729,52.530748440364675],
            [13.556915862578336,52.58615568398193],
            [13.398076074459823,52.58721038098207],
            [13.319090168893752,52.53391645377815]
          ],[
            [13.485741749869192,52.52863630444935],
            [13.417171568113616,52.538140116149634],
            [13.386792373664775,52.47209896001283],
            [13.420643476050799,52.43613089218505],
            [13.518724875270635,52.45570539616972],
            [13.564727655436144,52.48954346528927],
            [13.485741749869192,52.52863630444935]
          ]]
        ],
        "type": "MultiPolygon"
      }`.parseJsonString;

  auto result = simplifyGeometry(0, 0, 0, GeoJsonGeometry.fromJson(multiPolygon).to!MultiPolygon, FeatureDecorators());

  result.should.equal(`{
     "coordinates": [[
        [[13.3190871575, 52.5339156003], [13.3112718145, 52.497469482], [13.3086667002, 52.4673363967], [13.3668236317, 52.4017196239], [13.5482294202, 52.3969510584], [13.620274306, 52.454117941], [13.6220080545, 52.530746147], [13.5569071459, 52.5861512684], [13.3980760205, 52.5872100833], [13.3190871575, 52.5339156003]],
        [[13.4857336259, 52.5286312294], [13.5647224888, 52.4895388449], [13.5187197631, 52.455699986], [13.4206417004, 52.4361257094], [13.3867841973, 52.4720973525], [13.4171652203, 52.5381393608], [13.4857336259, 52.5286312294]]
      ]],
     "type": "MultiPolygon"
  }`.parseJsonString);

  result = simplifyGeometry(10, 550, 336, GeoJsonGeometry.fromJson(multiPolygon).to!MultiPolygon, FeatureDecorators());

  result.should.equal(`{
    "coordinates": [[[[13.3242165378, 52.4497985047], [13.3668236317, 52.4017196239], [13.5482294202, 52.3969510584], [13.620274306, 52.454117941], [13.6214061833, 52.5042394734], [13.3242165378, 52.5042394734], [13.3242165378, 52.4497985047]], [[13.4015614838, 52.5042394734], [13.5350241855, 52.5042394734], [13.5647224888, 52.4895388449], [13.5187197631, 52.455699986], [13.4206417004, 52.4361257094], [13.3867841973, 52.4720973525], [13.4015614838, 52.5042394734]]]],
    "type": "MultiPolygon"
  }`.parseJsonString);
}

/// it can simplify the barcelona intersecting shore multi polygon
unittest {
  auto largePolygon = readText("tests/fixtures/barcelona.json").parseJsonString;
  auto result = simplifyGeometry(4, 8, 5, GeoJsonGeometry.fromJson(largePolygon).to!MultiPolygon, FeatureDecorators());

  result.should.equal(`{
    "type":"MultiPolygon",
    "coordinates":[[[
      [2.0699340106, 41.4071637598],
      [2.1907214837, 41.3749904025],
      [2.1939823682, 41.3802548751],
      [2.1973151179, 41.3811176418],
      [2.1954286558, 41.38178493],
      [2.195913746, 41.3831868871],
      [2.1999831143, 41.3851886676],
      [2.198321231, 41.3868803599],
      [2.2004322719, 41.388315905],
      [2.2019324584, 41.3871836467],
      [2.2001987099, 41.3858626538],
      [2.200791598, 41.3854043439],
      [2.2023905992, 41.386853401],
      [2.2027229759, 41.3866040307],
      [2.1988422539, 41.3836586927],
      [2.2006838002, 41.383833934],
      [2.203612308, 41.3874127958],
      [2.2012946546, 41.3887876734],
      [2.2020492394, 41.3899603401],
      [2.0699340106, 41.4071637598]
    ]]]}`.parseJsonString);
}

/// removes a hole when it is outside of the polygon
unittest {
  auto largePolygon = readText("tests/fixtures/holeOverflow.json").parseJsonString;
  auto result = simplifyGeometry(11, 524, 749, GeoJsonGeometry.fromJson(largePolygon).to!MultiPolygon, FeatureDecorators());

  result.should.equal(`{
    "type":"MultiPolygon",
    "coordinates":[[
      [[-87.9081994683, 43.3602936538], [-87.905423674, 43.3637289715], [-87.8967549316, 43.3641469436], [-87.8967728979, 43.3629126113], [-87.899252248, 43.3628407711], [-87.8992342817, 43.3606985873], [-87.9034922962, 43.3606724626], [-87.9034743299, 43.3575308903], [-87.89102368, 43.362605657], [-87.8839000398, 43.3626905594], [-87.8868644803, 43.3554864949], [-87.8821303587, 43.3668636924], [-87.8829388425, 43.3685550683], [-87.8829837582, 43.3760905868], [-87.8611097811, 43.4091926737], [-87.8809805152, 43.4028948133], [-87.8809445826, 43.3994225522], [-87.8809176331, 43.3945271093], [-87.8827142637, 43.3907801965], [-87.8839449556, 43.390519079], [-87.8897660386, 43.3777164121], [-87.901659733, 43.3654269654], [-87.9081994683, 43.3651330852], [-87.9081994683, 43.3602936538]],
      [[-87.8859571818, 43.3787480377], [-87.8812859424, 43.3795968304], [-87.882911893, 43.3775531786], [-87.8863614237, 43.3760514098], [-87.8871609243, 43.3771287694], [-87.8859571818, 43.3787480377]]], [[[-87.8815284875, 43.4098974712], [-87.8807469532, 43.4095515994], [-87.8807200037, 43.4109611779], [-87.8816273022, 43.4109350749], [-87.8815284875, 43.4098974712]]], [[[-87.8741802685, 43.4203771036], [-87.8710451481, 43.4203249057], [-87.8709822661, 43.425374845], [-87.8730214417, 43.4253291756], [-87.8752223142, 43.4219038726], [-87.8741802685, 43.4203771036]]], [[[-87.8657450879, 43.4193592363], [-87.8655205091, 43.4233784077], [-87.8610558822, 43.4243896832], [-87.8671644261, 43.4237698712], [-87.8677752805, 43.4193396618], [-87.8657450879, 43.4193592363]]
    ]]
  }`.parseJsonString);
}

/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.features.visibility;

import ogm.crates.all;
import ogm.models.feature;
import vibe.data.json;
import std.algorithm;
import std.array;
import crate.base;
import ogm.http.request;

auto getUserTeam(Json userId, OgmCrates crates) {
  auto list = [userId.to!string];

  return getUserTeams!"owners"(crates, list) ~
    getUserTeams!"leaders"(crates, list) ~
    getUserTeams!"members"(crates, list) ~
    getUserTeams!"guests"(crates, list);
}

Json getComputedVisibility(OgmCrates crates, Json feature) {
  auto mapIds = feature["maps"].byValue.map!(a => ObjectId(a)).array;

  auto maps = crates.map.get
    .where("_id").anyOf(mapIds)
    .and.exec.array;

  if(maps.length == 0 && feature["computedVisibility"].type == Json.Type.object) {
    feature["computedVisibility"]["isPublic"] = false;
    return feature["computedVisibility"];
  }

  auto teams = maps.map!(a => a["visibility"]["team"]);
  auto isPublicMap = maps.map!(a => a["visibility"]["isPublic"].to!bool);

  bool isPublic = feature["visibility"].to!int != 0;

  if(isPublic && isPublicMap.all!"a == false") {
    isPublic = false;
  }

  auto computedVisibility = Json.emptyObject;
  computedVisibility["isDefault"] = false;
  computedVisibility["isPublic"] = isPublic;

  if(teams.empty) {
    computedVisibility["team"] = feature["info"]["originalAuthor"].getUserTeam(crates).front;
  } else {
    computedVisibility["team"] = teams.front;
  }

  return computedVisibility;
}

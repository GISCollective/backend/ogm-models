/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.features.middlewares.FeatureMaskMapper;

import crate.base;
import crate.ctfe;
import crate.http.request;
import gis_collective.hmq.log;
import ogm.crates.all;
import ogm.http.request;
import openapi.definitions;
import vibe.data.json;
import vibe.http.server;
import std.algorithm;

///
class FeatureMaskMapper {

  private {
    OgmCrates crates;
  }

  ///
  this(OgmCrates crates) {
    this.crates = crates;
  }

  @rule
  void updateRule(ref CrateRule rule) @safe {
    if(rule.response.schema is null) {
      return;
    }

    rule.response.schema.properties["isMasked"] = new Schema();
    rule.response.schema.properties["isMasked"].readOnly = true;
    rule.response.schema.properties["isMasked"].nullable = true;
    rule.response.schema.properties["isMasked"].type = SchemaType.boolean;

    rule.modelDescription.fields.basic ~= FieldDescription("bool", "isMasked", "isMasked", [], false, true, true, false);
  }

  @mapper
  void maskMapper(HTTPServerRequest req, ref Json item) {
    if("unmasked" !in item || item["unmasked"].type != Json.Type.object) {
      return;
    }

    auto unmasked = item["unmasked"];
    item.remove("unmasked");

    auto request = RequestUserData(req);

    if(!request.userId || "computedVisibility" !in item) {
      return;
    }

    string team = item["computedVisibility"]["team"].to!string;
    auto session = request.session(crates);

    if(request.isAdmin || session.all.teams.canFind(team)) {
      item["position"] = unmasked;
    } else {
      item["isMasked"] = true;
    }
  }
}
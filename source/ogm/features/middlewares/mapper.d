/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.features.middlewares.mapper;

import crate.base;
import crate.http.request;
import vibe.data.json;
import ogm.crates.all;
import ogm.middleware.adminrequest;
import ogm.relationFix;
import vibe.http.server;
import gis_collective.hmq.log;

import std.algorithm;
import std.array;
import ogm.markdown;

///
class FeatureMapper {

  private {
    OgmCrates crates;
  }

  bool keepCanViewFlag;
  bool ignoreIssues;

  ///
  this(OgmCrates crates) {
    this.crates = crates;
  }

  @mapper
  void mapper(HTTPServerRequest req, ref Json item) @trusted nothrow {

    try {
      if("positionBox" in item) {
        item.remove("positionBox");
      }
    } catch(Exception e) { error(e); }

    try {
      if(item["description"].type == Json.Type.string) {
        item["description"] = item["description"].to!string.blocksFromMarkdown;
      }
    } catch(Exception e) { error(e); }

    try {
      if(!ignoreIssues && "canEdit" in item && item["canEdit"]) {
        auto count = crates.issue.get
          .where("feature").equal(item["_id"].to!string).and
          .where("status").equal("open").and
          .size;

        if(count > 0) {
          item["issueCount"] = count;
        }
      }
    } catch(Exception e) { error(e); }

    try {
      Json[] contributors;

      if(item["contributors"].type == Json.Type.array) {
        foreach(id; item["contributors"]) {
          if(id.type == Json.Type.string && id.length > 0) {
            contributors ~= id;
          }
        }
      }

      item["contributors"] = contributors.serializeToJson;
    } catch(Exception e) { error(e); }

    if(!keepCanViewFlag) {
      try {
        if("canView" in item) {
          item.remove("canView");
        }
      } catch(Exception e) { error(e); }
    }

    fixRelationList!"pictures"(item);
    fixRelationList!"sounds"(item);
  }
}

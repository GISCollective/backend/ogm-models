/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.features.middlewares.FeatureAttributesMiddleware;

import crate.base;
import crate.error;

import vibe.data.json;
import vibe.http.server;

import gis_collective.hmq.log;
import ogm.crates.all;
import ogm.http.request;
import ogm.icons.TaxonomyTree;

import std.algorithm;
import std.string;

class FeatureAttributesMiddleware {
  private {
    OgmCrates crates;
    TaxonomyTree iconTree;
  }

  ///
  this(OgmCrates crates, TaxonomyTree iconTree) {
    this.crates = crates;
    this.iconTree = iconTree;
  }

  @mapper
  void cleanAttributes(HTTPServerRequest req, ref Json item) {
    auto request = RequestUserData(req);

    if(request.isAdmin) {
      return;
    }

    if(item["canView"] == true || item["canEdit"] == true) {
      return;
    }

    auto iconIds = item["icons"].byValue.map!(a => a.to!string).filter!(a => isObjectId(a));

    Json[string] attributes;

    foreach(id; iconIds) {
      auto icon = iconTree.getIcon(id);
      if(icon.type != Json.Type.object) {
        continue;
      }

      string iconName = icon["name"].to!string;
      attributes[iconName] = fixAttributes(id, item, icon);
    }

    item["attributes"] = attributes.serializeToJson;
  }

  Json fixAttributes(string id, ref Json item, ref Json icon) {
    string[] publicAttributes = iconTree.getPublicIconAttributes(id);
    string[] names = icon.getAllIconNames;

    Json[] groups;
    foreach (name; names) {
      if(name !in item["attributes"]) {
        continue;
      }

      groups ~= extractValidAttributes(item["attributes"][name], publicAttributes);
    }

    if(icon["allowMany"] == true) {
      return groups.serializeToJson;
    }

    if(groups.length == 0) {
      return Json.emptyObject;
    }

    return groups[0];
  }
}

string[] getAllIconNames(Json icon) {
  string[] names;

  if(icon["otherNames"].type == Json.Type.array) {
    foreach(name; icon["otherNames"]) {
      names ~= name.to!string;
    }
  }

  string iconName = icon["name"].to!string;
  names ~= iconName;

  return names;
}

string[] getPublicIconAttributes(TaxonomyTree iconTree, string id) {
  auto iconAttributes = iconTree.getIconNode(id).attributes;
  string[] publicAttributes;

  foreach (Json attribute; iconAttributes) {
    if("isPrivate" !in attribute || attribute["isPrivate"] != true) {
      publicAttributes ~= attribute["name"].to!string;
    }
  }

  return publicAttributes;
}

Json[] extractValidAttributes(Json valueGroup, string[] iconAttributes) {
  Json[] result;

  if(valueGroup.type == Json.Type.array) {
    foreach(size_t key, Json value; valueGroup) {
      result ~= value.extractValidAttributes(iconAttributes);
    }
  }

  if(valueGroup.type == Json.Type.object) {
    Json attributes = Json.emptyObject;
    foreach(string key, value; valueGroup) {
      auto match = iconAttributes.filter!(a => a == key);

      if(!match.empty) {
        attributes[match.front] = value;
      }
    }
    result ~= attributes;
  }

  return result;
}

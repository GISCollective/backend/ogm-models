/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.features.middlewares.FeatureSoundsMiddleware;

import crate.base;
import crate.error;
import vibe.data.json;
import ogm.crates.all;
import vibe.http.server;
import gis_collective.hmq.log;

import std.algorithm;

class FeatureSoundsMiddleware {
  private {
    OgmCrates crates;
  }

  ///
  this(OgmCrates crates) {
    this.crates = crates;
  }

  Json getTeamId(CrateAccess crate, Json id) {
    auto range = crate.get.where("_id").equal(ObjectId.fromJson(id)).and.withProjection(["visibility"]).and.exec;

    if(range.empty) {
      return Json();
    }

    return range.front["visibility"]["team"];
  }

  @create @put @patch
  void validateSoundList(HTTPServerRequest request) {
    auto item = request.json;

    if("feature" !in request.json || "sounds" !in request.json["feature"]) {
      return;
    }

    auto sounds = request.json["feature"]["sounds"];

    if(sounds.length == 0) {
      return;
    }

    auto soundsTeam = request.json["feature"]["sounds"]
      .byValue
      .map!(id => this.getTeamId(crates.sound, id));

    Json mapTeam = this.getTeamId(crates.map, item["feature"]["maps"][0]);

    auto firstInvalidSound = soundsTeam.countUntil!(a => a != mapTeam);

    enforce!CrateValidationException(firstInvalidSound == -1, "The sound with id `" ~ sounds[firstInvalidSound].to!string ~ "` can not be added to this feature.");
  }
}
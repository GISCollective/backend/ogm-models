/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.features.formats.csv;

import crate.base;
import geo.json;
import crate.attributes;
import crate.collection.csv;

import ogm.crates.all;
import ogm.models.picture;
import ogm.models.sound;
import ogm.markdown;
import ogm.csv;

import std.datetime;
import std.conv;
import std.range;
import std.algorithm;
import std.traits;
import std.conv;
import std.path;
import std.exception;
import std.string;
import ogm.csv;

import vibe.data.json;
import vibe.core.log;

import geo.json : GeoJsonGeometry;
import geo.wkt;

version(unittest) {
  import fluent.asserts;
}

string addGeometry(T)(string prefix = "") if(is(T == GeoJsonGeometry)) {
  return prefix ~ ".lon" ~ "," ~ prefix ~ ".lat,wkt,geoJson";
}

string csvHeader(T)(string prefix = "") if(is(T == ObjectId) || is(T == SysTime)) {
  return prefix;
}

string csvHeader(T)(string prefix = "") if(!is(T == ObjectId) && !is(T == SysTime) && !is(T == GeoJsonGeometry)) {
  string result;
  string glue;

  if(prefix != "") {
    prefix = prefix ~ ".";
  }

  static foreach(member; __traits(allMembers, T)) {
    static if(member != "monitor") {
      static if(!isCallable!(__traits(getMember, T, member))) {
        static if(isAggregateType!(typeof(__traits(getMember, T, member)))) {
          result ~= glue ~ csvHeader!(typeof(__traits(getMember, T, member)))(prefix ~ member );
        } else {
          result ~= glue ~ prefix ~ member;
        }
        glue = ",";
      }
    }
  }

  return result;
}

void addGeometry(ref CsvRow row, ref Json value) {
  auto geometry = value.deserializeJson!GeoJsonGeometry;

  if("type" in value && value["type"] == "Point") {
    try {
      row.addField("position.lon", value["coordinates"][0]);
      row.addField("position.lat", value["coordinates"][1]);
    } catch(Exception e) {
      logError(e.message);
    }
  }

  try {
    row.addField("position.wkt", geometry.toWKTString);
  } catch(Exception e) {
    logError(e.message);
  }

  try {
    row.addField("position.geoJson", geometry.toJson);
  } catch(Exception e) {
    logError(e.message);
  }
}

string toCsv(T)(ref Json value) if(is(T == ObjectId) || is(T == SysTime)) {
  return value.to!string;
}

struct IdResolver {
  Json[string] cache;

  Json getName(string model)(Json id) {
    string key = model ~ "." ~ id.to!string;

    if(key !in cache) {
      try {
        enforce(model in crateGetters);
        cache[key] = crateGetters[model](id.to!string).withProjection(["name"]).exec.front["name"].to!string;
      } catch(Exception e) {
        logError(e.message);
        cache[key] = "";
      }
    }

    return cache[key];
  }

  Json getNames(string model)(Json idList) {
    auto list = (cast(Json[]) idList).map!(a => getName!model(a).to!string).array.join(";");

    return list.serializeToJson;
  }

  Json getPictures(Json idList) {
    auto list = (cast(Json[]) idList).map!(a => buildPath(PictureFileSettings.baseUrl, "pictures", a.to!string, "picture")).array.join(";");

    return list.serializeToJson;
  }

  Json getSounds(Json idList) {
    auto list = (cast(Json[]) idList).map!(a => buildPath(SoundFileSettings.baseUrl, "sounds", a.to!string, "sound")).array.join(";");

    return list.serializeToJson;
  }

  Json getUserEmail(Json id) {
    if(id.type != Json.Type.string) {
      return Json("");
    }

    string key = "user.email." ~ id.to!string;

    if(key !in cache) {
      try {
        enforce("User" in crateGetters);
        cache[key] = crateGetters["User"](id.to!string).withProjection(["email"]).exec.front["email"].to!string;
      } catch(Exception e) {
        logError(e.message);
        cache[key] = id.to!string;
      }
    }

    return cache[key];
  }
}

private immutable string[] positionAttributes = [
  "attributes.position details.type",
  "attributes.position details.altitudeAccuracy",
  "attributes.position details.altitude",
  "attributes.position details.accuracy"
];

private immutable string[] sourceAttributes = [
  "attributes.source.answer",
  "attributes.source.type",
  "attributes.source.campaign"
];

private immutable string[] infoAttributes = [
  "createdOn",
  "lastChangeOn",
  "changeIndex",
  "author.email",
  "originalAuthor.email"
];

CsvHeader getFeatureCsvHeader(Json[] icons) {
  auto header = new CsvHeader();
  header.add!Feature;

  foreach (icon; icons) {
    auto name = icon["name"].to!string;
    auto attributes = icon["attributes"].byValue.map!(a => a["name"].to!string).array;

    foreach(attribute; attributes) {
      header.add("attributes." ~ name ~ "." ~ attribute);
    }
  }

  foreach(attribute; positionAttributes) {
    header.add(attribute);
  }

  foreach(attribute; sourceAttributes) {
    header.add(attribute);
  }

  foreach(attribute; infoAttributes) {
    header.add(attribute);
  }

  header.fields = header.fields.filter!(a => a.indexOf("decorators.") == -1 && a.indexOf("info.") == -1 && a != "attributes").array;
  header.reindex;

  return header;
}

/// It does not add the decorators headers
unittest {
  auto header = getFeatureCsvHeader([]);

  header.toString.should.equal("_id,maps,maps.name,name,description," ~
    "position.lon,position.lat,position.wkt,position.geoJson," ~
    "pictures,pictures.name,pictures.url,sounds,sounds.name,sounds.url," ~
    "icons,icons.name," ~
    "visibility,contributors,source.type,source.modelId,source.remoteId,source.syncAt,computedVisibility.isPublic,computedVisibility.isDefault," ~
    "attributes.position details.type,attributes.position details.altitudeAccuracy,attributes.position details.altitude,attributes.position details.accuracy," ~
    "attributes.source.answer,attributes.source.type,attributes.source.campaign," ~
    "createdOn,lastChangeOn,changeIndex,author.email,originalAuthor.email\n");
}

InputRange!string toCsv(IQuery query, Json[] icons, void delegate(ref Json) mapper, string[] extraHeaders = []) {
  auto header = getFeatureCsvHeader(icons);

  foreach(field; extraHeaders) {
    header.add(field);
  }

  header.reindex;

  IdResolver resolver;

  size_t index;

  string convert(Json a) {
    index++;

    mapper(a);

    string result = "";
    if(index == 1) {
      result ~= header.toString;
    }

    try {
      auto row = header.toRow;
      result ~= row.addFeatureFields(a, resolver);
    } catch(Exception e) {
      logError(e.message);
    }

    return result;
  }

  return query.exec.map!(a => convert(a)).inputRangeObject;
}

version(unittest) {
  struct MockIdResolver {
    Json getName(string model)(Json id) {
      return Json(model ~ "." ~ id.to!string);
    }

    Json getNames(string model)(Json idList) {
      auto list = (cast(Json[]) idList).map!(a => getName!model(a).to!string).array.join(";");
      return list.serializeToJson;
    }

    Json getPictures(Json idList) {
      auto list = (cast(Json[]) idList).map!(a => buildPath("baseUrl", "pictures", a.to!string, "picture")).array.join(";");

      return list.serializeToJson;
    }

    Json getSounds(Json idList) {
      auto list = (cast(Json[]) idList).map!(a => buildPath("baseUrl", "sounds", a.to!string, "sounds")).array.join(";");

      return list.serializeToJson;
    }

    Json getUserEmail(Json id) {
      return id;
    }
  }
}

string addFeatureFields(T)(ref CsvRow row, Json feature, T resolver) {
  auto position = feature["position"];
  feature.remove("position");

  if(position.type == Json.Type.object) {
    row.addGeometry(position);
  }

  if(feature["description"].type == Json.Type.string) {
    row.addField("description", feature["description"].to!string);
    feature.remove("description");
  }

  if(feature["description"].type == Json.Type.object) {
    row.addField("description", feature["description"].toMarkdown);
    feature.remove("description");
  }

  if(feature["maps"].type == Json.Type.array) {
    row.addField("maps.name", resolver.getNames!"Map"(feature["maps"]));
  }

  if(feature["icons"].type == Json.Type.array) {
    row.addField("icons.name", resolver.getNames!"Icon"(feature["icons"]));
  }

  if(feature["pictures"].type == Json.Type.array) {
    row.addField("pictures.url", resolver.getPictures(feature["pictures"]));
  }

  if(feature["sounds"].type == Json.Type.array) {
    row.addField("sounds.url", resolver.getSounds(feature["sounds"]));
  }

  Json attributes = Json.emptyObject;
  if(feature["attributes"].type == Json.Type.object) {
    attributes = feature["attributes"];

    foreach (string key, Json group; attributes) {
      if(group.type == Json.Type.object) {
        row.addAll("attributes." ~ key, group);
      }

      if(group.type == Json.Type.array && group.length > 0) {
        row.addAll("attributes." ~ key, group[0]);
      }
    }

    feature.remove("attributes");
  }

  if(feature["info"].type == Json.Type.object) {
    row.addField("createdOn", feature["info"]["createdOn"]);
    row.addField("lastChangeOn", feature["info"]["lastChangeOn"]);
    row.addField("changeIndex", feature["info"]["changeIndex"]);
    row.addField("author.email", resolver.getUserEmail(feature["info"]["author"]));
    row.addField("originalAuthor.email", resolver.getUserEmail(feature["info"]["originalAuthor"]));

    feature.remove("info");
  }

  row.addRow!Feature(feature);

  string result = row.toString;

  foreach (string key, Json groupList; attributes) {
    if(groupList.type != Json.Type.array){
      continue;
    }

    if(groupList.length <= 1) {
      continue;
    }

    foreach (size_t index, Json group; groupList) {
      if(index == 0) {
        continue;
      }

      auto listRow = row.header.toRow;
      listRow.addField("_id", feature["_id"]);
      listRow.addField("name", feature["name"]);
      listRow.addGeometry(position);

      listRow.addAll("attributes." ~ key, group);
      result ~= listRow.toString;
    }
  }

  return result;
}

/// It ignores attributes when an icon is not added to the header
unittest {
  MockIdResolver resolver;
  Json feature = `{
    "attributes": {
      "icon name": {
        "property": 1000
      }
    }
  }`.parseJsonString;
  auto header = getFeatureCsvHeader([]);

  auto row = header.toRow;

  addFeatureFields(row, feature, resolver)
    .should.equal(`,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,` ~ "\n");
}

/// It adds icon object attributes when an icon is added to the header
unittest {
  MockIdResolver resolver;
  Json feature = `{
    "attributes": {
      "icon name": {
        "property1": 1000,
        "property2": 2000
      }
    }
  }`.parseJsonString;

  auto icon = `{
    "name": "icon name",
    "attributes": [{ "name": "property1" }, { "name": "property2" }]
  }`.parseJsonString;

  auto header = getFeatureCsvHeader([icon]);

  auto row = header.toRow;

  addFeatureFields(row, feature, resolver)
    .should.equal(`,,,,,,,,,,,,,,,,,,,,,,,,,1000,2000,,,,,,,,,,,,` ~ "\n");
}

/// It adds icon list attributes when an icon is added to the header and there is only one object in the list
unittest {
  MockIdResolver resolver;
  Json feature = `{
    "attributes": {
      "icon name": [{
        "property1": 1000,
        "property2": 2000
      }]
    }
  }`.parseJsonString;

  auto icon = `{
    "name": "icon name",
    "attributes": [{ "name": "property1" }, { "name": "property2" }]
  }`.parseJsonString;

  auto header = getFeatureCsvHeader([icon]);

  auto row = header.toRow;

  addFeatureFields(row, feature, resolver)
    .should.equal(`,,,,,,,,,,,,,,,,,,,,,,,,,1000,2000,,,,,,,,,,,,` ~ "\n");
}

/// It adds icon list attributes when an icon is added to the header and there are 2 objects in the list
unittest {
  MockIdResolver resolver;
  Json feature = `{
    "_id": "some-id",
    "name": "feature name",
    "position": {
      "type": "Point",
      "coordinates": [ 13, 52 ]
    },
    "attributes": {
      "icon name": [{
        "property1": 1000,
        "property2": 2000
      }, {
        "property1": 3000,
        "property2": 4000
      }]
    }
  }`.parseJsonString;

  auto icon = `{
    "name": "icon name",
    "attributes": [{ "name": "property1" }, { "name": "property2" }]
  }`.parseJsonString;

  auto header = getFeatureCsvHeader([icon]);

  auto row = header.toRow;
  addFeatureFields(row, feature, resolver)
    .should.equal(
      `some-id,,,feature name,,13,52,POINT(13 52),"{""type"":""Point"",""coordinates"":[13,52]}",,,,,,,,,,,,,,,,,1000,2000,,,,,,,,,,,,` ~ "\n" ~
      `some-id,,,feature name,,13,52,POINT(13 52),"{""type"":""Point"",""coordinates"":[13,52]}",,,,,,,,,,,,,,,,,3000,4000,,,,,,,,,,,,` ~ "\n");
}

/// It adds the position details
unittest {
  MockIdResolver resolver;
  Json feature = `{
    "attributes": {
      "position details": {
        "altitudeAccuracy": 1000,
        "type": "manual",
        "altitude": 49.60104718711227,
        "accuracy": 6.82
      }
    }
  }`.parseJsonString;
  auto header = getFeatureCsvHeader([]);

  auto row = header.toRow;
  addFeatureFields(row, feature, resolver)
    .should.equal(`,,,,,,,,,,,,,,,,,,,,,,,,,manual,1000,49.60104718711227,6.82,,,,,,,,` ~ "\n");
}

/// It adds the geometry in all formats
unittest {
  MockIdResolver resolver;
  Json feature = `{
    "position": {
      "type": "Point",
      "coordinates": [ 13.44079856916428, 52.49165406229062 ]
    }
  }`.parseJsonString;
  auto header = getFeatureCsvHeader([]);

  auto row = header.toRow;

  addFeatureFields(row, feature, resolver)
    .should.equal(`,,,,,13.44079856916428,52.49165406229062,POINT(13.4407985691643 52.4916540622906),"{""type"":""Point"",""coordinates"":[13.44079856916428,52.49165406229062]}",,,,,,,,,,,,,,,,,,,,,,,,,,,,` ~ "\n");
}

/// It adds the resolved maps
unittest {
  MockIdResolver resolver;
  Json feature = `{
    "maps": ["1", "2"]
  }`.parseJsonString;
  auto header = getFeatureCsvHeader([]);

  auto row = header.toRow;
  addFeatureFields(row, feature, resolver)
    .should.equal(",1;2,Map.1;Map.2,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,\n");
}

/// It adds the resolved icons
unittest {
  MockIdResolver resolver;
  Json feature = `{
    "icons": ["1", "2"]
  }`.parseJsonString;
  auto header = getFeatureCsvHeader([]);

  auto row = header.toRow;
  addFeatureFields(row, feature, resolver)
    .should.equal(",,,,,,,,,,,,,,,1;2,Icon.1;Icon.2,,,,,,,,,,,,,,,,,,,,\n");
}

/// It adds the resolved pictures
unittest {
  MockIdResolver resolver;
  Json feature = `{
    "pictures": ["1", "2"]
  }`.parseJsonString;
  auto header = new CsvHeader();
  header.add!Feature;

  auto row = header.toRow;
  addFeatureFields(row, feature, resolver);

  auto lines = row.toString.split("\n");
  row.toString.should.equal(",,,,,,,,,,,,,,1;2,,baseUrl/pictures/1/picture;baseUrl/pictures/2/picture,,,,,,,,,,,,,,,,,,,,,,\n");
}

/// It adds the resolved sounds
unittest {
  MockIdResolver resolver;
  Json feature = `{
    "sounds": ["1", "2"]
  }`.parseJsonString;
  auto header = getFeatureCsvHeader([]);

  auto row = header.toRow;
  addFeatureFields(row, feature, resolver)
    .should.equal(",,,,,,,,,,,,1;2,,baseUrl/sounds/1/sounds;baseUrl/sounds/2/sounds,,,,,,,,,,,,,,,,,,,,,,\n");
}

/// It adds the resolved info data
unittest {
  MockIdResolver resolver;
  Json feature = `{
    "info": { "originalAuthor": "1", "author": "2", "createdOn": "2015-01-01T00:00:00Z", "lastChangeOn": "2015-01-01T00:00:00Z", "changeIndex": 10 }
  }`.parseJsonString;
  auto header = getFeatureCsvHeader([]);

  auto row = header.toRow;
  addFeatureFields(row, feature, resolver)
    .should.equal(",,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,2015-01-01T00:00:00Z,2015-01-01T00:00:00Z,10,2,1\n");
}

/// It adds nothing when the description has no blocks
unittest {
  MockIdResolver resolver;
  Json feature = `{
    "description": { "blocks": [] }
  }`.parseJsonString;
  auto header = new CsvHeader();
  header.add!Feature;

  auto row = header.toRow;
  addFeatureFields(row, feature, resolver)
    .should.equal(",,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,\n");
}

/// It adds the string description as string
unittest {
  MockIdResolver resolver;
  Json feature = `{
    "description": "some string"
  }`.parseJsonString;
  auto header = new CsvHeader();
  header.add!Feature;

  auto row = header.toRow;
  addFeatureFields(row, feature, resolver)
    .should.equal(",,,,some string,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,\n");
}

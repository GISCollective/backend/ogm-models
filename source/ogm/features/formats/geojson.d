/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.features.formats.geojson;

import crate.base;
import geo.json;

import ogm.crates.all;

import std.range;
import std.algorithm;

import vibe.data.json;
import ogm.features.middlewares.FeatureMaskMapper;

string toGeoJson(ref Json value) {
  string geometry;
  auto properties = Json.emptyObject;

  if("position" in value) {
    geometry = value["position"].to!string;
  }

  foreach(string key, Json val; value) {
    if(key != "position") {
      properties[key] = val;
    }
  }

  return `{"type":"Feature","geometry":` ~ geometry ~ `,"properties":` ~ properties.to!string ~ "}";
}

auto toGeoJson(IQuery query, void delegate(ref Json) mapper) {

  string conv(ref Json value) {
    mapper(value);
    return toGeoJson(value);
  }

  return query.exec.map!(a => conv(a)).joiner("\n,");
}

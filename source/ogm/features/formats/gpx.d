/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.features.formats.gpx;

import crate.base;
import geo.json;

import ogm.crates.all;
import ogm.markdown;

import std.range;
import std.algorithm;
import std.conv;

import vibe.data.json;
import geo.gpx;
import geo.xml;
import geo.epsg;

string toGPXPoint(ref Json coordinates, ref Json icon, ref Json feature) {
  string type;

  if(icon.type == Json.Type.object && icon["name"].type == Json.Type.string) {
    type = icon["name"].to!string;
  }

  WayPoint wayPoint;
  wayPoint.position.longitude = coordinates[0].to!double;
  wayPoint.position.latitude = coordinates[1].to!double;
  wayPoint.name = feature["name"].to!string;
  wayPoint.desc = feature["description"].toMarkdown;
  wayPoint.type = type;

  return wayPoint.toString;
}

string toGPXLine(ref Json coordinates, ref Json icon, ref Json feature) {
  string type;

  if(icon.type == Json.Type.object && icon["name"].type == Json.Type.string) {
    type = icon["name"].to!string;
  }

  Route route;
  route.name = feature["name"].to!string;
  route.desc = feature["description"].toMarkdown;
  route.type = type;

  foreach(point; coordinates) {
    import std.stdio;

    WayPoint wayPoint;
    wayPoint.position.longitude = point[0].to!double;
    wayPoint.position.latitude = point[1].to!double;

    route.rtept ~= wayPoint;
  }

  return route.toString;
}

///
string toGPX(ref Json value) {
  Json geometry;
  Json icon;

  auto properties = Json.emptyObject;
  string type;

  if("position" in value) {
    geometry = value["position"]["coordinates"];
  }

  if(value["icons"].length > 0) {
    auto iconId = value["icons"][0].to!string;
    icon = crateGetters["Icon"](iconId).exec.front;
  }

  if(value["position"]["type"] == "Point") {
    return toGPXPoint(geometry, icon, value);
  }

  if(value["position"]["type"] == "MultiPoint") {
    return geometry[].map!(a => toGPXPoint(a, icon, value)).joiner("\n").array.to!string;
  }

  if(value["position"]["type"] == "LineString") {
    return toGPXLine(geometry, icon, value);
  }

  if(value["position"]["type"] == "MultiLineString") {
    return geometry[].map!(a => toGPXLine(a, icon, value)).joiner("\n").array.to!string;
  }

  return " ";
}

///
auto toGPX(IQuery query, void delegate(ref Json) mapper) {
  string conv(ref Json value) {
    mapper(value);
    return toGPX(value);
  }

  return query.exec.map!(a => conv(a)).joiner("\n");
}

/// Extract the points from the gpx tracks
Json getTrackPoints(Json gpxJson) {
  auto newPosition = Json.emptyObject;
  newPosition["type"] = "MultiLineString";

  auto gpx = GPX.fromDom(parseDOM(gpxJson["value"].to!string));

  newPosition["coordinates"] = gpx.trk
    .map!(a => a.trkseg)
    .joiner
    .map!(a => a.trkpt.map!(a => [Json(a.position.longitude), Json(a.position.latitude)]).array)
    .array
    .serializeToJson;

  return newPosition;
}

/// Extract the points from the gpx routes
Json getRoutePoints(Json gpxJson) {
  auto newPosition = Json.emptyObject;
  newPosition["type"] = "MultiLineString";

  auto gpx = GPX.fromDom(parseDOM(gpxJson["value"].to!string));

  newPosition["coordinates"] = gpx.rte
    .map!(a => a.rtept.map!(a => [a.position.longitude, a.position.latitude]).array)
    .array
    .serializeToJson;

  return newPosition;
}
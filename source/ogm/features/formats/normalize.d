/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.features.formats.normalize;

import vibe.data.json;
import ogm.features.formats.gpx;


///
Json convertToGeoJson(Json position) {
  if(position["type"] == "gpx.track") {
    return getTrackPoints(position);
  }

  if(position["type"] == "gpx.route") {
    return getRoutePoints(position);
  }

  Json newPosition = Json.emptyObject;
  newPosition["type"] = position["type"];
  newPosition["coordinates"] = position["coordinates"];

  return newPosition;
}

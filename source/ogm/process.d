/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.process;

import vibe.core.core;
import vibe.core.log;
import std.process;
import std.datetime;
import std.exception;
import crate.error;

void run(string command) {
  logDebug(command);

  auto pid = spawnShell(command);

  auto waitResult = tryWait(pid);
  while(!waitResult.terminated) {
    sleep(5.msecs);
    waitResult = tryWait(pid);
  }

  enforce!CrateException(waitResult.status == 0, "Command failed: `" ~ command ~ "`.");
}
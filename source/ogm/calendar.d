/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.calendar;

import std.datetime;

interface CalendarService {
  SysTime now();
}

class SysCalendar : CalendarService {
  static CalendarService instance;

  static this() {
    SysCalendar.instance = new SysCalendar();
  }

  SysTime now() {
    auto value = Clock.currTime.toUTC;
    value.fracSecs = Duration.zero;

    return value;
  }
}


class CalendarMock : CalendarService {
  SysTime _now;

  this(string strDate) {
    _now = SysTime.fromISOExtString(strDate);
  }

  SysTime now() {
    return _now;
  }
}
/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.auth;

import crate.base;
import crate.auth.usercollection;
import crate.auth.middleware;

import vibe.http.router;
import vibe.data.json;

import vibeauth.collections.client;
import vibeauth.authenticators.OAuth2;
import vibeauth.data.client;

import ogm.models.user;

import std.file;
import std.conv;

class ReplaceMeId {
  UserCrateCollection collection;

  this(UserCrateCollection collection) {
    this.collection = collection;
  }

  @getItem
  void getItem(HTTPServerRequest req) @trusted {
    if("email" !in req.context) {
      return;
    }

    if("id" !in req.params || req.params["id"] != "me"){
      return;
    }

    auto email = req.context["email"].to!string;

    auto user = collection[email];
    req.context["id"] = user.id;
    req.params["id"] = user.id;
  }
}

class Authentication {

  protected {
    OAuth2 _oAuth2;
    Crate!User crate;
    UserCrateCollection collection;
    ClientCollection clientCollection;
    OAuth2Configuration oauthConfig;

    Client[] clients;
  }

  static {
    private Authentication _instance;

    Authentication instance(Crate!User crate, Client[] clients = []) {
      if(_instance is null) {
        _instance = new Authentication(crate, clients);
      }

      return _instance;
    }

    void reset() {
      _instance = null;
    }
  }

  this(Crate!User crate, Client[] clients = []) {
    this.crate = crate;
    immutable(string[]) accessList = [];
    this.clients = clients;

    collection = new UserCrateCollection(accessList, crate);
    clientCollection = new ClientCollection(clients);
  }

  UserCrateCollection getCollection() {
    return collection;
  }

  OAuth2 oAuth2() {
    if(_oAuth2 is null) {
      _oAuth2 = new OAuth2(collection);
    }

    return _oAuth2;
  }

  private IdentifiableContributionMiddleware _identifiableContributionMiddleware;
  IdentifiableContributionMiddleware identifiableContributionMiddleware() {
    if(_identifiableContributionMiddleware is null) {
      _identifiableContributionMiddleware = new IdentifiableContributionMiddleware(collection, clientCollection, oauthConfig);
    }

    return _identifiableContributionMiddleware;
  }

  private ContributionMiddleware _contributionMiddleware;
  ContributionMiddleware contributionMiddleware() {
    if(_contributionMiddleware is null) {
      _contributionMiddleware = new ContributionMiddleware(collection, clientCollection, oauthConfig);
    }

    return _contributionMiddleware;
  }

  private PublicContributionMiddleware _publicContributionMiddleware;
  PublicContributionMiddleware publicContributionMiddleware() {
    if(_publicContributionMiddleware is null) {
      _publicContributionMiddleware = new PublicContributionMiddleware(collection, clientCollection, oauthConfig);
    }

    return _publicContributionMiddleware;
  }

  private PublicDataMiddleware _publicDataMiddleware;
  PublicDataMiddleware publicDataMiddleware() {
    if(_publicDataMiddleware is null) {
      _publicDataMiddleware = new PublicDataMiddleware(collection, clientCollection, oauthConfig);
    }

    return _publicDataMiddleware;
  }

  private PublicItemDataMiddleware _publicItemDataMiddleware;
  PublicItemDataMiddleware publicItemDataMiddleware() {
    if(_publicItemDataMiddleware is null) {
      _publicItemDataMiddleware = new PublicItemDataMiddleware(collection, clientCollection, oauthConfig);
    }

    return _publicItemDataMiddleware;
  }

  private PrivateDataMiddleware _privateDataMiddleware;
  PrivateDataMiddleware privateDataMiddleware() {
    if(_privateDataMiddleware is null) {
      _privateDataMiddleware = new PrivateDataMiddleware(collection, clientCollection, oauthConfig);
    }

    return _privateDataMiddleware;
  }

  private ReplaceMeId _replaceMeId;
  ReplaceMeId replaceMeId() {
    if(_replaceMeId is null) {
      _replaceMeId = new ReplaceMeId(collection);
    }

    return _replaceMeId;
  }
}

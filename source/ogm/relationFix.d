/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.relationFix;

import vibe.data.json;
import gis_collective.hmq.log;

void fixRelationList(string key)(ref Json feature) nothrow {
  try {
    if(key !in feature) {
      return;
    }
  } catch(Exception e) {
    error(e);
    return;
  }

  auto validElements = Json.emptyArray;
  try {
    foreach(item; feature[key]) {
      if(item.type == Json.Type.string) {
        validElements ~= item;
      }
    }
  } catch(Exception e) {}

  try feature[key] = validElements;
  catch(Exception) {}
}

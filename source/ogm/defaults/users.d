/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.defaults.users;

import ogm.crates.defaults;
import ogm.crates.all;
import vibe.core.log;
import vibeauth.data.usermodel;
import crate.auth.usercollection;

void setupDefaultUsers(OgmCrates crates) {
  auto userData = crates.user;
  auto count = userData.get.size;

  if(count > 0) {
    logInfo("There are some users. There is no need to create the default one.");
    return;
  }

  UserModel user;
  user.isActive = true;
  user.firstName = "John";
  user.lastName = "Doe";
  user.username = "admin";
  user.email = "admin@example.com";
  user.scopes = ["admin"];

  auto collection = new UserCrateCollection([], userData);
  collection.createUser(user, "admin");
}

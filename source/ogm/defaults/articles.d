/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.defaults.articles;

import vibe.core.log;
import vibe.data.json;

import ogm.crates.all;
import ogm.models.all;
import ogm.crates.defaults;
import ogm.models.modelinfo;

import crate.base;

import std.path;
import std.file;
import std.datetime;

void checkDefaultArticles(OgmCrates crates, bool useShortList = false) {
  auto info = ModelInfo();
  info.createdOn = Clock.currTime;
  info.lastChangeOn = Clock.currTime;
  info.author = "@system";

  auto articlesPath = buildPath("defaults", "articles");
  if(!articlesPath.exists) {
    articlesPath = buildPath("..", "..", "defaults", "articles");
  }


  auto articleFiles = dirEntries(articlesPath,"*.json", SpanMode.depth);

  size_t index = 0;
  foreach (file; articleFiles) {
    if(useShortList && index >= 5) {
      return;
    }

    auto jsonArticle = file.name.readText.parseJsonString;
    jsonArticle["_id"] = "1";
    jsonArticle["info"] = info.serializeToJson;

    jsonArticle["visibility"] = Json.emptyObject;

    crates.checkArticle(jsonArticle);
    index++;
  }
}

void checkArticle(OgmCrates crates, Json article) {
  if(crates.article.get.where("slug").equal(article["slug"].to!string).and.size > 0) {
    return;
  }

  logInfo("`%s` article does not exist. Adding the default.", article["slug"]);

  article["visibility"]["team"] = crates.team.publishersId;
  article["visibility"]["isPublic"] = true;
  article["visibility"]["isDefault"] = false;
  article.remove("_id");

  crates.article.addItem(article);
}

void setupDefaultArticles(OgmCrates crates, bool useShortList = false) {
  crates.checkDefaultArticles(useShortList);
}

/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.defaults.baseMaps;

import ogm.crates.defaults;
import ogm.crates.all;
import vibe.data.json;
import ogm.models.attribution;

///
void setupDefaultBaseMaps(OgmCrates crates) {
  crates.addDefaultBaseMap(
    "Open Street Maps",
    "road",
    [ MapLayer("Open Street Maps", Json.emptyObject) ],
    0,
    [ Attribution("© OpenStreetMap contributors", "https://www.openstreetmap.org/copyright") ]);

  crates.addDefaultBaseMap(
    "Stamen Terrain",
    "spa",
    [ MapLayer("Stamen", `{ "layer": "terrain" }`.parseJsonString) ],
    1,
    [ Attribution("Map tiles by Stamen Design", "http://stamen.com/"),
      Attribution("© OpenStreetMap contributors", "https://www.openstreetmap.org/copyright") ]);

  crates.addDefaultBaseMap(
    "Stamen Toner",
    "pen-nib",
    [ MapLayer("Stamen", `{ "layer": "toner" }`.parseJsonString) ],
    2,
    [ Attribution("Map tiles by Stamen Design", "http://stamen.com/"),
      Attribution("© OpenStreetMap contributors", "https://www.openstreetmap.org/copyright") ]);
}

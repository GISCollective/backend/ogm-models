/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.defaults.teams;

import ogm.crates.defaults;
import ogm.crates.all;

///
void setupDefaultTeams(OgmCrates crates) {
  crates.addDefaultTeam("General");
  crates.addDefaultTeam("Publishers", true, "The team in charge of editing the main articles.");
}

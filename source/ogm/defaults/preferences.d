/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.defaults.preferences;

import ogm.crates.defaults;
import ogm.crates.all;
import vibe.data.json;
import vibe.core.log;
import std.conv;
import vibe.service.configuration.general;
import vibe.inet.url;

void setupDefaultPreferences(OgmCrates crates, GeneralConfig config) {
  auto pictures = crates.picture;
  auto preferences = crates.preference;

  bool exists(string name) {
    return preferences.get.where("name").equal(name).and.size > 0;
  }

  void checkDefaultPictureFor(string name)(string value) {
    auto range = pictures.get.where("name").equal(value).and.exec;

    if(range.empty) {
      logError("The default picture `" ~ name ~ "` was not found.");
      return;
    }
    //enforce(!range.empty, "The default picture `" ~ name ~ "` was not found.");

    if(exists(name)) {
      string id = preferences.get.where("name").equal(name).and.exec.front["_id"].to!string;

      try {
        LazyPicture(pictures.getItem(id).exec.front).toType;
      } catch(Exception e) {
        logError(e.message);
        preferences.deleteItem(id);
      }
    }

    checkDefaultFor!name(range.front["_id"].to!string);
  }

  void checkDefaultFor(string name)(string value, bool isSecret = false) {
    if(exists(name)) {
      return;
    }

    logInfo("Adding default preference value for `" ~ name ~ "`. " ~ `{"name": "` ~ name ~ `", "value": "` ~ value ~ `, "isSecret": ` ~ isSecret.to!string ~` }`);
    preferences.addItem(parseJsonString(`{"name": "` ~ name ~ `", "value": "` ~ value ~ `", "isSecret": ` ~ isSecret.to!string ~` }`));
  }

  checkDefaultFor!("appearance.name")(`GISCollective`);

  if(!exists("appearance.logo")) {
    checkDefaultPictureFor!("appearance.logo")("logo");
  }

  if(!exists("appearance.cover")) {
    checkDefaultPictureFor!("appearance.cover")("cover");
  }

  checkDefaultFor!("appearance.mapExtent")("[ -118, -28, 37, 65 ]");
  checkDefaultFor!("appearance.showWelcomePresentation")("false");

  checkDefaultFor!("register.enabled")(`false`);
  checkDefaultFor!("register.mandatory")(`true`);
  checkDefaultFor!("register.emailDomains")(``);
  checkDefaultFor!("register.url")(``);

  checkDefaultFor!("secret.mail.type")("smtp", true);
  checkDefaultFor!("secret.mailersend.key")("", true);
  checkDefaultFor!("secret.mailersend.from")("", true);
  checkDefaultFor!("secret.smtp.authType")("none", true);
  checkDefaultFor!("secret.smtp.connectionType")("plain", true);
  checkDefaultFor!("secret.smtp.tlsValidationMode")("none", true);
  checkDefaultFor!("secret.smtp.tlsVersion")("any", true);
  checkDefaultFor!("secret.smtp.host")("", true);
  checkDefaultFor!("secret.smtp.port")("", true);
  checkDefaultFor!("secret.smtp.localname")("", true);
  checkDefaultFor!("secret.smtp.password")("", true);
  checkDefaultFor!("secret.smtp.username")("", true);
  checkDefaultFor!("secret.smtp.from")("", true);

  checkDefaultFor!("captcha.enabled")("false", false);
  checkDefaultFor!("secret.recaptcha.siteKey")("", true);
  checkDefaultFor!("secret.recaptcha.secretKey")("", true);
  checkDefaultFor!("secret.mtcaptcha.siteKey")("", true);
  checkDefaultFor!("secret.mtcaptcha.privateKey")("", true);

  checkDefaultFor!("profile.detailedLocation")("false", false);

  checkDefaultFor!("access.allowProposingSites")("true", false);
  checkDefaultFor!("access.isMultiProjectMode")("true", false);
  checkDefaultFor!("access.allowManageWithoutTeams")("true", false);

  checkDefaultFor!("locationServices.maskingPrecision")("0", true);

  checkDefaultFor!("integrations.slack.webhook")("", true);
  checkDefaultFor!("integrations.google.client_id")("");
  checkDefaultFor!("integrations.google.client_secret")("", true);
  checkDefaultFor!("integrations.mapbox")("", true);
  checkDefaultFor!("integrations.nominatim")("", true);
  checkDefaultFor!("integrations.matomo.url")("", false);
  checkDefaultFor!("integrations.matomo.siteId")("", false);

  checkDefaultFor!("spaces.domains")(URL(config.serviceUrl).host, false);
}

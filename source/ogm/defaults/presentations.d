/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.defaults.presentations;

import vibe.core.log;
import vibe.data.json;

import crate.base;

import std.file;
import std.path;
import std.string;
import std.datetime;
import std.conv;

import ogm.crates.all;
import ogm.models.all;
import ogm.crates.defaults;
import ogm.defaults.articles;
import ogm.models.modelinfo;

void checkDefaultPresentations(OgmCrates crates) {
  crates.checkDefaultPresentation("welcome");
}

void checkExistence(OgmCrates crates, Presentation presentation) {
  if(crates.presentation.get.where("slug").equal(presentation.slug).and.size > 0) {
    return;
  }

  logInfo("`%s` presentation does not exist. Adding the default.", presentation.slug);

  auto jsonPresentation = presentation.serializeToJson;
  jsonPresentation.remove("_id");
  jsonPresentation["visibility"]["team"] = crates.team.publishersId;

  crates.presentation.addItem(jsonPresentation);
}


void checkDefaultPresentation(OgmCrates crates, string name) {
  auto path = buildPath("defaults", "presentations", name ~ ".json");

  if(!path.exists) {
    path = buildPath("..", "..", "defaults", "presentations", name ~ ".json");
  }

  auto slides = path.readText.parseJsonString;

  auto info = ModelInfo();
  info.createdOn = Clock.currTime;
  info.lastChangeOn = Clock.currTime;

  auto presentation = Presentation();
  presentation.info = info;
  presentation.visibility.isPublic = true;
  presentation.slug = name;

  foreach (size_t row, slide; slides) {
    string slug = slide["slug"].to!string;
    string articleSlug = "presentation--" ~ name ~ "--" ~ slug;

    auto content = Json.emptyObject;
    content["blocks"] = slide["content"];

    auto article = Article(ObjectId.fromString("1"), "presentation--" ~ name ~ "--" ~ slug, slug, content);
    crates.checkArticle(article.serializeToJson);

    auto picturePath = buildPath("defaults", "presentations", name ~ "-" ~ row.to!string ~ ".png");

    if(!picturePath.exists) {
      picturePath = buildPath("..", "..", "defaults", "presentations", name ~ "-" ~ row.to!string ~ ".png");
    }

    auto picture = crates.picture.addDefault("image/png", name ~ "-" ~ row.to!string, picturePath);

    presentation.cols ~= [
      PageCol(0, 0, row.to!int, parseJsonString(`{ "id": "` ~ articleSlug ~ `", "model": "article" }`), "article"),
      PageCol(0, 1, row.to!int, parseJsonString(`{ "id": "` ~ picture._id.toString ~ `", "model": "picture" }`), "picture")
    ];
  }

  crates.checkExistence(presentation);
}

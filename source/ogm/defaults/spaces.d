/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.defaults.spaces;

import vibe.core.log;
import vibe.data.json;
import vibe.inet.url;

import crate.base;

import std.file;
import std.path;
import std.string;
import std.datetime;
import std.conv;
import std.file;

import ogm.crates.all;
import ogm.models.all;
import ogm.crates.defaults;
import ogm.defaults.articles;
import ogm.models.modelinfo;
import ogm.defaults.pages;
import vibe.service.configuration.general;

void checkDefaultSpaces(OgmCrates crates, GeneralConfig config) {
  auto info = ModelInfo();
  info.createdOn = Clock.currTime;
  info.lastChangeOn = Clock.currTime;

  if(crates.space.get.where("visibility.isDefault").equal(true).and.size > 0) {
    return;
  }

  auto spaceJson = `{
    "isTemplate": true,
    "hasDomainValidated": false,
    "domainValidationKey": "",
    "allowDomainChange": true,
    "landingPageId": "",
    "name": "GISCollective Website",
    "domain": "",
    "matomoSiteId": "",
    "matomoUrl": "",
    "info": {
      "changeIndex": 0,
      "originalAuthor": "@unknown",
      "author": "@unknown",
      "createdOn": "2022-03-09T20:03:25.872Z",
      "lastChangeOn": "2022-03-09T20:03:25.872Z"
    },
    "visibility": { "isDefault": true, "isPublic": true },
    "attributions": [{ "url": "https://giscollective.com/", "name": "© GISCollective" }],
    "cols": {},
    "logo": null
  }`.parseJsonString;

  spaceJson["name"] = config.serviceName;
  spaceJson["domain"] = URL(config.serviceUrl).host;
  spaceJson["visibility"]["team"] = crates.team.publishersId;
  spaceJson["info"]["createdOn"] = Clock.currTime.toISOExtString;
  spaceJson["info"]["lastChangeOn"] = Clock.currTime.toISOExtString;

  crates.space.addItem(spaceJson);
}

void setupDefaultSpaces(OgmCrates crates, GeneralConfig config) {
  crates.checkDefaultSpaces(config);
}

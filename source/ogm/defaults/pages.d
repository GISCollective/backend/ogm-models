/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.defaults.pages;

import vibe.core.log;
import vibe.data.json;

import crate.base;

import std.file;
import std.path;
import std.string;
import std.datetime;
import std.conv;
import std.path;
import std.file;
import std.exception;

import ogm.crates.all;
import ogm.models.all;
import ogm.crates.defaults;
import ogm.defaults.articles;
import ogm.models.modelinfo;

void checkDefaultPages(OgmCrates crates) {
  auto info = ModelInfo();
  info.createdOn = Clock.currTime;
  info.lastChangeOn = Clock.currTime;

  auto pagesPath = buildPath("defaults", "pages");
  if(!pagesPath.exists) {
    pagesPath = buildPath("..", "..", "defaults", "pages");
  }

  auto pagesFiles = dirEntries(pagesPath,"*.json", SpanMode.depth);
  auto spaceRange = crates.space.get.where("visibility.isDefault").equal(true).and.exec;

  enforce(!spaceRange.empty, "There is no default space.");

  auto spaceId = spaceRange.front["_id"].to!string;

  foreach (file; pagesFiles) {
    auto jsonPage = file.name.readText.parseJsonString;
    jsonPage["_id"] = "1";
    jsonPage["info"] = `{
      "createdOn": "2021-01-01T00:00:00Z",
      "lastChangeOn": "2021-01-01T00:00:00Z",
      "changeIndex": 0,
      "author": "@system"
    }`.parseJsonString;

    jsonPage["visibility"] = Json.emptyObject;
    jsonPage["visibility"]["team"] = crates.team.publishersId;
    jsonPage["visibility"]["isPublic"] = true;
    jsonPage["space"] = spaceId;

    crates.checkPage(jsonPage);
  }
}

void checkPage(OgmCrates crates, Json page) {
  if(crates.page.get.where("slug").equal(page["slug"].to!string).and.size > 0) {
    return;
  }

  logInfo("`%s` page does not exist. Adding the default.", page["slug"].to!string);

  page.remove("_id");

  crates.page.addItem(page);
}


Json idForSlug(Crate!Page crate, string slug) {
  auto range = crate.get.where("slug").equal(slug).and.exec;

  if(range.empty) {
    return Json();
  }

  return range.front["_id"];
}
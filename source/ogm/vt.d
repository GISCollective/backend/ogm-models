/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.vt;

import vibe.data.json;

import vibe.http.client;
import vibe.core.log;
import vibe.stream.operations;
import vibe.core.file;

import std.string;

version(unittest) {
  import fluent.asserts;
}

@safe
Json getRemoteJson(string strUrl) {
  Json result;

  requestHTTP(strUrl,
    (scope req) {
      req.headers["Connection"] = "close";

      req.headers.remove("Accept-Encoding");
    },
    (scope res) {
      if(res.statusCode == 200) {
        result = res.readJson;
      } else {
        logError("getRemoteJson ERROR: %s", res.bodyReader.readAllUTF8());
      }
    }
  );

  return result;
}

@safe
void downloadFile(string strUrl, string destinationPath) {
  requestHTTP(strUrl,
    (scope req) { req.headers["Connection"] = "close"; },
    (scope res) {
      if(res.statusCode == 200) {
        auto fileStream = openFile(destinationPath, FileMode.createTrunc);
        res.bodyReader.pipe(fileStream);
      } else {
        logError("downloadFile ERROR: %s", res.bodyReader.readAllUTF8());
      }
    }
  );
}

Json downloadMapBoxBaseMap(Json layer) {
  auto token = layer["options"]["token"].to!string;
  auto styleLink = layer["options"]["style"].to!string;

  auto url = styleLink.replace("mapbox://styles", "https://api.mapbox.com/styles/v1") ~ `?access_token=` ~ token;

  auto response = getRemoteJson(url);

  response["sprite"] = response["sprite"].toHttpSprite(token);

  foreach(string key, source; response["sources"]) {
    response["sources"][key] = source.toLocalSource(token);
  }

  return response;
}

string getSpriteUrl(string extension)(Json value) {
  auto pieces = value.to!string.split('?');

  pieces[0] ~= "@2x." ~ extension;

  return pieces.join('?');
}

/// it returns the sprite url when there are no query params
unittest {
  auto sprite = Json("https://openmaptiles.github.io/klokantech-basic-gl-style/sprite");

  sprite.getSpriteUrl!"json".should.equal("https://openmaptiles.github.io/klokantech-basic-gl-style/sprite@2x.json");
}


/// it returns the sprite url when there are query params
unittest {
  auto sprite = Json("https://openmaptiles.github.io/klokantech-basic-gl-style/sprite?param");

  sprite.getSpriteUrl!"json".should.equal("https://openmaptiles.github.io/klokantech-basic-gl-style/sprite@2x.json?param");
}

Json toHttpSprite(Json sprite, string accessToken) {
  auto url = sprite.to!string;

  if(url.startsWith("mapbox://sprites/")) {
    url = url.replace("mapbox://sprites/", "https://api.mapbox.com/styles/v1/") ~ "/sprite";

    if(accessToken) {
      url ~= "?access_token=" ~ accessToken;
    }
  }

  return Json(url);
}

Json toLocalSource(Json source, string accessToken) {
  auto url = source["url"].to!string;

  if(url.startsWith("mapbox://")) {
    url = url.replace("mapbox://", "https://api.mapbox.com/v4/") ~ ".json";

    if(accessToken) {
      url ~= "?access_token=" ~ accessToken;
    }
  }

  source["url"] = url;

  return source;
}
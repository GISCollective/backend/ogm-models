/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.middleware.AuthQueryParam;

import vibe.http.server;

import crate.base;

/// Moves an authorization query param to headers
class AuthQueryParam {
  @any
  void handler(HTTPServerRequest req, HTTPServerResponse res) {
    if("authorization" in req.query && "Authorization" !in req.headers) {
      req.headers["Authorization"] = "Bearer " ~ req.query["authorization"];
    }
  }
}
/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.middleware.creationRightsFlag;

import vibe.http.server;
import vibe.data.json;
import crate.base;
import crate.error;
import ogm.crates.all;
import ogm.http.request;
import std.exception;
import std.exception;

class CreationRightsFlag(string modelName) {
  private {
    OgmCrates crates;
    string flagName;
  }

  this(OgmCrates crates, string flagName) {
    this.crates = crates;
    this.flagName = flagName;
  }

  @create
  creationRule(HTTPServerRequest req) {
    scope request = RequestUserData(req);

    if(request.isAdmin) {
      return;
    }

    auto teamId = req.json[modelName]["visibility"]["team"].to!string;
    auto team = this.crates.team.getItem(teamId).and.exec.front;

    enforce!ForbiddenException(team[flagName].to!bool == true, "You can't create this " ~ modelName ~ " for this team.");
  }

  @put @patch
  validateTeamChange(HTTPServerRequest req) {
    auto request = RequestUserData(req);
    mixin(`auto record = this.crates.` ~ modelName ~ `.getItem(request.itemId).and.exec.front;`);

    enforce!ForbiddenException(record["visibility"]["team"] == req.json[modelName]["visibility"]["team"], "You can't change the team.");
  }
}
/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.middleware.attributes.IconAttributesMapper;

import ogm.crates.all;
import ogm.http.request;
import ogm.icons.TaxonomyTree;

import std.algorithm;
import std.array;

import crate.base;
import crate.http.request;

import vibe.data.json;
import vibe.core.log;
import vibe.http.server;

/// Fill in the icon attributes from the parent icons
class IconAttributesMapper {
  private {
    TaxonomyTree iconsTree;
    OgmCrates crates;
  }

  this(CrateListGetter iconGetter, CrateGetter iconSetGetter) {
    this.iconsTree = new TaxonomyTree(iconGetter, iconSetGetter);
  }

  this(OgmCrates crates, TaxonomyTree iconsTree) {
    this.iconsTree = iconsTree;
    this.crates = crates;
  }

  @mapper
  Json mapper(HTTPServerRequest req, ref Json item) nothrow @trusted {
    try {
      Json newIcon = item.clone();

      scope request = RequestUserData(req);

      if(request.isAdmin) {
        fill(newIcon, ["*"]);
      } else {
        scope session = request.session(crates);
        fill(newIcon, session.all.iconSets);
      }

      return newIcon;
    } catch(Exception e) {
      try logError(e.toString);
      catch(Exception) {}
    }

    return item;
  }

  /// Adds all the attributes form the parent icons
  void fill(ref Json item, string[] visibleIconSets = null) {
    if("parent" !in item || item["parent"] == "") {
      return;
    }

    auto attributes = iconsTree.getIconNode(item["_id"].to!string).attributes;

    if(visibleIconSets.length == 1 && visibleIconSets[0] == "*") {
      item["attributes"] = attributes;
      return;
    }

    item["attributes"] = Json.emptyArray;

    string currentSet = item["iconSet"].to!string;
    foreach(attribute; attributes) {
      string iconSet = attribute["from"]["iconSet"].to!string;

      if(currentSet == iconSet || visibleIconSets.canFind!(a => a == iconSet)) {
        item["attributes"] ~= attribute;
      }
    }
  }
}

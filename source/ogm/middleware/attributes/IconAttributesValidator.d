/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.middleware.attributes.IconAttributesValidator;

import ogm.crates.all;
import ogm.http.request;

import vibe.data.json;
import vibe.http.server;

import std.string;
import std.algorithm;
import std.exception;

import crate.json;
import crate.attributes;
import crate.error;
import ogm.icons.TaxonomyTree;

class IconAttributesValidator {
  private {
    TaxonomyTree iconTree;
    string modelName;
  }

  struct Parameters {
    @describe("If false, then the attributes wil not be validated.")
    @example("true", "Validate the attributes.")
    @example("false", "Do not validate the attributes.")
    bool validation = true;
  }


  ///
  this(TaxonomyTree iconTree, string modelName) {
    this.iconTree = iconTree;
    this.modelName = modelName;
  }

  @create @put @patch
  void checkRequiredAttributes(HTTPServerRequest req, Parameters parameters) {
    if(!parameters.validation) {
      return;
    }
    auto record = req.json[modelName];
    if(!record.exists("icons")) return;
    auto iconIds = record["icons"].byValue.map!(a => a.to!string);

    foreach(id; iconIds) {
      auto attributesDefinition = iconTree.getIconNode(id).attributes;
      auto icon = iconTree.getIcon(id);
      auto name = icon["name"].to!string;
      auto type = record["attributes"].type == Json.Type.object ?
        record["attributes"][name].type :
        Json.Type.undefined;

      switch(type) {
        case Json.Type.array:
          this.validateAttributesList(record["attributes"][name], attributesDefinition, name);
          break;

        case Json.Type.object:
          this.validateAttributes(record["attributes"][name], attributesDefinition, name);
          break;

        default:
          this.validateAttributes(Json(), attributesDefinition, name);
      }
    }
  }

  void validateAttributesList(Json attributes, Json[] attributesDefinition, string iconName) {
    foreach(attributeInstance; attributes) {
      this.validateAttributes(attributeInstance, attributesDefinition, iconName);
    }
  }

  void validateAttributes(Json attributes, Json[] attributesDefinition, string iconName) {
    bool isSet;
    bool hasValue;

    foreach (Json attribute; attributesDefinition) {
      if("isRequired" !in attribute || attribute["isRequired"] == false) {
        continue;
      }

      auto name = attribute["name"].to!string;

      if(attributes.type == Json.Type.object) {
        isSet = attributes.exists(name);
        hasValue = attributes[name].type != Json.Type.undefined &&
          attributes[name].type != Json.Type.null_ &&
          attributes[name].to!string.strip() != "";
      }

      enforce!CrateValidationException(isSet && hasValue,
        "The item can not be added. Required icon attribute " ~ name ~ " is not set for " ~ iconName ~ ".");
    }
  }
}

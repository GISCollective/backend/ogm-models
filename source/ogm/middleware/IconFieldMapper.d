/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.middleware.IconFieldMapper;

import crate.base;
import crate.http.request;
import vibe.data.json;
import ogm.crates.all;
import ogm.middleware.adminrequest;
import ogm.relationFix;
import vibe.http.server;
import gis_collective.hmq.log;

import std.algorithm;
import std.array;
import ogm.markdown;

///
class IconFieldMapper {

  private {
    OgmCrates crates;
  }

  bool keepCanViewFlag;
  bool ignoreIssues;

  ///
  this(OgmCrates crates) {
    this.crates = crates;
  }

  @mapper
  void mapper(HTTPServerRequest req, ref Json item) {
    if("icons" !in item || item["icons"].type != Json.Type.array) {
      item["icons"] = Json.emptyArray;
      return;
    }

    if("icons" in req.query && req.query["icons"] != "") {
      string[] selectedIcons = req.query["icons"].split(",");

      auto icons = item["icons"].byValue.filter!(a => selectedIcons.canFind(a.to!string)).array ~
        item["icons"].byValue.filter!(a => !selectedIcons.canFind(a.to!string)).array;

      item["icons"] = icons.serializeToJson;
    }

    Json[] items;
    items.reserve(item["icons"].length);

    foreach(id; item["icons"]) {
      if(id.type == Json.Type.string && !items.canFind(id)) {
        items ~= id;
      }
    }

    item["icons"] = items.serializeToJson;
  }
}

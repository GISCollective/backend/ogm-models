/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.middleware.sort;

import ogm.crates.all;
import ogm.http.request;

import vibe.http.router;

import crate.base;

class SortMiddleware {
  private {
    OgmCrates crates;
    string sortField;
    int sortOrder;
  }

  ///
  this(OgmCrates crates, string sortField, int sortOrder) {
    this.crates = crates;

    this.sortField = sortField;
    this.sortOrder = sortOrder;
  }

  ///
  @getList
  IQuery get(IQuery selector, HTTPServerRequest req) {
    selector.sort(sortField, sortOrder);

    return selector;
  }
}

/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.middleware.adminrequest;

import std.datetime;
import crate.base;

import vibe.http.router;

import ogm.crates.all;
import ogm.models.user;
import cachetools.cachelru;

/// Mark requests made by administrators
class AdminRequest {
  private {
    Crate!User userModel;
    CacheLRU!(string, bool) cache;
  }

  ///
  this(Crate!User userModel) {
    this.userModel = userModel;

    cache = new CacheLRU!(string, bool);
    cache.size = 2048;      // keep 2048 elements in cache
    cache.ttl = 60.seconds; // set 60 seconds TTL for items in cache
  }

  ///
  bool isAdmin(const string email) {
    auto tmp = cache.get(email);
    bool result;

    if(tmp.isNull) {
      auto selector = userModel.get;
      selector.where("email").equal(email);
      selector.where("scopes").arrayContains("admin");
      result = selector.size > 0;

      cache.put(email, result);
    } else {
      result = tmp.get;
    }

    return result;
  }

  /// Middleware that checks if the token is owned by an admin
  @any
  void any(HTTPServerRequest req, HTTPServerResponse) {
    if("email" !in req.context) {
      return;
    }

    immutable email = req.context["email"].get!string;

    if(isAdmin(email)) {
      req.context["isAdmin"] = true;
    }
  }
}

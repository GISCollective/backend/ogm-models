/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.middleware.event.EventOccurrencesMiddleware;

import crate.base;
import crate.error;
import std.algorithm;
import std.array;
import ogm.calendar;
import ogm.crates.all;
import vibe.data.json;
import vibe.http.router;

class EventOccurrencesMiddleware {
  OgmCrates crates;

  this(OgmCrates crates) {
    this.crates = crates;
  }

  ///
  @create @put @patch
  void addOccurrences(HTTPServerRequest req) {
    this.addOccurrences(req.json["event"]);
  }

  ///
  void addOccurrences(ref Json data) {
    data["allOccurrences"] = Json.emptyArray;
    TimeInterval[] allOccurrences;

    foreach (entry; data["entries"].byValue.map!(a => a.deserializeJson!CalendarEntry)) {
      auto currentEntry = entry;

      if(currentEntry.repetition == CalendarEntryRepetition.NoRepeat) {
        allOccurrences ~= currentEntry.toInterval;
        continue;
      }

      while(currentEntry.isValid) {
        allOccurrences ~= currentEntry.toInterval;
        currentEntry = currentEntry.next;
      }
    }

    allOccurrences.sort!((a, b) => a.begin < b.begin);
    data["allOccurrences"] = allOccurrences.serializeToJson;

    auto now = SysCalendar.instance.now;

    auto futureIntervals = allOccurrences.filter!(a => a.begin >= now);

    data["nextOccurrence"] = null;

    if(!futureIntervals.empty) {
      data["nextOccurrence"] = futureIntervals.front.serializeToJson;
    }
  }
}

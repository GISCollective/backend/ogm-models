/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.middleware.serviceaccess;

import vibe.data.json;
import vibe.http.router;

import crate.base;
import crate.lazydata.base;

import std.conv;
import std.datetime;
import std.algorithm;

import ogm.crates.all;

import crate.collection.memory;

version(unittest) {
  import fluent.asserts;
}

class ServiceAccess {
  private Crate!Preference preference;

  LazyField!bool isPublic;
  Json notFound;

  this(Crate!Preference preference) {
    this.preference = preference;

    this.isPublic = lazyField({
      auto range = this.preference.get.where("name").equal("register.mandatory").and.exec;

      if(range.empty) {
        return false;
      }

      auto item = range.front["value"].to!string;

      return item == "false";
    }, 10.seconds);

    notFound = `{ "errors": [{
      "status": 404,
      "title": "not found",
      "message": "Only authenticated users can access the service."
    }]}`.parseJsonString;
  }

  bool hasEmberSession(HTTPServerRequest req) {
    return "email" !in req.context && "ember_simple_auth-session" in req.cookies;
  }

  /// Middleware that allows only authenticated users if the service is not public
  void checkAccess(HTTPServerRequest req, HTTPServerResponse res) {
    if(req.method == HTTPMethod.OPTIONS) {
      return;
    }

    if(req.requestPath.to!string == "/preferences") {
      return;
    }

    if(req.requestPath.to!string.startsWith("/service/")) {
      return;
    }

    if(this.isPublic.compute) {
      return;
    }

    if("Authorization" in req.headers) {
      return;
    }

    if(hasEmberSession(req)) {
      return;
    }

    res.writeJsonBody(notFound, 404);
  }
}

/// If the `register.mandatory` preference is not set the service shuld be private
unittest {
  Crate!Preference preference = new MemoryCrate!Preference;
  auto serviceAccess = new ServiceAccess(preference);

  serviceAccess.isPublic.compute.should.equal(false);
}

/// If the `register.mandatory` preference is false then the service should be public
unittest {
  Crate!Preference preference = new MemoryCrate!Preference;

  preference.addItem(`{
    "name": "register.mandatory",
    "value": "false"
  }`.parseJsonString);

  auto serviceAccess = new ServiceAccess(preference);

  serviceAccess.isPublic.compute.should.equal(true);
}

/// If the `register.mandatory` preference is true then the service should be public
unittest {
  Crate!Preference preference = new MemoryCrate!Preference;

  preference.addItem(`{
    "name": "register.mandatory",
    "value": "true"
  }`.parseJsonString);

  auto serviceAccess = new ServiceAccess(preference);

  serviceAccess.isPublic.compute.should.equal(false);
}

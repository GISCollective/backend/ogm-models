/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.middleware.SortingMiddleware;

import ogm.http.request;
import crate.base;
import vibe.http.server;
import ogm.crates.all;

class SortingMiddleware {
  struct QueryParams {
    @describe("If is set the records will be sorted by the values in the field.")
    @example("name", "Sort by name.")
    string sortBy;

    @describe("If sortOrder is set, the records will be returned in the requested order.")
    @example("asc", "Return items in ascending order.")
    @example("desc", "Return items in descending order.")
    string sortOrder;
  }

  private {
    OgmCrates crates;
    string defaultField;
    int defaultOrder;
  }

  this(OgmCrates crates, string defaultField = "", int defaultOrder = 1) {
    this.crates = crates;
    this.defaultField = defaultField;
    this.defaultOrder = defaultOrder;
  }

  ///
  @getList
  void applySorting(IQuery selector, QueryParams params) {
    if(params.sortBy == "" && defaultField != "") {
      selector.sort(defaultField, defaultOrder);
    }

    if(params.sortBy != "") {
      selector.sort(params.sortBy, params.sortOrder == "asc" ? 1 : -1);
    }
  }
}
/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.middleware.authsession;

import crate.base;
import crate.auth.usercollection;

import vibeauth.data.user;
import vibeauth.data.token;

import vibe.http.router;
import vibe.data.json;

import ogm.crates.all;
import std.datetime;

import std.algorithm;

/// Create vibe auth sessions from ember-simple-auth cookie
class AuthSession {
  private {
    UserCrateCollection userCollection;
  }

  ///
  this(UserCrateCollection userCollection) {
    this.userCollection = userCollection;
  }

  /// Middleware that checks if the token is owned by an admin
  void handler(HTTPServerRequest req, HTTPServerResponse res) {
    if("ember_simple_auth-session" !in req.cookies || "User-Agent" !in req.headers) {
      return;
    }

    Json data;

    try {
      data = req.cookies["ember_simple_auth-session"].parseJsonString;
    } catch(Exception) {
      return;
    }

    if(data.type != Json.Type.object) {
      return;
    }

    if("authenticated" !in data) {
      return;
    }

    if(data["authenticated"].type != Json.Type.object || "access_token" !in data["authenticated"]) {
      return;
    }

    string bearer = data["authenticated"]["access_token"].to!string;

    vibeauth.data.user.User user;
    try {
      user = userCollection.byToken(bearer);
    } catch(Exception) {
      return;
    }

    res.headers["Access-Control-Allow-Headers"] = "Set-Cookie";

    auto scopes = user.getScopes;
    auto expiration = Clock.currTime + 3600.seconds;

    Token token;

    try {
      token = userCollection[user.email].createToken(expiration, scopes, "webLogin");
    } catch(Exception e) {
      return;
    }

    res.setCookie("auth-token", token.name);
    res.cookies["auth-token"].maxAge = 3600;
    res.cookies["auth-token"].secure = true;
    res.cookies["auth-token"].sameSite = Cookie.SameSite.strict;
    req.cookies.add("auth-token", token.name);
  }
}

/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.middleware.SlugMiddleware;

import crate.error;
import ogm.http.request;
import crate.base;
import vibe.http.server;
import vibe.data.json;
import std.exception;


class SlugMiddleware {

  struct Params {
    @describe("When the slug is used, the team id is mandatory.")
    string team;
  }

  private {
    CrateAccess crate;
  }

  this(CrateAccess crate) {
    this.crate = crate;
  }

  ///
  @patch @replace @getItem
  void replaceSlug(HTTPServerRequest req, Params params) {
    if("id" !in req.params) {
      return;
    }

    if(isObjectId(req.params["id"])) {
      return;
    }

    enforce!CrateValidationException(params.team, "The team must be set for slug requests");

    auto r = crate.get.where("slug").equal(req.params["id"]).and.exec;

    if(r.empty) {
      return;
    }

    req.params["id"] = r.front["_id"].to!string;
    req.context["id"] = r.front["_id"].to!string;
  }
}
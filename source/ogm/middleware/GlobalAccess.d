/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.middleware.GlobalAccess;

import vibe.http.router;
import crate.lazydata.base;
import crate.base;
import crate.error;
import ogm.models.preference;
import ogm.crates.all;
import std.datetime;
import std.exception;
import ogm.http.request;

class GlobalAccessMiddleware {
  LazyField!bool allowManageWithoutTeams;

  private {
    static GlobalAccessMiddleware _instance;

    Crate!Preference preference;
    OgmCrates crates;
  }
  static resetInstance() {
    _instance = null;
  }

  static instance(OgmCrates crates) {
    if(_instance is null) {
      _instance = new GlobalAccessMiddleware(crates);
    }

    return _instance;
  }

  this(OgmCrates crates) {
    this.preference = crates.preference;
    this.crates = crates;

    allowManageWithoutTeams = LazyField!bool({
      auto range = this.preference.get.where("name").equal("access.allowManageWithoutTeams").and.exec;

      if(range.empty) {
        return true;
      }

      auto item = range.front["value"].to!string;

      return item != "false";
    }, 5.seconds);
  }

  @create
  void handler(HTTPServerRequest req) {
    auto request = RequestUserData(req);

    if(allowManageWithoutTeams.compute || request.isAdmin) {
      return;
    }

    auto teams = request.session(crates).all.teams;

    enforce!CrateValidationException(teams.length > 0, "You need to be part of a team to perform this request.");
  }
}

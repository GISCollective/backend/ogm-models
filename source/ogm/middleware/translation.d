/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.middleware.translation;

import vibe.data.json;
import vibe.http.router;

import crate.base;
import crate.error;
import crate.lazydata.base;
import crate.http.request;
import crate.http.queryParams;
import crate.ctfe;

import std.conv;
import std.string;
import std.algorithm;
import std.exception;
import crate.json;

import ogm.crates.all;
import ogm.models.modelinfo;

import crate.collection.memory;

version(unittest) {
  import fluent.asserts;

}

struct Translate {}
auto translate() { return Translate(); }

class ModelTranslationMiddleware(Model) {

  struct Parameters {
    @describe("Specify the preffered model language. If the parameter is missing, the default locale is used.")
    @example("en-us", "Get or send english values.")
    @example("ro-ro", "Get or send romanian values.")
    @example("none", "Force getting the data using the default language.")
    string locale;
  }

  private {
    OgmCrates crates;
    immutable string modelName;
    ModelDescription description;
    string key;
  }

  ///
  this(OgmCrates crates) {
    this.crates = crates;
    this.modelName = Model.stringof;
    this.description = describe!Model;
    this.key = description.singular.toLower;
  }

  FieldTranslation getNewTranslation(HTTPServerRequest req, string locale) {
    FieldTranslation translation;
    translation.fields = extractTranslations(description.fields, req.json[key]);

    if("info" in req.json[key]) {
      translation.info = req.json[key]["info"].deserializeJson!ModelInfo;
    }

    translation.locale = locale;
    translation.modelName = description.singular;

    if("id" in req.params) {
      translation.itemId = req.params["id"];
    }

    return translation;
  }

  void create(HTTPServerRequest req, HTTPServerResponse) {
    auto params = parseQueryParams!Parameters(req.query);
    create(req, params);
  }

  /// Middleware applied for create routes
  @create
  void create(HTTPServerRequest req, Parameters params) {
    if(params.locale == "") {
      return;
    }

    req.context["translation"] = getNewTranslation(req, params.locale).serializeToJsonString;
  }

  /// Middleware applied for patch route
  @patch
  void patch(Parameters params) @safe {
    enforce!CrateNotImplementedException(params.locale == "", "You can't update locale with patch requests");
  }

   /// Delete the translations
  @delete_
  void delete_(HTTPServerRequest req) {
    auto itemId = req.params["id"].to!string;

    scope range = crates.fieldTranslation.get
      .where("itemId").equal(itemId).and
      .where("modelName").equal(description.singular).and
        .withProjection(["_id"])
        .exec;

    foreach(item; range) {
      crates.fieldTranslation.deleteItem(item["_id"].to!string);
    }
  }

  /// Middleware applied for put route
  @replace
  void replace(Parameters params, HTTPServerRequest req) @trusted {
    if(params.locale == "") {
      return;
    }

    Json selectedValue = crateGetters[description.singular](req.params["id"]).exec.front;
    auto itemId = req.params["id"].to!string;

    scope range = crates.fieldTranslation.get
        .where("locale").equal(params.locale).and
        .where("itemId").equal(itemId).and
        .where("modelName").equal(description.singular).and
          .exec;

    if(range.empty) {
      auto translation = getNewTranslation(req, params.locale);
      crates.fieldTranslation.addItem(translation.serializeToJson);
    } else {
      auto translation = range.front;
      translation["fields"] = extractTranslations(description.fields, req.json[key]);
      crates.fieldTranslation.updateItem(translation);
    }

    if(selectedValue.type == Json.Type.object) {
      req.json[key] = selectedValue;
    }
  }

  @mapper
  void mapper(HTTPServerRequest req, ref Json item, Parameters params) @trusted {
    if(params.locale == "" && "Accept-Language" in req.headers) {
      params.locale = extractLocale(req.headers["Accept-Language"]);
    }

    if(params.locale == "" || params.locale == "none") {
      return;
    }

    if("translation" in req.context) {
      Json translation = req.context["translation"].get!string.parseJsonString;
      translation["itemId"] = item["_id"];
      crates.fieldTranslation.addItem(translation);

      req.context.remove("translation");
    }

    scope range = crates.fieldTranslation.get
        .where("locale").equal(params.locale).and
        .where("itemId").equal(item["_id"].to!string).and
        .where("modelName").equal(description.singular).and
          .exec;

    if(range.empty) {
      return;
    }

    item = mixRecursive(item, range.front["fields"]);
  }
}

/// Extract the translation fields from the json data
Json extractTranslations(ModelFields fields, Json model) {
  Json result = Json.emptyObject;

  foreach(field; fields.basic) {
    if(field.name !in model) {
      continue;
    }

    if(field.attributes.canFind("translate()")) {
      Json value = model[field.name];

      if(value.type == Json.Type.array || value.type == Json.Type.object || value.to!string.strip != "") {
        result[field.name] = value;
      }

      continue;
    }
  }

  foreach(field; fields.objects) {
    if(field.name !in model) {
      continue;
    }

    if(field.listType.length > 0 && field.listType[0] == ListType.list) {
      result[field.name] = Json.emptyArray;

      foreach (value; model[field.name]) {
        result[field.name] ~= extractTranslations(field.fields, value);
      }

      if(result[field.name].length == 0) {
        result.remove(field.name);
      }
    }

    if(field.listType.length > 0 && field.listType[0] == ListType.hashmap) {
      result[field.name] = Json.emptyObject;

      foreach (string key, value; model[field.name]) {
        result[field.name][key] = extractTranslations(field.fields, value);
      }

      if(result[field.name].length == 0) {
        result.remove(field.name);
      }
    }
  }

  return result;
}

string extractLocale(string headerValue) {
  if(headerValue.canFind(",") || headerValue.canFind(";")) {
    return "";
  }

  return headerValue;
}
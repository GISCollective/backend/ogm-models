/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.middleware.StaticDbContent;

import ogm.http.request;
import crate.base;
import vibe.http.server;
import vibe.data.json;
import gis_collective.hmq.log;

enum CanGetAll {
  nobody,
  admin,
  anyone
}

class StaticDbContent {

  private {
    bool allowAdminChanges;
    CanGetAll canGetAll;
    string modelName;

    string deleteMessage;
    string createMessage;
    string replaceMessage;
  }

  this(string modelName, bool allowAdminChanges = false, CanGetAll canGetAll = CanGetAll.nobody) {
    this.modelName = modelName;
    this.allowAdminChanges = allowAdminChanges;
    this.canGetAll = canGetAll;

    this.deleteMessage =`{"errors": [{ "description": "You can't delete the '` ~ modelName ~ `'.","status": 403,"title": "Forbidden"}]}`;
    this.createMessage = `{"errors": [{ "description": "You can't create this '` ~ modelName ~ `'.","status": 403,"title": "Forbidden"}]}`;
    this.replaceMessage = `{"errors": [{ "description": "You can't replace the '` ~ modelName ~ `'.","status": 403,"title": "Forbidden"}]}`;

  }

  /// Don't allow if you are not admin
  @patch @replace
  void replace(HTTPServerRequest req, HTTPServerResponse res) {
    if(!RequestUserData(req).isAdmin || !allowAdminChanges) {
      res.writeBody(replaceMessage, 403, "text/json");
    }
  }

  /// Don't allow.
  @getList
  void getList(HTTPServerRequest req, HTTPServerResponse res) {
    bool isAllowed = canGetAll == CanGetAll.anyone;

    if(RequestUserData(req).isAdmin && canGetAll == CanGetAll.admin) {
      isAllowed = true;
    }

    if(!isAllowed) {
      res.writeBody(`{"errors": [{ "description": "You can't get the '` ~ modelName ~ `' list.","status": 403,"title": "Forbidden"}]}`, 403, "text/json");
    }
  }

  /// Don't allow.
  @delete_
  void delete_(HTTPServerRequest req, HTTPServerResponse res) {
    if(!this.allowAdminChanges) {
      res.writeBody(deleteMessage, 403, "text/json");
      return;
    }

    if(!RequestUserData(req).isAdmin) {
      res.writeBody(deleteMessage, 403, "text/json");
    }
  }

  /// Don't allow.
  @create
  void create(HTTPServerRequest req, HTTPServerResponse res) {
    if(!this.allowAdminChanges) {
      res.writeBody(createMessage, 403, "text/json");
      return;
    }

    if(!RequestUserData(req).isAdmin) {
      res.writeBody(createMessage, 403, "text/json");
    }
  }

  @mapper
  void mapper(HTTPServerRequest request, ref Json item) @trusted nothrow {
    try {
      auto userData = RequestUserData(request);

      if(userData.isAdmin && allowAdminChanges) {
        item["canEdit"] = true;
      } else {
        item["canEdit"] = false;
      }
    } catch(Exception e) {
      error(e);
    }
  }
}
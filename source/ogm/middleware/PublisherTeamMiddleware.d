/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.middleware.PublisherTeamMiddleware;

import ogm.http.request;
import crate.base;
import vibe.http.server;
import vibe.data.json;
import gis_collective.hmq.log;

import std.string;

class PublisherTeamMiddleware {

  private {
    string modelName;
    string createMessage;
    CrateAccess teamCrate;
  }

  this(string modelName, CrateAccess teamCrate) {
    this.modelName = modelName;
    this.teamCrate = teamCrate;

    auto plural = modelName.capitalize ~ 's';

    this.createMessage = `{"errors": [{ "description": "` ~ plural ~ ` can be created only for publisher teams.","status": 403,"title": "Forbidden"}]}`;

  }

  @create
  void create(HTTPServerRequest req, HTTPServerResponse res) {
    if(RequestUserData(req).isAdmin) {
      return;
    }

    Json teamId;

    if(req.json[modelName].type == Json.Type.object && req.json[modelName]["visibility"].type == Json.Type.object) {
      teamId = req.json[modelName]["visibility"]["team"];
    }

    if(teamId.type != Json.Type.string) {
      res.writeBody(createMessage, 403, "text/json");
      return;
    }

    auto exists = teamCrate.get
      .where("_id").equal(ObjectId.fromJson(teamId)).and
      .where("isPublisher").equal(true).and
      .size;

    if(!exists) {
      res.writeBody(createMessage, 403, "text/json");
    }
  }
}
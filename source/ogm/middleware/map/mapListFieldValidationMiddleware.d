/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.middleware.map.mapListFieldValidationMiddleware;

import crate.base;
import crate.error;
import crate.json;
import ogm.crates.all;
import vibe.data.json;
import vibe.http.server;

import std.array;
import std.algorithm;
import std.exception;

class MapListFieldValidationMiddleware(string fieldName, string name) {
  private {
    OgmCrates crates;
    CrateAccess crate;
  }

  this(OgmCrates crates, CrateAccess crate) {
    this.crates = crates;
    this.crate = crate;
  }

  bool validateMap(ref Json map) {
    if(!map.exists(fieldName ~ ".list")) {
      return true;
    }

    if(!map.exists("visibility.team")) {
      return true;
    }

    auto team = ObjectId.fromJson(map["visibility"]["team"]);

    if(map[fieldName]["useCustomList"] == false) {
      return true;
    }

    auto ids = map[fieldName]["list"].byValue.map!(a => ObjectId.fromJson(a)).array;

    auto range = crate.get
      .where("_id").containsAny(ids).and
      .where("visibility.isDefault").equal(false).and
      .where("visibility.team").not.equal(team).and
      .exec;

    if(!range.empty) {
      throw new CrateValidationException("You can't use the " ~ name ~ " `" ~ range.front["name"].to!string ~ "` with id `" ~ range.front["_id"].to!string ~ "` because it belongs to another team");
    }

    return true;
  }

  @create @put @patch
  void requestHandler(HTTPServerRequest req) {
    if("map" !in req.json) {
      return;
    }

    validateMap(req.json["map"]);
  }
}

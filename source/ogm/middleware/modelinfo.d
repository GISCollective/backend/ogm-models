/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.middleware.modelinfo;

import vibe.http.router;
import vibe.data.json;

import crate.json;
import crate.base;
import crate.error;
import std.conv;

import ogm.models.modelinfo;
import ogm.http.request;
import ogm.crates.all;

import std.string;
import std.path;

struct ModelInfoParameters {
  @describe("Get all the items created by the user with the provided id.")
  string author;
}

class ModelInfoMiddleware {
  private {
    CrateAccess crate;
    immutable string key;
  }

  this(const string key, CrateAccess crate) {
    this.key = key;
    this.crate = crate;
  }

  ///
  @get
  IQuery get(IQuery selector, ModelInfoParameters parameters) {
    if(parameters.author != "") {
      selector.where("info.originalAuthor").equal(parameters.author);
    }

    return selector;
  }

  /// Middleware applied for create routes
  @create
  void create(HTTPServerRequest req) {
    scope request = RequestUserData(req);

    string author = request.userId;

    if(!request.isAuthenticated && "info" in req.json[key] && "author" in req.json[key]["info"]) {
      author = req.json[key]["info"]["author"].to!string;
    }

    setupModelInfo(req.json[key]);

    req.json[key]["info"]["author"] = author;
    req.json[key]["info"]["originalAuthor"] = author;
  }

  /// Middleware applied for put route
  void updateModelInfo(ModelInfoParameters params, HTTPServerRequest req) @trusted {
    enforce!CrateValidationException(key in req.json, "object type expected to be `" ~ key ~ "`");

    scope request = RequestUserData(req);
    scope storedData = crate.getItem(request.itemId).exec.front;

    bool isChange = false;

    auto storedFlatData = storedData.toFlatJson;
    foreach(key, value; req.json[key].toFlatJson) {
      if(key.indexOf("info.") == 0 || key == "info" ) continue;
      if(key in storedFlatData && storedFlatData[key].type == Json.Type.null_ && value.type == Json.Type.null_) continue;
      if(key in storedFlatData && storedFlatData[key].type == Json.Type.undefined && value.type == Json.Type.undefined) continue;

      if(key !in storedFlatData || storedFlatData[key] != value) {
        isChange = true;
        break;
      }
    }

    if(!isChange) {
      req.json[key]["info"] = storedData["info"];
      return;
    }

    if("info" !in storedData) {
      setupModelInfo(storedData);
    }

    if("info" !in req.json[key]) {
      req.json[key]["info"] = Json.emptyObject;
    }

    updateModelInfoJson(storedData["info"], req.json[key]["info"], true);

    req.json[key]["info"]["author"] = request.userId;
  }

  /// Middleware applied for put route
  @replace @patch
  void replace(ModelInfoParameters params, HTTPServerRequest req) @safe {
    this.updateModelInfo(params, req);
  }
}

/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.middleware.userdata;

import crate.auth.usercollection;
import crate.base;

import ogm.crates.all;

import vibe.http.server;
import vibe.data.json;
import vibe.container.dictionarylist;

import std.algorithm;
import std.array;
import std.datetime;
import std.exception;
import cachetools.cachelru;

/// Add the user maps and teams to the request context
class UserDataMiddleware {
  private {
    OgmCrates crates;
    CacheLRU!(string, string) cache;
  }

  static {
    private UserDataMiddleware _instance;

    UserDataMiddleware instance(ref OgmCrates crates) {
      if(_instance is null) {
        _instance = new UserDataMiddleware(crates);
      }

      return _instance;
    }
  }

  ///
  this(OgmCrates crates) {
    this.crates = crates;

    cache = new CacheLRU!(string, string);
    cache.size = 2048; // keep 2048 elements in cache
    cache.ttl = 60.seconds; // set 60 seconds TTL for items in cache
  }

  /// Middleware that queries the DB for the maps and teams
  @any
  void any(HTTPServerRequest req, HTTPServerResponse res) {
    setupUserData(req);
  }

  void setupUserData(T)(T req) {
    if("email" !in req.context) {
      return;
    }

    immutable email = req.context["email"].get!string;
    auto cachedId = cache.get(email);

    if(cachedId.isNull) {
      auto range = crates.user.get.where("email").equal(email).and.withProjection(["_id"]).exec;
      enforce(!range.empty, "User email not found!");
      cache.put(email, range.front["_id"].to!string);
      cachedId = cache.get(email);
    }

    req.context["user_id"] = cachedId.get;
  }
}

/// The user data middleware should fill the user data to the
/// Request context
unittest {
  import fluent.asserts;
  import ogm.test.fixtures;

  setupTestData();

  void returnContext(HTTPServerRequest req, HTTPServerResponse res) {
    res.statusCode = 200;
    res.writeBody(req.context["email"].get!(string) ~ ":" ~ req.context["user_id"].get!(string));
  }

  auto auth = new Authentication(crates.user, []);
  auto router = new URLRouter;
  auto itemFilter = new UserDataMiddleware(crates);

  router
    .any("*", &auth.oAuth2.permisiveAuth)
    .get("*", &itemFilter.any);

  router
    .any("*", &returnContext)
    .request
    .header("Authorization", "Bearer " ~ bearerToken.name)
    .get("/maps")
    .expectStatusCode(200)
    .end((Response response) => () {
      response.bodyString.should.equal(`owner@gmail.com:000000000000000000000004`);
    });
}

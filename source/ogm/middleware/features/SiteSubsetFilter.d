/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.middleware.features.SiteSubsetFilter;

import ogm.http.request;
import crate.collection.memory;
import crate.error;
import crate.base;
import vibe.http.router;

import std.array;
import std.algorithm;
import std.conv;
import std.math;

///
class SiteSubsetFilter {
  struct Parameters {
    @describe("Get all items from the provided area. The value should be a pair of two lon,lat coordinates, with top-left and bottom-right points.")
    double[] viewbox;
  }

  double mapViewBox(double value) {
    if(value > 179.9) {
      value = 179.9;
    }

    if(value < -179.9) {
      value = -179.9;
    }

    return value;
  }

  /// Filter that for get requests returns only the public and the logged user maps
  /// It denies adding/editing/removing maps if they are not owned
  @get
  IQuery get(IQuery selector, Parameters parameters, HTTPServerRequest request) {
    enforce!CrateValidationException(!request.query.byKeyValue.empty, "You must provide at least one query selector.");

    parameters.viewbox = parameters.viewbox.map!(a => this.mapViewBox(a)).filter!(a => !isNaN(a)).array;

    if(parameters.viewbox.length >= 4) {
      if(parameters.viewbox.length != 4) {
        return new MemoryQuery([]);
      }

      double[][][] polygon = [[
        [parameters.viewbox[2], parameters.viewbox[1]],
        [parameters.viewbox[2], parameters.viewbox[3]],
        [parameters.viewbox[0], parameters.viewbox[3]],
        [parameters.viewbox[0], parameters.viewbox[1]],
        [parameters.viewbox[2], parameters.viewbox[1]]
      ]];

      selector.where("position").insidePolygon(polygon);
    }

    return selector;
  }

  /// Disable model changes
  @update
  IQuery update(IQuery selector, HTTPServerRequest req) {
    throw new CrateValidationException("You can not update the model.");
  }
}

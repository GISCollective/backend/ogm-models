/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.middleware.features.GetFeaturesFilter;

import ogm.middleware.features.FeaturesFilter;
import crate.auth.usercollection;
import crate.base;
import crate.error;
import crate.collection.memory;
import crate.http.request;
import crate.http.queryParams;
import crate.lazydata.base;

import ogm.http.request;
import ogm.crates.all;

import vibe.http.server;
import vibe.data.json;

import std.algorithm;
import std.exception;
import std.datetime;
import std.array;
import std.string;
import std.conv;
import std.math;
import std.range;

import openapi.definitions;

import ogm.selectors.icons;
import ogm.icons.TaxonomyTree;

///
class GetFeaturesFilter : FeaturesFilter {

  struct Parameters {
    @describe("Get all the items that have assigned any of the icons in the list. The value should be a comma sepparated list of icon ids.")
    string[] icons;

    @describe("Get all the items that have icons in the provided category.")
    @example("Nature", "Get all the items that are in the `Nature` category.")
    @example("Sustainable-Living", "Get all the items that are in the `Sustainable-Living` category.")
    string iconCategory;

    @describe("Get all the items that have icons in the provided subcategory.")
    @example("Justice", "Get all the items that are in the `Justice` subcategory.")
    @example("Eco-Information", "Get all the items that are in the `Eco-Information` subcategory.")
    string iconSubcategory;

    @describe("Get all items that have at least one issue.")
    @example("true", "Get all items that have at least one issue.")
    string issues;

    @describe("Get all the items that are inside the provided area. If this parameter is provided, `lon` and `lat` will be ignored.")
    @example("Germany", "Get all the items that were matched to the `Germany` area.")
    string area;

    @describe("Start showing the items ordered by distance from the provided longitude. If you pass `lon` you also need to pass a value for `lat`.")
    @example("13.4063", "Sort items by distance from Berlin.")
    double lon;

    @describe("Start showing the items ordered by distance from the provided latitude. If you pass `lat` you also need to pass a value for `lon`.")
    @example("52.4886", "Sort items by distance from Berlin.")
    double lat;

    @describe("Get all items from the provided area. The value should be a pair of two lon,lat coordinates, with top-left and bottom-right points.")
    double[] viewbox;

    @describe("Define how the icons filter is applied when an icon list is provided. Default value is 'or'.")
    @example("or", "Features matching at least one icon will be returned.")
    @example("and", "Features matching all icons will be returned.")
    string iop;

    @describe("Filter events based on attrributes using the format: key1|value1|key2|value2 ...")
    string attributes;
  }

  ///
  this(UserCrateCollection users, OgmCrates crates, TaxonomyTree icons) {
    super(users, crates, icons);
  }

  string getPositionField(HTTPServerRequest request) {
    if("map" !in request.query) {
      return "position";
    }

    auto userData = RequestUserData(request);
    auto session = userData.session(crates);
    auto isMasked = false;

    if(session.all.maps.canFind(request.query["map"]) || userData.isAdmin) {
      auto mapId = ObjectId.fromString(request.query["map"]);
      isMasked = crates.map.get.where("_id").equal(mapId).and.where("mask.isEnabled").equal(true).and.size > 0;
    }


    return isMasked ? "unmasked" : "position";
  }

  /// Filter that for get requests returns only the public and the logged user maps
  /// It denies adding/editing/removing maps if they are not owned
  IQuery getWrapper(IQuery selector, HTTPServerRequest request) {
    auto params = request.query.parseQueryParams!Parameters;

    return get(selector, params, request);
  }

  /// Filter that for get requests returns only the public and the logged user maps
  /// It denies adding/editing/removing maps if they are not owned
  @get
  IQuery get(IQuery selector, Parameters parameters, HTTPServerRequest request) {
    IconSelector iconSelector;
    ObjectId[] selectedIds;

    if(parameters.viewbox.length >= 4) {
      string positionField = getPositionField(request);

      if(parameters.viewbox.length != 4) {
        return new MemoryQuery([]);
      }

      double[][][] polygon = [[
        [parameters.viewbox[2], parameters.viewbox[1]],
        [parameters.viewbox[2], parameters.viewbox[3]],
        [parameters.viewbox[0], parameters.viewbox[3]],
        [parameters.viewbox[0], parameters.viewbox[1]],
        [parameters.viewbox[2], parameters.viewbox[1]]
      ]];

      selector.where(positionField).insidePolygon(polygon);
    }

    bool hasIcons;
    bool hasAndIconsOperation = parameters.iop == "and" ?true : false;

    if(parameters.icons.length > 0) {
      iconSelector.idList = parameters.icons;
      hasIcons = true;
    }

    if(parameters.iconCategory != "") {
      iconSelector.category = parameters.iconCategory;
      hasIcons = true;
    }

    if(parameters.iconSubcategory != "") {
      iconSelector.subcategory = parameters.iconSubcategory;
      hasIcons = true;
    }

    if(hasIcons && !hasAndIconsOperation) {
      selector.where("icons").anyOf(iconSelector.ids(crates.icon.get));
    }

    if(hasIcons && hasAndIconsOperation) {
      selector.where("icons").containsAll(iconSelector.ids(crates.icon.get));
    }

    if(parameters.area != "") {
      selectedIds = getArea(parameters.area);

      if(selectedIds.length == 0) {
        return this.emptySelector;
      }
    }

    if(!isNaN(parameters.lat) && !isNaN(parameters.lon) && parameters.area == "") {
      string positionField = getPositionField(request);
      selector = selector.where(positionField).near(parameters.lon, parameters.lat, 0).and;
    }

    if(parameters.issues == "true") {
      auto issuesQuery = crates.issue.get.where("status").equal("open").and;

      if(selectedIds.length > 0) {
        issuesQuery = issuesQuery.where("feature").containsAny(selectedIds.map!(a => a.to!string).array).and;
      }

      selectedIds = issuesQuery.and.withProjection(["feature"]).exec.map!(a => ObjectId.fromJson(a["feature"])).array;
    }

    if(selectedIds.length > 0) {
      selector = selector.where("_id").containsAny(selectedIds).and;
    }

    if(parameters.attributes) {
      auto pieces = parameters.attributes.split("|")
        .chunks(2);

      foreach (a; pieces) {
        selector.where("attributes." ~ a[0]).equal(a[1]);
      }
    }

    return selector;
  }
}

/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.middleware.features.FeaturesFilter;

import crate.auth.usercollection;
import crate.base;
import crate.error;
import crate.collection.memory;
import crate.http.request;
import crate.lazydata.base;

import ogm.http.request;
import ogm.crates.all;

import vibe.http.server;
import vibe.data.json;

import std.algorithm;
import std.exception;
import std.datetime;
import std.array;
import std.string;
import std.conv;

import openapi.definitions;

import ogm.icons.TaxonomyTree;
import ogm.selectors.icons;

///
class FeaturesFilter {

  protected {
    UserCrateCollection users;
    OgmCrates crates;
    IQuery emptySelector;

    LazyField!(string[]) publicMaps;
  }

  TaxonomyTree icons;

  ///
  this(UserCrateCollection users, OgmCrates crates, TaxonomyTree icons) {
    this.users = users;
    this.crates = crates;
    this.emptySelector = new MemoryQuery();
    this.icons = icons;

    this.publicMaps = LazyField!(string[])({
      return crates.map
        .get
        .where("visibility.isPublic").equal(true)
        .and
        .exec
        .map!(a => a["_id"].to!string)
        .array;
    }, 10.seconds);
  }

  ///
  void checkContributors(HTTPServerRequest req, ref Json feature) {
    if("maps" !in feature) {
      return;
    }

    auto request = RequestUserData(req);
    auto session = request.session(crates);
    auto maps = feature["maps"][].map!(a => a.to!string).array;

    immutable isAdminRequest = request.isAdmin;
    immutable hasOwnerMaps = session.owner.maps.map!(a => maps.canFind(a)).canFind(true);
    immutable hasLeaderMaps = session.leader.maps.map!(a => maps.canFind(a)).canFind(true);
    immutable hasGuestMaps = session.guest.maps.map!(a => maps.canFind(a)).canFind(true);

    auto userMaps = session.all.maps;
    maps = maps.filter!(a => !userMaps.canFind(a)).array;
    immutable hasOtherPublicMaps = maps.length > 0;

    maps = maps.filter!(a => !publicMaps.compute.canFind(a)).array;

    if(request.isAuthenticated) {
      if(!hasOwnerMaps && !hasLeaderMaps && !isAdminRequest) {
        enforce!CrateValidationException("contributors" !in feature, "The feature can not contain the contributors field.");
      }

      if("contributors" !in feature) {
        feature["contributors"] = [ request.userId ].serializeToJson;
      }
    } else {
      enforce!CrateValidationException("contributors" !in feature, "The feature can not contain the contributors field.");
      feature["contributors"] = [ "@anonymous" ].serializeToJson;
    }

    if((hasOtherPublicMaps || hasGuestMaps) && !isAdminRequest) {
      enforce!CrateValidationException(
        "isPublished" !in feature || feature["isPublished"] == false,
        "The feature can not be published. Set isPublished to false or remove it.");
    }
  }

  ///
  ObjectId[] getCategoryIcons(string category) {
    return crates.icon.get
      .where("category")
      .equal(category)
      .and
      .exec
      .map!(a => ObjectId.fromString(a["_id"].to!string))
      .array;
  }

  ObjectId[] getArea(string name) {
    ObjectId[] result;

    auto query = crates.meta.get
      .where("type").equal("Feature.area").and
      .where("model").equal("Feature").and
      .where("data.names").arrayContains(name.toLower).and.exec;

    if(!query.empty) {
      result = query.map!(a => ObjectId.fromString(a["itemId"].to!string)).array;
    }

    return result;
  }
}

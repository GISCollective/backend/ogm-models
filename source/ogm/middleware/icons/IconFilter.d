/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.middleware.icons.IconFilter;

import ogm.crates.all;
import ogm.models.icon;
import ogm.http.request;
import ogm.selectors.icons;
import ogm.filter.indirectvisibility;
import ogm.middleware.features.GetFeaturesFilter;
import ogm.middleware.features.SiteSubsetFilter;

import vibe.http.router;
import vibe.data.json;

import crate.base;
import crate.error;
import crate.lazydata.base;
import crate.collection.memory;
import crate.http.request;
import crate.http.queryParams;
import crate.collection.EmptyQuery;
import ogm.filter.mapFilter;
import ogm.filter.visibility;

import std.algorithm: map, canFind, filter, startsWith, canFind, setIntersection;
import std.array : array, join;
import std.conv;
import std.datetime;
import std.path;

///
class IconFilter {
  private {
    OgmCrates crates;
    GetFeaturesFilter featuresFilter;
    SiteSubsetFilter sitesSubsetFilter;
    VisibilityFilter!("features", true, true) visibility;
    MapFilter mapFilter;

    LazyField!(string[]) publicIconSets;
    LazyField!(string[]) defaultIconSets;

    IQuery emptySelector;
  }

  struct Parameters {
    @describe("If it's true, only the items that the user can edit will be returned.")
    @example("true", "Get only the items that the user can edit.")
    @example("false", "Get all the items that the user can view.")
    bool edit;

    @describe("If it's true, the result will be the icons grouped by category and sub category, instead of a list.")
    @example("true", "Get only the items that the user can edit.")
    @example("false", "Get all the items that the user can view.")
    bool group;

    @describe("Get all the items that have icons in the provided category.")
    @example("not set", "Get all the items that nave no category.")
    @example("Nature", "Get all the items that are in the `Nature` category.")
    @example("Sustainable-Living", "Get all the items that are in the `Sustainable-Living` category.")
    string category;

    @describe("Get all the items that have icons in the provided subcategory.")
    @example("not set", "Get all the items that nave no subcategory.")
    @example("Justice", "Get all the items that are in the `Justice` subcategory.")
    @example("Eco-Information", "Get all the items that are in the `Eco-Information` subcategory.")
    string subcategory;

    @describe("Id of an `icon set`. If is present, then only the `icons` that belong to the `icon set` wil be returned.")
    string[] iconSet;

    @describe("Get all icons of the sites in the provided viewbox. The value should be a pair of two lon,lat coordinates, with top-left and bottom-right points.")
    double[] viewbox;

    @describe("Used with `viewbox` to query only icons belonging to one map")
    string map;

    @describe("When it set to a map id, it will return only the icons related to a map.")
    string iconSetsMap;

    @describe("When it set to a campaign id it will return only the icons that can be added as optional and mandatory icons.")
    string iconSetsCampaign;
  }

  ///
  this(OgmCrates crates, GetFeaturesFilter featuresFilter, VisibilityFilter!("features", true, true) visibility, MapFilter mapFilter) {
    this.crates = crates;
    this.emptySelector = new MemoryQuery();
    this.mapFilter = mapFilter;

    this.publicIconSets = lazyField({
      return this.crates.iconSet
        .get.where("visibility.isPublic").equal(true).and
        .exec
        .map!(a => a["_id"].to!string)
        .array;
    }, 5.seconds);

    this.defaultIconSets = lazyField({
      return this.crates.iconSet
        .get
        .where("visibility.isPublic").equal(true).and
        .where("visibility.isDefault").equal(true).and
        .exec
        .map!(a => a["_id"].to!string)
        .array;
    }, 5.seconds);

    this.featuresFilter = featuresFilter;
    this.visibility = visibility;
    this.sitesSubsetFilter = sitesSubsetFilter;
  }

  string[] getSiteIcons(HTTPServerRequest req, double[] viewbox, string map) {
    GetFeaturesFilter.Parameters params;
    params.viewbox = viewbox;

    auto visibilityParams = req.query.parseQueryParams!(VisibilityParameters);
    auto mapFilterParams = req.query.parseQueryParams!(MapFilter.Parameters);
    mapFilterParams.map = map;

    auto selector = crates.feature.get;
    selector = this.visibility.get(selector, visibilityParams, req);
    selector = this.featuresFilter.get(selector, params, req);
    selector = this.mapFilter.get(selector, mapFilterParams, req);

    string[] ids;

    foreach(item; selector.withProjection(["icons"]).exec) {
      foreach(id; cast(Json[]) item["icons"]) {
        if(id.type == Json.Type.string && !ids.canFind(id.to!string)) {
          ids ~= id.to!string;
        }
      }
    }

    return ids;
  }

  string[] getMapIconSets(string mapId, string[] ownedMaps, bool isAdmin) {
    auto mapRange = crates.map.getItem(mapId).and.exec;

    if(mapRange.empty) {
      return [];
    }

    auto map = mapRange.front;
    bool isPrivate = "visibility" !in map || map["visibility"]["isPublic"] != true;

    if(!isAdmin && isPrivate && !ownedMaps.canFind(mapId)) {
      return [];
    }

    if("iconSets" !in map || map["iconSets"]["useCustomList"] != true) {
      return defaultIconSets.compute;
    }

    return map["iconSets"]["list"].deserializeJson!(string[]);
  }

  string[] getIconIds(string mapId, string[] ownedMaps, bool isAdmin) {
    string[] result;

    auto range = crates.meta.get
      .where("type").equal("Map.icons").and
      .where("itemId").equal(mapId)
      .and.exec;

    if(!range.empty) {
      result = range.front["data"]["icons"].deserializeJson!(string[]);
    }

    return result;
  }

  /// Filter that for get requests returns only the public and the logged user maps
  /// It denies adding/editing/removing maps if they are not owned
  @get
  IQuery get(IQuery selector, Parameters parameters, HTTPServerRequest req) {
    scope request = RequestUserData(&req.context, req.params);
    scope session = request.session(crates);

    IconSelector iconSelector;
    scope string[] visibleIconSets;

    if(parameters.map) {
      iconSelector.idList = getIconIds(parameters.map, session.all.maps, request.isAdmin);

      if(iconSelector.idList.length == 0 && parameters.iconSet.length == 0) {
        parameters.iconSet = getMapIconSets(parameters.map, session.all.maps, request.isAdmin);
      }

      if(iconSelector.idList.length == 0 &&  parameters.iconSet.length == 0) {
        return EmptyQuery.instance;
      }
    }

    if(parameters.iconSetsMap) {
      parameters.iconSet = getMapIconSets(parameters.iconSetsMap, session.all.maps, request.isAdmin);

      if(parameters.iconSet.length == 0) {
        return this.emptySelector;
      }
    }

    if(parameters.iconSetsCampaign) {
      auto range = crates.campaign.get.where("_id").equal(ObjectId.fromString(parameters.iconSetsCampaign)).and.exec;

      if(range.empty) {
          return this.emptySelector;
      }

      auto campaign = range.front;

      if(campaign["visibility"]["isPublic"] == false && !request.isAdmin && !session.all.campaigns.canFind(parameters.iconSetsCampaign)) {
        return this.emptySelector;
      }

      parameters.iconSet = defaultIconSets.compute;

      if(campaign["map"]["isEnabled"] == true) {
        parameters.iconSet = getMapIconSets(campaign["map"]["map"].to!string, session.all.maps, true);

        if(parameters.iconSet.length == 0) {
          return this.emptySelector;
        }
      }
    }

    if(request.isAdmin) {
      visibleIconSets = parameters.iconSet;
    }

    if(!request.isAdmin) {
      if(parameters.edit) {
        enforce!CrateValidationException(request.userId != "@anonymous", "Can not find your icons.");
        visibleIconSets = session.owner.iconSets;
      } else {
        visibleIconSets = publicIconSets.compute ~ session.all.iconSets;
      }

      if(parameters.iconSet.length > 0) {
        visibleIconSets = visibleIconSets.filter!(a => parameters.iconSet.canFind(a)).array;

        if(visibleIconSets.length == 0) {
          return this.emptySelector;
        }
      }

      if(visibleIconSets.length == 0) {
        return this.emptySelector;
      }
    }

    if(parameters.iconSet.length) {
      visibleIconSets = visibleIconSets.filter!(a => parameters.iconSet.canFind(a)).array;
    }

    iconSelector.category = parameters.category;
    iconSelector.subcategory = parameters.subcategory;
    iconSelector.iconSet = visibleIconSets.join(",");

    if(parameters.viewbox.length == 4) {
      auto siteIcons = this.getSiteIcons(req, parameters.viewbox, parameters.map);

      if(iconSelector.idList.length == 0) {
        iconSelector.idList = siteIcons;
      } else if(siteIcons.length > 0) {
        iconSelector.idList = setIntersection(siteIcons, iconSelector.idList).array;
      }

      if(iconSelector.idList.length == 0) {
        return this.emptySelector;
      }
    }

    iconSelector.apply(selector);

    return selector;
  }

  /// It denies adding/editing/removing maps if the items are not owned
  @put @patch @delete_
  IQuery checkRights(IQuery selector, HTTPServerRequest req) {
    scope request = RequestUserData(&req.context, req.params);
    scope session = request.session(crates);

    auto icon = crates.icon.getItem(request.itemId).exec.front;
    string set = icon["iconSet"].to!string;

    if(request.isAdmin) {
      return selector;
    }

    string actionName = "edit";

    if(req.method == HTTPMethod.DELETE) {
      actionName = "remove";
    }

    if(!session.owner.iconSets.canFind(set) && session.all.iconSets.canFind(set)) {
      auto e = new CrateException("You don't have enough rights to " ~ actionName ~ " `" ~ request.itemId ~ "`.");
      e.title = "Can not " ~ actionName ~ " icon.";
      e.statusCode = 400;
      throw e;
    }

    if(!session.owner.iconSets.canFind(set)) {
      auto e = new CrateException("Icon `" ~ request.itemId ~ "` not found.");
      e.title = "Icon not found.";
      e.statusCode = 404;
      throw e;
    }

    return selector;
  }

  /// Middleware applied for create routes
  @create
  void create(HTTPServerRequest req) {
    scope request = RequestUserData(req);
    scope session = request.session(crates);

    if(request.isAdmin) {
      return;
    }

    string set = req.json["icon"]["iconSet"].to!string;
    if(!session.owner.iconSets.canFind(set) && session.all.iconSets.canFind(set)) {
      auto e = new CrateException("You don't have enough rights to add an icon.");
      e.title = "Can not add a new icon.";
      e.statusCode = 400;
      throw e;
    }

    enforce!CrateNotFoundException(session.owner.iconSets.canFind(set),
      "Can not add an icon for icon set `" ~ set ~ "`.");
  }
}


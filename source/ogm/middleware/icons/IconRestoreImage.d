/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.middleware.icons.IconRestoreImage;

import ogm.crates.all;
import ogm.models.icon;
import ogm.http.request;
import ogm.selectors.icons;
import ogm.filter.indirectvisibility;

import vibe.http.router;
import vibe.data.json;

import crate.base;
import crate.error;
import crate.lazydata.base;
import crate.collection.memory;
import crate.http.request;
import crate.http.queryParams;

import std.algorithm: map, canFind, filter, startsWith, canFind;
import std.array : array, join;
import std.conv;
import std.datetime;
import std.path;

///
class IconRestoreImage {
  private {
    OgmCrates crates;
  }


  ///
  this(OgmCrates crates) {
    this.crates = crates;
  }

  /// It denies adding/editing/removing maps if the items are not owned
  @patch @replace @put
  void checkImageValue(HTTPServerRequest req) {
    scope request = RequestUserData(&req.context, req.params);
    auto icon = crates.icon.getItem(request.itemId).exec.front;

    string value;

    if("image" !in req.json["icon"]) {
      req.json["icon"]["image"] = Json.emptyObject;
    }

    value = req.json["icon"]["image"]["value"].to!string;

    if(!value.startsWith("data:") && req.json["icon"]["image"]["value"] != icon["image"]["value"]) {
      req.json["icon"]["image"]["value"] = icon["image"]["value"];
    }
  }

  /// It denies adding/editing/removing maps if the items are not owned
  @patch @replace @delete_
  IQuery checkRights(IQuery selector, HTTPServerRequest req) {
    scope request = RequestUserData(&req.context, req.params);
    scope session = request.session(crates);

    auto icon = crates.icon.getItem(request.itemId).exec.front;
    string set = icon["iconSet"].to!string;

    if(request.isAdmin) {
      return selector;
    }

    string actionName = "edit";

    if(req.method == HTTPMethod.DELETE) {
      actionName = "remove";
    }

    if(!session.owner.iconSets.canFind(set) && session.all.iconSets.canFind(set)) {
      auto e = new CrateException("You don't have enough rights to " ~ actionName ~ " `" ~ request.itemId ~ "`.");
      e.title = "Can not " ~ actionName ~ " icon.";
      e.statusCode = 400;
      throw e;
    }

    if(!session.owner.iconSets.canFind(set)) {
      auto e = new CrateException("Icon `" ~ request.itemId ~ "` not found.");
      e.title = "Icon not found.";
      e.statusCode = 404;
      throw e;
    }

    return selector;
  }
}

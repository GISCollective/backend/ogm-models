/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.middleware.user.UserDataValidator;

import crate.collection.memory;
import vibe.data.json;
import openapi.definitions : Schema;
import ogm.http.request;
import ogm.models.user;

import std.algorithm;
import std.exception;
import std.array;
import std.string;
import std.conv;

import crate.base;
import crate.error;

import vibe.inet.url;
import vibe.http.router;
import vibe.http.form;
import vibe.data.json;

class UserDataValidator {
  private {
    Crate!User crate;
  }

  this(Crate!User crate) {
    this.crate = crate;
  }

  /// Middleware applied for create routes
  @create
  void create(HTTPServerRequest req) @safe {
    enforce("user" in req.json, "Missing `user` from the request.");
    validateUserFields(req.json["user"]);
  }

  /// Middleware applied for put route
  @put
  void replace(HTTPServerRequest req) @trusted {
    enforce("user" in req.json, "Missing `user` from the request.");

    auto request = RequestUserData(req);

    enforce!CrateValidationException(request.isAdmin || request.userId == request.itemId, "You can't edit other users data.");

    validateUserFields(req.json["user"]);

    auto data = crate.getItem(req.params["id"]).exec.front;

    static foreach(field; ["email", "username"]) {
      enforce!CrateValidationException(req.json["user"][field].type == Json.Type.string,
        "The `" ~ field ~ "` field is missing from the `user` object.");
    }

    req.json["user"]["_id"] = data["_id"];

    foreach(string key, val; data) {
      if(key !in req.json["user"]) {
        req.json["user"][key] = val;
      }
    }
  }

  @patch @create @delete_
  void checkOperations(HTTPServerRequest req, HTTPServerResponse res) @trusted {
    string operation = req.requestPath.head.name.to!string.toLower;

    if(res.headerWritten) {
      return;
    }

    if(operation == "listtokens" ||
       operation == "token" ||
       operation == "revoke" ||
       operation == "downgrade" ||
       operation == "delete" ||
       operation == "remove" ||
       operation == "promote" ||
       operation == "testnotification" ||
       operation == "changepassword") {
      return;
    }

    throw new ForbiddenException("The operation is not supported.");
  }
}

private void validateUserFields(Json data) @trusted {
  static foreach(field; ["isActive", "password", "salt", "scopes", "tokens", "lastActivity"]) {
    enforce!CrateValidationException(data[field].type == Json.Type.undefined, "`" ~ field ~ "` must not be present.");
  }
}

version(unittest) {
  import fluent.asserts;
  import crate.http.router;
  import crate.api.rest.policy;
  import crate.base;
  import ogm.middleware.user.PublicUserMapper;
  import ogm.middleware.user.UserTermFilter;

  MemoryCrate!User userCrate;

  auto getTestRoute() {
    auto router = new URLRouter();
    userCrate = new MemoryCrate!User;
    auto mapper = new PublicUserMapper;
    auto userValidator = new UserDataValidator(userCrate);

    router.crateSetup.add(userCrate, userValidator, mapper, new UserTermFilter);

    return router;
  }

  auto userJson = `{
      "_id": "000000000000000000000001",
      "email": "test2@asd.asd",
      "name": "test",
      "username": "test_user",
      "isActive": true,
      "password": "password",
      "salt": "salt",
      "lastActivity": 2000,
      "scopes": ["scopes"],
      "tokens": [{ "name": "token2", "expire": "2100-01-01T00:00:00", "type": "Bearer", "scopes": [] }],
    }`;
}

@("it should hide the sensible fields on GET")
unittest {
  auto router = getTestRoute;
  userCrate.addItem(userJson.parseJsonString);

  request(router)
    .get("/users")
      .expectStatusCode(200)
      .end((Response response) => () {
        response.bodyJson.keys.array.should.equal(["users"]);
        response.bodyJson["users"].length.should.be.greaterThan(0);

        auto user = response.bodyJson["users"][0];
        user.keys.should.contain(["_id", "email", "username", "isActive"]);
        user.keys.should.not.contain(["password", "salt", "scopes", "tokens"]);
        user["_id"].to!string.should.equal("000000000000000000000001");
        user["email"].to!string.should.equal("test2@asd.asd");
        user["username"].to!string.should.equal("test_user");
      });

  request(router)
    .get("/users/000000000000000000000001")
      .expectStatusCode(200)
      .end((Response response) => () {
        auto user = response.bodyJson["user"];
        user.keys.should.contain(["_id", "email", "username", "isActive"]);
        user.keys.should.not.contain(["password", "salt", "scopes", "tokens"]);

        user["_id"].to!string.should.equal("000000000000000000000001");
        user["email"].to!string.should.equal("test2@asd.asd");
        user["username"].to!string.should.equal("test_user");
      });
}

/// it should not accept hidden fields on POST
unittest {
  auto router = getTestRoute;
  auto data = Json.emptyObject;
  data["user"] = userJson.parseJsonString;

  request(router)
    .post("/users")
      .send(data)
      .expectStatusCode(400)
      .end((Response response) => () {
        response.bodyJson.keys.should.equal(["errors"]);
        userCrate.get.exec.empty.should.equal(true);
      });
}

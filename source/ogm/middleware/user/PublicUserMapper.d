/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.middleware.user.PublicUserMapper;

import ogm.models.user;
import vibe.data.json;
import openapi.definitions : Schema;
import crate.ctfe;
import vibe.http.server;
import ogm.http.request;

import std.algorithm;

version(unittest) {
  import fluent.asserts;
}

/// Hides all the user private data
class PublicUserMapper {
  void mapper(ref Schema schema) {
    foreach(key, val; schema.properties) {
      if(!canFind(["_id", "email", "lastActivity", "username", "salt", "scopes"], key)) {
        schema.properties.remove(key);
      }
    }
  }

  @mapper
  Json mapper(Json item, HTTPServerRequest req) @trusted nothrow {
    Json newItem;

    try {
      auto request = RequestUserData(req);
      newItem = Json.emptyObject;

      static foreach(field; ["_id", "email", "username", "isActive"]) {
        newItem[field] = item[field];
      }

      if(request.isAdmin) {
        static foreach(field; ["lastActivity"]) {
          newItem[field] = item[field];
        }
      }

    } catch (Exception e) {}

    return newItem;
  }
}

/// Check the schema update
// unittest {
//   import crate.openapi;

//   enum definition = describe!User;
//   auto middleware = new PublicUserMapper();
//   auto schema = definition.fields.toOpenApiSchema;

//   middleware.mapper(schema);

//   schema.serializeToJson.should.equal(`{
//         "type": "object",
//         "properties": {
//                 "email": {
//                         "type": "string"
//                 },
//                 "username": {
//                         "type": "string"
//                 },
//                 "lastActivity": {
//                         "type": "integer",
//                         "format": "int64"
//                 },
//                 "scopes": {
//                         "type": "array",
//                         "items": {
//                                 "type": "string"
//                         }
//                 },
//                 "salt": {
//                         "type": "string"
//                 },
//                 "_id": {
//                         "type": "string"
//                 }
//         },
//         "required": [
//                 "username",
//                 "email",
//                 "password",
//                 "salt",
//                 "isActive",
//                 "scopes",
//                 "tokens"
//         ]
//   }`.parseJsonString);
// }
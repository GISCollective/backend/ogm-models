/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.middleware.user.UserTermFilter;

import vibeauth.data.usermodel;
import vibe.data.json;
import openapi.definitions : Schema;

import crate.base;

/// Filter designed to work with the getList operation. It will filter all the users
/// that have the "term" parameter in the email
class UserTermFilter {

  struct Parameters {
    @describe("Receive only users that have the provided `term` in their email.")
    string term;
  }

  /// Call the "like" method on the crate selector
  @getList
  IQuery any(IQuery selector, Parameters parameters) @safe {
    if(parameters.term != "") {
      selector.or
        .where("email").like(parameters.term).or
        .where("username").like(parameters.term).or
        .where("firstName").like(parameters.term).or
        .where("lastName").like(parameters.term);
    }

    selector.sort("lastActivity", -1);

    return selector;
  }
}

version(unittest) {
  import vibe.http.router;
  import ogm.middleware.user.PublicUserMapper;
  import ogm.middleware.user.UserDataValidator;
  import crate.collection.memory;
  import ogm.models.user;
  import crate.http.router;
  import fluentasserts.vibe.request;
  import fluent.asserts;

  auto getTestRoute() {
    auto router = new URLRouter();
    userCrate = new MemoryCrate!User;
    auto mapper = new PublicUserMapper;
    auto userValidator = new UserDataValidator(userCrate);

    router.crateSetup.add(userCrate, userValidator, mapper, new UserTermFilter);

    return router;
  }
}

/// it should search users email by a term
unittest {
  auto router = getTestRoute;
  userCrate.addItem(userJson.parseJsonString);

  request(router)
    .get("/users?term=john")
      .expectStatusCode(200)
      .end((Response response) => () {
        response.bodyJson["users"].length.should.equal(0);
      });

  request(router)
    .get("/users?term=test")
      .expectStatusCode(200)
      .end((Response response) => () {
        response.bodyJson["users"].length.should.equal(1);

        auto user = response.bodyJson["users"][0];
        user.keys.should.contain(["_id", "email", "username", "isActive"]);
        user.keys.should.not.contain(["password", "salt", "scopes", "tokens"]);
        user["_id"].to!string.should.equal("000000000000000000000001");
        user["email"].to!string.should.equal("test2@asd.asd");
        user["username"].to!string.should.equal("test_user");
      });
}

/// it should search users name by a term
unittest {
  auto router = getTestRoute;
  userCrate.addItem(userJson.parseJsonString);

  request(router)
    .get("/users?term=test_user")
      .expectStatusCode(200)
      .end((Response response) => () {
        response.bodyJson["users"].length.should.equal(1);

        auto user = response.bodyJson["users"][0];
        user.keys.should.contain(["_id", "email", "username", "isActive"]);
        user.keys.should.not.contain(["password", "salt", "scopes", "tokens"]);
        user["_id"].to!string.should.equal("000000000000000000000001");
        user["email"].to!string.should.equal("test2@asd.asd");
        user["username"].to!string.should.equal("test_user");
      });
}

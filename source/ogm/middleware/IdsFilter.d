/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.middleware.IdsFilter;

import ogm.crates.all;

import crate.collection.memory;
import crate.base;
import crate.error;

import vibe.data.json;

import std.algorithm;
import std.array;

class IdsFilter {
  struct Parameters {
    @describe("A list of ids sepparated by comma")
    string[] ids;
  }

  private {
    OgmCrates crates;
  }

  this(OgmCrates crates) {
    this.crates = crates;
  }

  @get
  IQuery get(IQuery selector, Parameters params) {
    if(params.ids.length == 0) {
      return selector;
    }

    auto ids = params.ids.map!(a => ObjectId.fromString(a)).array;

    auto results = selector.where("_id").anyOf(ids).and.exec.array;
    Json[] items;

    foreach (id; params.ids) {
      auto range = results.find!(a => a["_id"] == id);

      if(!range.empty) {
        items ~= range.front;
      }
    }

    return new MemoryQuery(items);
  }
}
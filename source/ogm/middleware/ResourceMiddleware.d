/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.middleware.ResourceMiddleware;

import ogm.crates.all;
import ogm.http.request;

import vibe.http.router;
import vibe.data.json;
import std.algorithm;
import std.array;

import crate.base;
import crate.error;
import gis_collective.hmq.log;

interface IResourceMiddleware {
  void replace(HTTPServerRequest req);
  void delete_(HTTPServerRequest req);
  void mapper(ref Json item) @trusted nothrow;
}

class ResourceMiddleware(string key, string listKey) : IResourceMiddleware {
  private {
    CrateAccess itemCrate;
    CrateAccess resourceCrate;
  }

  ///
  this(CrateAccess itemCrate, CrateAccess resourceCrate) {
    this.itemCrate = itemCrate;
    this.resourceCrate = resourceCrate;
  }

  ///
  @patch @replace
  void replace(HTTPServerRequest req) {
    if(listKey !in req.json[key]) {
      return;
    }

    auto item = itemCrate.getItem(req.params["id"]).exec.front;

    if(item[listKey].type != Json.Type.array) {
      item[listKey] = Json.emptyArray;
    }

    if(req.json[key][listKey].type != Json.Type.array) {
      req.json[key][listKey] = Json.emptyArray;
    }

    Json[] resourceList = cast(Json[]) req.json[key][listKey];

    foreach(id; item[listKey]) {
      if(!resourceList.canFind(id)) {
        this.removeResource(id.to!string);
      }
    }
  }

  ///
  @delete_
  void delete_(HTTPServerRequest req) {
    auto item = itemCrate.getItem(req.params["id"]).exec.front;

    if(listKey !in item || item[listKey].type != Json.Type.array) {
      return;
    }

    foreach(id; item[listKey]) {
      this.removeResource(id.to!string);
    }
  }

  @mapper
  void mapper(ref Json result) @trusted nothrow {
    try {
      ObjectId[] idList;

      if(result[listKey].type != Json.Type.array) {
        result[listKey] = Json.emptyArray;
      }

      foreach(id; result[listKey]) {
        if(id.type != Json.Type.string) {
          continue;
        }

        try idList ~= ObjectId.fromString(id.to!string);
        catch(Exception err) {
          error(err);
        }
      }

      if(idList.length == 0) {
        result[listKey] = Json.emptyArray;
        return;
      }

      Json[] validResources = resourceCrate.get
        .where("_id").containsAny(idList).and
        .withProjection(["_id"])
        .exec
        .map!(a => a["_id"])
        .array;

      if(validResources.length != idList.length) {
        try result[listKey] = validResources.serializeToJson;
        catch(Exception err) {
          error(err);
        }
      }
    } catch(Exception err) {
      error(err);
    }
  }

  static if(listKey == "pictures") {
    void removeResource(string id) {
      try {
        if(id == "null") {
          return;
        }

        auto containingItems = itemCrate.get.where("pictures").arrayContains(ObjectId.fromString(id)).and.size;

        if(containingItems > 1) {
          return;
        }

        resourceCrate.removePicture(id);
      } catch(CrateNotFoundException e) {
        error(e);
      }
    }
  } else {
    void removeResource(string id) {
      try {
        if(id == "null") {
          return;
        }

        auto containingItems = itemCrate.get.where(listKey).arrayContains(ObjectId.fromString(id)).and.size;

        if(containingItems > 1) {
          return;
        }

        resourceCrate.deleteItem(id);
      } catch(CrateNotFoundException e) {
        error(e);
      }
    }
  }
}

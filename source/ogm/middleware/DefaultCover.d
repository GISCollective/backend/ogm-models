/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.middleware.DefaultCover;

import ogm.crates.all;
import crate.lazydata.base;
import vibe.http.server;
import vibe.data.json;

class DefaultCoverMiddleware {

  private {
    LazyField!string defaultCover;
    string modelName;
  }

  ///
  this(OgmCrates crates, string modelName) {
    this.modelName = modelName;
    this.defaultCover = LazyField!string({
      return crates.picture.get
        .where("name")
        .equal("default")
        .and.exec
        .front["_id"]
        .to!string;
    });
  }

  /// Middleware applied for create routes
  @create
  void create(HTTPServerRequest req) {
    if("cover" !in req.json[modelName] ||
      req.json[modelName]["cover"].type == Json.Type.null_ ||
      req.json[modelName]["cover"].type == Json.Type.undefined) {
        req.json[modelName]["cover"] = defaultCover.compute;
    }
  }
}
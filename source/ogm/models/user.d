/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.models.user;

import crate.base;
import crate.attributes;
import crate.lazydata.base;
import std.datetime;

import vibeauth.data.token;
import vibe.data.json;

alias LazyUser = Lazy!User;

struct User {
  ObjectId _id;

  @optional {
    /// The users salutation eg. mr/ms or unset
    string salutation;

    /// The users title eg. dr
    string title;

    /// The users first name
    string firstName;

    /// The users last name
    string lastName;
  }

  ///
  string username;

  /// The users email
  string email;

  /// The password hash
  @hidden string password;

  /// String concatenated with the pasword before hashing
  @hidden string salt;

  /// Flag used to determine if the user can perform any actions
  bool isActive;

  ///
  @optional SysTime createdAt;

  /// The timestamp of the users last activity
  @optional ulong lastActivity;

  /// Scopes that the user has access to
  @hidden string[] scopes;

  /// A list of active tokens
  @hidden Token[] tokens;
}

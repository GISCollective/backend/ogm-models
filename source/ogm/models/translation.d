/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.models.translation;

import crate.base;
import crate.error;
import crate.resource.file;
import crate.resource.gridfs;
import crate.resource.migration;
import crate.lazydata.base;
import crate.mime;

import vibe.data.json;
import std.datetime;
import std.conv;
import std.string;
import std.range;
import std.uuid;
import std.path;
import std.file;
import std.exception;
import std.traits;
import std.stdio : File;

version(unittest) {
  import fluent.asserts;
}

alias LazyTranslation = Lazy!Translation;

/// Settings used for translation files
struct TranslationFileSettings {
  static {
    /// The base url to the translation model eg http://ogm.com/files/
    string baseUrl;

    @optional {
      IGridFsCollection files;
      IGridFsCollection chunks;

      size_t chunkSize = 255 * 1024;
    }
  }
}

alias TranslationFile = GridFsResource!TranslationFileSettings;

struct Translation {
  ObjectId _id;

  string name;
  string locale;

  @optional {
    TranslationFile file;
    string[string] customTranslations;
  }
}

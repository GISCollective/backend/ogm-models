/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.models.picture;

import std.file;
import std.path;
import std.algorithm;
import std.array;
import std.conv;
import std.string;
import std.uuid;
import std.exception;
import std.process;
import std.datetime;
import std.functional;

import crate.error;
import crate.base;
import crate.lazydata.base;
import crate.resource.base;
import crate.resource.file;
import crate.resource.gridfs;
import crate.resource.migration;
import crate.resource.image;
import crate.mime;

import vibe.data.json;
import vibe.core.log;
import vibe.core.core;

version(unittest) {
  import fluent.asserts;
}

alias LazyPicture = Lazy!Picture;

/// Settings used for picture files
struct PictureFileSettings {
  static {
    ///
    string path = "pictures/";

    /// The base url to the pictures model eg http://ogm.com/files/
    string baseUrl;

    /// The picture used by default by teams, maps, sites and icon sets
    string defaultPicture;

    /// The picture used by default as an app cover
    string defaultCover;

    /// The GISCollective logo
    string defaultLogo;

    @optional {
      IGridFsCollection files;
      IGridFsCollection chunks;

      size_t chunkSize = 255 * 1024;
    }
  }
}

alias PictureFileStorage = GridFsResource!PictureFileSettings;
alias PictureFile = CrateImageCollectionResource!(PictureFileStorage, PictureSizes);
alias AppIconFile = CrateImageCollectionResource!(PictureFileStorage, IconSizes);
alias AppCoverFile = CrateImageCollectionResource!(PictureFileStorage, CoverSizes);

enum PictureSizes = [
  ImageSettings("xs", "100", "100"),
  ImageSettings("sm", "400", "400"),
  ImageSettings("md", "800", "800"),
  ImageSettings("lg", "1600", "1600"),
  ImageSettings("png-512", "512", "512", "png")
];

enum IconSizes = [
  ImageSettings("x-16",  "16", "16", "png"),
  ImageSettings("x-32",  "32", "32", "png"),
  ImageSettings("x-64",  "64", "64", "png"),
  ImageSettings("x-128", "128", "128", "png"),
  ImageSettings("x-144", "144", "144", "png"),
  ImageSettings("x-152", "152", "152", "png"),
  ImageSettings("x-192", "192", "192", "png"),
  ImageSettings("x-512", "512", "512", "png")
];

enum CoverSizes = [
  ImageSettings("x-640",  "640", "1136"),
  ImageSettings("x-750",  "750", "1294"),
  ImageSettings("x-1125", "1125", "2436"),
  ImageSettings("x-1242", "1242", "2148"),
  ImageSettings("x-1668", "1668", "2224"),
  ImageSettings("x-1536", "1536", "2048"),
  ImageSettings("x-2048", "2048", "2732")
];

/// Represents a picture file.
/// it is saved in the owners file
struct Picture {
  /// The item id
  ObjectId _id;

  /// The resource name
  string name;

  /// The owner name. Usually is the user id.
  @optional string owner;

  /// The picture resource
  PictureFile picture;

  /// Picture metadata
  @optional PictureMeta meta;

  /// Picture hash
  @optional string hash;

  /// The url from where the file was downloaded
  @optional string sourceUrl;

  /// Rotate the picture
  void rotate() {
    string format = toExtension(this.picture.contentType)[1..$].toUpper;
    picture.read(imageAction(picture.inputStream, format, "-rotate 270"));
  }

  /// Create a picture from json
  static Picture fromJson(Json data) @safe {
    Picture obj;

    obj._id = ObjectId.fromJson(data["_id"]);
    obj.name = data["name"].to!string.cleanFileName;
    obj.owner = data["owner"].to!string.cleanFileName;

    if(data["meta"].type == Json.Type.object) {
      obj.meta = data["meta"].deserializeJson!(typeof(Picture.meta));
    }

    if(obj.name == "null") {
      obj.name = randomUUID.to!string.replace("-", "");
    }

    enforce!CrateValidationException(
      data["picture"].type == Json.Type.string || data["picture"].type == Json.Type.object,
      "Invalid picture format provided.");

    auto pictureString = data["picture"].to!string;

    if(pictureString.startsWith("data:")) {
      enum dataLength = "data:".length;
      auto contentStart = pictureString.indexOf(";base64,");
      const string mime = pictureString[dataLength..contentStart];

      while(buildPath(PictureFileSettings.path, obj.owner, obj.name ~ mime.toExtension).exists) {
        obj.name = randomUUID.to!string.replace("-", "");
      }

      obj.picture = PictureFile.fromString(buildPath(obj.owner, obj.name, pictureString));
    } else {
      obj.picture = PictureFile.fromString(pictureString);
    }

    return obj;
  }

  /// Convert the picture to json
  Json toJson() @safe const {
    Json obj = Json.emptyObject;

    obj["_id"] = _id.toString;
    obj["name"] = name;
    obj["owner"] = owner;
    obj["meta"] = meta.serializeToJson;

    if(picture !is null) {
      obj["picture"] = picture.toString;
    }

    return obj;
  }
}

struct PictureMeta {
  @optional:
    string attributions;
    string renderMode;
    Json link;
    Json data;
    size_t width;
    size_t height;
    string format;
    string mime;
    string[string] properties;
    bool disableOptimization;
    string disableOptimizationReason;
}

Json clone(CrateAccess pictureCrate, string pictureId) {
  auto pictureRange = pictureCrate.get.where("_id").equal(ObjectId.fromString(pictureId)).and.exec;

  if(pictureRange.empty) {
    return Json();
  }

  auto jsonPicture = pictureRange.front;
  auto picture = Lazy!Picture.fromJson(jsonPicture, (&itemResolver).toDelegate).toType;

  auto file = picture.picture;
  picture.picture = new PictureFile();
  picture.picture.read(file.inputStream);

  jsonPicture = Lazy!Picture.fromModel(picture, (&itemResolver).toDelegate).toJson;
  jsonPicture.remove("_id");

  jsonPicture = pictureCrate.addItem(jsonPicture);

  return jsonPicture;
}

void removePicture(CrateAccess pictureCrate, string pictureId) {
  if(pictureId == "null") {
    return;
  }

  auto pictureRange = pictureCrate.get.where("_id").equal(ObjectId.fromString(pictureId)).and.exec;

  if(pictureRange.empty) {
    return;
  }

  auto jsonPicture = pictureRange.front;
  auto picture = Lazy!Picture.fromJson(jsonPicture).toType;

  if(picture.owner == "@system") {
    return;
  }

  try {
    picture.picture.remove();
  } catch(Exception e) {
    logError("Can't delete the picture resource. " ~ e.message);
  }

  pictureCrate.deleteItem(pictureId);
}

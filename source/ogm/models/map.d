/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.models.map;

import std.datetime;

import crate.http.request;
import crate.base;
import geo.json;
import crate.lazydata.base;

import ogm.models.basemap;

import ogm.models.team;
import ogm.models.picture;
import ogm.models.iconset;
import ogm.models.meta;
import ogm.models.modelinfo;
import ogm.models.visibility;

import ogm.http.request;
import ogm.http.request;

import vibe.data.json;
import vibe.http.server;

alias LazyMap = Lazy!Map;

///
@("swift import: GEOSwift")
struct Map {
  ///
  ObjectId _id;

  /// The map name
  string name;

  /// The map description
  @("ts type: ArticleBody")
  @("swift type: ArticleBody")
  @(`swift default: ArticleBody()`)
  Json description;

  /// Who can access the map
  Visibility visibility;

  /// The map area
  @("ts type: GeoJsonGeometry")
  @("swift type: Geometry")
  @("kotlin type: JsonObject")
  @(`kotlin default: JsonObject(emptyMap())`)
  @optional Polygon area;

  /// An image representative for the map rendered
  /// as a landscape image
  Picture cover;

  /// The icon set allowed to be used with the map
  IconSets iconSets;

  @optional {
    ///
    BaseMaps baseMaps;

    ///
    string tagLine;

    /// When the project begun
    SysTime startDate;

    /// When the project ended
    SysTime endDate;

    ///
    ModelInfo info;

    /// An alternative image representative for the map
    /// rendered as a square
    Picture squareCover;

    /// Mask applied to features belonging to this map
    Mask mask;

    /// Features belonging to this map are used to index other sites
    bool isIndex;

    ///
    bool hideOnMainMap;

    /// When this option is enabled the ui will not provide non member users
    /// links to download map files
    bool showPublicDownloadLinks;

    /// License attached to all sites attached to the map
    License license;

    /// Shows unpublished sites as pending
    bool addFeaturesAsPending;

    ///
    Cluster cluster = Cluster();

    ///
    IconSprite sprites;
  }
}

struct Cluster {
  ClusterMode mode = ClusterMode.hexagons;

  @optional {
    string map;
  }
}

enum ClusterMode {
  none = 0,
  hexagons = 1,
  map = 2
}

struct Mask {
  bool isEnabled;
  string map;
}

///
struct BaseMaps {
  ///
  bool useCustomList;

  ///
  BaseMap[] list;
}


///
struct IconSets {
  ///
  bool useCustomList;

  ///
  IconSet[] list;
}

///
struct License {
  @optional {
    string name;
    string url;
  }
}

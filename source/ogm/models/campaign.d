/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.models.campaign;

import std.datetime;

import crate.http.request;
import crate.base;
import geo.json;
import crate.lazydata.base;

import ogm.models.basemap;

import ogm.models.team;
import ogm.models.map;
import ogm.models.picture;
import ogm.models.icon;
import ogm.models.meta;
import ogm.models.modelinfo;
import ogm.models.visibility;

import ogm.http.request;
import ogm.http.request;

import vibe.data.json;
import vibe.http.server;

alias LazyCampaign = Lazy!Campaign;

///
struct Campaign {
  ///
  ObjectId _id;

  /// The campaign name
  string name;

  /// The campaign article
  @("ts type: ArticleBody")
  @("swift type: ArticleBody")
  Json article;

  /// Who can access the campaign
  Visibility visibility;

  ///
  ModelInfo info;

  /// An image representative for the map rendered
  /// as a landscape image
  Picture cover;

  ///
  Icon[] icons;

  @optional {
    ///
    Icon[] optionalIcons;

    /// On which map will the answers be added
    OptionalMap map;

    /// When the project begun
    SysTime startDate;

    /// When the project ended
    SysTime endDate;

    CampaignOptions options;

    Question[] questions;

    Question[] contributorQuestions;
  }
}

struct CampaignOptions {
  string featureNamePrefix;

  @optional {
    bool showNameQuestion;
    string nameLabel;

    bool showDescriptionQuestion;
    string descriptionLabel;

    string iconsLabel;
  }

  bool registrationMandatory;

  @optional:
    CampaignLabelQuestion name;
    CampaignLabelQuestion description;
    CampaignLabelQuestion icons;
    CampaignLocationQuestion location;
    CampaignPicturesQuestion pictures;
}

struct CampaignLabelQuestion {
  @optional:
    bool customQuestion = false;
    string label;
}

class CampaignLocationQuestion {
  @optional:
    bool customQuestion = false;
    string label;
    bool allowGps = true;
    bool allowManual = true;
    bool allowAddress = true;
    bool allowExistingFeature = false;
}

struct CampaignPicturesQuestion {
  @optional:
    bool customQuestion = false;
    string label;
    bool isMandatory = false;
}

struct OptionalMap {
  bool isEnabled;
  string map;
}

struct Question {
  string question;
  string help;
  string type;
  string name;
  bool isRequired;
  bool isVisible;

  Json options;
}
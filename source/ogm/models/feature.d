/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.models.feature;

import ogm.models.map;
import ogm.models.picture;
import ogm.models.sound;
import ogm.models.icon;
import ogm.models.modelinfo;
import ogm.models.source;

import geo.json;

import crate.base;
import crate.lazydata.base;
import crate.resource.file;
import crate.attributes;

import vibe.data.json;

import std.variant;
import std.datetime;
import ogm.models.visibility;

alias LazyFeature = Lazy!Feature;

@("swift import: GEOSwift")
struct Feature {
  ObjectId _id;

  Map[] maps;
  string name;

  @("ts type: ArticleBody")
  @("swift type: ArticleBody")
  @(`swift default: ArticleBody()`)
  @("kotlin type: ArticleBody")
  @(`kotlin default: ArticleBody()`)
  Json description;

  @("ts type: GeoJsonGeometry")
  @("swift type: Geometry")
  @(`swift default: try! Geometry(wkt: "POINT (0 0)")`)
  @("kotlin type: JsonObject")
  @(`kotlin default: JsonObject(emptyMap())`)
  GeoJsonGeometry position;

  @optional {
    ModelInfo info;
    Picture[] pictures;
    Sound[] sounds;
    Icon[] icons;
    VisibilityEnum visibility;
    string[] contributors;

    @("swift type: [String: AttributeGroup]")
    @("kotlin type: HashMap<String, JsonElement>")
    @(`kotlin default: HashMap()`)
    Json[string] attributes;

    @("ts type: GeoJsonGeometry")
    @("swift type: Geometry")
    @("kotlin type: JsonObject")
    @(`kotlin default: JsonObject(emptyMap())`)
    @hidden {
      GeoJsonGeometry unmasked;
      GeoJsonGeometry positionBox;
      GeoJsonGeometry unmaskedBox;
      bool isLarge;
    }

    FeatureDecorators decorators;

    Source source;
    Visibility computedVisibility;
  }
}

@("swift import: GEOSwift")
struct SimpleFeature {
  ObjectId _id;

  ObjectId featureId;

  Map[] maps;
  Icon[] icons;

  @optional
  GeoJsonGeometry position;

  int z;
  int x;
  int y;

  Visibility computedVisibility;

  int _ver = 0;
  size_t changeIndex;
}

enum VisibilityEnum: int {
  Private = 0,
  Pending = -1,
  Public = 1,
}

struct FeatureDecorators {
  @optional:
    bool useDefault = true;
    double[] center;
    int showAsLineAfterZoom = 99;
    bool showLineMarkers;
    bool keepWhenSmall = true;
    size_t changeIndex;
    uint minZoom = 0;
    uint maxZoom = 20;
}
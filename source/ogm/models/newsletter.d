/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.models.newsletter;

import crate.base;

import ogm.models.visibility;
import ogm.models.modelinfo;
import ogm.models.article;

import std.datetime;
import vibe.data.json;

struct Newsletter {
  ObjectId _id;
  ModelInfo info;
  Visibility visibility;

  string name;
  string description;

  @optional:
    Article welcomeMessage;
}

/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.models.issue;

import crate.base;
import crate.error;
import crate.resource.file;
import crate.resource.gridfs;
import crate.resource.migration;
import crate.lazydata.base;
import crate.mime;

import ogm.models.feature;
import vibe.data.json;
import std.datetime;
import std.conv;
import std.string;
import std.range;
import std.uuid;
import std.path;
import std.file;
import std.exception;
import std.traits;
import std.stdio : File;

version(unittest) {
  import fluent.asserts;
}

alias LazyIssue = Lazy!Issue;

/// Settings used for picture files
struct IssueFileSettings {
  static {
    ///
    string path = "files/";

    /// The base url to the pictures model eg http://ogm.com/files/
    string baseUrl;

    @optional {
      IGridFsCollection files;
      IGridFsCollection chunks;

      size_t chunkSize = 255 * 1024;
    }
  }
}

alias IssueFile = GridFsResource!IssueFileSettings;

struct Issue {
  ObjectId _id;
  string feature;

  string author;
  string title;

  SysTime creationDate;

  @optional {
    string description;
    string attributions;
    string other;
    string assignee;
    bool processed;

    IssueFile file;

    SysTime resolveDate;

    enum Status {
      open = "open",
      resolved = "resolved",
      rejected = "rejected"
    }

    Status status;

    enum Type {
      none = "none",
      textSugestion = "textSugestion",
      newImage = "newImage"
    }

    Type type;
  }
}

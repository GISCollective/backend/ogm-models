/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.models.visibility;

import ogm.models.team;

import vibe.data.json;

///
struct Visibility {
  /// The team that owns the set
  Team team;

  /// True, if other teams can use it
  bool isPublic;

  /// The default value show that the item can be used as a default value
  @optional bool isDefault;
}

///
struct VisibilityOptional {
  @optional:
    /// The team that owns the set
    Team team;

    /// True, if other teams can use it
    bool isPublic;

    /// The default value show that the item can be used as a default value
    bool isDefault;
}

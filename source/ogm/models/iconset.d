/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.models.iconset;

import crate.base;
import crate.resource.file;
import crate.lazydata.base;

import ogm.models.visibility;
import ogm.models.team;
import ogm.models.picture;
import ogm.models.source;

import vibe.data.json;

alias LazyIconSet = Lazy!IconSet;

///
struct IconSet {
  ///
  ObjectId _id;

  /// The id of the icon set
  string name;

  /// Some few words about the icon set
  @("ts type: ArticleBody")
  @("swift type: ArticleBody")
  @("kotlin type: ArticleBody")
  @(`kotlin default: ArticleBody()`)
  @optional Json description;

  /// Who can access the icon set
  Visibility visibility;

  @optional {
    /// An optional cover for the icon set
    Picture cover;

    /// Options for how the icons are rendered on the page
    IconStyle styles;

    ///
    size_t ver;

    Source source;

    IconSprite sprites;
  }
}

///
struct PolygonStyleProperties {
  @optional:
    /// deprecated in favour of showAsLineAfterZoom
    bool hideBackgroundOnZoom = false;

    ///
    uint showAsLineAfterZoom = 99;

    ///
    string borderColor = "#fff";

    ///
    string backgroundColor = "rgba(255,255,255,0.6)";

    ///
    uint borderWidth = 1;

    ///
    uint[] lineDash;
}

///
struct LineStyleProperties {
  @optional:
    ///
    string borderColor = "#fff";

    ///
    string backgroundColor = "rgba(255,255,255,0.6)";

    ///
    uint borderWidth = 1;

    ///
    uint[] lineDash;

    ///
    uint markerInterval = 700;
}

///
struct PointStyleProperties {
  @optional:

    ///
    bool isVisible = true;

    ///
    string shape = "circle";

    ///
    string borderColor = "#fff";

    ///
    string backgroundColor = "rgba(255,255,255,0.6)";

    ///
    uint borderWidth = 1;

    ///
    uint size = 21;
}

///
struct LabelStyleProperties {
  @optional:
    bool isVisible = false;
    string text = "wrap";
    @name("align") string align_ = "center";
    string baseline = "bottom";
    string weight = "bold";

    string color = "#000";
    string borderColor = "#fff";
    int borderWidth = 2;

    int size = 10;
    int lineHeight = 1;
    int offsetX = 0;
    int offsetY = 0;
}

struct IconStyle {
  @optional {
    PointStyleProperties site;
    LabelStyleProperties siteLabel;

    LineStyleProperties line;
    PointStyleProperties lineMarker;
    LabelStyleProperties lineLabel;

    PolygonStyleProperties polygon;
    PointStyleProperties polygonMarker;
    LabelStyleProperties polygonLabel;
  }
}

struct IconSprite {
  @optional {
    Picture small;
    Picture large;
  }
}
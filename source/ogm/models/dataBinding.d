/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.models.dataBinding;

import vibe.data.json;
import std.datetime;
import crate.base;

import ogm.models.visibility;
import ogm.models.modelinfo;
import ogm.models.mapFile;

struct DataBinding {
  ObjectId _id;

  string name;

  DataBindingDestination destination;

  ///
  Json descriptionTemplate;
  FieldMapping[] fields;
  string[] extraIcons;
  string updateBy;
  string crs;
  @optional SysTime analyzedAt;

  ///
  Connection connection;

  Visibility visibility;

  ModelInfo info;
}

struct Connection {
  string type;
  Json config;
}

struct DataBindingDestination {
  string type;
  string modelId;
  bool deleteNonSyncedRecords;
}

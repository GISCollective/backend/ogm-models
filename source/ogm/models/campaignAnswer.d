/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.models.campaignAnswer;

import std.datetime;

import crate.http.request;
import crate.base;
import geo.json;
import crate.lazydata.base;

import ogm.models.campaign;
import ogm.models.modelinfo;
import ogm.models.picture;
import ogm.models.sound;
import ogm.models.icon;

import ogm.http.request;
import ogm.http.request;

import vibe.data.json;
import vibe.http.server;

import geo.json;

alias LazyCampaignAnswer = Lazy!CampaignAnswer;

enum CampaignAnswerStatus: int {
  processing = 0,
  pending = 1,
  published = 2,
  unpublished = 3,
  rejected = 4
};

///
@("swift import: GEOSwift")
struct CampaignAnswer {
  ///
  ObjectId _id;

  ///
  ModelInfo info;

  ///
  Campaign campaign;

  ///
  @("swift type: [String: AttributeGroup]")
  Json[string] attributes;

  @optional {
    ///

    @("ts type: GeoJsonGeometry")
    @("swift type: Geometry")
    @("kotlin type: JsonObject")
    @(`kotlin default: JsonObject(emptyMap())`)
    GeoJsonGeometry position;

    ///
    Icon[] icons;

    ///
    Picture[] pictures;

    ///
    Sound[] sounds;

    CampaignAnswerStatus status;

    string featureId;

    Review review;

    Json[string] contributor;
  }
}

struct Review {
  string user;
  SysTime reviewedOn;
}
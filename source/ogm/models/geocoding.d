/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.models.geocoding;

import ogm.models.icon;

import geo.json;

import geo.json;
import crate.base;
import crate.lazydata.base;
import ogm.models.map;

alias LazyGeocoding = Lazy!Geocoding;

@("swift import: GEOSwift")
struct Geocoding {
  ObjectId _id;

  string name;

  @("ts type: GeoJsonGeometry")
  @("swift type: Geometry")
  @(`swift default: try! Geometry(wkt: "POINT (0 0)")`)
  @("kotlin type: JsonObject")
  @(`kotlin default: JsonObject(emptyMap())`)
  GeoJsonGeometry geometry;
  string[] icons;
  double score;

  /// License attached to all sites attached to the map
  License license;
}

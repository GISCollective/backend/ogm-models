/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.models.changeset;

import vibe.data.json;
import std.string;
import std.datetime;
import crate.base;

public import crate.collection.notifications;

struct ChangeSet {
  ObjectId _id;

  string model;
  string author;
  ObjectId itemId;

  SysTime time;
  CrateChangeType type;

  @optional {
    Json[string] added;
    Json[string] removed;

    string reference;
  }
}

bool isChanged(const ref ChangeSet changeSet, string key) {
  auto result = key in changeSet.added || key in changeSet.removed ? true : false;

  if(result) {
    return result;
  }

  foreach(string changeKey, Json value; changeSet.added) {
    if(changeKey.indexOf(key) == 0) {
      return true;
    }
  }

  foreach(string changeKey, Json value; changeSet.removed) {
    if(changeKey.indexOf(key) == 0) {
      return true;
    }
  }

  return false;
}

bool anyIsChanged(T...)(const ref ChangeSet changeSet, T fields) {
  static foreach(field; fields) {
    if(changeSet.isChanged(field)) {
      return true;
    }
  }

  return false;
}
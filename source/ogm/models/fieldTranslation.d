/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.models.fieldTranslation;

import crate.base;
import crate.error;
import crate.resource.file;
import crate.resource.gridfs;
import crate.resource.migration;
import crate.lazydata.base;
import crate.mime;

import ogm.models.modelinfo;

import vibe.data.json;
import std.datetime;
import std.conv;
import std.string;
import std.range;
import std.uuid;
import std.path;
import std.file;
import std.exception;
import std.traits;
import std.stdio : File;

version(unittest) {
  import fluent.asserts;
}

alias LazyFieldTranslation = Lazy!FieldTranslation;

struct FieldTranslation {
  ObjectId _id;

  string modelName;
  string itemId;
  string locale;

  Json fields;

  ///
  @optional ModelInfo info;
}

/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.models.page;

import crate.base;
import ogm.models.layout;
import ogm.models.space;

import ogm.models.modelinfo;
import vibe.data.json;
import ogm.models.visibility;
import ogm.models.picture;

struct Page {
  ObjectId _id;
  ModelInfo info;
  Visibility visibility;

  string name;
  string slug;

  PageCol[] cols;

  @optional:
    string description;
    PageContainer[] containers;
    Space space;
    LayoutContainer[] layoutContainers;
    Picture cover;

    /// set the categories/models that are shown on the page, category for articles or the parent model id
    string[] categories;
}

struct PageCol {
  @optional:
    int container;
    int col;
    int row;

    @("swift type: [String:Json]")
    @(`swift default: [:]`)
    Json data;
    string type;
    string gid;
    string name;
}

struct PageContainer {
  string backgroundColor;
}

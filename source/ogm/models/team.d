/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.models.team;

import ogm.models.picture;
import crate.base;
import crate.lazydata.base;
import vibe.data.json;
import std.datetime;

alias LazyTeam = Lazy!Team;

struct TimeTracking {
  double hours;
  SysTime date;
  string details;
}

struct SubscriptionDetails {
  string name;

  SysTime expire;
  string[] domains;
  string comment;

  @optional:
    string details;
    double monthlySupportHours = 0;

    TimeTracking[] support;
    TimeTracking[] invoices;
}

struct SocialMediaLinks {
  @optional:
    string facebook;
    string youtube;
    string xTwitter;
    string instagram;
    string whatsApp;
    string tikTok;
    string linkedin;
}

struct Invitation {
  string email;
  string role;
}

struct Team {
  ObjectId _id;

  string name;

  @("ts type: ArticleBody")
  @("swift type: ArticleBody")
  Json about;

  string[] owners;
  string[] leaders;
  string[] members;
  string[] guests;

  bool isPublic;

  @optional {
    /// The team logo
    Picture logo;

    /// A list of images with the team
    Picture[] pictures;

    /// True if this is the default team
    bool isDefault;

    /// True if the team can edit Pages and presentations
    bool isPublisher;

    /// True if the team can create data bindings
    bool allowCustomDataBindings;

    /// True if the team can create their own newsletters
    bool allowNewsletters;

    /// True if the team can create their own events
    bool allowEvents;

    /// True if the team can view advanced reports
    bool allowReports;

    SubscriptionDetails subscription;

    string[] categories;

    SocialMediaLinks socialMediaLinks;

    string contactEmail;

    Invitation[] invitations;
  }
}

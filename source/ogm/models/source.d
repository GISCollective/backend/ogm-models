/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.models.source;

import std.datetime;
import vibe.data.json;

struct Source {
  @optional {
    string type;
    string modelId;
    string remoteId;
    SysTime syncAt;
  }
}

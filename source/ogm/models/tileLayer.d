/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.models.tileLayer;

import crate.base;
import crate.lazydata.base;

import geo.geometries;
import geo.xyz;
import geo.json;

import vibe.data.json;

alias PropertyTable = string[string];
alias IntPosition = int[];

///
struct TileLayer {
  ///
  ObjectId _id;

  /// A flag that marks the tile ready to be rendered
  bool isReady;

  /// A flag that marks the tile data is public
  bool isPublic;

  /// The map that was used to generate the tile;
  string mapId;

  /// The team that owns the tile
  string teamId;

  /// The map changeIndex
  size_t changeIndex;

  /// The z/x/y path to the tile
  ZXY path;

  /// The tile resolution
  uint extent;

  ///
  string layerName;

  ///
  @optional Json geometries;

  ///
  PropertyTable[] properties;
}

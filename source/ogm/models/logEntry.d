/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.models.logEntry;

import crate.base;
import crate.lazydata.base;

import vibe.data.json;

alias LazyLegend = Lazy!LogEntry;

struct LogEntry {
  ObjectId _id;

  size_t timestamp;
  string reference;

  string level;
  string message;
}

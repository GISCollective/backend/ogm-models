/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.models.modelinfo;

import std.datetime;
import vibe.data.json;
import ogm.calendar;

version(unittest) {
  import fluent.asserts;
}

///
struct ModelInfo {
  SysTime createdOn;
  SysTime lastChangeOn;
  size_t changeIndex;

  string author;

  @optional:
    string originalAuthor;
}

///
ModelInfo defaultModelInfo() {
  auto now = SysCalendar.instance.now;

  return ModelInfo(now, now, 0, "@unknown", "@unknown");
}

/// Setup the default values
unittest {
  auto now = Clock.currTime;
  now.fracSecs = Duration.zero;

  defaultModelInfo().should.equal(ModelInfo(now, now, 0, "@unknown", "@unknown"));
}

struct ModelInfoRules {
  bool canChangeOriginalData;

  Json value;

  void update(Json newValue) {
    auto now = Clock.currTime.toUTC;
    now.fracSecs = Duration.zero;

    if(value.type != Json.Type.object) {
      value = Json.emptyObject;
    }

    if("createdOn" !in value) {
      value["createdOn"] = now.toISOExtString;
    }

    if("originalAuthor" !in value) {
      value["originalAuthor"] = "@unknown";
    }

    if("author" !in value) {
      value["author"] = "@unknown";
    }

    value["lastChangeOn"] = SysCalendar.instance.now.toISOExtString;

    if("changeIndex" !in value) {
      value["changeIndex"] = 0;
    } else {
      value["changeIndex"] = value["changeIndex"].to!size_t + 1;
    }

    if(canChangeOriginalData) {
      if("originalAuthor" in newValue && newValue["originalAuthor"] != "") {
        value["originalAuthor"] = newValue["originalAuthor"];
      }
      if("createdOn" in newValue) {
        value["createdOn"] = newValue["createdOn"];
      }

      if("author" in newValue) {
        value["author"] = newValue["author"];
      }
    }
  }
}

///
void updateModelInfoJson(ref Json originalValue, ref Json destination, bool canChangeOriginalData = false) {
  auto rules = ModelInfoRules(canChangeOriginalData, originalValue);

  rules.update(destination);

  destination = rules.value.clone;
}

/// it should add the init values when they are missing
unittest {
  auto originalValue = Json.emptyObject;
  auto destination = Json.emptyObject;

  updateModelInfoJson(originalValue, destination);

  destination.should.equal(defaultModelInfo().serializeToJson);
}

/// it should copy the created on if it exists
unittest {
  auto originalValue = `{ "createdOn": "2018-05-28T07:06:07Z" }`.parseJsonString;
  auto destination = Json.emptyObject;

  updateModelInfoJson(originalValue, destination);

  destination["createdOn"].should.equal("2018-05-28T07:06:07Z");
}

/// it should increment the changeIndex
unittest {
  auto originalValue = `{ "changeIndex": 10 }`.parseJsonString;
  auto destination = Json.emptyObject;

  updateModelInfoJson(originalValue, destination);

  destination["changeIndex"].should.equal(11);
}

/// it should increment string changeIndex
unittest {
  auto originalValue = `{ "changeIndex": "10" }`.parseJsonString;
  auto destination = Json.emptyObject;

  updateModelInfoJson(originalValue, destination);

  destination["changeIndex"].should.equal(11);
}

///
void setupModelInfo(ref Json value) {
  auto info = defaultModelInfo().serializeToJson;

  if("info" !in value) {
    value["info"] = info;
  }

  static foreach(k; __traits(allMembers, ModelInfo)) {
    if(k !in value) {
      value["info"][k] = info[k];
    }
  }
}
/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.models.preference;

import vibe.data.json;

import crate.base;

struct Preference {
  ObjectId _id;
  string name;

  @("swift type: String")
  @(`swift default: ""`)
  Json value;

  @optional bool isSecret;
}

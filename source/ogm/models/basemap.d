/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.models.basemap;

import ogm.models.picture;
import ogm.models.visibility;
import ogm.models.attribution;
import vibe.data.json;

import crate.base;
import crate.lazydata.base;

alias LazyBaseMap = Lazy!BaseMap;

struct MapLayer {
  string type;

  @("swift type: [String:String]")
  @(`swift default: [:]`)
  @optional Json options;
}

struct BaseMap {
  ObjectId _id;
  string name;
  string icon;

  MapLayer[] layers;

  /// Who can access the icon set
  Visibility visibility;

  @optional {
    /// A base map screenshot
    Picture cover;

    /// An overlay string shown on the maps
    Attribution[] attributions;

    /// The order in the default list
    int defaultOrder;
  }
}

/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.models.mapFile;

import crate.base;
import crate.resource.file;
import crate.lazydata.base;

import ogm.models.map;
import ogm.models.batchjob;
import crate.resource.gridfs;

import vibe.data.json;
import ogm.models.meta;
import std.datetime;
import std.array;

alias LazyMapFile = Lazy!MapFile;

/// Settings used for map files
struct MapFileSettings {
  static {
    /// The base url to the pictures model eg http://ogm.com/files/
    string baseUrl;

    @optional {
      IGridFsCollection files;
      IGridFsCollection chunks;

      size_t chunkSize = 255 * 1024;
    }
  }
}

///
alias MapFileResource = GridFsResource!MapFileSettings;

///
struct MapFileOptions {
  @optional {
    string uuid;

    string destinationMap;
    string updateBy;
    string crs;
    SysTime analyzedAt;

    ///
    FieldMapping[] fields;

    ///
    string[] extraIcons;
  }
}

struct FieldMapping {
  @optional {
    string key;
    string destination;
    string[] preview;
  }
}


struct FieldError {
  @optional {
    string code;
    long line;
    long col;
  }
}

///
struct MapFile {
  ///
  ObjectId _id;

  /// The icon set that the icon is part of
  Map map;

  /// The icon file
  MapFileResource file;

  /// Various options related to the map
  @optional MapFileOptions options;
}

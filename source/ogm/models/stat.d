/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.models.stat;

import crate.base;
import crate.lazydata.base;
import ogm.models.map;
import std.datetime;
import vibe.data.json;

alias LazyStat = Lazy!Stat;

struct Stat {
  ObjectId _id;

  string name;
  double value;

  @optional {
    SysTime lastUpdate;
  }
}

/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.models.model;

import std.range;
import std.algorithm;
import std.conv;

import crate.base;
import crate.ctfe;
import crate.collection.memory;
import crate.collection.proxy;
import crate.error;
import crate.http.queryParams;

import ogm.models.map;

import vibe.data.json;
import vibe.http.router;

alias GetFilter = IQuery delegate(IQuery, HTTPServerRequest, HTTPServerResponse);

struct Model {
  ObjectId _id;

  string name;

  size_t itemCount;
}


struct ModelFactory {
  ObjectId _id;
  string name;

  CrateAccess crate;
  GetFilter[] filters;

  Json toJson(HTTPServerRequest request, HTTPServerResponse response) @trusted {
    auto result = Json.emptyObject;

    auto selector = crate.get;
    foreach(filter; filters) {
      selector = filter(selector, request, response);
    }

    result["_id"] = _id.toJson;
    result["name"] = name;
    result["itemCount"] = selector.size;

    return result;
  }

  void add(IQuery delegate(IQuery selector, HTTPServerRequest req) next) {

    IQuery wrap(IQuery selector, HTTPServerRequest req, HTTPServerResponse res) {
      return next(selector, req);
    }

    filters ~= &wrap;
  }

  void add(Parameters)(IQuery delegate(IQuery, Parameters, HTTPServerRequest) next) {

    IQuery wrap(IQuery selector, HTTPServerRequest req, HTTPServerResponse res) {
      auto params = req.query.parseQueryParams!Parameters;

      return next(selector, params, req);
    }

    filters ~= &wrap;
  }
}

///
class ModelCrate : Crate!Model {

  @safe:
    /// Get items from the crate
    IQuery get() {
      return new MemoryQuery([]);
    }

    /// Get one item by id
    IQuery getItem(const string id) {
      auto query = get;
      query.where("_id").equal(ObjectId(id));

      return query;
    }

    /// Add a new item
    Json addItem(const Json) {
      throw new CrateValidationException("You can not add models.");
    }

    /// Update an existing item
    Json updateItem(const Json) {
      throw new CrateValidationException("You can not update models.");
    }

    /// Delete an existing item
    void deleteItem(const string) {
      throw new CrateValidationException("You can not delete models.");
    }
}


class ModelPageFilter {

  private {
    size_t index;
    ModelFactory[] models;
  }

  void add(Type, T...)(Crate!Type crate, T middlewareList) {
    index++;
    auto model = ModelFactory(ObjectId.fromString(index.to!string));

    model.name = Singular!Type;
    model.crate = crate;

    foreach(middleware; middlewareList) {
      static if(__traits(hasMember, middleware, "any")) {
        model.add(&middleware.any);
      }

      static if(__traits(hasMember, middleware, "get") && !__traits(hasMember, middleware, "getWrapper")) {
        model.add(&middleware.get);
      }

      static if(__traits(hasMember, middleware, "getWrapper")) {
        model.add(&middleware.getWrapper);
      }
    }

    models ~= model;
  }

  @get
  IQuery get(IQuery, HTTPServerRequest request, HTTPServerResponse response) {
    IQuery query;

    if("id" in request.params) {
      query = new MemoryQuery(models
        .filter!(a => a._id.to!string == request.params["id"])
        .map!(a => a.toJson(request, response))
          .inputRangeObject);
    } else {
      query = new MemoryQuery(models.map!(a => a.toJson(request, response)).inputRangeObject);
    }

    return query;
  }
}

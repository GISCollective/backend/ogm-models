/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.models.legend;

import ogm.models.map;
import ogm.models.picture;
import ogm.models.icon;

import geo.json;
import crate.base;
import crate.lazydata.base;
import crate.resource.file;

import vibe.data.json;

alias LazyLegend = Lazy!Legend;

struct Legend {
  ulong _id;

  string name;
  size_t count;

  LegendSubcategory[] subcategories;
}

struct LegendSubcategory {
  string name;
  string uid;
  size_t count;

  LegendIcon[] icons;
}

struct LegendIcon {
  Icon icon;
  string uid;
  size_t count;
}

/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.models.presentation;

import crate.base;
import ogm.models.layout;

import ogm.models.modelinfo;
import vibe.data.json;
import ogm.models.visibility;
import ogm.models.page;
import ogm.models.picture;

struct Presentation {
  ObjectId _id;
  ModelInfo info;
  Visibility visibility;

  string name;
  string slug;

  PageCol[] cols;

  @optional {
    bool hasIntro;
    bool hasEnd;

    int introTimeout;
    Picture cover;
    Json endSlideOptions;
  }
}

/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.models.event;

import crate.base;

import ogm.models.visibility;
import ogm.models.modelinfo;
import ogm.models.picture;
import ogm.models.calendar;
import ogm.models.icon;

import std.datetime;
import vibe.data.json;
import gis_collective.hmq.log;

struct Event {
  ObjectId _id;
  ModelInfo info;
  Visibility visibility;

  /// The event name
  string name;

  /// The event article
  @("ts type: ArticleBody")
  @("swift type: ArticleBody")
  Json article;

  Calendar calendar;
  Icon[] icons;

  CalendarEntry[] entries;

  EventLocation location;

  @optional:
    Picture cover;
    TimeInterval[] allOccurrences;
    TimeInterval nextOccurrence;
    Json attributes;
}

struct EventLocation {
  enum Type {
    FeatureId = "Feature",
    Text = "Text",
  }

  Type type;
  string value;
}

enum CalendarEntryRepetition {
  NoRepeat = "norepeat",
  Daily = "daily",
  Weekly = "weekly",
  Monthly = "monthly",
  Yearly = "yearly",
}

struct CalendarEntry {
  SysTime begin;
  SysTime end;
  SysTime intervalEnd;
  CalendarEntryRepetition repetition;
  string timezone;
}

///
bool isValid(const ref CalendarEntry entry) nothrow {
  return entry.begin.year > 1 && entry.end <= entry.intervalEnd;
}

CalendarEntry next(const ref CalendarEntry entry) {
  if(entry.repetition == CalendarEntryRepetition.NoRepeat) {
    return CalendarEntry();
  }

  auto newEntry = CalendarEntry(entry.begin, entry.end, entry.intervalEnd, entry.repetition, entry.timezone);

  if(entry.repetition == CalendarEntryRepetition.Daily) {
    newEntry.begin += 1.days;
    newEntry.end += 1.days;
  }

  if(entry.repetition == CalendarEntryRepetition.Weekly) {
    newEntry.begin += 7.days;
    newEntry.end += 7.days;
  }

  if(entry.repetition == CalendarEntryRepetition.Monthly) {
    newEntry.begin.incMonth;
    newEntry.end.incMonth;
  }

  if(entry.repetition == CalendarEntryRepetition.Yearly) {
    newEntry.begin.year = newEntry.begin.year + 1;
    newEntry.end.year = newEntry.end.year + 1;
  }

  if(newEntry.end > newEntry.intervalEnd) {
    return CalendarEntry();
  }

  return newEntry;
}

struct TimeInterval {
  SysTime begin;
  SysTime end;

  @optional:
    int dayOfWeek;
}

int toDayIndex(SysTime date) {

  switch(date.dayOfWeek) {
    case DayOfWeek.mon:
      return 0;
    case DayOfWeek.tue:
      return 1;
    case DayOfWeek.wed:
      return 2;
    case DayOfWeek.thu:
      return 3;
    case DayOfWeek.fri:
      return 4;
    case DayOfWeek.sat:
      return 5;
    case DayOfWeek.sun:
      return 6;
    default:
  }

  return -1;
}

TimeInterval toInterval(CalendarEntry entry) {
  auto interval = TimeInterval();
  interval.begin = entry.begin;
  interval.end = entry.end;

  try {
    auto timezone = PosixTimeZone.getTimeZone(entry.timezone);
    interval.begin.timezone = timezone;
    interval.end.timezone = timezone;
    interval.dayOfWeek = interval.begin.toDayIndex;
  } catch(Exception e) {
    error(e);
  }

  return interval;
}

void incMonth(ref SysTime date) {
  if(date.month == 12) {
    date.month = cast(Month) 1;
    date.year = date.year + 1;
    return;
  }

  date.month = cast(Month) (date.month + 1);
}
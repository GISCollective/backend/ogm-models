/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.models.meta;

import vibe.data.json;

import crate.base;

Crate!Meta metaCrate;

/// Model used to store metadata about the stored data
struct Meta {
  ObjectId _id;

  string type;
  string model;
  string itemId;
  long changeIndex;

  @optional Json data;
}

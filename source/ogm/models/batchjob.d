/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.models.batchjob;

import vibe.data.json;

import crate.base;
import crate.lazydata.base;
import std.datetime;
import ogm.models.visibility;
import ogm.models.modelinfo;

Crate!BatchJob batchJobCrate;

alias LazyBatchJob = Lazy!BatchJob;

enum BatchJobStatus : string {
  scheduled = "scheduled",
  preparing = "preparing",
  running = "running",
  success = "success",
  error = "error",
  cancelled = "cancelled",
  timeout = "timeout"
}

///
struct RunEntry {
  /// An unique string id for the run
  string runId;

  /// The current running status
  BatchJobStatus status;

  /// Time when the job started
  SysTime time;

  /// The last job ping. It's used to check if the process died.
  SysTime ping;

  /// How many seconds it needed to run
  ulong duration;

  /// The total number of tasks
  size_t total;

  /// The number of tasks sent to the queue
  size_t sent;

  /// The number of the processed tasks
  size_t processed;

  /// The number of tasks that were ignored
  size_t ignored;

  /// The number of errors
  size_t errors;
}

struct BatchJob {
  /// The main job id
  ObjectId _id;

  // The job name
  string name;

  /// Info about the job visibility
  Visibility visibility;

  /// The history of the executed jobs
  @optional RunEntry[] runHistory;

  ///
  ModelInfo info;
}

/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.models.attribution;

import vibe.data.json;

///
struct Attribution {
  @optional {
    string name;
    string url;
  }
}

/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.models.space;

import crate.base;

import vibe.data.json;
import ogm.models.modelinfo;
import ogm.models.visibility;
import ogm.models.picture;
import ogm.models.attribution;
import ogm.models.page;
import ogm.models.layout;

struct Space {
  ObjectId _id;

  string name;

  ModelInfo info;

  Visibility visibility;

  @optional {
    Picture logo;
    Picture logoSquare;
    Picture cover;

    string description;

    ColorPalette colorPalette;
    FontStyles fontStyles;

    Attribution[] attributions;

    string landingPageId;
    string globalMapPageId;

    string domain;
    string domainValidationKey;

    bool allowDomainChange;
    bool hasDomainValidated;
    string domainStatus;

    string matomoUrl;
    string matomoSiteId;
    string locale;

    PageCol[string] cols;
    LayoutContainer[string] layoutContainers;

    Link[string] redirects;

    bool isTemplate;

    SpaceSearchOptions searchOptions;

    DefaultModels defaultModels;
  }
}

struct CustomColor {
  string name;
  string lightValue;
  string darkValue;
}

struct DefaultModels {
  @optional:
    string map;
    string campaign;
    string calendar;
    string newsletter;
}

struct Link {
  @optional:
    string route;
    string model;
    string path;
    string url;
    string anchor;
}

struct FontStyles {
  string[] h1;
  string[] h2;
  string[] h3;
  string[] h4;
  string[] h5;
  string[] h6;
  string[] paragraph;
}

struct ColorPalette {
  string blue = "#3961d0";
  string indigo = "#2a1a68";
  string purple = "#663eff";
  string pink = "#ff3e76";
  string red = "#e50620";
  string orange = "#e55b06";
  string yellow = "#e5ca06";
  string green = "#107353";
  string teal = "#5d7388";
  string cyan = "#159ea7";

  @optional CustomColor[] customColors;
}

struct SpaceSearchOptions {
  bool features;
  bool maps;
  bool campaigns;
  bool iconSets;
  bool teams;
  bool icons;
  bool geocodings;
  bool events;
  bool articles;
}
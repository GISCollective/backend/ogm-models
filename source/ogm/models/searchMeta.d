/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.models.searchMeta;

import crate.base;
import ogm.models.picture;
import ogm.models.icon;
import ogm.models.visibility;
import vibe.data.json;
import geo.json;
import std.datetime;

struct SearchMeta {
  ObjectId _id;

  Visibility visibility;

  string title;
  string description;

  string relatedId;
  string relatedModel;

  string[] keywords;

  @optional:
    Icon[] icons;
    Picture cover;
    FeatureFields feature;
    SysTime lastChangeOn;
    SysTime lastDuplicateCheckOn;
    string[] categories;
}

struct FeatureFields {
  @optional:
    GeoJsonGeometry centroid;
    bool isGeoCoding;
    size_t area;
    size_t distance;
    string[] maps;
}
/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.models.metric;

import crate.base;

import ogm.models.modelinfo;
import vibe.data.json;
import std.datetime;

struct Metric {
  ObjectId _id;

  string name;
  string type;
  string reporter;
  double value;

  @optional {
    string[string] labels;
    SysTime time;
  }
}

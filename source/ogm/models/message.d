/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.models.message;

import crate.base;
import std.datetime;
import crate.lazydata.base;

alias LazyMessage = Lazy!Message;

enum MessageType {
  admin = "admin",
  auth = "auth",
  message = "message"
}


///
struct MessageRecipient {
  string type;
  string value;
}

///
struct Message {
  ///
  ObjectId _id;
  string uniqueKey;

  MessageType type = MessageType.message;

  MessageRecipient to;

  string subject;
  string text;
  string html;

  string[string] actions;

  bool useGenericTemplate = true;

  bool isSent;
  SysTime sentOn;
}

/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.models.layout;

import crate.base;

import ogm.models.modelinfo;
import vibe.data.json;
import ogm.models.visibility;

struct LayoutContainer {
  @optional:
    LayoutRow[] rows;
    string[] options;
    LayoutLayers layers;
    string gid;
    string visibility;
    Json data;
    string anchor;
    string height;
}

struct LayoutLayers {
  uint count = 1;
  uint showContentAfter = 1;
  string effect = "none";
}

struct LayoutRow {
  @optional:
    LayoutCol[] cols;
    string[] options;
    Json data;
}

struct LayoutCol {
  @optional:
    string type;

    @("swift type: [String:String]")
    @(`swift default: [:]`)
    Json data;
    string[] options;

    int componentCount;

    string name;
}

/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.models.sound;

import crate.base;
import crate.lazydata.base;
import crate.resource.gridfs;
import crate.resource.sound;

import ogm.models.visibility;
import ogm.models.modelinfo;

import vibe.data.json;
import vibe.core.log;
import vibe.core.core;

version(unittest) {
  import fluent.asserts;
}

alias LazySound = Lazy!Sound;

/// Settings used for sound files
struct SoundFileSettings {
  static {
    ///
    string path = "sound/";

    /// The base url to the sound model eg http://ogm.com/files/
    string baseUrl;

    @optional {
      IGridFsCollection files;
      IGridFsCollection chunks;

      size_t chunkSize = 255 * 1024;
    }
  }
}

enum SoundSettings = [
  AudioSettings("mp3-192",  "192k", "libmp3lame", ".mp3"),
  AudioSettings("ogg-192",  "192k", "libvorbis",  ".ogg"),
  AudioSettings("aac-192",  "192k", "aac", ".aac"),
  AudioSettings("weba-192", "192k", "libvorbis", ".webm"),
];

alias SoundFileStorage = GridFsResource!SoundFileSettings;
alias SoundFile = SoundResource!(SoundFileStorage, SoundSettings);

/// Represents a sound file
struct Sound {
  /// The item id
  ObjectId _id;

  /// The resource name
  string name;

  /// Who can access the icon set
  VisibilityOptional visibility;

  ///
  ModelInfo info;

  /// The sound resource
  SoundFile sound;

  /// The feature associated to the sound
  @optional string feature;

  /// The campaign answer associated to the sound
  @optional string campaignAnswer;

  /// The url from where the file was downloaded
  @optional string sourceUrl;

  ///
  @optional string attributions;
}

/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.models.icon;

import crate.base;
import crate.resource.file;
import crate.lazydata.base;

import ogm.models.picture;
import ogm.models.iconset;
import ogm.models.visibility;
import ogm.middleware.translation;

import crate.resource.image;
import crate.resource.gridfs;
import crate.resource.migration;

import vibe.data.json;

alias LazyIcon = Lazy!Icon;

/// Settings used for picture files
struct IconSettings {
  static {
    ///
    string path = "files/";

    /// The base url to the pictures model eg http://ogm.com/files/
    string baseUrl;

    @optional {
      IGridFsCollection files;
      IGridFsCollection chunks;

      size_t chunkSize = 255 * 1024;
    }
  }
}

enum IconSizes = [
  ImageSettings("sm", "21", "21"),
  ImageSettings("md", "64", "64"),
  ImageSettings("lg", "100", "100"),
  ImageSettings("png", "255", "255", "png")
];

alias IconFileStorage = GridFsResource!IconSettings;
alias IconFile = CrateImageCollectionResource!(IconFileStorage, IconSizes);

///
struct IconAttribute {
  ///
  string name;

  ///
  string type;

  @optional {
    ///
    string options;

    /// The field to set the attribute private or public
    bool isPrivate;

    /// The field to set the attribute as required or not required
    bool isRequired;

    /// The attribute name for the selected language.
    /// For english names, it is the same as the name.
    @translate string displayName;

    /// The help message for the selected language.
    @translate string help;

    ///
    bool isInherited;

    ///
    string[] otherNames;
  }
}

///
enum VerticalIndex {
  unknown = 0,
  deepUnderground = 8,
  underground = 9,
  surface = 10,
  groundFeature = 11,
  aboveGround = 12,
  highAltitude = 13
}

enum ZoomRelevance {
  all = 4,
  county = 2,
  country = 1,
  world = 0
}

///
struct Icon {
  ///
  ObjectId _id;

  /// The icon set that the icon is part of
  IconSet iconSet;

  @optional @translate {
    string category;
    string subcategory;
  }

  ///
  string name;

  ///
  @("ts type: ArticleBody")
  @("swift type: ArticleBody")
  @("kotlin type: ArticleBody")
  @(`kotlin default: ArticleBody()`)
  @optional @translate Json description;

  /// The icon file
  OptionalIconImage image;

  /// An attribute set for the current item.
  IconAttribute[] attributes;


  @optional {
    ///
    VerticalIndex verticalIndex;

    ///
    uint minZoom = 0;

    ///
    uint maxZoom = 20;

    /// The parent icon that contains the base set of the attributes
    string parent;

    /// True if a site can add many instances of the icon
    bool allowMany;

    ///
    bool keepWhenSmall = true;

    /// Other names that will be matched with the attributes
    string[] otherNames;

    /// The icon name for the selected language.
    /// For the default locale, it is the same as the name.
    @translate string localName;

    /// Options for how the icon is rendered on the page
    InheritedStyle styles;

    bool enforceSvg;

    ///
    IconMeasurements measurements;

    Visibility visibility;

    long order = 999;
  }
}

struct IconMeasurements {
  @optional {
    ///
    uint width;

    ///
    uint height;

    /// An array of four numbers, with the first two specifying the left, top corner, and the last two specifying the right, bottom corner.
    /// If present, and if the image uses icon-text-fit, the symbol's text will be fit inside the content box.
    int[] content;

    /// An array of two-element arrays, consisting of two numbers that represent the from position and the to position of areas that can be stretched.
    int[][] stretchX;

    /// ditto
    int[][] stretchY;
  }
}

///
struct OptionalIconImage {
  @optional {
    bool useParent;
    IconFile value;
  }
}

///
struct InheritedStyle {
  bool hasCustomStyle;

  @optional IconStyle types;
}

/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.models.userProfile;

import crate.base;
import crate.lazydata.base;
import std.datetime;

import vibe.data.json;
import ogm.models.picture;
import ogm.middleware.StaticDbContent;

alias LazyUserProfile = Lazy!UserProfile;

struct UserProfile {
  ObjectId _id;

  string statusEmoji;
  string statusMessage;

  @optional {
    string salutation;
    string title;
  }

  string firstName;
  string lastName;

  string skype;
  string linkedin;
  string twitter;
  string website;
  Location location;
  string jobTitle;
  string organization;
  string bio;

  bool showPrivateContributions;

  @optional {
    bool showCalendarContributions;
    bool showWelcomePresentation;

    SysTime joinedTime;
    Picture picture;
  }
}

struct Location {
  bool isDetailed;

  string simple;

  @optional DetailedLocation detailedLocation;

  ///
  Json toJson() const @safe {
    Json result = Json.emptyObject;

    result["isDetailed"] = this.isDetailed;
    result["simple"] = this.simple;
    result["detailedLocation"] = this.detailedLocation.serializeToJson;

    return result;
  }

  ///
  static Location fromJson(Json src) @safe {
    Location result;

    if(src.type == Json.Type.null_ || src.type == Json.Type.undefined) {
      return result;
    }

    if(src.type == Json.Type.string) {
      result.simple = src.to!string;
    }

    if(src.type == Json.Type.object) {
      result.simple = src["simple"].to!string;
      result.isDetailed = src["isDetailed"].to!bool;

      if(src["detailedLocation"].type == Json.Type.object) {
        result.detailedLocation = src["detailedLocation"].deserializeJson!DetailedLocation;
      }
    }

    return result;
  }
}

struct DetailedLocation {
  @optional:
    string country;
    string province;
    string city;
    string postalCode;
}

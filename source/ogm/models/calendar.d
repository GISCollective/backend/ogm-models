/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.models.calendar;

import crate.base;

import ogm.models.visibility;
import ogm.models.modelinfo;
import ogm.models.picture;
import ogm.models.iconset;
import ogm.models.campaign;
import ogm.models.icon;
import ogm.models.map;

import std.datetime;
import vibe.data.json;

struct Calendar {
  ObjectId _id;
  Visibility visibility;
  ModelInfo info;

  /// The calndar name
  string name;

  /// The calendar article
  @("ts type: ArticleBody")
  @("swift type: ArticleBody")
  Json article;

  /// The icon set allowed to be used with the calendar
  IconSets iconSets;

  /// Which sites can be used for the events locations
  OptionalMap map;

  CalendarType type = CalendarType.Events;

  @optional:
    Picture cover;
    IconAttribute[] attributes;
    string recordName;
}

enum CalendarType {
  Events = "events",
  Schedules = "schedules",
}
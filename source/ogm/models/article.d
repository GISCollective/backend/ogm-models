/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.models.article;

import crate.base;

import ogm.models.modelinfo;
import ogm.models.picture;
import ogm.models.visibility;
import ogm.models.event;
import vibe.data.json;
import std.datetime;

enum ArticleType : string {
  any = "any",
  newsletterWelcomeMessage = "newsletter-welcome-message",
  newsletterArticle = "newsletter-article",
  templateNewsletterWelcomeMessage = "template-newsletter-welcome-message"
}

enum ArticleStatus : string {
  unknown = "",
  daft = "draft",
  pending = "pending",
  publishing = "publishing",
  sent = "sent"
}

struct Article {
  ObjectId _id;

  @optional string slug;

  string title;

  @("ts type: ArticleBody")
  @("swift type: ArticleBody")
  Json content;

  @optional {
    string[] categories;

    ArticleType type;
    string relatedId;
    SysTime releaseDate;
    ArticleStatus status;
    string subject;

    long order = 999;

    Picture cover;
    Picture[] pictures;

    Event[] events;
  }

  ModelInfo info;

  /// Who can access the article
  Visibility visibility;
}

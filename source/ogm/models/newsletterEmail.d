/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.models.newsletterEmail;

import std.datetime;
import crate.base;
import ogm.models.newsletter;

struct NewsletterEmail {
  ObjectId _id;

  string email;

  NewsletterSubscription[] list;
}

struct NewsletterSubscription {
  Newsletter newsletter;
  SysTime joinedOn;
}

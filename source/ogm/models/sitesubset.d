/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.models.sitesubset;

import crate.base;
import crate.lazydata.base;
import ogm.models.feature;

import vibe.data.json;

alias LazySiteSubset = Lazy!SiteSubset;

@("source:sites", "singular:sitesubset", "plural:sitesubsets")
struct SiteSubset {
  string _id;

  Feature feature;
/*
  static {
    SiteSubset fromJson(Json data) @safe {
      SiteSubset result;

      result._id = data["_id"].to!string;
      result.site = data.deserializeJson!Site;

      return result;
    }
  }

  Json toJson() const @safe {
    Json result = Json.emptyObject;

    result["site"] = this.site.serializeToJson;
    result["_id"] = result["site"]["_id"];

    return result;
  }*/
}

/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.TimedCache;

import std.datetime;
import std.container.rbtree;
import std.algorithm;

/// Cache implementation that removes the elements when the time from the last
/// get a predefined interval.
class TimedCache(T) {
  ///
  alias CacheGetter = T delegate(string);


  /// The items that are stored in the internal container
  struct Item {
    string key;
    T value;

    SysTime lastAccess;

    int opCmp(ref const Item otherItem) const {
      return otherItem.key < key ? 1 : -1;
    }
  }

  private {
    Item[string] container;
    CacheGetter getter;
    Duration validationTime;
    size_t storedSize;
  }

  ///
  this(CacheGetter getter, Duration validationTime = 1.seconds, size_t storedSize = 200) {
    this.getter = getter;
    this.validationTime = validationTime;
    this.storedSize = storedSize;
  }

  /// Removes expired items and optimizes the internal container
  void reindex() {
    auto now = Clock.currTime;

    foreach(item; container.byValue.filter!(a => now - a.lastAccess > validationTime)) {
      container.remove(item.key);
    }
  }

  /// Checks if a key is stored in the cache
  bool exists(string key) {
    if(key !in container) {
      return false;
    }

    const expire = container[key].lastAccess + validationTime;

    return expire > Clock.currTime;
  }

  /// Get an item by key. If the item is not already stored in the cache,
  /// the getter will be called and the result is stored in the cache for
  /// the configured time.
  T get(string key) {

    if(!exists(key)) {
      auto value = this.getter(key);
      container[key] = Item(key, value, Clock.currTime);

      if(container.length > storedSize) {
        this.reindex;
      }

      return value;
    }

    container[key].lastAccess += 10.msecs;

    return container[key].value;
  }
}

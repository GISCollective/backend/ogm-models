/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/

module ogm.csv;

import vibe.data.json;
import vibe.core.log;

import std.array;
import std.algorithm;
import std.conv;
import std.string;
import std.traits;
import std.datetime;

import crate.collection.csv;
import crate.base;

import geo.json : GeoJsonGeometry;

version(unittest) {
  import ogm.models.feature;
  import fluent.asserts;
}

///
class CsvHeader {
  string[] fields;
  string[] hiddenFields;
  size_t[string] order;

  size_t index(string name) {
    if(name !in order) {
      return fields.length;
    }

    return order[name];
  }

  bool isHidden(string name) {
    if(name !in order) {
      return true;
    }

    if(hiddenFields.length == 0) {
      return false;
    }

    foreach(field; hiddenFields) {
      if(field.canFind(name)) {
        return true;
      }
    }

    return false;
  }

  void add(string name) {
    fields ~= name;
  }

  void add(T)(string prefix = "") if(is(T == GeoJsonGeometry)) {
    fields ~= prefix ~ ".lon";
    fields ~= prefix ~ ".lat";
    fields ~= prefix ~ ".wkt";
    fields ~= prefix ~ ".geoJson";
  }

  void add(T)(string prefix = "") if(is(T == Json)) {
    fields ~= prefix;
  }

  void add(T)(string prefix = "") if(is(T == ObjectId) || is(T == SysTime)) {
    fields ~= prefix;
  }

  void add(T : U[], U)(string prefix = "") if(is(U == class) || is(U == struct)) {
    fields ~= prefix;
    fields ~= prefix ~ ".name";

    if(prefix == "pictures" || prefix == "sounds") {
      fields ~= prefix ~ ".url";
    }
  }

  void add(T : U[], U)(string prefix = "") if(!is(U == class) && !is(U == struct)) {
    fields ~= prefix;
  }

  void add(T)(string prefix = "") if(!is(T == ObjectId) && !is(T == SysTime) && !is(T == GeoJsonGeometry) && !is(T == Json)) {
    string result;

    if(prefix != "") {
      prefix = prefix ~ ".";
    }

    static foreach(member; __traits(allMembers, T)) {
      static if(member != "monitor" && member != "Monitor" && member != "team") {
        static if(!isCallable!(__traits(getMember, T, member)) && hasUDA!(__traits(getMember, T, member), hidden)) {
          hiddenFields ~= [ prefix ~ member ];
        }

        static if(!isCallable!(__traits(getMember, T, member)) && !hasUDA!(__traits(getMember, T, member), hidden)) {
          static if(is(typeof(__traits(getMember, T, member)) == Json)) {
            fields ~= [ prefix ~ member ];
          } else static if(isArray!(typeof(__traits(getMember, T, member)))) {
            this.add!(typeof(__traits(getMember, T, member)))(prefix ~ member);
          } else static if(isAggregateType!(typeof(__traits(getMember, T, member)))) {
            this.add!(typeof(__traits(getMember, T, member)))(prefix ~ member);
          } else {
            fields ~= [ prefix ~ member ];
          }
        }
      }
    }

    reindex;
  }

  void reindex() {
    foreach(i; 0..fields.length) {
      order[fields[i]] = i;
    }
  }

  CsvRow toRow() {
    return CsvRow(this);
  }

  override string toString() {
    return fields.join(",") ~ "\n";
  }
}

struct CsvRow {
  CsvHeader header;
  string[] row;

  void addField(T)(string key, T value) {
    if(header.isHidden(key)) {
      return;
    }

    if(row.length == 0) {
      row.length = header.fields.length;
    }

    auto index = header.index(key);

    if(row.length <= index) {
      return;
    }

    row[index] = value.to!string.escapeCsvValue;
  }

  void addRow(T)(ref Json value) if(!is(T == ObjectId) && !is(T == SysTime) && !is(T == GeoJsonGeometry)) {
    static foreach(member; __traits(allMembers, T)) {
      static if(member != "monitor") {
        static if(!isCallable!(__traits(getMember, T, member))) {
          try {
            if(value[member].type == Json.Type.array) {
              addField(member, (cast(Json[]) value[member]).map!(a => a.to!string.escapeCsvValue).joiner(";").array);
            } else if(value[member].type == Json.Type.object || value[member].type == Json.Type.array) {
              addAll(member, value[member]);
            } else if(value[member].type != Json.Type.null_ && value[member].type != Json.Type.undefined) {
              addField(member, value[member]);
            }
          } catch(Exception e) {
            logError(e.message);
          }
        }
      }
    }
  }

  void addAll(string key, ref Json obj) {
    if(obj.type != Json.Type.object) {
      return;
    }

    foreach(string objKey, value; obj) {
      if(value.type == Json.Type.object || value.type == Json.Type.array) {
        addAll(key ~ "." ~ objKey, value);
      } else {
        addField(key ~ "." ~ objKey, value);
      }
    }
  }

  void addList(string key, ref Json obj) {
    if(header.isHidden(key)) {
      return;
    }

    if(obj.type != Json.Type.array) {
      addField(key, obj);
      return;
    }

    auto index = header.index(key);

    auto glue = "";
    foreach (value; obj) {
      auto item = value.to!string;

      if(item.strip != "") {
        row[index] = glue ~ item;
        glue = ";";
      }
    }

  }

  string toString() {
    if(row.length == 0) {
      row.length = header.fields.length;
    }

    return row.join(",") ~ "\n";
  }
}

/// The csv output should add the structs fields
unittest {
  auto header = new CsvHeader;
  header.add!Feature;

  header.fields.should.equal(["_id", "maps", "maps.name", "name", "description", "position.lon",
    "position.lat", "position.wkt", "position.geoJson", "info.createdOn", "info.lastChangeOn",
    "info.changeIndex", "info.author", "info.originalAuthor", "pictures", "pictures.name",
    "pictures.url", "sounds", "sounds.name", "sounds.url", "icons", "icons.name", "visibility",
    "contributors", "attributes", "decorators.useDefault", "decorators.center",
    "decorators.showAsLineAfterZoom", "decorators.showLineMarkers", "decorators.keepWhenSmall",
    "decorators.changeIndex", "decorators.minZoom", "decorators.maxZoom", "source.type", "source.modelId",
    "source.remoteId", "source.syncAt", "computedVisibility.isPublic", "computedVisibility.isDefault"]);

  header.index("_id").should.equal(0);
  header.index("attributes").should.equal(24);
}

/// The csv output should add a new field
unittest {
  auto header = new CsvHeader;
  header.add("_id");
  header.add("value");
  header.reindex;

  header.index("_id").should.equal(0);
  header.index("other").should.equal(2);
}

/// Should be able to create a row from an existing header
unittest {
  auto header = new CsvHeader;
  header.add("_id");
  header.add("value");
  header.reindex;

  auto row = header.toRow;
  row.addField("_id", "1");
  row.addField("value", 3);

  row.toString.should.equal("1,3\n");
}

/// Should be able to add an object json
unittest {
  auto header = new CsvHeader;
  header.add("_id.key");
  header.reindex;

  auto row = header.toRow;
  auto obj = `{"key": "value"}`.parseJsonString;

  row.addAll("_id", obj);

  row.toString.should.equal("value\n");
}

/// Should be able to add a nested object json
unittest {
  auto header = new CsvHeader;
  header.add("_id.key.other");
  header.reindex;

  auto row = header.toRow;
  auto obj = `{"key": { "other": "value"}}`.parseJsonString;

  row.addAll("_id", obj);

  row.toString.should.equal("value\n");
}

/// Should not output the header when no fields were added
unittest {
  auto header = new CsvHeader;
  header.add("_id");
  header.add("value");
  header.reindex;

  auto row = header.toRow;
  row.addField("_id", "1");
  row.addField("value", 3);

  row.toString.should.equal("1,3\n");
}

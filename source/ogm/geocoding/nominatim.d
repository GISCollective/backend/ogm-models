/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.geocoding.nominatim;

import ogm.models.geocoding;
import ogm.crates.geocoding;
import ogm.crates.all;
import geo.json;
import vibe.data.json;
import crate.lazydata.base;
import crate.base;
import std.datetime;
import std.uri;
import std.string;
import gis_collective.hmq.log;

class NominatimResolver : GeocodingResolver {
  alias RequestHandler = @safe Json delegate(string);

  private {
    OgmCrates crates;
    RequestHandler request;
    LazyField!string searchUrl;
    LazyField!string mapCache;
    immutable static string searchOptions = "&format=geojson&addressdetails=1&extratags=1&namedetails=1&polygon_threshold=0.001&polygon_geojson=1";
  }

  this(OgmCrates crates, RequestHandler request) {
    this.crates = crates;
    this.request = request;

    this.searchUrl = LazyField!string({
      auto range = crates.preference
        .get
        .where("name").equal("integrations.nominatim")
        .and
        .exec;

      if(range.empty) {
        return "";
      }

      return range.front["value"].to!string;
    }, 10.seconds);

    this.mapCache = LazyField!string({
      auto range = crates.map
        .get
        .where("name").equal("Nominatim cache")
        .and
        .exec;

      if(range.empty) {
        return "";
      }

      return range.front["_id"].to!string;
    }, 5.minutes);
  }

  @trusted
  Geocoding[] resolve(string query) {
    Geocoding[] results;
    string url = searchUrl.compute;

    if(url == "") {
      return results;
    }

    string queryComponent = "?q=" ~ query.encodeComponent.replace("%20", "+");
    url = url ~ queryComponent ~ searchOptions;

    try {
      auto response = this.request(url);

      if(response.type != Json.Type.object || response["features"].type != Json.Type.array) {
        return results;
      }

      foreach (feature; response["features"]) {
        if(feature.type != Json.Type.object || feature["type"] != "Feature") {
          continue;
        }

        results ~= toGeocoding(feature);
      }
    } catch(Exception e) {
      error(e);
    }

    return results;
  }

  Geocoding toGeocoding(Json feature) {
    string category;
    string type;
    string placeRank;

    if(feature["properties"]["category"].type == Json.Type.string) {
      category = feature["properties"]["category"].to!string;
    }

    if(feature["properties"]["type"].type == Json.Type.string) {
      type = feature["properties"]["type"].to!string;
    }

    if(feature["properties"]["place_rank"].type == Json.Type.string) {
      placeRank = feature["properties"]["place_rank"].to!string;
    }

    Geocoding record;
    record._id = ObjectId.generate();
    record.name = feature["properties"]["display_name"].to!string;
    record.geometry = GeoJsonGeometry.fromJson(feature["geometry"]);
    record.icons = this.getIcons(category, type, placeRank);
    record.license.name = "Data © OpenStreetMap contributors, ODbL 1.0";
    record.license.url = "https://osm.org/copyright";

    string mapId = this.mapCache.compute;
    bool exists = crates.feature.get.where("name").equal(record.name).and.size > 0;

    if(mapId != "" && !exists) {
      Json cache = `{
        "maps": [],
        "name": [],
        "description": "",
        "position": {},
        "info": {},
        "icons": [],
        "attributes": {"osm": {}}
      }`.parseJsonString;

      cache["name"] = record.name;
      cache["icons"] = record.icons.serializeToJson;
      cache["position"] = feature["geometry"];
      cache["maps"] = [ mapId ].serializeToJson;
      cache["attributes"]["geocoding"] = feature["properties"]["address"];
      cache["attributes"]["other"] = feature["properties"]["extratags"];
      cache["attributes"]["names"] = feature["properties"]["namedetails"];
      cache["info"]["createdOn"] = Clock.currTime.toISOExtString;
      cache["info"]["lastChangeOn"] = Clock.currTime.toISOExtString;

      foreach(string key, value; feature["properties"]) {
        if(value.type != Json.Type.object && value.type != Json.Type.array) {
          cache["attributes"]["osm"][key] = value;
        }
      }

      crates.feature.addItem(cache);
    }

    return record;
  }

  string[] getIcons(string category, string type, string placeRank) {
    string[] results;

    auto range = crates.icon.get
      .where("otherNames").arrayContains(category ~ "." ~ type ~ "." ~ placeRank)
      .and
      .withProjection(["_id"])
      .and.exec;

    if(!range.empty) {
      results ~= range.front["_id"].to!string;
    }

    range = crates.icon.get
      .where("otherNames").arrayContains(category ~ "." ~ type)
      .and
      .withProjection(["_id"])
      .and.exec;

    if(!range.empty) {
      results ~= range.front["_id"].to!string;
    }

    range = crates.icon.get
      .where("otherNames").arrayContains(category)
      .and
      .withProjection(["_id"])
      .and.exec;

    if(!range.empty) {
      results ~= range.front["_id"].to!string;
    }

    range = crates.icon.get
      .where("otherNames").arrayContains(type)
      .and
      .withProjection(["_id"])
      .and.exec;

    if(!range.empty) {
      results ~= range.front["_id"].to!string;
    }

    return results;
  }
}

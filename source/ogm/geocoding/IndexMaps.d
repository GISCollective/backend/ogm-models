/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.geocoding.IndexMaps;

import ogm.models.geocoding;
import ogm.crates.geocoding;
import ogm.crates.all;
import geo.json;
import vibe.data.json;
import crate.lazydata.base;
import crate.base;
import std.datetime;
import std.uri;
import std.string;
import std.algorithm;
import std.array;
import std.uni;

class IndexMapsResolver : GeocodingResolver {
  private {
    OgmCrates crates;
    LazyField!(ObjectId[]) indexMaps;
    LazyField!(License[string]) licenses;
  }

  this(OgmCrates crates) {
    this.crates = crates;
    this.indexMaps = LazyField!(ObjectId[])({
      auto range = crates.map
        .get
        .where("isIndex").equal(true)
        .and
        .exec;

      return range.map!(a => ObjectId.fromJson(a["_id"])).array;
    }, 10.seconds);

    this.licenses = LazyField!(License[string])({
      auto range = crates.map
        .get
        .where("isIndex").equal(true)
        .and
        .exec;

      License[string] licenses;

      foreach (map; range) {
        if(map["license"].type == Json.Type.object) {
          licenses[map["_id"].to!string] = map["license"].deserializeJson!License;
        }
      }

      return licenses;
    }, 10.seconds);
  }

  @trusted
  Geocoding[] resolve(string query) {
    Geocoding[] results;

    auto range = crates.feature.get
      .where("maps").anyOf(indexMaps.compute)
      .and.searchText(query)
      .and
      .withProjection([ "_id", "name", "position", "icons", "maps", "score"])
      .sort("score", 1)
      .limit(5)
      .exec;

    foreach (feature; range) {
      auto item = toGeocoding(feature);
      item.score = levenshteinDistance(query.toLower, item.name.toLower);

      if(item.score < 18) {
        results ~= item;
      }
    }

    return results.sort!"a.score < b.score".array;
  }

  Geocoding toGeocoding(Json feature) {
    Geocoding result;
    result._id = ObjectId.fromJson(feature["_id"]);
    result.name = feature["name"].to!string;
    result.geometry = GeoJsonGeometry.fromJson(feature["position"]);
    result.icons = feature["icons"].deserializeJson!(string[]);

    auto mapId = feature["maps"][0].to!string;
    result.license = licenses.compute[mapId];

    return result;
  }
}

/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.service.microwebservice;

import vibe.service.stats;
import ogm.crates.all;
import vibe.service.webservice;

import vibe.http.server;
import vibe.http.router;
import vibe.core.log;
import vibe.core.core : runEventLoop;
import vibe.service.configuration.http;

import vibe.service.configuration.general;

///
struct MicroConfig {
  /// Configurations for all services
  GeneralConfig general;
}

///
abstract class MicroWebService(Config) : WebService!(Config) {
  ///
  protected OgmCrates crates;

  ///
  abstract void setupApi(URLRouter router);

  /// The main service logic
  int main() {
    auto router = new URLRouter;
    router.setupStats();
    log("Added stats routes.");

    crates = this.configuration.general.dbConfiguration.setupOgmCrates;
    log("Db connection ready.");

    setupApi(router);

    log("Api setup ready.");


    static if(__traits(hasMember, Config, "http")) {
      auto config = this.configuration.http.toVibeConfig;
    } else {
      auto config = MicroHTTP.defaultConfig;
      pragma(msg, "Using the default http server config for `" , typeof(this), "`.");
    }
    config.serverString = this.info.name;

    logInfo("The maximum accepted request size is %s kb", config.maxRequestSize / 1024);
    logInfo("The server string is `%s`", config.serverString);

    listenHTTP(config, router);

    return runEventLoop();
  }
}

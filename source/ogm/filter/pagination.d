/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.filter.pagination;

import vibe.http.router;
import crate.base;
import crate.http.queryParams;
import std.conv;

class PaginationFilter {
  struct Parameters {

    @describe("If it's greater than 0, the server will return at most this number of items.")
    @example("10", "Get `10` items.")
    @example("200", "Get `200` items.")
    size_t limit;

    @describe("Set the number of items that will be skiped from the list.")
    @describe("It can be used for pagination. You can set `skip` to be equal to `limit`*`page number` to get the expected page.")
    @example("10", "Don't return the first `10` items.")
    @example("200", "Don't return the first `200` items.")
    size_t skip;
  }

  IQuery getWrapper(IQuery selector, HTTPServerRequest request) {
    auto params = request.query.parseQueryParams!Parameters;

    return get(selector, params);
  }

  @get
  IQuery get(IQuery selector, Parameters parameters) {
    if(parameters.limit > 0) {
      selector = selector.limit(parameters.limit);
    }

    if(parameters.skip > 0) {
      selector = selector.skip(parameters.skip);
    }

    return selector;
  }
}

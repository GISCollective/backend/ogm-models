/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.filter.visibility;

import ogm.crates.all;
import ogm.http.request;

import vibe.http.router;
import vibe.data.json;

import crate.base;
import crate.error;
import crate.json;
import crate.http.request;
import crate.collection.memory;
import crate.attributes;

import vibe.http.server;
import vibe.data.json;

import std.algorithm : canFind, filter;
import std.string : split;
import std.array : array;

alias ObjectId_ = crate.base.ObjectId;

///
struct VisibilityParameters {
  @describe("If it's true, and the user is an admin, all items will be returned.")
  @example("true", "Get all the items.")
  @example("false", "Get only the items that belongs to the current user.")
  bool all;

  @describe("If it's true, only the items that the user can edit will be returned.")
  @example("true", "Get only the items that the user can edit.")
  @example("false", "Get all the items that the user can view.")
  bool edit;

  @describe("An id of a `team`. If it is present, only the items owned by the team will be returned.")
  string team;

  @describe("An id of a `team`. If it is present, the default items and the ones owned by the team will be returned.")
  string usableByTeam;

  @describe("If it's set to `true`, only the published items are returned. If is set to `false` then only unpublished items wil be returned")
  @example("true", "Get all the published items.")
  @example("false", "Get all the unpublished items.")
  string published;

  @describe("If it's true, only the default items set wil be retrieved.")
  @example("true", "Get only the default items.")
  @example("false", "Get all items.")
  @name("default")
  bool default_;
}

///
class VisibilityFilter(string field, bool allowPublicContributions = false, bool allowLeadersEdit = false, bool allowMembersAdd = false) {
  private {
    OgmCrates crates;
    CrateAccess items;
  }

  ///
  this(OgmCrates crates, CrateAccess items) {
    this.crates = crates;
    this.items = items;
  }

  static if(field == "features") {
    enum visibilityKey = "computedVisibility";
  } else {
    enum visibilityKey = "visibility";
  }

  enum isDefaultKey = visibilityKey ~ ".isDefault";
  enum isPublicKey = visibilityKey ~ ".isPublic";
  enum teamKey = visibilityKey ~ ".team";

  ///
  @get
  IQuery get(IQuery selector, VisibilityParameters parameters, HTTPServerRequest req) {
    scope request = RequestUserData(req);
    scope session = request.session(crates);
    scope teams = session.all.teams;
    scope isAdmin = request.isAdmin;
    bool isPublicQuery = true;

    if(parameters.usableByTeam != "") {
      parameters.team = parameters.usableByTeam;
    }

    string[] selectedTeams = parameters.team.split(",").filter!(a => a != "").array;

    immutable hasTeamQuery = selectedTeams.length > 0;

    bool hasPublishedQuery = parameters.published != "";
    bool published = parameters.published == "true";

    static if(field == "features") {
      if("visibility" in req.query && request.userId != "@anonymous") {
        hasPublishedQuery = true;
        published = req.query["visibility"] == "1" || req.query["visibility"] == "-1";
      }
    }

    if(hasPublishedQuery) {
      enforce!ForbiddenException(request.userId != "@anonymous", "You can't use the `unpublished` query because you are not authenticated.");
    }

    if(!isAdmin && hasTeamQuery) {
      foreach(string team; selectedTeams) {
        if(teams.canFind(team)) {
          continue;
        }

        auto range = crates.team.getItem(team).exec;
        enforce!CrateNotFoundException(!range.empty, "Can't find the team `" ~ team ~ "`.");
      }
    }

    auto visibilitySelector = selector.or;

    if(parameters.usableByTeam != "") {
      visibilitySelector
        .where(isDefaultKey).equal(true).and
        .where(isPublicKey).equal(true).or;
    }

    if(parameters.default_) {
      visibilitySelector.where(isDefaultKey).equal(true).and;
    }

    if(hasTeamQuery) {
      isPublicQuery = !session.all.teams.canFind(selectedTeams);
      visibilitySelector.where(teamKey).anyOf(ObjectId.fromStringList(selectedTeams)).and;
    }

    if(parameters.edit) {
      if(isAdmin && parameters.all) {
        return selector;
      }

      enforce!ForbiddenException(request.userId != "@anonymous", "You can't edit items.");

      auto teamIds = session.owner.teamsIds;

      static if(field == "features") {
        teamIds ~= session.leader.teamsIds;
      }

      visibilitySelector.where(teamKey).containsAny(teamIds);

      return selector;
    }

    if(isAdmin) {
      if(hasPublishedQuery) {
        visibilitySelector.where(isPublicKey).equal(published);
      }

      return selector;
    }

    if(isPublicQuery && !hasPublishedQuery) {
      visibilitySelector.where(isPublicKey).equal(true);
    }

    immutable addPrivateMaps = teams.length > 0 && !hasTeamQuery;

    if(hasPublishedQuery && published) {
      visibilitySelector.where(isPublicKey).equal(published);
    }

    if(hasPublishedQuery && !published) {
      visibilitySelector.where(isPublicKey).equal("__ignore");
    }

    if(addPrivateMaps) {
      auto privateVisibility = visibilitySelector.or;

      privateVisibility.where(teamKey).containsAny(session.all.teamsIds);

      if(parameters.default_) {
        privateVisibility.where(isDefaultKey).equal(true);
      }

      if(hasPublishedQuery) {
        privateVisibility.where(isPublicKey).equal(published);
      }
    }

    return selector;
  }

  /// Middleware applied for create routes
  @create
  void create(HTTPServerRequest req) {
    enforce!CrateValidationException(req.json.type == Json.Type.object, "You must pass an JSON object.");
    auto request = RequestUserData(req);
    string key = getRestApiKey(req.json);

    enforce!CrateValidationException(visibilityKey in req.json[key], "The `" ~ visibilityKey ~ "` object is missing.");
    enforce!CrateValidationException("team" in req.json[key][visibilityKey], "The `" ~ teamKey ~"` value is missing.");

    if("isDefault" !in req.json[key][visibilityKey]) {
      req.json[key][visibilityKey]["isDefault"] = false;
    }

    if("isPublic" !in req.json[key][visibilityKey]) {
      req.json[key][visibilityKey]["isPublic"] = false;
    }

    if(req.json[key][visibilityKey]["isDefault"] == true) {
      enforce!CrateValidationException(req.json[key][visibilityKey]["isPublic"] == true,
        "You can't create private default items.");
    }

    if(request.isAdmin) {
      return;
    }

    enforce!ForbiddenException(req.json[key][visibilityKey]["isDefault"] == false,
      "You don't have enough rights to add a default item.");

    auto session = request.session(crates);

    string team = req.json[key][visibilityKey]["team"].to!string;

    static if(allowPublicContributions) {
      if(team) {
        enforce!ForbiddenException(session.publicTeams.canFind(team) || session.all.teams.canFind(team), "There is no team with id `" ~ team ~ "`.");
      }
    } else {
      if(!session.all.teams.canFind(team)) {
        enforce!ForbiddenException(session.owner.teams.canFind(team), "You can't add an item for the team `" ~ team ~ "`.");
      }

      static if(allowMembersAdd) {
        enforce!ForbiddenException(!session.guest.teams.canFind(team), "You don't have enough rights to add an item.");
      } else {
        enforce!ForbiddenException(session.owner.teams.canFind(team), "You don't have enough rights to add an item.");
      }
    }
  }

  /// Filter that for update requests returns only the logged user maps
  /// It denies adding/editing/removing maps if they are not owned
  // @put @update @delete_
  @any
  void update(HTTPServerRequest req) {
    if(req.method == HTTPMethod.GET) {
      return;
    }

    scope request = RequestUserData(req);
    string key;

    bool hasPayload = req.method != HTTPMethod.DELETE;

    auto originalItem = items.getItem(request.itemId).exec.front;

    if(originalItem[visibilityKey].type != Json.Type.object) {
      originalItem[visibilityKey] = Json.emptyObject;
    }

    if(originalItem[visibilityKey]["isDefault"].type != Json.Type.bool_) {
      originalItem[visibilityKey]["isDefault"] = false;
    }

    if(hasPayload) {
      key = getRestApiKey(req.json);

      if(originalItem[visibilityKey].type != Json.Type.object) {
        originalItem[visibilityKey] = Json.emptyObject;
      }

      if(req.json[key][visibilityKey].type != Json.Type.object) {
        req.json[key][visibilityKey] = originalItem[visibilityKey];
      }

      if("isDefault" !in req.json[key][visibilityKey]) {
        req.json[key][visibilityKey]["isDefault"] = originalItem[visibilityKey]["isDefault"];
      }

      if("isPublic" !in req.json[key][visibilityKey]) {
        req.json[key][visibilityKey]["isPublic"] = originalItem[visibilityKey]["isPublic"];
      }

      if("team" !in req.json[key][visibilityKey]) {
        req.json[key][visibilityKey]["team"] = originalItem[visibilityKey]["team"];
      }
    }

    if(request.isAdmin) {
      return;
    }

    if(hasPayload) {
      enforce!ForbiddenException(req.json[key][visibilityKey]["isDefault"] == originalItem[visibilityKey]["isDefault"],
          "You don't have enough rights to change the `isDefault` flag.");
    }

    scope session = request.session(crates);

    mixin(`auto allItems = session.all.` ~ field ~ `;`);

    if("info" in originalItem && request.userId != "" && allItems.canFind(request.itemId)) {
      if(originalItem["info"]["originalAuthor"].to!string == request.userId) {
        return;
      }

      if(originalItem["info"]["author"].to!string == request.userId) {
        return;
      }
    }

    mixin(`auto itemList = session.owner.` ~ field ~ `;`);
    static if(allowLeadersEdit) {
      mixin(`itemList ~= session.leader.` ~ field ~ `;`);
    }

    if(allItems.canFind(request.itemId)) {
      immutable action = req.method == HTTPMethod.DELETE ? "remove" : "edit";
      enforce!ForbiddenException(itemList.canFind(request.itemId), "You don't have enough rights to " ~ action ~ " `" ~ request.itemId ~ "`.");
    } else {
      enforce!CrateNotFoundException(itemList.canFind(request.itemId), "Item `" ~ request.itemId ~ "` not found.");
    }
  }

  @mapper
  void mapper(HTTPServerRequest req, ref Json item) @trusted nothrow {
    try {
      scope request = RequestUserData(req);
      auto session = request.session(crates);

      if(item[visibilityKey].type != Json.Type.object) {
        item[visibilityKey] = Json.emptyObject;
      }

      if("canEdit" !in item) {
        static if(field == "features" || allowLeadersEdit) {
          item.editableByTeamLeadersAndOwners(session);
        } else {
          item.editableByTeamOwners(session);
        }
      }
    } catch(Exception e) {}
  }

  void canViewMapper(HTTPServerRequest req, ref Json item) {
    if("canEdit" in item && item["canEdit"]) {
      item["canView"] = true;
      return;
    }

    if(item["visibility"] == 1) {
      item["canView"] = true;
      return;
    }

    if("computedVisibility" !in item) {
      return;
    }

    scope request = RequestUserData(req);
    auto session = request.session(crates);

    auto team = item["computedVisibility"]["team"].to!string;

    item["canView"] = session.all.teams.canFind(team);
  }
}

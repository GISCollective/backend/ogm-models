/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.filter.indirectvisibility;

import gis_collective.hmq.log;

import ogm.crates.all;
import ogm.rights;
import ogm.http.request;

import vibe.http.router;
import vibe.data.json;

import crate.base;
import crate.json;
import crate.error;
import crate.http.request;
import crate.lazydata.base;

import vibe.http.server;
import vibe.data.json;

import std.algorithm;
import std.array : array;
import std.datetime : seconds;

alias ObjectId_ = crate.base.ObjectId;

///
class IndirectVisibility {
  struct Parameters {
    @describe("If it's true, only the items that the user can edit will be returned.")
    @example("true", "Get only the items that the user can edit.")
    @example("false", "Get all the items that the user can view.")
    bool edit;

    @describe("If it's set, only the items that bellongs to the map will be returned.")
    string map;

    @describe("If it's set to `1`, only the published items are returned. If is set to `0` then only unpublished items wil be returned")
    @example("1", "Get all the public items.")
    @example("0", "Get all the private items.")
    @example("-1", "Get all the pending items.")
    string visibility;

    @describe("If it's true, and the user is an admin, all items will be returned.")
    @example("true", "Get all the items.")
    @example("false", "Get only the items that belongs to the current user.")
    bool all;

    @describe("Get all the items created by the user with the provided id.")
    string author;

    @describe("If is set to true, only features that should be visible on the main map will be returned.")
    @example("true", "Show only features related to maps that are visible on the main map.")
    bool onMainMap;

    @describe("If is set to true, only features that belongs to indexed maps will be returned.")
    @example("true", "Show only features related to maps that have isIndex = true.")
    bool onlyIndexes;
  }

  bool pendingMode;

  private {
    OgmCrates crates;
    CrateAccess items;

    LazyField!(ObjectId[]) publicMaps;
    LazyField!(ObjectId[]) indexedMaps;
    LazyField!(ObjectId[]) publicUnindexedMaps;
  }

  LazyField!(ObjectId[]) hiddenOnMainMap;
  LazyField!(ObjectId[]) visibleOnMainMap;

  ///
  this(OgmCrates crates, CrateAccess items) {
    this.crates = crates;
    this.items = items;

    this.publicMaps = LazyField!(ObjectId[])({
      return crates.map
        .get
        .where("visibility.isPublic").equal(true)
        .and
        .exec
        .map!(a => ObjectId(a["_id"]))
        .array;
    }, 10.seconds);

    this.indexedMaps = LazyField!(ObjectId[])({
      return crates.map
        .get
        .where("isIndex").equal(true)
        .and
        .exec
        .map!(a => ObjectId(a["_id"]))
        .array;
    }, 10.seconds);

    this.publicUnindexedMaps = LazyField!(ObjectId[])({
      auto indexedIds = indexedMaps.compute;

      return this.publicMaps.compute.filter!(id => !indexedIds.canFind(id))
        .array;
    }, 10.seconds);

    this.hiddenOnMainMap = LazyField!(ObjectId[])({
      return crates.map.get
        .or.where("hideOnMainMap").equal(true)
        .or.where("isIndex").equal(true)
        .and
        .withProjection(["_id"])
        .exec
        .map!(a => ObjectId(a["_id"]))
        .array;
    }, 10.seconds);

    this.visibleOnMainMap = LazyField!(ObjectId[])({
      return crates.map.get
        .where("hideOnMainMap").equal(false).and
        .where("isIndex").equal(false).and
        .withProjection(["_id"])
        .exec
        .map!(a => ObjectId(a["_id"]))
        .array;
    }, 20.seconds);
  }

  ObjectId[] filterOnMainMap(ObjectId[] idList) {
    ObjectId[] newList;
    newList.reserve(idList.length);

    auto computedList = hiddenOnMainMap.compute;

    foreach(id; idList) {
      if(!computedList.canFind(id)) {
        newList ~= id;
      }
    }

    return newList;
  }

  IQuery readableAdminItems(IQuery selector, Parameters parameters, RequestUserData request) {
    if(parameters.onMainMap) {
      selector
        .where("maps")
        .anyOf(this.visibleOnMainMap.compute);
    }

    if(parameters.map != "") {
      selector.where("maps").arrayContains(ObjectId(parameters.map));
    } else if(parameters.onlyIndexes) {
      selector.where("maps").anyOf(indexedMaps.compute);
    }

    return selector;
  }

  /// Select the sites that the user can read
  IQuery readableItems(IQuery selector, Parameters parameters, RequestUserData request) {
    if(request.isAdmin) {
      return readableAdminItems(selector, parameters, request);
    }

    auto visibilitySelector = selector.or;

    setupPublicVisibility(visibilitySelector);

    ObjectId[] accessibleMapsIds = request.isAdmin || request.hasItemId ? publicMaps.compute : publicUnindexedMaps.compute;

    auto userMaps = parameters.map == ""
      ? request.session(crates).all.mapsIds
      : request.session(crates).all.searchMapId(parameters.map);

    if(parameters.map != "") {
      auto mapId = ObjectId(parameters.map);
      accessibleMapsIds = accessibleMapsIds.canFind(mapId) ? [mapId] : [];
    }

    if(parameters.onlyIndexes) {
      accessibleMapsIds = indexedMaps.compute;
      userMaps = [];
    }

    if(parameters.onMainMap) {
      accessibleMapsIds = filterOnMainMap(accessibleMapsIds);
      userMaps = filterOnMainMap(userMaps);
    }

    visibilitySelector
      .where("maps")
      .anyOf(accessibleMapsIds);

    if(userMaps.length > 0) {
      auto or = visibilitySelector.or;

      or.where("maps").anyOf(userMaps);

      if(parameters.author != "") {
        or.where("info.originalAuthor").equal(parameters.author);
      }
    }

    return selector;
  }

  private void setupPublicVisibility(T)(T selector) {
    if(pendingMode) {
      selector.where("visibility").anyOf([-1, 1]);
      return;
    }

    selector.where("visibility").equal(1);
  }

  /// Select the published sites that the user can read
  IQuery publishedItems(IQuery selector, Parameters parameters, RequestUserData request) {
    setupPublicVisibility(selector);

    if(request.isAdmin) {
      if(parameters.map != "") {
        selector.where("maps").arrayContains(ObjectId(parameters.map));
      }

      return selector;
    }

    ObjectId[] accessibleMapsIds;

    if(parameters.map == "") {
      accessibleMapsIds = publicMaps.compute;
      auto userMaps = request.session(crates).all.mapsIds;

      accessibleMapsIds.reserve(accessibleMapsIds.length + userMaps.length);

      foreach(ref id; userMaps) {
        if(!accessibleMapsIds.canFind(id)) {
          accessibleMapsIds ~= id;
        }
      }

    } else {
      accessibleMapsIds = request.session(crates).all.searchMapId(parameters.map);
    }

    selector
      .where("maps")
      .anyOf(accessibleMapsIds);

    return selector;
  }

  /// Select the sites that the user can edit
  IQuery editableItems(IQuery selector, Parameters parameters, RequestUserData request) {
    if(request.isAdmin && parameters.all) {
      return selector;
    }

    string[] userMaps = request.session(crates).all.maps;
    if(parameters.map != "") {
      userMaps = userMaps.filter!(a => a == parameters.map).array;
    }

    selector.where("maps")
      .anyOf(userMaps.map!(a => ObjectId(a)).array);

    return selector;
  }

  /// Filter that for get requests returns only the public and the logged user maps
  /// It denies adding/editing/removing maps if they are not owned
  @get
  IQuery get(IQuery selector, Parameters parameters, HTTPServerRequest request) {
    scope requestUserData = RequestUserData(request);

    if(!requestUserData.isAuthenticated && parameters.visibility != "") {
      throw new ForbiddenException("You can't use the `visibility` parameter, without an auth token");
    }

    if(parameters.visibility != "" && parameters.visibility != "0" && parameters.visibility != "1" && parameters.visibility != "-1") {
      throw new ForbiddenException("You provided an invalid value for the `visibility` parameter.");
    }

    if(parameters.author != "") {
      selector.where("info.originalAuthor").equal(parameters.author);
    }

    if(parameters.edit) {
      selector = this.editableItems(selector, parameters, requestUserData);
    } else {
      selector = this.readableItems(selector, parameters, requestUserData);
    }

    if(parameters.visibility == "1") {
      selector = this.publishedItems(selector, parameters, requestUserData);
    }

    if(parameters.visibility == "0") {
      selector = selector
        .where("visibility").equal(0)
        .and;
    }

    if(parameters.visibility == "-1") {
      selector = selector
        .where("visibility").equal(-1)
        .and;
    }

    return selector;
  }

  /// Middleware applied for create routes
  @create
  void create(HTTPServerRequest req) {
    scope request = RequestUserData(req);
    string key = getRestApiKey(req.json);

    static foreach(fieldName; ["canEdit", "issueCount", "unmasked"]) {
      enforce!CrateValidationException(
        fieldName !in req.json[key],
        "The item can not contain the " ~ fieldName ~ " field.");
    }

    scope rights = request.session(crates).createItemRights(req.json[key], publicMaps.compute.map!(a => a.toString).array);
    rights.handle;

    if("contributors" !in req.json[key]) {
      req.json[key]["contributors"] = [ Json(request.userId) ];
    }

    if(key == "feature") {
      auto maps = req.json[key]["maps"];

      if(maps.length > 0) {
        auto addFeaturesAsPending = crates.map.getItem(maps[0].to!string).withProjection(["addFeaturesAsPending"]).exec.front["addFeaturesAsPending"] == true;

        if(addFeaturesAsPending) {
          req.json[key]["visibility"] = -1;
        }
      }
    }
  }

  ///
  @patch @replace
  void patch(HTTPServerRequest req) {
    auto request = RequestUserData(req);
    auto session = request.session(crates);
    string key = getRestApiKey(req.json);

    static foreach(fieldName; ["canEdit", "issueCount", "unmasked"]) {
      enforce!CrateValidationException(
        fieldName !in req.json[key],
        "The item can not contain the " ~ fieldName ~ " field.");
    }

    auto originalItem = items.getItem(request.itemId).exec.front;

    if(!request.isAdmin && "maps" in req.json[key]) {
      if(originalItem["maps"][].map!(a => session.all.maps.canFind(a)).canFind(false)) {
        throw new CrateNotFoundException("Item `" ~ request.itemId ~ "` not found.");
      }

      if(req.json[key]["maps"][].map!(a => session.all.maps.canFind(a)).canFind(false)) {
        throw new CrateNotFoundException("Map not found.");
      }
    }

    if("contributors" !in originalItem) {
      originalItem["contributors"] = Json.emptyArray;
    }

    req.json[key]["contributors"] = originalItem["contributors"];

    scope rights = session.replaceItemRights(req.json[key]);
    rights.handle;

    if(!req.json[key]["contributors"][].map!(a => a.to!string).canFind(request.userId)) {
      req.json[key]["contributors"] ~= Json(request.userId);
    }
  }

  /// Filter applied when sites are edited or deleted
  @update @delete_
  IQuery update(IQuery selector, HTTPServerRequest req) {
    auto request = RequestUserData(req);

    if(!request.isAdmin) {
      selector = this.editableItems(selector, Parameters(), request);
    }

    return selector;
  }

  ///
  @delete_
  void delete_(HTTPServerRequest req) {
    auto request = RequestUserData(req);

    if(request.isAdmin) {
      return;
    }

    auto item = items.getItem(request.itemId).exec.front;
    auto maps = item["maps"].byValue.map!(a => a.to!string).array;

    scope rights = request.session(crates).deleteItemRights(maps);

    if(rights !is null) {
      auto exception = rights.validate;

      if(exception !is null) {
        throw exception;
      }
    }
  }

  @mapper
  void mapper(HTTPServerRequest req, ref Json result) {
    auto request = RequestUserData(req);
    auto session = request.session(crates);

    if(request.isAdmin) {
      result["canEdit"] = true;
      result["canView"] = true;

      return;
    }

    string[] itemMaps;

    if("maps" in result) {
      itemMaps = result["maps"][].map!(a => a.to!string).array;
    }

    auto userMaps = session.owner.maps ~ session.leader.maps;
    auto allMaps = session.all.maps;

    if(userMaps.length > 0) {
      result["canEdit"] = itemMaps
        .map!(a => userMaps.canFind(a))
        .canFind(true);
    }

    if(session.all.maps.length > 0) {
      result["canView"] = itemMaps
        .map!(a => allMaps.canFind(a))
        .canFind(true);
    }

    auto memberMaps = session.member.maps;
    canEditAsMember(result, request.userId, memberMaps);
  }

  void canEditAsMember(ref Json record, string userId, const ref string[] memberMaps) {
    if(memberMaps.length == 0) {
      return;
    }

    if(record["canEdit"].type == Json.Type.bool_ && record["canEdit"] == true) {
      return;
    }

    auto itemMaps = record["maps"][].map!(a => a.to!string).array;
    auto isMember = itemMaps
      .map!(a => memberMaps.canFind(a))
      .canFind(true);

    if(!isMember) {
      return;
    }

    if("contributors" !in record) {
      return;
    }

    record["canEdit"] = record["contributors"][].map!(a => a.to!string)
      .canFind(userId);
  }
}

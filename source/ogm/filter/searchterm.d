/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.filter.searchterm;

import crate.auth.usercollection;
import crate.base;
import crate.error;
import crate.collection.memory;
import crate.http.queryParams;

import vibe.http.router;
import vibe.data.json;

import ogm.crates.all;
import std.array;
import std.algorithm;
import std.range;
import std.exception;
import std.conv;

///
class SearchTermFilter {

  struct Parameters {
    @describe("If is set, only items that contain the substring in their name. The search is case insensitive.")
    @example("Natur", "It will return all items which have `natur` in their name.")
    string term;
  }

  IQuery getWrapper(IQuery selector, HTTPServerRequest request) {
    auto params = request.query.parseQueryParams!Parameters;

    return get(selector, params);
  }

  /// Filter that for get requests will filter items that contain the term in their name
  @get
  IQuery get(IQuery selector, Parameters parameters) {
    if(parameters.term != "") {
      auto or = selector.or;

      or.where("name").like(parameters.term).
      or.where("title").like(parameters.term).
      or.where("description").like(parameters.term).
      or.where("description.blocks").arrayFieldLike("data.text", parameters.term);
    }

    return selector;
  }
}

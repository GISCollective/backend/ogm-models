/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.filter.searchtag;

import crate.base;
import crate.error;
import crate.collection.memory;
import crate.http.queryParams;

import vibe.http.router;
import vibe.data.json;

import ogm.crates.all;
import std.array;
import std.algorithm;
import std.range;
import std.exception;
import std.conv;

///
class SearchTagFilter {

  struct Parameters {
    @describe("If is set, it will return all items that contain the #tag in the description. You `don't` need to add the `#` char.")
    @example("mytag", "It will return all items which have `#mytag` in their description.")
    string tag;
  }

  IQuery getWrapper(IQuery selector, HTTPServerRequest request) {
    auto params = request.query.parseQueryParams!Parameters;

    return get(selector, params);
  }

  /// Filter that for get requests will filter items that contain the tag in their description
  @get
  IQuery get(IQuery selector, Parameters parameters) {
    if(parameters.tag != "") {
      auto or = selector.or;

      or.where("description").like("#" ~ parameters.tag).
      or.where("description.blocks").arrayFieldLike("data.text", "#" ~ parameters.tag);
    }

    return selector;
  }
}

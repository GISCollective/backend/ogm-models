/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.filter.mapFilter;

import gis_collective.hmq.log;

import ogm.crates.all;
import ogm.rights;
import ogm.http.request;

import vibe.http.router;
import vibe.data.json;

import crate.base;
import crate.json;
import crate.error;
import crate.http.request;
import crate.lazydata.base;

import vibe.http.server;
import vibe.data.json;

import std.algorithm;
import std.array : array;
import std.datetime : seconds;

alias ObjectId_ = crate.base.ObjectId;

///
class MapFilter {
  struct Parameters {
    @describe("If it's set, only the items that bellongs to the map will be returned.")
    string map;

    @describe("If is set to true, only features that should be visible on the main map will be returned.")
    @example("true", "Show only features related to maps that are visible on the main map.")
    bool onMainMap;

    @describe("If is set to true, only features that belongs to indexed maps will be returned.")
    @example("true", "Show only features related to maps that have isIndex = true.")
    bool onlyIndexes;
  }

  private {
    OgmCrates crates;

    LazyField!(ObjectId[]) publicMaps;
    LazyField!(ObjectId[]) indexedMaps;
    LazyField!(ObjectId[]) publicUnindexedMaps;
  }

  LazyField!(ObjectId[]) hiddenOnMainMap;
  LazyField!(ObjectId[]) visibleOnMainMap;

  ///
  this(OgmCrates crates) {
    this.crates = crates;

    this.publicMaps = LazyField!(ObjectId[])({
      return crates.map
        .get
        .where("visibility.isPublic").equal(true)
        .and
        .exec
        .map!(a => ObjectId(a["_id"]))
        .array;
    }, 10.seconds);

    this.indexedMaps = LazyField!(ObjectId[])({
      return crates.map
        .get
        .where("isIndex").equal(true)
        .and
        .exec
        .map!(a => ObjectId(a["_id"]))
        .array;
    }, 10.seconds);

    this.publicUnindexedMaps = LazyField!(ObjectId[])({
      auto indexedIds = indexedMaps.compute;

      return this.publicMaps.compute.filter!(id => !indexedIds.canFind(id))
        .array;
    }, 10.seconds);

    this.hiddenOnMainMap = LazyField!(ObjectId[])({
      return crates.map.get
        .or.where("hideOnMainMap").equal(true)
        .or.where("isIndex").equal(true)
        .and
        .withProjection(["_id"])
        .exec
        .map!(a => ObjectId(a["_id"]))
        .array;
    }, 10.seconds);

    this.visibleOnMainMap = LazyField!(ObjectId[])({
      return crates.map.get
        .where("hideOnMainMap").equal(false).and
        .where("isIndex").equal(false).and
        .withProjection(["_id"])
        .exec
        .map!(a => ObjectId(a["_id"]))
        .array;
    }, 20.seconds);
  }

  ObjectId[] filterOnMainMap(ObjectId[] idList) {
    ObjectId[] newList;
    newList.reserve(idList.length);

    auto computedList = hiddenOnMainMap.compute;

    foreach(id; idList) {
      if(!computedList.canFind(id)) {
        newList ~= id;
      }
    }

    return newList;
  }

  /// Select the sites that the user can read
  @getList
  IQuery get(IQuery selector, Parameters parameters, HTTPServerRequest req) {
    if(parameters.onlyIndexes) {
      selector.where("maps").anyOf(indexedMaps.compute);
      return selector;
    }

    if(parameters.onMainMap) {
      selector.where("maps").not.anyOf(hiddenOnMainMap.compute);
      return selector;
    }

    if(parameters.map == "") {
      selector.where("maps").not.anyOf(indexedMaps.compute);
      return selector;
    }

    auto request = RequestUserData(req);
    auto session = request.session(crates);

    auto mapId = ObjectId(parameters.map);

    selector.where("maps").arrayContains(mapId);

    return selector;
  }
}

/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.batch.log;

import std.datetime;
import ogm.models.logEntry;

import crate.base;

import vibe.core.log : logInfo, logError;
import vibe.data.json;

interface IBatchLog {
  void info(string message);
  void error(string message);
  void opCall(string message);
}

class VibeBatchLog : IBatchLog {
  void info(string message) {
    logInfo(message);
  }

  void error(string message) {
    logError(message);
  }

  void opCall(string message) {
    info(message);
  }
}

class MemoryBatchLog : IBatchLog {
  string log;

  void info(string message) {
    log ~= message ~ "\n";
  }

  void error(string message) {
    log ~= message ~ "\n";
  }

  void opCall(string message) {
    info(message);
  }
}

class CrateBatchLog : IBatchLog {
  private {
    Crate!LogEntry log;
    string batchJobId;
  }

  this(Crate!LogEntry log, string batchJobId) {
    this.log = log;
    this.batchJobId = batchJobId;
  }

  void info(string message) {
    auto entry = Json.emptyObject;

    entry["timestamp"] = Clock.currTime.toUnixTime;
    entry["reference"] = batchJobId;
    entry["level"] = "info";
    entry["message"] = message;

    log.addItem(entry);
  }

  void error(string message) {
    auto entry = Json.emptyObject;

    entry["timestamp"] = Clock.currTime.toUnixTime;
    entry["reference"] = batchJobId;
    entry["level"] = "error";
    entry["message"] = message;

    log.addItem(entry);
  }

  void opCall(string message) {
    info(message);
  }
}
/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.selectors.icons;

import std.algorithm;
import std.array;

import crate.base;
import ogm.models.icon;


struct IconSelector {
  string category;
  string subcategory;
  string iconSet;

  string[] idList;

  ///
  void apply(IQuery selector) {
    if(category != "" && category != "not set") {
      selector.where("category").equal(category);
    }

    if(category == "not set") {
      selector.where("category").equal("");
    }

    if(subcategory != "" && subcategory != "not set") {
      selector.where("subcategory").equal(subcategory);
    }

    if(subcategory == "not set") {
      selector.where("subcategory").equal("");
    }

    if(iconSet != "") {
      ObjectId[] setList = iconSet.split(",").map!(a => ObjectId.fromString(a)).array;
      selector.where("iconSet").containsAny(setList);
    }

    if(idList.length > 0) {
      selector.where("_id").anyOf(idList.map!(a => ObjectId.fromString(a)).array);
    }
  }

  ///
  ObjectId[] ids(IQuery selector) {
    this.apply(selector);

    return ObjectId.fromJsonList(selector.exec.array);
  }
}
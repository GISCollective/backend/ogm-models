/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.meta;

import ogm.crates.all;

import vibe.data.json;
import std.algorithm;
import std.random;

class MetaQuery {

  private {
    OgmCrates crates;
    string type;
    string model;
    static Random rnd;
  }

  static this() {
    MetaQuery.rnd = Random(unpredictableSeed);
  }

  this(string model, string type, OgmCrates crates) {
    this.crates = crates;
    this.model = model;
    this.type = type;
  }

  @delete_
  void delete_(string id) {
    auto metaRange = crates.meta.get
      .where("type").equal(this.type).and
      .where("itemId").equal(id).and
      .exec;

    if(metaRange.empty) {
        return;
    }

    foreach(meta; metaRange) {
      crates.meta.deleteItem(meta["_id"].to!string);
    }
  }

  auto get() {
    return crates.meta.get
      .where("type").equal(this.type).and;
  }

  Meta get(string id, bool forceNewMeta = false) {
    auto metaCount = crates.meta.get
      .where("type").equal(this.type).and
      .where("itemId").equal(id).and
      .size;

    if(metaCount == 0 || forceNewMeta) {
      Meta meta;
      meta.model = this.model;
      meta.type = this.type;
      meta.itemId = id;
      meta.changeIndex = -1;

      auto result = crates.meta.addItem(meta.serializeToJson);

      return result.deserializeJson!Meta;
    }

    return crates.meta.get
      .where("model").equal(this.model).and
      .where("type").equal(this.type).and
      .where("itemId").equal(id).and
      .exec.front
      .deserializeJson!Meta;
  }
}

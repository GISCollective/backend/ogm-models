/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.message;

import ogm.crates.all;

import crate.base;
import vibe.data.json;
import std.algorithm;
import std.random;
import gis_collective.hmq.broadcast.base;
import ogm.contentBlocks;
import handlebars.tpl;
import std.exception;
import std.string;

Message messageFromArticle(Crate!Article article, string slug, string defaultSlug = "") {
  Message message;
  auto range = article.get
      .where("slug").equal(slug)
      .and.exec;

    if(range.empty && defaultSlug != "") {
      return messageFromArticle(article, defaultSlug);
    }

    if(range.empty) {
      message.text = "no message " ~ slug;
      message.html = "no article for " ~ slug;
      message.subject = slug;

      return message;
    }

    auto record = range.front;
    auto content = record["content"];

    message.subject = record["title"].to!string;
    message.text = content.toTextContent(true);
    message.html = content.toHtmlContent(true);

  return message;
}

void fill(T)(ref Message message, T controller) {
  message.html = render!T(message.html, controller);
  message.text = render!T(message.text, controller);
  message.subject = render!T(message.subject, controller);
}

void send(Crate!Message messageCrate, Message message, IBroadcast broadcast) {
  auto notification = Json.emptyObject;

  notification["id"] = messageCrate.addItem(message.serializeToJson)["_id"];

  broadcast.push("notifications.message", notification);
}

string fetchEmail(OgmCrates crates, string id) {
  enforce(id.isObjectId, "The id " ~ id ~ " is not an ObjectId.");

  auto range = crates.user.get.where("_id").equal(ObjectId(id)).and.exec;

  enforce(!range.empty, "The user " ~ id ~ " does not exist.");

  return range.front["email"].to!string;
}

string fetchName(OgmCrates crates, string id) {
  if(!id.isObjectId) {
    return "there";
  }

  auto range = crates.userProfile.get.where("_id").equal(ObjectId(id)).and.exec;

  if(range.empty) {
    return "there";
  }

  return range.front["firstName"].to!string;
}

string getPreference(OgmCrates crates, string name) {
  auto range = crates.preference.get
    .where("name").equal(name)
    .and.exec;

  if(range.empty) {
    return "---";
  }

  return range.front["value"].to!string;
}

string getRegisterUrl(OgmCrates crates, string defaultValue) {
  auto value = getPreference(crates, "register.url").split("/")[0];

  if(value.startsWith("http://") || value.startsWith("https://")) {
    return value;
  }

  return defaultValue;
}
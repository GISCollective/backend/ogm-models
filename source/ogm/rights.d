/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.rights;

import std.algorithm;
import std.array;
import ogm.http.request;
import vibe.data.json;
import ogm.crates.all;
import crate.error;
import std.exception;
import vibe.http.server;

import crate.error;

version(unittest) {
  import fluent.asserts;
}

interface IRights {
  Exception validate();
}

void handle(IRights rights) {
  if(rights is null) {
    return;
  }

  auto exception = rights.validate;

  if(exception !is null) {
    throw exception;
  }
}

/// Check if a item can be deleted
class DeleteItemRights : IRights {
  string id;
  ///
  bool hasOwnerMaps;
  ///
  bool hasLeaderMaps;

  ///
  Exception validate() {
    if(hasOwnerMaps || hasLeaderMaps) {
      return null;
    }

    return new CrateValidationException("Item `" ~ id ~ "` can not be deleted.");
  }
}

/// A item cannot be deleted if the user is not owner or leader
unittest {
  auto deleteItemRights = new DeleteItemRights();

  deleteItemRights.id = "1";
  deleteItemRights.hasOwnerMaps = false;
  deleteItemRights.hasLeaderMaps = false;

  deleteItemRights.validate.msg.should.equal("Item `1` can not be deleted.");
}

/// A item can be deleted if the user is an owner
unittest {
  auto deleteItemRights = new DeleteItemRights();

  deleteItemRights.hasOwnerMaps = true;
  deleteItemRights.hasLeaderMaps = false;

  deleteItemRights.validate.should.beNull;
}

/// A item can be deleted if the user is a leader
unittest {
  auto deleteItemRights = new DeleteItemRights();

  deleteItemRights.hasOwnerMaps = false;
  deleteItemRights.hasLeaderMaps = true;

  deleteItemRights.validate.should.beNull;
}

/// A item can be deleted if the user is a leader and owner
unittest {
  auto deleteItemRights = new DeleteItemRights();

  deleteItemRights.hasOwnerMaps = true;
  deleteItemRights.hasLeaderMaps = true;

  deleteItemRights.validate.should.beNull;
}

///
IRights deleteItemRights(UserSession session, const string[] maps) {
  immutable isInUserMaps = session.all.maps.map!(a => maps.canFind(a)).canFind(true);

  enforce!CrateValidationException(isInUserMaps, "You can't delete this feature.");

  auto rights = new DeleteItemRights;
  rights.id = session.request.itemId;
  rights.hasOwnerMaps = session.owner.maps.map!(a => maps.canFind(a)).canFind(true);
  rights.hasLeaderMaps = session.leader.maps.map!(a => maps.canFind(a)).canFind(true);

  return rights;
}

///
class CreateMemberItemRights : IRights {
  protected const {
    Json item;
    string[] existingMaps;
  }

  this(const Json item, const string[] maps) {
    this.item = item;
    this.existingMaps = maps;
  }

  Exception validate() {
    if("maps" in item && item["maps"][].map!(a => existingMaps.canFind(a.to!string)).canFind(false)) {
      return new CrateNotFoundException("The map does not exist.");
    }

    return null;
  }
}

///
class CreateItemRights : CreateMemberItemRights {
  this(const Json item, const string[] maps) {
    super(item, maps);
  }

  override Exception validate() {
    auto exception = super.validate;

    if(exception !is null) {
      return exception;
    }

    if("visibility" in item && item["visibility"] != 0) {
      return new CrateValidationException("The item can not be published. Set visibility to 0 or remove it.");
    }

    if("contributors" in item) {
      return new CrateValidationException("The item can not contain the contributors field.");
    }

    return null;
  }
}

/// An anonymous item can be added if contains only public maps or user maps
unittest {
  Json item = `{ "maps": ["1", "2"] }`.parseJsonString;
  auto rights = new CreateItemRights(item, []);

  rights.validate.msg.should.equal("The map does not exist.");

  rights = new CreateItemRights(item, ["1", "2"]);
  rights.validate.should.beNull();

  rights = new CreateItemRights(item, ["1", "3"]);
  rights.validate.msg.should.equal("The map does not exist.");
}

/// An anonymous item can not be published
unittest {
  Json item = `{ "maps": ["1", "2"], "visibility": 1 }`.parseJsonString;
  auto rights = new CreateItemRights(item, ["1", "2"]);
  rights.validate.msg.should.equal("The item can not be published. Set visibility to 0 or remove it.");
}

IRights createItemRights(UserSession session, const Json item, const string[] publicMaps) {
  if(session.request.isAdmin) {
    return null;
  }

  const maps = item["maps"][].map!(a => a.to!string).array;
  immutable hasOwnerMaps = session.owner.maps.map!(a => maps.canFind(a)).canFind(true);
  immutable hasLeaderMaps = session.leader.maps.map!(a => maps.canFind(a)).canFind(true);

  if(hasOwnerMaps || hasLeaderMaps) {
    return null;
  }

  immutable hasMemberMaps = session.member.maps.map!(a => maps.canFind(a)).canFind(true);
  if(hasMemberMaps) {
    return new CreateMemberItemRights(item, session.member.maps);
  }

  return new CreateItemRights(item, publicMaps ~ session.guest.maps);
}


///
class ReplaceItemRights : CreateMemberItemRights {
  const string userId;

  this(const Json item, const string[] maps, const string userId) {
    super(item, maps);
    this.userId = userId;
  }

  override Exception validate() {
    auto exception = super.validate;

    if(exception !is null) {
      return exception;
    }

    if(item["contributors"][].map!(a => a.to!string).canFind(userId)) {
      return null;
    }

    return new CrateValidationException("You don't have enough rights to edit the item.");
  }
}

///
IRights replaceItemRights(UserSession session, const Json item) {
  if(session.request.isAdmin) {
    return null;
  }

  const(string)[] maps;

  if("maps" in item) {
    maps = item["maps"][].map!(a => a.to!string).array;
  }

  immutable hasOwnerMaps = session.owner.maps.map!(a => maps.canFind(a)).canFind(true);
  immutable hasLeaderMaps = session.leader.maps.map!(a => maps.canFind(a)).canFind(true);

  if(hasOwnerMaps || hasLeaderMaps) {
    return null;
  }

  immutable hasMemberMaps = session.member.maps.map!(a => maps.canFind(a)).canFind(true);
  if(hasMemberMaps) {
    return new ReplaceItemRights(item, session.member.maps, session.request.userId);
  }

  return new ReplaceItemRights(item, session.guest.maps, "");
}

void validateMapAccess(OgmCrates crates, Json map, HTTPServerRequest req) {
  auto isPublic = map["visibility"]["isPublic"];

  auto request = RequestUserData(req);
  auto session = request.session(crates);

  enforce!CrateNotFoundException(isPublic || request.isAdmin || session.all.maps.canFind(request.itemId), "The map does not exist.");
}
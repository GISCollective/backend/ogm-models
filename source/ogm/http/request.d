/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.http.request;

import ogm.crates.all;

private import crate.base;
import crate.lazydata.base;
import crate.http.request;

import vibe.data.json;
import vibe.http.router;

import std.conv;
import std.array;
import std.exception;
import std.algorithm;

version(unittest) {
  import fluent.asserts;
}

///
string[] stringListContext(string name, T)(const T context) {
  if(name !in context) {
    return [];
  }

  return context[name].get!(string[]).dup;
}

alias mapsId = stringListContext;
alias VibeContext = typeof(HTTPServerRequest.context);
alias VibeParams = typeof(HTTPServerRequest.params);

struct RequestUserData {
  VibeContext* context;
  VibeParams params;

  this(HTTPServerRequest request) {
    enforce(request !is null, "There is a null reqest!");
    this(&request.context, request.params);
  }

  this(VibeContext *context, VibeParams params) {
    this.context = context;
    this.params = params;
  }

  bool isAdmin() {
    return "isAdmin" in *context ? true : false;
  }

  bool isAuthenticated() {
    return "user_id" in *context ? true : false;
  }

  string userId() @trusted {
    if("user_id" in *context) {
      return (*context)["user_id"].get!string.idup;
    }

    return "@anonymous";
  }

  string userEmail() @trusted {
    enforce("email" in *context, "The email was not set in the request context.");

    return (*context)["email"].get!string.dup.to!string;
  }

  string itemId() @trusted {
    string id = "id" in *context ? (*context)["id"].get!string : params["id"];
    return id;
  }

  bool hasItemId() {
    if("id" in *context) {
      return true;
    }

    if("id" in params) {
      return true;
    }

    return false;
  }

  UserSession session(OgmCrates crates) {
    return UserSession(this, crates);
  }

  UserSession session() {
    return UserSession(this);
  }

  bool hasCache(string key) @trusted {
    return key in *context ? true : false;
  }

  string[] getCache(string key) @trusted {
    enforce(key in *context, "`" ~ key ~ "` is not cached.");
    return (*context)[key].get!(string[]);
  }

  ObjectId[] getCacheId(string key) @trusted {
    enforce(key in *context, "`" ~ key ~ "` is not cached.");
    return (*context)[key].get!(ObjectId[]);
  }

  void setCache(string key, string[] value) @trusted {
    (*context)[key] = value;
  }

  void setCache(string key, ObjectId[] value) @trusted {
    (*context)[key] = value;
  }
}

/// It should check if the value is stored in the vibe context
unittest {
  VibeContext context;
  RequestUserData userData;
  userData.context = &context;

  userData.hasCache("testCache").should.equal(false);

  context["testCache"] = ["1","2"];

  userData.hasCache("testCache").should.equal(true);
  userData.getCache("testCache").should.equal(["1","2"]);
}

/// It should cache a value
unittest {
  VibeContext context;
  RequestUserData userData;
  userData.context = &context;

  userData.hasCache("testCache").should.equal(false);
  userData.setCache("testCache", ["1","2"]);
  userData.hasCache("testCache").should.equal(true);
  userData.getCache("testCache").should.equal(["1","2"]);
}

/// Session that lazely fetches the most used data
struct UserSession {
  private {
    RequestUserData userData;
    OgmCrates crates;
  }

  this(RequestUserData userData, OgmCrates crates) {
    this.userData = userData;
    this.crates = crates;
  }

  this(RequestUserData userData) {
    this.userData = userData;
  }

  RequestUserData request() {
    return userData;
  }

  string[] teams(string key)() @trusted {
    if(!userData.isAuthenticated) {
      return [];
    }

    enum cacheKey = key ~ ".team";

    if(userData.hasCache(cacheKey)) {
      return userData.getCache(cacheKey);
    }

    enforce(crates.team !is null, "The `teams` crate is not set, and the `" ~ cacheKey ~ "` is not set.");

    auto ids = [userData.userId, userData.userEmail];
    auto result = getUserTeams!(key)(crates, ids);
    userData.setCache(cacheKey, result);

    return result;
  }

  string[] publicTeams() @trusted {
    enum cacheKey = "public.team";

    if(userData.hasCache(cacheKey)) {
      return userData.getCache(cacheKey);
    }

    enforce(crates.team !is null, "The `teams` crate is not set, and the `" ~ cacheKey ~ "` is not set.");

    string[] result;

    result = crates.team
      .get
      .where("isPublic").equal(true)
      .and
      .withProjection(["_id"])
      .exec
      .map!(a => a["_id"].to!string)
      .array;

    userData.setCache(cacheKey, result);

    return result;
  }

  auto owner() {
    return UserLevel!"owner"(userData, crates, teams!"owners");
  }

  auto leader() {
    return UserLevel!"leader"(userData, crates, teams!"leaders");
  }

  auto member() {
    return UserLevel!"member"(userData, crates, teams!"members");
  }

  auto guest() {
    return UserLevel!"guests"(userData, crates, teams!"guests");
  }

  auto all() {
    return UserLevel!"all"(userData, crates, this.ownTeams);
  }

  auto ownTeams() {
    return teams!"owners" ~ teams!"leaders" ~ teams!"members" ~ teams!"guests";
  }

  LazyUserProfile profile() {
    if(!userData.isAuthenticated) {
      return LazyUserProfile(Json.emptyObject);
    }

    auto profileRange = crates.userProfile.get.where("_id").equal(ObjectId(userData.userId)).and.exec;

    if(profileRange.empty) {
      return LazyUserProfile(Json.emptyObject);
    }

    auto profile = profileRange.front;

    return LazyUserProfile(profile);
  }
}

struct UserLevel(string key) {
  RequestUserData userData;
  OgmCrates crates;
  string[] teams;

  ObjectId[] teamIds() {
    enum cacheKey = key ~ ".teamsIds";

    if(userData.hasCache(cacheKey)) {
      return userData.getCacheId(cacheKey);
    }

    ObjectId[] ids;
    ids.length = teams.length;

    foreach(index, strId; teams) {
      ids[index] = ObjectId(strId);
    }

    userData.setCache(cacheKey, ids);

    return ids;
  }

  string[] query(string crateName)() {
    if(!userData.isAuthenticated || teams.length == 0) {
      return [];
    }

    enum cacheKey = key ~ ".query." ~ crateName;

    if(userData.hasCache(cacheKey)) {
      return userData.getCache(cacheKey);
    }

    mixin(`auto crate = crates.` ~ crateName ~`;`);
    enforce(crate !is null, "The `crates." ~ crateName ~ "` is null and the `" ~ cacheKey ~ "` is not set.");

    string[] result;

    result = crate
      .get
      .where("team").containsAny(this.teamIds)
      .and
      .withProjection([ "_id" ])
      .exec
      .map!(a => a["_id"].to!string)
      .array;

    userData.setCache(cacheKey, result);

    return result;
  }

  string[] queryVisibility(string crateName, string teamKey = "visibility.team")() {
    enum cacheKey = key ~ ".queryVisibility." ~ crateName;

    if(userData.hasCache(cacheKey)) {
      return userData.getCache(cacheKey);
    }

    mixin(`auto crate = crates.` ~ crateName ~`;`);
    enforce(crate !is null, "The `crates." ~ crateName ~ "` is null and the `" ~ cacheKey ~ "` is not set.");

    string[] result;

    result = crate
      .get
      .where(teamKey).containsAny(this.teamIds)
      .and
      .withProjection([ "_id" ])
      .exec
      .map!(a => a["_id"].to!string)
      .array;

    userData.setCache(cacheKey, result);

    return result;
  }

  ObjectId[] queryIds(string crateName)() {
    if(!userData.isAuthenticated || teams.length == 0) {
      return [];
    }

    enum cacheKey = key ~ ".queryIds." ~ crateName;

    if(userData.hasCache(cacheKey)) {
      return userData.getCacheId(cacheKey);
    }

    ObjectId[] result;
    mixin("auto tmp = this." ~ crateName ~";");

    result.length = tmp.length;

    foreach(index, item; tmp) {
      result[index] = ObjectId(item);
    }

    userData.setCache(key, result);

    return result;
  }

  ObjectId[] searchId(string crateName)(string id) {
    mixin("auto tmp = this." ~ crateName ~";");

    if(tmp.canFind(id)) {
      return [ ObjectId(id) ];
    }

    return [];
  }

  alias maps = queryVisibility!"map";
  alias iconSets = queryVisibility!"iconSet";
  alias baseMaps = queryVisibility!"baseMap";
  alias batchJobs = queryVisibility!"batchJob";
  alias articles = queryVisibility!"article";
  alias sounds = queryVisibility!"sound";
  alias campaigns = queryVisibility!"campaign";
  alias pages = queryVisibility!"page";
  alias newsletters = queryVisibility!"newsletter";
  alias spaces = queryVisibility!"space";
  alias presentations = queryVisibility!"presentation";
  alias dataBindings = queryVisibility!"dataBinding";
  alias searchMeta = queryVisibility!"searchMeta";
  alias calendars = queryVisibility!"calendar";
  alias events = queryVisibility!"event";
  alias features = queryVisibility!("feature", "computedVisibility.team");

  alias teamsIds = queryIds!"teams";
  alias mapsIds = queryIds!"maps";
  alias iconSetsIds = queryIds!"iconSets";
  alias baseMapsIds = queryIds!"baseMaps";
  alias batchJobsIds = queryIds!"batchJobs";
  alias articlesIds = queryIds!"articles";
  alias soundsIds = queryIds!"sounds";
  alias newsletterIds = queryIds!"newsletters";
  alias campaignIds = queryIds!"campaigns";
  alias dataBindingIds = queryIds!"dataBindings";
  alias searchMetaIds = queryIds!"searchMeta";
  alias featuresIds = queryIds!"features";
  alias calendarsIds = queryIds!"calendars";
  alias eventsIds = queryIds!"events";

  alias searchMapId = searchId!"maps";
}

///
void editableByTeamOwners(ref Json result, UserSession session) {
  if(session.request.isAdmin) {
    result["canEdit"] = true;
    return;
  }

  if("info" in result && session.request.userId != "" && session.request.userId != "@anonymous") {
    if(result["info"]["originalAuthor"].to!string == session.request.userId) {
      result["canEdit"] = true;
      return;
    }

    if(result["info"]["author"].to!string == session.request.userId) {
      result["canEdit"] = true;
      return;
    }
  }

  auto teams = session.owner.teams;
  if(teams.length > 0 && result["visibility"].type != Json.Type.object && result["computedVisibility"].type != Json.Type.object) {
    result["canEdit"] = teams.canFind(result["team"].to!string);
    return;
  }

  if(teams.length > 0 && result["visibility"].type == Json.Type.object) {
    result["canEdit"] = teams.canFind(result["visibility"]["team"].to!string);
    return;
  }

  if(session.request.userId != "@anonymous" && "canEdit" !in result) {
    result["canEdit"] = false;
  }
}

void editableByTeamLeadersAndOwners(ref Json result, UserSession session) {
  if(session.request.isAdmin) {
    result["canEdit"] = true;
    return;
  }

  if("info" in result && session.request.userId != "" && session.request.userId != "@anonymous") {
    if(result["info"]["originalAuthor"].to!string == session.request.userId) {
      result["canEdit"] = true;
      return;
    }

    if(result["info"]["author"].to!string == session.request.userId) {
      result["canEdit"] = true;
      return;
    }
  }

  string visibilityKey;

  if(result["visibility"].type == Json.Type.object) {
    visibilityKey = "visibility";
  }

  if(result["computedVisibility"].type == Json.Type.object) {
    visibilityKey = "computedVisibility";
  }

  auto teams = session.owner.teams ~ session.leader.teams;
  if(teams.length > 0 && visibilityKey) {
    auto team = result[visibilityKey]["team"].to!string;
    result["canEdit"] = teams.canFind(team);
    return;
  }

  if(teams.length > 0 && result["visibility"].type == Json.Type.object) {
    result["canEdit"] = teams.canFind(result["visibility"]["team"].to!string);
    return;
  }

  if(session.request.userId != "@anonymous" && "canEdit" !in result) {
    result["canEdit"] = false;
  }
}

string[] getUserTeams(string key)(ref OgmCrates crates, ref string[] userIds) @trusted {
  return crates.team
    .get
    .where(key).anyOf(userIds)
    .and
    .withProjection(["_id"])
    .exec
    .map!(a => a["_id"].to!string)
    .array;
}
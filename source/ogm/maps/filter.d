/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.maps.filter;

import std.algorithm;
import std.array;
import std.datetime;
import ogm.http.request;

import ogm.maps.middlewares.get;
import ogm.maps.middlewares.create;
import ogm.maps.middlewares.update;
import ogm.maps.middlewares.publish;
import ogm.crates.all;

import vibe.http.router;
import vibe.data.json;

import crate.base;
import crate.http.request;

import crate.lazydata.base;
import gis_collective.hmq.log;

///
class MapsFilter {
  GetMapsFilter getMapsFilter;
  CreateMapsFilter createMapsFilter;
  UpdateMapsFilter updateMapsFilter;
  MapPublishRules mapPublishRules;
  MapIsIndexRules mapIsIndexRules;

  LazyField!Json publicDefaultBaseMaps;
  LazyField!(string[]) publicBaseMaps;

  LazyField!Json publicDefaultIconSets;
  LazyField!(string[]) publicIconSets;

  private OgmCrates crates;

  ///
  this(OgmCrates crates) {
    this.getMapsFilter = new GetMapsFilter(crates);
    this.createMapsFilter = new CreateMapsFilter(crates);
    this.updateMapsFilter = new UpdateMapsFilter(crates);
    this.mapPublishRules = new MapPublishRules(crates);
    this.mapIsIndexRules = new MapIsIndexRules(crates);
    this.crates = crates;

    this.publicDefaultBaseMaps = lazyField({
      return crates.baseMap
        .get
        .where("visibility.isPublic").equal(true).and
        .where("visibility.isDefault").equal(true).and
        .withProjection(["_id"])
        .exec
        .map!(a => a["_id"].to!string)
        .array
        .serializeToJson;
    }, 5.seconds);

    this.publicBaseMaps = lazyField({
      return crates.baseMap
        .get
        .where("visibility.isPublic").equal(true).and
        .withProjection(["_id"])
        .exec
        .map!(a => a["_id"].to!string)
        .array;
    }, 5.seconds);

    this.publicDefaultIconSets = lazyField({
      return crates.iconSet
        .get
        .where("visibility.isPublic").equal(true).and
        .where("visibility.isDefault").equal(true).and
        .withProjection(["_id"])
        .exec
        .map!(a => a["_id"].to!string)
        .array
        .serializeToJson;
    }, 5.seconds);

    this.publicIconSets = lazyField({
      return crates.iconSet
        .get
        .where("visibility.isPublic").equal(true).and
        .withProjection(["_id"])
        .exec
        .map!(a => a["_id"].to!string)
        .array;
    }, 5.seconds);
  }

  /// Filter that for get requests returns only the public and the logged user maps
  /// It denies adding/editing/removing maps if they are not owned
  @get
  IQuery get(IQuery selector, GetMapsFilter.Parameters parameters, HTTPServerRequest req) {
    return getMapsFilter.get(selector, parameters, req);
  }

  /// Middleware applied for create routes
  @create
  void create(HTTPServerRequest req) {
    mapPublishRules.check(req);
    mapIsIndexRules.check(req);
    createMapsFilter.create(req);
  }

  /// Filter that for update requests returns only the logged user maps
  /// It denies adding/editing/removing maps if they are not owned
  @replace @patch
  void replace(HTTPServerRequest req) {
    mapPublishRules.check(req);
    mapIsIndexRules.check(req);
    return updateMapsFilter.replace(req);
  }

  /// ditto
  @delete_
  void delete_(HTTPServerRequest req) {
    return updateMapsFilter.delete_(req);
  }


  Json cleanStringList(ref Json list, string[] validIds) @trusted {
    Json[] result;

    foreach (item; list.byValue) {
      if(item.type == Json.Type.string && validIds.canFind(item)) {
        result ~= item;
      }
    }

    return result.serializeToJson;
  }

  @mapper
  mapper(HTTPServerRequest req, ref Json result) {
    scope request = RequestUserData(req);
    scope session = request.session(crates);

    try {
      if("iconSets" !in result || result["iconSets"].type != Json.Type.object) {
        result["iconSets"] = Json.emptyObject;
      }

      if("useCustomList" !in result["iconSets"] || result["iconSets"]["useCustomList"] == false) {
        result["iconSets"]["list"] = publicDefaultIconSets.compute;
      } else {
        result["iconSets"]["list"] = cleanStringList(result["iconSets"]["list"], publicIconSets.compute ~ session.all.iconSets);
      }

    } catch(Exception e) {
      error(e);
    }

    try {
      if("baseMaps" !in result || result["baseMaps"].type != Json.Type.object) {
        result["baseMaps"] = Json.emptyObject;
      }

      if("useCustomList" !in result["baseMaps"] || result["baseMaps"]["useCustomList"] == false) {
        result["baseMaps"]["list"] = publicDefaultBaseMaps.compute;
      }

      result["baseMaps"]["list"] = cleanStringList(result["baseMaps"]["list"], publicBaseMaps.compute ~ session.all.baseMaps);
    } catch(Exception e) {
      error(e);
    }

    if(result["cluster"].type != Json.Type.object) {
      result["cluster"] = `{ "mode": 1, "map": "" }`.parseJsonString;
    }
  }
}

/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.maps.middlewares.get;

import crate.auth.usercollection;
import crate.base;
import crate.error;
import crate.http.request;
import crate.http.queryParams;
import crate.collection.memory;
import geo.json;
import crate.lazydata.base;

import vibe.http.router;
import vibe.data.json;

import ogm.meta;
import ogm.maps.middlewares.base;
import ogm.crates.all;
import ogm.http.request;

import std.array;
import std.string;
import std.algorithm;
import std.range;
import std.exception;
import std.conv;
import std.datetime;
import std.math;

///
class GetMapsFilter : MapsFilter {

  struct Parameters {
    @describe("Get all the items that are inside the provided area. If this parameter is provided, `lon` and `lat` will be ignored.")
    @example("Germany", "Get all the items that were matched to the `Germany` area.")
    string area;

    @describe("Get maps that are market as indexed. They can be used to mask sites or index existing features, based on custom polygons.")
    bool isIndex;

    @describe("Start showing the items ordered by distance from the provided longitude. If you pass `lon` you also need to pass a value for `lat`.")
    @example("13.4063", "Sort items by distance from Berlin.")
    double lon;

    @describe("Start showing the items ordered by distance from the provided latitude. If you pass `lat` you also need to pass a value for `lon`.")
    @example("52.4886", "Sort items by distance from Berlin.")
    double lat;

    @describe("A pair of lon,lat coordinates. If it's present, only the maps containing that coordinates will be returned.")
    double[] containing;

    @describe("If you want to get a faster respose, it's better to not receive the map extents which might be quite large.")
    @example("false", "Receive the area field.")
    @example("true", "Ignore the area field.")
    bool withoutArea;

    @describe("A boolean value if you want to receive the the maps where the current user can or can not contribute.")
    @example("false", "All maps where the current user can not add features as team member.")
    @example("true", "All maps where the current user can add features as team member.")
    string canAdd;

    ///
    bool all;
  }

    ///
  this(OgmCrates crates) {
    super(crates);
  }

  /// Filter that for get requests returns only the public and the logged user maps
  /// It denies adding/editing/removing maps if they are not owned
  IQuery get(IQuery selector, HTTPServerRequest request) {
    auto params = request.query.parseQueryParams!Parameters;

    return get(selector, params, request);
  }

  /// Filter that for get requests returns only the public and the logged user maps
  /// It denies adding/editing/removing maps if they are not owned
  @get
  IQuery get(IQuery selector, Parameters parameters, HTTPServerRequest req) {
    auto userData = RequestUserData(req);
    auto session = userData.session(crates);

    if(parameters.withoutArea) {
      selector.withProjection([
        "_id", "name", "description", "visibility", "cover",
        "iconSets", "tagLine", "startDate", "endDate", "info"
      ]);
    }

    if(!isNaN(parameters.lat) && !isNaN(parameters.lon) && parameters.area == "") {
      selector.where("area").near(parameters.lon, parameters.lat, 0).and;
    }

    if(parameters.area != "") {
      auto selectedIds = getArea(parameters.area);

      if(selectedIds.length == 0) {
        return this.emptySelector;
      }

      selector.where("_id").containsAny(selectedIds).and;
    }

    if(parameters.isIndex) {
      selector.where("isIndex").equal(true).and;
    } else if(!userData.isAdmin) {
      selector.where("isIndex").equal(false).and;
    }

    if(parameters.containing.length >= 2) {
      selector.where("area").containsPoint(parameters.containing[0], parameters.containing[1]).and;
    }

    auto isAll = userData.isAdmin && parameters.all;

    if(parameters.canAdd == "true" && !isAll) {
      selector.where("_id").containsAny(session.all.mapsIds).and;
    }

    if(parameters.canAdd == "true" && isAll) {
      selector.where("_id").containsAny(session.all.mapsIds).and;
    }

    if(parameters.canAdd == "false") {
      selector.where("_id").not.containsAny(session.all.mapsIds).and;
    }

    return selector;
  }
}

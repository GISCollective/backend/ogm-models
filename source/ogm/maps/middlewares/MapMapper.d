/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.maps.middlewares.MapMapper;

import crate.attributes;
import ogm.markdown;
import vibe.data.json;
import gis_collective.hmq.log;

class MapMapper {
  @mapper
  void updateDescription(ref Json item) @trusted nothrow {
    try {
      if(item["description"].type == Json.Type.string) {
        item["description"] = item["description"].to!string.blocksFromMarkdown;
      }

    } catch(Exception e) { error(e); }
  }
}
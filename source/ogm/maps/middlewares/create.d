/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.maps.middlewares.create;

import crate.auth.usercollection;
import crate.base;
import crate.error;
import crate.http.request;
import crate.collection.memory;
import geo.json;
import crate.lazydata.base;

import vibe.http.router;
import vibe.data.json;

import ogm.meta;
import ogm.maps.middlewares.base;
import ogm.crates.all;
import ogm.http.request;

import std.array;
import std.string;
import std.algorithm;
import std.range;
import std.exception;
import std.conv;
import std.datetime;
import std.math;

///
class CreateMapsFilter : MapsFilter {

  ///
  this(OgmCrates crates) {
    super(crates);
  }

  /// Middleware applied for create routes
  @create
  void create(HTTPServerRequest req) {
    scope request = RequestUserData(req);
    scope session = request.session(crates);

    this.checkArea(req);
    this.checkIconSet(req, session);

    if(request.isAdmin) {
      return;
    }

    string team = req.json["map"]["visibility"]["team"].to!string;
    if(!session.owner.teams.canFind(team) && session.all.teams.canFind(team)) {
      auto e = new CrateException("You don't have enough rights to add a map.");
      e.title = "Can not add a new map.";
      e.statusCode = 400;
      throw e;
    }

    enforce!CrateNotFoundException(session.owner.teams.canFind(team), "Can not add a map for team `" ~ team ~ "`.");
  }
}

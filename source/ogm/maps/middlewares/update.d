/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.maps.middlewares.update;

import crate.auth.usercollection;
import crate.base;
import crate.error;
import crate.http.request;
import crate.collection.memory;
import geo.json;
import crate.lazydata.base;

import vibe.http.router;
import vibe.data.json;

import ogm.meta;
import ogm.maps.middlewares.base;
import ogm.crates.all;
import ogm.http.request;
import ogm.models.picture;

import std.array;
import std.string;
import std.algorithm;
import std.range;
import std.exception;
import std.conv;
import std.datetime;
import std.math;

///
class UpdateMapsFilter : MapsFilter {
  ///
  this(OgmCrates crates) {
    super(crates);
  }

    /// It denies adding/editing/removing maps if they are not owned
  void checkRights(string type)(HTTPServerRequest req) {
    scope request = RequestUserData(req);
    scope session = request.session(crates);
    this.checkArea(req);

    if(request.isAdmin) {
      return;
    }

    if(!session.owner.maps.canFind(request.itemId) && session.all.maps.canFind(request.itemId)) {
      throw new ForbiddenException("You don't have enough rights to " ~ type ~" `" ~ request.itemId ~ "`.");
    }

    this.checkIconSet(req, session);

    enforce!CrateNotFoundException(session.owner.maps.canFind(request.itemId), "Item `" ~ request.itemId ~ "` not found.");
  }

  /// It denies adding/editing/removing maps if they are not owned
  @patch @replace
  void replace(HTTPServerRequest req) {
    this.checkRights!"edit"(req);

    if("cover" !in req.json["map"]) {
      return;
    }

    string newCover = req.json["map"]["cover"].to!string;

    auto map = crates.map.getItem(req.params["id"]).exec.front;
    string coverId = map["cover"].to!string;

    if(coverId != newCover) {
      crates.picture.removePicture(coverId);
    }
  }

  /// It denies adding/editing/removing maps if they are not owned
  /// and removes the picture from db
  @delete_
  void delete_(HTTPServerRequest req) {
    this.checkRights!"remove"(req);

    auto map = crates.map.getItem(req.params["id"]).exec.front;
    auto coverId = map["cover"].to!string;

    crates.picture.removePicture(coverId);
  }
}

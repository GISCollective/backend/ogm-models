/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.maps.middlewares.publish;

import vibe.http.server;
import vibe.data.json;

import crate.error;
import crate.base;
import ogm.crates.all;
import ogm.http.request;

///hideOnMainMap

alias MapPublishRules = MapAdminKey!"hideOnMainMap";
alias MapIsIndexRules = MapAdminKey!"isIndex";

class MapAdminKey(string key) {

  OgmCrates crates;

  this(OgmCrates crates) {
    this.crates = crates;
  }

  Json getCurrentMapValue(string id) {
    auto currentValue = Json(false);
    auto map = crates.map.get.where("_id").equal(ObjectId.fromString(id)).and.exec;

    if(map.empty) {
      return currentValue;
    }

    if(key in map.front) {
      currentValue = map.front[key];
    }

    return currentValue;
  }

  void check(HTTPServerRequest req) {
    scope request = RequestUserData(req);

    bool isChanged;
    auto currentValue = Json(false);

    if(request.hasItemId) {
      currentValue = getCurrentMapValue(request.itemId);
    }

    auto newValue = Json(false);

    if(key in req.json["map"]) {
      newValue = req.json["map"][key];
    }

    isChanged = currentValue != newValue;

    if(!request.isAdmin) {
      enforce!ForbiddenException(!isChanged, "You need to be administrator to change or set '" ~ key ~"'.");
    }
  }
}


/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.maps.middlewares.base;

import crate.auth.usercollection;
import crate.base;
import crate.error;
import crate.http.request;
import crate.collection.memory;
import geo.json;
import geo.algorithm;
import crate.lazydata.base;

import vibe.http.router;
import vibe.data.json;

import ogm.meta;
import ogm.crates.all;
import ogm.http.request;

import std.array;
import std.string;
import std.algorithm;
import std.range;
import std.exception;
import std.conv;
import std.datetime;
import std.math;

///
class MapsFilter {
  LazyField!string defaultCover;
  LazyField!string defaultIconSet;
  LazyField!(string[]) publicMaps;

  OgmCrates crates;
  MetaQuery metaQuery;
  IQuery emptySelector;

  Json defaultArea;

  ///
  this(OgmCrates crates) {
    this.crates = crates;
    this.emptySelector = new MemoryQuery();
    this.metaQuery = new MetaQuery("Map", "Map.area", crates);

    this.defaultArea = `{ "type": "Polygon", "coordinates": [[
            [-169,-55 ], [ 180,-55 ], [ 180, 71 ], [-169, 71 ], [-169,-55 ]]]}`.parseJsonString;

    this.publicMaps = LazyField!(string[])({
      return crates.map
        .get
        .where("visibility.isPublic").equal(true)
        .and
        .exec
        .map!(a => a["_id"].to!string)
        .array;
    }, 10.seconds);

    this.defaultCover = LazyField!string({
      return crates.picture.get
        .where("name")
        .equal("default")
        .and.exec
        .front["_id"]
        .to!string;
    });

    this.defaultIconSet = LazyField!string({
      auto range = crates.iconSet.get
        .where("visibility.isDefault")
        .equal(true)
        .and.exec;

      enforce!CrateNotFoundException(!range.empty, "There is no default icon set.");

      return range.front["_id"]
        .to!string;
    }, 10.seconds);
  }

  ObjectId[] getArea(string name) {
    ObjectId[] result;

    auto query = crates.meta.get
      .where("type").equal("Map.area").and
      .where("model").equal("Map").and
      .where("data.names").arrayContains(name.toLower).and.exec;

    if(!query.empty) {
      result = query.map!(a => ObjectId.fromString(a["itemId"].to!string)).array;
    }

    return result;
  }

  protected void checkIconSet(HTTPServerRequest req, UserSession session) {
    if(req.json.type != Json.Type.object || "map" !in req.json) {
      return;
    }

    if("iconSets" !in req.json["map"] || req.json["map"]["iconSets"].type != Json.Type.object) {
      req.json["map"]["iconSets"] = Json.emptyObject;
      req.json["map"]["iconSets"]["useCustomList"] = false;
      req.json["map"]["iconSets"]["list"] = Json.emptyArray;
      return;
    }

    if("list" !in req.json["map"]["iconSets"]) {
      req.json["map"]["iconSets"]["list"] = Json.emptyArray;
    }

    if("useCustomList" !in req.json["map"]["iconSets"]) {
      req.json["map"]["iconSets"]["useCustomList"] = false;
      req.json["map"]["iconSets"]["list"] = Json.emptyArray;
    }

    foreach(jsonId; req.json["map"]["iconSets"]["list"]) {
      auto id = ObjectId.fromJson(jsonId);
      auto iconSetRange = crates.iconSet.get.where("_id").equal(id).and.exec;

      enforce!CrateNotFoundException(!iconSetRange.empty, "Icon set `" ~ id.to!string ~ "` not found.");

      auto iconSet = iconSetRange.front;

      if(iconSet["visibility"]["isPublic"] == true) {
        return;
      }

      enforce!CrateNotFoundException(iconSet["visibility"]["team"] == req.json["map"]["visibility"]["team"],
        "Icon set `" ~ id.to!string ~ "` belongs to a different team.");
    }

  }

  protected void checkArea(HTTPServerRequest req) {
    if(req.json.type != Json.Type.object || "map" !in req.json) {
      return;
    }

    if("area" !in req.json["map"] || req.json["map"]["area"].type != Json.Type.object || "coordinates" !in req.json["map"]["area"]) {
      req.json["map"]["area"] = defaultArea;
      return;
    }

    auto coordinates = req.json["map"]["area"]["coordinates"];

    if(coordinates.type == Json.Type.array && coordinates.length > 0) {
      double[2][] points = (cast(Json[]) coordinates[0])
        .map!(a => cast(Json[]) a)
        .map!(a => cast(double[2]) a.map!(b => b.to!double).array[0..2]).array;

          req.json["map"]["area"]["coordinates"][0] = rewind(points).map!(a =>
            Json(
                a[0..$].map!(b => Json(b)).array)).array;
    }
  }
}

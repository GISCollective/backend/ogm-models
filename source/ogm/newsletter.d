/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.newsletter;

import crate.error;
import gis_collective.hmq.broadcast.base;
import ogm.crates.all;
import std.exception;
import std.algorithm;
import ogm.calendar;
import std.datetime;
import std.net.isemail;
import std.typecons : Yes, No;
import vibe.data.json;


void subscribe(string email, string newsletterId, OgmCrates crates, IBroadcast broadcast) {
  auto emailStatus = isEmail(email, No.checkDns, EmailStatusCode.any);

  enforce!CrateValidationException(emailStatus.valid, emailStatus.status);

  auto recordRange = crates.newsletterEmail.get.where("email").equal(email).and.exec;
  Json record;

  if(recordRange.empty) {
    record = Json.emptyObject;
    record["email"] = email;
    record["list"] = Json.emptyArray;

    record = crates.newsletterEmail.addItem(record);
  } else {
    record = recordRange.front;
  }

  if(!record["list"].byValue.map!(a => a["newsletter"].to!string).canFind(newsletterId)) {
    auto subscription = Json.emptyObject;

    subscription["newsletter"] = newsletterId;
    subscription["joinedOn"] = SysCalendar.instance.now.toISOExtString;

    record["list"].appendArrayElement(subscription);
  }

  crates.newsletterEmail.updateItem(record);

  auto message = Json.emptyObject;
  message["id"] = newsletterId;
  message["email"] = email;

  broadcast.push("newsletter.subscribe", message);
}
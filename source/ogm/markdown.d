/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.markdown;

import vibe.data.json;
import ogm.markdown_vibe;
import std.string;
import std.algorithm;
import std.array;

version(unittest) {
  import fluent.asserts;
}

string toMarkdown(ref Json article) {
  string result;

  if(article["blocks"].type == Json.Type.array) {
    foreach(block; article["blocks"]) {
      if(block.type == Json.Type.object) {
        switch(block["type"].to!string) {
          case "header":
            result ~= toHeader(block["data"]);
            break;
          case "paragraph":
            result ~= toParagraph(block["data"]);
            break;
          default:
        }
      }
    }
  }

  return result.strip;
}

string toHeader(ref Json data) {
  string headerPrefix = "##";

  return headerPrefix ~ " " ~ data["text"].to!string ~ "\n\n";
}

string toParagraph(ref Json data) {
  return data["text"].to!string ~ "\n\n";
}

/// It returns an empty string for an empty article
unittest {
  auto article = `{}`.parseJsonString;

  article.toMarkdown.should.equal("");
}

/// It converts a title and a paragraph
unittest {
  auto article = `{"blocks" : [{
    "type" : "header",
    "data" : {
        "text" : "Some title",
        "level" : 2
    }},
    {
      "type" : "paragraph",
      "data" : {
          "text" : "Hey. Try to edit this text."
    }}
  ]}`.parseJsonString;

  article.toMarkdown.should.equal("## Some title\n\nHey. Try to edit this text.");
}

///
Json blocksFromMarkdown(string text) {
  Json result = Json.emptyObject;

  result["blocks"] = Json.emptyArray;

  auto rootBlock = text.getMarkdownBlocks;

  foreach(mdBlock; rootBlock.blocks) {
    auto block = Json.emptyObject;
    block["data"] = Json.emptyObject;

    if(mdBlock.type == BlockType.code) {
      block["type"] = "code";
      block["data"]["code"] = mdBlock.text[0];
    }

    if(mdBlock.type == BlockType.header) {
      block["type"] = "header";
      block["data"]["level"] = mdBlock.headerLevel;
      block["data"]["text"] = mdBlock.text[0].strip;
    }

    if(mdBlock.type == BlockType.uList) {
      block["type"] = "list";
      block["data"]["style"] = "unordered";
      block["data"]["items"] = mdBlock.blocks.map!(a => a.text.length > 0 ? a.text[0].strip : "").array.serializeToJson;
    }

    if(mdBlock.type == BlockType.oList) {
      block["type"] = "list";
      block["data"]["style"] = "ordered";
      block["data"]["items"] = mdBlock.blocks.map!(a => a.text.length > 0 ? a.text[0].strip : "").array.serializeToJson;
    }

    if(block["type"].type != Json.Type.string && mdBlock.text.length) {
      LinkRef[string] links;
      auto dst = appender!string();
      scope settings = new MarkdownSettings;
      writeMarkdownEscaped(dst, mdBlock, links,settings);

      block["type"] = "paragraph";
      block["data"]["text"] = dst[].strip;
    }

    result["blocks"] ~= block;
  }

  return result;
}

/// It converts an empty string to a list with no blocks
unittest {
  "".blocksFromMarkdown.should.equal(`{
    "blocks": []
  }`.parseJsonString);
}

/// It converts a string to a paragraph
unittest {
  "some paragraph".blocksFromMarkdown.should.equal(`{
    "blocks": [{
      "type": "paragraph",
      "data": {
          "text": "some paragraph"
      }
    }]
  }`.parseJsonString);
}

/// It converts a string with an \n to a paragraph
unittest {
  "some\nparagraph".blocksFromMarkdown.should.equal(`{
    "blocks": [{
      "type": "paragraph",
      "data": {
        "text": "some\nparagraph"
      }
    }]
  }`.parseJsonString);
}

/// It converts a markdown with 2 paragraphs to 2 paragraph blocks
unittest {
  auto description = "paragraph1 and\n\nparagraph2";

  description.blocksFromMarkdown.should.equal(`{
    "blocks": [{
      "type": "paragraph",
      "data": {
          "text": "paragraph1 and"
      }
    },
    {
      "type": "paragraph",
      "data": {
          "text": "paragraph2"
      }
    }]
  }`.parseJsonString);
}

/// It converts headers to header blocks
unittest {
  auto description = "Heading\n=======\n\nSub-heading\n-----------\n\n# Alternative heading #";

  description.blocksFromMarkdown.should.equal(`{
    "blocks": [{
      "type" : "header",
      "data" : {
          "text" : "Heading",
          "level" : 1
      }
    },
    {
      "type" : "header",
      "data" : {
          "text" : "Sub-heading",
          "level" : 2
      }
    },
    {
      "type" : "header",
      "data" : {
          "text" : "Alternative heading",
          "level" : 1
      }
    }]
  }`.parseJsonString);
}

/// It converts ulists to list blocks
unittest {
  auto description = "\n - item1\n - item2\n";

  description.blocksFromMarkdown.should.equal(`{
    "blocks": [{
      "type" : "list",
      "data" : {
        "style" : "unordered",
        "items" : [
          "item1",
          "item2"
        ]
      }
    }]
  }`.parseJsonString);
}

/// It converts olists to list blocks
unittest {
  auto description = "\n 1. item1\n 2. item2\n";

  description.blocksFromMarkdown.should.equal(`{
    "blocks": [{
      "type" : "list",
      "data" : {
        "style" : "ordered",
        "items" : [
          "item1",
          "item2"
        ]
      }
    }]
  }`.parseJsonString);
}


/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.icons.nodes.INode;

import vibe.data.json;

/// The interface that represents a node in the tree
interface INode {
  string name();
  string id();
  string setId();

  bool hasChildren();
  INode[] children();

  bool hasParent();
  INode parent();

  Json[] attributes();
  Json[] attributes(string[] trace);
}

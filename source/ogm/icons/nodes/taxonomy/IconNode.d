/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.icons.nodes.taxonomy.IconNode;

import ogm.icons.nodes.INode;
import ogm.icons.TaxonomyTree;

import std.algorithm;
import std.exception;
import std.array;

import vibe.data.json;

import crate.base;

version(unittest) {
  import std.string;
  import fluent.asserts;
  import crate.collection.memory;
  import crate.test.mock_query;

  import ogm.icons.nodes.taxonomy.RootNode;
}

///
class IconNode : INode {
  private {
    TaxonomyTree tree;
    string _id;
  }

  ///
  this(TaxonomyTree tree, string _id) {
    this.tree = tree;
    this._id = _id;
  }

  /// get the icon name
  string name() {
    return this.tree.getIcon!"name"(_id);
  }

  ///
  string id() {
    return _id;
  }

  string setId() {
    return "";
  }

  ///
  bool hasChildren() {
    return tree.getIconIdListWithParent(_id).length > 0;
  }

  ///
  INode[] children() {
    return tree.getIconIdListWithParent(_id).map!(a => tree.getIconNode(a)).array;
  }

  ///
  bool hasParent() {
    return true;
  }

  ///
  INode parent() {
    const parent = tree.getIcon!"parent"(_id);

    if(parent != "") {
      return tree.getIconNode(parent);
    }

    return tree.root;
  }

  Json[] attributes() {
    return this.attributes([]);
  }

  Json[] attributes(string[] trace) {
    Json[] parentAttributes;

    foreach (id; trace) {
      if(id == _id) {
        return parentAttributes;
      }
    }

    if(hasParent) {
      auto nextTrace = trace;
      nextTrace ~= [_id];

      foreach (Json value; this.parent.attributes(nextTrace)) {
        if(value.type != Json.Type.object) {
          continue;
        }

        value["isInherited"] = true;
        parentAttributes ~= value;
      }
    }

    auto strAttributes = this.tree.getIcon!"attributes"(_id);

    if(strAttributes == "") {
      return parentAttributes;
    }

    auto list = strAttributes.parseJsonString;

    if(list.type != Json.Type.array) {
      return parentAttributes;
    }

    Json[] attributes;
    attributes.length = list.length;

    foreach(size_t i, Json value; list) {
      if("name" !in value || value["name"] == "") {
        continue;
      }

      value["from"] = Json.emptyObject;
      value["from"]["icon"] = _id;
      value["from"]["iconSet"] = this.tree.getIcon!"iconSet"(_id);

      attributes[i] = value;
    }

    Json[] result = [];
    foreach(parentAttribute; parentAttributes) {
      bool exists;

      foreach(ref attribute; attributes) {
        if(attribute["name"] == parentAttribute["name"]) {
          exists = true;
          attribute["from"] = parentAttribute["from"];
          attribute["isInherited"] = true;
          break;
        }
      }

      if(!exists) {
        result ~= parentAttribute;
      }
    }

    return result ~ attributes;
  }
}

/// It should have the name equal to the icon name
unittest {
  IQuery getIcons() @safe {
    return new MemoryQuery([
      `{ "_id": "000000000000000000000001", "category": "1", "subcategory": "2", "name":"icon name", "parent": "" }`.parseJsonString
    ]);
  }

  IQuery getIconSet(string id) @safe {
    return new MemoryQuery([
      `{ "_id": "000000000000000000000001" }`.parseJsonString,
    ]);
  }

  auto tree = new TaxonomyTree(&getIcons, &getIconSet);
  auto node = new IconNode(tree, "000000000000000000000001");

  node.name.should.equal("icon name");
}

/// An icon with category and no subcategory should have the root parent
unittest {
  IQuery getIcons() @safe {
    return new MemoryQuery([
      `{ "_id": "000000000000000000000001", "category": "1", "subcategory": "", "name":"icon name", "parent": "" }`.parseJsonString
    ]);
  }

  IQuery getIconSet(string id) @safe {
    return new MemoryQuery([
      `{ "_id": "000000000000000000000001" }`.parseJsonString,
    ]);
  }

  auto tree = new TaxonomyTree(&getIcons, &getIconSet);
  auto node = new IconNode(tree, "000000000000000000000001");

  node.hasParent.should.equal(true);
  (cast(RootNode) node.parent).should.not.equal(null);
}

/// An icon with category and subcategory should have the root parent
unittest {
  IQuery getIcons() @safe {
    return new MemoryQuery([
      `{ "_id": "000000000000000000000001", "category": "1", "subcategory": "2", "name":"icon name", "parent": "" }`.parseJsonString
    ]);
  }

  IQuery getIconSet(string id) @safe {
    return new MemoryQuery([
      `{ "_id": "000000000000000000000001" }`.parseJsonString,
    ]);
  }

  auto tree = new TaxonomyTree(&getIcons, &getIconSet);
  auto node = new IconNode(tree, "000000000000000000000001");

  node.hasParent.should.equal(true);
  (cast(RootNode) node.parent).should.not.equal(null);
}

/// An icon with no category and no subcategory should have the root node as parent
unittest {
  IQuery getIcons() @safe {
    return new MemoryQuery([
      `{ "_id": "000000000000000000000001", "name":"icon name", "parent": "" }`.parseJsonString
    ]);
  }

  IQuery getIconSet(string id) @safe {
    return new MemoryQuery([
      `{ "_id": "000000000000000000000001" }`.parseJsonString,
    ]);
  }

  auto tree = new TaxonomyTree(&getIcons, &getIconSet);
  auto node = new IconNode(tree, "000000000000000000000001");

  node.hasParent.should.equal(true);
  node.parent.name.should.equal("");
  (cast(RootNode) node.parent).should.not.equal(null);
}

/// An icon with no category, no subcategory and parent icon the the icon node as parent
unittest {
  IQuery getIcons() @safe {
    return new MemoryQuery([
      `{ "_id": "000000000000000000000001", "name":"icon name", "parent": "000000000000000000000002" }`.parseJsonString,
      `{ "_id": "000000000000000000000002", "name":"parent icon", "parent": "" }`.parseJsonString
    ]);
  }

  IQuery getIconSet(string id) @safe {
    return new MemoryQuery([
      `{ "_id": "000000000000000000000001" }`.parseJsonString,
    ]);
  }

  auto tree = new TaxonomyTree(&getIcons, &getIconSet);
  auto node = new IconNode(tree, "000000000000000000000001");

  node.hasParent.should.equal(true);
  node.parent.name.should.equal("parent icon");
  (cast(IconNode) node.parent).should.not.equal(null);
}

/// A parent icon should query for children icons
unittest {
  auto iconsQuery = new MockQuery();

  IQuery getIcons() @safe {
    return iconsQuery;
  }


  IQuery getIconSet(string id) @safe {
    assert(false);
  }

  auto tree = new TaxonomyTree(&getIcons, &getIconSet);
  auto node = new IconNode(tree, "000000000000000000000002");

  node.children;
  iconsQuery.log.join(" ").should.equal(
    "where parent equal 000000000000000000000002 and withProjection (_id) distinct (_id)");
}

/// A parent icon should find all children icons
unittest {
  IQuery getIcons() @safe {
    return new MemoryQuery([
      `{ "_id": "000000000000000000000001", "name":"icon name", "parent": "000000000000000000000002" }`.parseJsonString,
      `{ "_id": "000000000000000000000002", "name":"parent icon", "parent": "" }`.parseJsonString
    ]);
  }

  IQuery getIconSet(string id) @safe {
    return new MemoryQuery([
      `{ "_id": "000000000000000000000001" }`.parseJsonString,
    ]);
  }

  auto tree = new TaxonomyTree(&getIcons, &getIconSet);
  auto node = new IconNode(tree, "000000000000000000000002");

  node.children.map!(a => a.name).should.equal(["icon name"]);
}

/// A child node should have all the parents attributes
unittest {
  IQuery getIcons() @safe {
    return new MemoryQuery([
      `{ "_id": "000000000000000000000003", "iconSet": "000000000000000000000001", "name":"level2", "parent": "000000000000000000000001", "attributes": [{ "name": "attr2", "type": "text" }] }`.parseJsonString,
      `{ "_id": "000000000000000000000001", "iconSet": "000000000000000000000002", "name":"level1", "parent": "000000000000000000000002", "attributes": [{ "name": "attr1", "type": "text" }] }`.parseJsonString,
      `{ "_id": "000000000000000000000002", "iconSet": "000000000000000000000002", "name":"parent icon", "parent": "", "attributes": [{ "name": "attr3", "type": "text" }] }`.parseJsonString
    ]);
  }

  IQuery getIconSet(string id) @safe {
    return new MemoryQuery([
      `{ "_id": "000000000000000000000001" }`.parseJsonString,
    ]);
  }

  auto tree = new TaxonomyTree(&getIcons, &getIconSet);
  auto node = new IconNode(tree, "000000000000000000000003");

  node.attributes.length.should.equal(3);
  node.attributes[0].should.equal(`{ "name": "attr3", "type": "text", "isInherited": true, "from": { "icon": "000000000000000000000002", "iconSet": "000000000000000000000002" }}`.parseJsonString);
  node.attributes[1].should.equal(`{ "name": "attr1", "type": "text", "isInherited": true, "from": { "icon": "000000000000000000000001", "iconSet": "000000000000000000000002" }}`.parseJsonString);
  node.attributes[2].should.equal(`{ "name": "attr2", "type": "text", "from": { "icon": "000000000000000000000003", "iconSet": "000000000000000000000001" }}`.parseJsonString);
}

/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.icons.nodes.category.IconNode;

import ogm.icons.nodes.INode;
import ogm.icons.CategoryTree;

import std.algorithm;
import std.exception;
import std.array;

import vibe.data.json;

import crate.base;

version(unittest) {
  import std.string;
  import fluent.asserts;
  import crate.collection.memory;
  import crate.test.mock_query;

  import ogm.icons.nodes.category.CategoryNode;
  import ogm.icons.nodes.category.SubcategoryNode;
  import ogm.icons.nodes.category.RootNode;
}

///
class IconNode : INode {
  private {
    CategoryTree tree;
    string _id;
  }

  ///
  this(CategoryTree tree, string _id) {
    this.tree = tree;
    this._id = _id;
  }

  /// get the icon name
  string name() {
    return this.tree.getIcon!"name"(_id);
  }

  ///
  string id() {
    return _id;
  }

  ///
  bool hasChildren() {
    return false;
  }

  string setId() {
    return this.tree.getIcon!"iconSet"(_id);
  }

  ///
  INode[] children() {
    return [];
  }

  ///
  bool hasParent() {
    return true;
  }

  ///
  INode parent() {
    const category = tree.getIcon!"category"(_id);
    const subcategory = tree.getIcon!"subcategory"(_id);

    if(category != "" && subcategory == "") {
      return tree.getCategoryNode(category);
    }

    if(category != "" && subcategory != "") {
      return tree.getSubcategoryNode(category, subcategory);
    }

    return tree.root;
  }

  Json[] attributes() {
    return [];
  }

  Json[] attributes(string[] trace) {
    return [];
  }
}

/// It should have the name equal to the icon name
unittest {
  IQuery getIcons() @safe {
    return new MemoryQuery([
      `{ "_id": "000000000000000000000001", "category": "1", "subcategory": "2", "name":"icon name", "parent": "" }`.parseJsonString
    ]);
  }

  IQuery getIconSet(string id) @safe {
    return new MemoryQuery([
      `{ "_id": "000000000000000000000001" }`.parseJsonString,
    ]);
  }

  auto tree = new CategoryTree(&getIcons, &getIconSet);
  auto node = new IconNode(tree, "000000000000000000000001");

  node.name.should.equal("icon name");
}

/// An icon with category and no subcategory should have the category parent
unittest {
  IQuery getIcons() @safe {
    return new MemoryQuery([
      `{ "_id": "000000000000000000000001", "category": "1", "subcategory": "", "name":"icon name", "parent": "" }`.parseJsonString
    ]);
  }

  IQuery getIconSet(string id) @safe {
    return new MemoryQuery([
      `{ "_id": "000000000000000000000001" }`.parseJsonString,
    ]);
  }

  auto tree = new CategoryTree(&getIcons, &getIconSet);
  auto node = new IconNode(tree, "000000000000000000000001");

  node.hasParent.should.equal(true);
  node.parent.name.should.equal("1");
  (cast(CategoryNode) node.parent).should.not.equal(null);
}

/// An icon with category and subcategory should have the subcategory parent
unittest {
  IQuery getIcons() @safe {
    return new MemoryQuery([
      `{ "_id": "000000000000000000000001", "category": "1", "subcategory": "2", "name":"icon name", "parent": "" }`.parseJsonString
    ]);
  }

  IQuery getIconSet(string id) @safe {
    return new MemoryQuery([
      `{ "_id": "000000000000000000000001" }`.parseJsonString,
    ]);
  }

  auto tree = new CategoryTree(&getIcons, &getIconSet);
  auto node = new IconNode(tree, "000000000000000000000001");

  node.hasParent.should.equal(true);
  node.parent.name.should.equal("2");
  node.parent.parent.name.should.equal("1");
  (cast(SubcategoryNode) node.parent).should.not.equal(null);
}

/// An icon with no category and no subcategory should have the root node as parent
unittest {
  IQuery getIcons() @safe {
    return new MemoryQuery([
      `{ "_id": "000000000000000000000001", "name":"icon name", "parent": "" }`.parseJsonString
    ]);
  }

  IQuery getIconSet(string id) @safe {
    return new MemoryQuery([
      `{ "_id": "000000000000000000000001" }`.parseJsonString,
    ]);
  }

  auto tree = new CategoryTree(&getIcons, &getIconSet);
  auto node = new IconNode(tree, "000000000000000000000001");

  node.hasParent.should.equal(true);
  node.parent.name.should.equal("");
  (cast(RootNode) node.parent).should.not.equal(null);
}

/// An icon with no category, no subcategory and parent icon should have the root node as parent
unittest {
  IQuery getIcons() @safe {
    return new MemoryQuery([
      `{ "_id": "000000000000000000000001", "name":"icon name", "parent": "000000000000000000000002" }`.parseJsonString,
      `{ "_id": "000000000000000000000002", "name":"parent icon", "parent": "" }`.parseJsonString
    ]);
  }

  IQuery getIconSet(string id) @safe {
    return new MemoryQuery([
      `{ "_id": "000000000000000000000001" }`.parseJsonString,
    ]);
  }

  auto tree = new CategoryTree(&getIcons, &getIconSet);
  auto node = new IconNode(tree, "000000000000000000000001");

  node.hasParent.should.equal(true);
  (cast(RootNode) node.parent).should.not.equal(null);
}

/// Should not query the icons for children
unittest {
  auto iconsQuery = new MockQuery();

  IQuery getIcons() @safe {
    return iconsQuery;
  }


  IQuery getIconSet(string id) @safe {
    assert(false);
  }

  auto tree = new CategoryTree(&getIcons, &getIconSet);
  auto node = new IconNode(tree, "000000000000000000000002");

  node.children;
  iconsQuery.log.join(" ").should.equal("");
}

/// A category icon should not have cildren
unittest {
  IQuery getIcons() @safe {
    return new MemoryQuery([
      `{ "_id": "000000000000000000000001", "name":"icon name", "parent": "000000000000000000000002" }`.parseJsonString,
      `{ "_id": "000000000000000000000002", "name":"parent icon", "parent": "" }`.parseJsonString
    ]);
  }

  IQuery getIconSet(string id) @safe {
    assert(false);
  }

  auto tree = new CategoryTree(&getIcons, &getIconSet);
  auto node = new IconNode(tree, "000000000000000000000002");

  node.hasChildren.should.equal(false);
  node.children.should.equal([]);
}

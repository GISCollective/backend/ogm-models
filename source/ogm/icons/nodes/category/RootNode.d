/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.icons.nodes.category.RootNode;

import ogm.icons.nodes.INode;
import ogm.icons.CategoryTree;

import std.algorithm;
import std.exception;
import std.array;

import vibe.data.json;

import crate.base;

version(unittest) {
  import std.string;
  import fluent.asserts;
  import crate.collection.memory;
  import crate.test.mock_query;
}

/// Root icon node for organising icons based on the `category` and `subcategory` field
class RootNode : INode {
  private {
    CategoryTree iconsTree;
  }

  this(CategoryTree iconsTree) {
    this.iconsTree = iconsTree;
  }

  string name() { return ""; }

  string id() {
    return "#";
  }

  string setId() {
    return "";
  }

  bool hasChildren() {
    return true;
  }

  /// Returns category nodes and iconNodes for icons
  /// without parents and categories
  INode[] children() {
    return iconsTree.getCategoryList.map!(a => iconsTree.getCategoryNode(a)).array;
  }

  bool hasParent() {
    return false;
  }

  INode parent() {
    return null;
  }

  Json[] attributes() {
    return [];
  }

  Json[] attributes(string[] trace) {
    return [];
  }
}

/// It should have no name
unittest {
  auto node = new RootNode(null);

  node.name.should.equal("");
}

/// It should have children
unittest {
  auto node = new RootNode(null);

  node.hasChildren.should.equal(true);
}

/// It should not have a parent
unittest {
  auto node = new RootNode(null);

  node.hasParent.should.equal(false);
  node.parent.should.equal(null);
}

/// Getting the childrens should query the getter
unittest {
  auto iconsQuery = new MockQuery();

  IQuery getIcons() @safe {
    return iconsQuery;
  }

  IQuery getIconSet(string id) @safe {
    assert(false);
  }

  auto iconsTree = new CategoryTree(&getIcons, &getIconSet);
  auto node = new RootNode(iconsTree);

  node.children;
  iconsQuery.log.join(" ").should.equal(
    "withProjection (category) distinct (category)");
}

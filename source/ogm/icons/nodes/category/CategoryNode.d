/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.icons.nodes.category.CategoryNode;

import ogm.icons.nodes.INode;
import ogm.icons.nodes.category.RootNode;
import ogm.icons.CategoryTree;

import std.algorithm;
import std.exception;
import std.array;

import vibe.data.json;

import crate.base;

version(unittest) {
  import std.string;
  import fluent.asserts;
  import crate.collection.memory;
  import crate.test.mock_query;
}

/// Icon node representing a category
class CategoryNode : INode {
  private {
    CategoryTree iconsTree;
    string categoryName;
  }

  this(CategoryTree iconsTree, string categoryName) {
    this.iconsTree = iconsTree;
    this.categoryName = categoryName;
  }

  string name() {
    return this.categoryName;
  }

  string id() {
    return "#" ~ categoryName;
  }

  string setId() {
    return "";
  }

  bool hasChildren() {
    return true;
  }

  INode[] children() {
    return this.iconsTree.getSubcategoryList(this.categoryName)
      .map!(a => iconsTree.getSubcategoryNode(categoryName, a))
      .array;
  }

  bool hasParent() {
    return true;
  }

  INode parent() {
    enforce(iconsTree !is null, "The iconsTree is null");

    return iconsTree.root;
  }

  Json[] attributes() {
    return [];
  }

  Json[] attributes(string[] trace) {
    return [];
  }
}

/// The category node has always a parent
unittest {
  auto node = new CategoryNode(null, "");
  node.hasParent.should.equal(true);
}

/// The category node should have an id
unittest {
  auto node = new CategoryNode(null, "test");
  node.id.should.equal("#test");
}

/// The parent of a category node should be the root node
unittest {
  IQuery getIcons() @safe {
    assert(false);
  }

  IQuery getIconSet(string id) @safe {
    assert(false);
  }

  auto iconsTree = new CategoryTree(&getIcons, &getIconSet);
  auto node = new CategoryNode(iconsTree, "");

  should(cast(RootNode) node.parent).not.equal(null);
}

/// Getting the childrens should query the getter
unittest {
  auto iconsQuery = new MockQuery();

  IQuery getIcons() @safe {
    return iconsQuery;
  }

  IQuery getIconSet(string id) @safe {
    assert(false);
  }

  auto iconsTree = new CategoryTree(&getIcons, &getIconSet);
  auto node = new CategoryNode(iconsTree, "category name");

  node.children;

  iconsQuery.log.join(" ").should.equal("where category equal category name and withProjection (subcategory) distinct (subcategory)");
}

/// Getting the childrens should return a list of subcategory nodes
unittest {
  IQuery getIcons() @safe {
    return new MemoryQuery([
      `{ "category": "test", "subcategory": "1", "parent": "" }`.parseJsonString,
      `{ "category": "test", "subcategory": "2", "parent": "" }`.parseJsonString
    ]);
  }

  IQuery getIconSet(string id) @safe {
    assert(false);
  }

  auto iconsTree = new CategoryTree(&getIcons, &getIconSet);
  auto node = new CategoryNode(iconsTree, "test");

  node.children.map!(a => a.name).should.equal(["1", "2"]);
}

/// Getting the name should return the category name
unittest {
  auto node = new CategoryNode(null, "test names");

  node.name.should.equal("test names");
}

/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.icons.nodes.category.SubcategoryNode;

import ogm.icons.nodes.INode;
import ogm.icons.CategoryTree;

import std.algorithm;
import std.exception;
import std.array;

import vibe.data.json;

import crate.base;

version(unittest) {
  import std.string;
  import fluent.asserts;
  import crate.collection.memory;
  import crate.test.mock_query;
}

///
class SubcategoryNode : INode {

  private {
    CategoryTree iconsTree;
    string categoryName;
    string subcategoryName;
  }

  this(CategoryTree iconsTree, string categoryName, string subcategoryName) {
    this.iconsTree = iconsTree;
    this.categoryName = categoryName;
    this.subcategoryName = subcategoryName;
  }

  string name() {
    return subcategoryName;
  }

  string setId() {
    return "";
  }

  string id() {
    return "#" ~ categoryName ~ "#" ~ subcategoryName;
  }

  bool hasChildren() {
    return true;
  }

  INode[] children() {
    auto idList = this.iconsTree.getIconIdList(categoryName, subcategoryName);
    return idList.map!(a => iconsTree.getIconNode(a)).array;
  }

  bool hasParent() {
    return true;
  }

  INode parent() {
    enforce(iconsTree !is null, "The iconsTree is null");
    return iconsTree.getCategoryNode(this.categoryName);
  }

  Json[] attributes() {
    return [];
  }

  Json[] attributes(string[] trace) {
    return [];
  }
}

/// The icons subcategory node name should be the category name
unittest {
  auto node = new SubcategoryNode(null, "category", "subcategory");
  node.name.should.equal("subcategory");
}

/// The icons subcategory should have children
unittest {
  auto node = new SubcategoryNode(null, "category", "subcategory");
  node.hasChildren.should.equal(true);
}

/// The icons subcategory should have a parent
unittest {
  auto node = new SubcategoryNode(null, "category", "subcategory");
  node.hasParent.should.equal(true);
}

/// The icons subcategory parent should be the category name
unittest {
  IQuery getIcons() @safe {
    return new MemoryQuery([
      `{ "category": "test", "subcategory": "1", "parent": "" }`.parseJsonString,
      `{ "category": "test", "subcategory": "2", "parent": "" }`.parseJsonString
    ]);
  }

  IQuery getIconSet(string id) @safe {
    assert(false);
  }

  auto iconsTree = new CategoryTree(&getIcons, &getIconSet);
  auto node = new SubcategoryNode(iconsTree, "category", "subcategory");
  node.parent.name.should.equal("category");
}

/// Getting the childrens should query the getter
unittest {
  auto iconsQuery = new MockQuery();

  IQuery getIcons() @safe {
    return iconsQuery;
  }

  IQuery getIconSet(string id) @safe {
    assert(false);
  }

  auto iconsTree = new CategoryTree(&getIcons, &getIconSet);
  auto node = new SubcategoryNode(iconsTree, "category name", "subcategory name");

  node.children;
  iconsQuery.log.join(" ").should.equal(
    "where category equal category name and where subcategory equal subcategory name " ~
    "and where parent equal  and withProjection (_id) distinct (_id)");
}

/// Getting the childrens should return a list of IconNodes
unittest {
  IQuery getIcons() @safe {
    return new MemoryQuery([
      `{ "_id": "000000000000000000000001", "category": "1", "subcategory": "2", "name":"icon1", "parent": "" }`.parseJsonString,
      `{ "_id": "000000000000000000000002", "category": "1", "subcategory": "2", "name":"icon2", "parent": "" }`.parseJsonString
    ]);
  }

  IQuery getIconSet(string id) @safe {
    return new MemoryQuery([
      `{ "_id": "000000000000000000000001" }`.parseJsonString,
    ]);
  }

  auto iconsTree = new CategoryTree(&getIcons, &getIconSet);
  auto node = new SubcategoryNode(iconsTree, "1", "2");

  node.children.map!(a => a.name).should.equal(["icon1", "icon2"]);
}

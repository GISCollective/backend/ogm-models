/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.icons.TaxonomyTree;

import vibe.core.log;
import vibe.data.json;

import crate.base;
import std.algorithm;
import std.array;
import std.exception;

import ogm.icons.nodes.INode;
import ogm.icons.IconCache;

import ogm.icons.nodes.taxonomy.RootNode;
import ogm.icons.nodes.taxonomy.IconNode;
import ogm.TimedCache;


version(unittest) {
  import fluent.asserts;
  import crate.test.mock_query;
}

///
class TaxonomyTree {
  private {
    CrateListGetter iconListGetter;
    CrateGetter iconSetGetter;

    INode _root;
    IconCache iconCache;
    TimedCache!IconNode iconTaxonomyNodeCache;

    const string iconSetId;
  }

  ///
  this(CrateListGetter iconListGetter, CrateGetter iconSetGetter, IconCache iconCache, string iconSetId = "") {
    this.iconListGetter = iconListGetter;
    this.iconSetGetter = iconSetGetter;

    this.iconSetId = iconSetId;
    this.iconCache = iconCache;

    this._root = new RootNode(this);
    this.iconTaxonomyNodeCache = new TimedCache!IconNode(&this.createIconNode);
  }

  ///
  this(CrateListGetter iconListGetter, CrateGetter iconSetGetter, string iconSetId = "") {
    this(iconListGetter, iconSetGetter, new IconCache(iconListGetter, iconSetGetter), iconSetId);
  }

  private {
    IconNode createIconNode(string id) {
      return new IconNode(this, id);
    }
  }

  INode root() {
    return this._root;
  }

  Json getIcon(string id) {
    return iconCache.get(id);
  }

  /// Get an icon property
  string getIcon(string field)(string iconId) {
    if(iconId == "") {
      return "";
    }

    auto icon = iconCache.get(iconId);

    if(icon.type != Json.Type.object || field !in icon) {
      return "";
    }

    return icon[field].to!string;
  }

  /// get a taxonomy icon node
  INode getIconNode(string id) {
    return iconTaxonomyNodeCache.get(id);
  }

  /// Get all root icons id
  string[] getRootIconIdList() {
    auto r = this.iconListGetter()
          .where("parent").equal("").and;

    if(iconSetId != "") {
      r = r.where("iconSet").equal(ObjectId.fromString(iconSetId)).and;
    }

    return r.withProjection(["_id"]).distinct("_id").map!(a => a.to!string).array;
  }

  string[] getIconIdListWithParent(string id) {
    auto r = this.iconListGetter().where("parent").equal(id).and;

    if(iconSetId != "") {
      r = r.where("iconSet").equal(ObjectId.fromString(iconSetId)).and;
    }

    return r.withProjection(["_id"]).distinct("_id").map!(a => a.to!string).array;
  }

  /// Check if an icon set is public
  bool isIconSetPublic(string id) {
    if(id == "") {
      return false;
    }

    auto range = iconSetGetter(id).withProjection([ "visibility" ]).exec;

    if(range.empty) {
      return false;
    }

    if("visibility" in range.front && "isPublic" in range.front["visibility"]) {
      return range.front["visibility"]["isPublic"].to!string == "true";
    }

    return false;
  }

  /// Given an icon id, all the parents in order will be returned. If you pass a
  /// visibleIconSets private icons will be returned too. If visibleIconSets is ["*"] all private
  /// icons will be returned.
  string[] parentIconIds(string iconId, string[] visibleIconSets = null, int maxLevels = int.max) {
    string[] parentIds = [ iconId ];

    size_t index;

    do {
      auto icon = iconCache.get(parentIds[index]);

      if(icon.type == Json.Type.object) {
        string parent = icon["parent"].to!string;

        bool isIconSetVisible;

        if(visibleIconSets.length == 1 && visibleIconSets[0] == "*") {
          isIconSetVisible = true;
        } else {
          string iconSet = getIcon!"iconSet"(parent);
          isIconSetVisible = visibleIconSets.canFind(iconSet) || isIconSetPublic(iconSet);
        }

        if(parentIds.canFind(parent)) {
          logError("A cyclic icon dependency was found for the icon with id %s: %s", iconId, parentIds);
          break;
        }

        if(parent != "" && isIconSetVisible) {
          parentIds ~= parent;
        }
      }

      index++;
    } while(index < parentIds.length && maxLevels > index);

    return parentIds[1 .. $];
  }

  ///
  TaxonomyTree subset(string iconSetId) {
    return new TaxonomyTree(iconListGetter, iconSetGetter, iconCache, iconSetId);
  }
}

/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.icons.IconCache;

import crate.base;
import vibe.data.json;
import ogm.TimedCache;

///
class IconCache {
  private {
    CrateListGetter iconListGetter;
    CrateGetter iconSetGetter;
    TimedCache!Json iconCache;
  }

  ///
  this(CrateListGetter iconListGetter, CrateGetter iconSetGetter) {
    this.iconListGetter = iconListGetter;
    this.iconSetGetter = iconSetGetter;

    this.iconCache = new TimedCache!Json(&this.getDbIcon);
  }

  Json getDbIcon(string id) {
    if(!isObjectId(id)) {
      return Json();
    }

    auto range = iconListGetter().where("_id").equal(ObjectId.fromString(id)).and.exec;

    if(range.empty) {
      return Json();
    }

    auto icon = range.front;

    if(icon["styles"].type != Json.Type.object) {
      icon["styles"] = Json.emptyObject;
      icon["styles"]["hasCustomStyle"] = false;
      icon["styles"]["types"] = Json.emptyObject;
    }

    if(icon["styles"]["hasCustomStyle"] == false) {
      auto iconSet = iconSetGetter(icon["iconSet"].to!string).and.exec.front;

      if("styles" in iconSet) {
        icon["styles"]["types"] = iconSet["styles"];
      }
    }

    return icon;
  }

  Json get(string id) {
    return iconCache.get(id);
  }
}


/// It copies the style from the icon set when an icon is using the default one
unittest {
  import ogm.test.fixtures;
  import fluent.asserts;
  import fluentasserts.vibe.json;
  import std.stdio;

  setupTestData();
  auto iconSet = crates.iconSet.getItem("000000000000000000000001").and.exec.front;

  iconSet["styles"]["site"] = `{
    "backgroundColor": "red",
    "isVisible": true,
    "size": 23,
    "shape": "square",
    "borderWidth": 1,
    "borderColor": "#fff"
  }`.parseJsonString;

  crates.iconSet.updateItem(iconSet);

  auto cache = new IconCache(&crates.icon.get, &crates.iconSet.getItem);

  auto result = cache.getDbIcon("000000000000000000000001");

  result["styles"]["types"]["site"].should.equal(`{
    "backgroundColor": "red",
    "isVisible": true,
    "size": 23,
    "shape": "square",
    "borderWidth": 1,
    "borderColor": "#fff"
  }`.parseJsonString);
}


/// It does not copy the style from the icon set when an icon is using a custom one
unittest {
  import ogm.test.fixtures;
  import fluent.asserts;
  import fluentasserts.vibe.json;
  import std.stdio;

  setupTestData();
  auto iconSet = crates.iconSet.getItem("000000000000000000000001").and.exec.front;

  iconSet["styles"]["site"] = `{
    "backgroundColor": "red",
    "isVisible": true,
    "size": 23,
    "shape": "square",
    "borderWidth": 1,
    "borderColor": "#fff"
  }`.parseJsonString;

  crates.iconSet.updateItem(iconSet);

  auto icon = crates.icon.getItem("000000000000000000000001").and.exec.front;
  icon["styles"]["hasCustomStyle"] = true;
  crates.icon.updateItem(icon);

  auto cache = new IconCache(&crates.icon.get, &crates.iconSet.getItem);
  auto result = cache.getDbIcon("000000000000000000000001");

  result["styles"]["types"]["site"].should.equal(`{
    "backgroundColor": "rgba(255,255,255,0.6)",
    "isVisible": true,
    "size": 21,
    "shape": "circle",
    "borderWidth": 1,
    "borderColor": "#fff"
  }`.parseJsonString);
}
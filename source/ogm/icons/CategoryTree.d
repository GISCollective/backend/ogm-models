/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.icons.CategoryTree;

import vibe.core.log;
import vibe.data.json;

import crate.base;
import std.algorithm;
import std.array;
import std.exception;

import ogm.icons.nodes.INode;
import ogm.icons.IconCache;
import ogm.icons.nodes.category.RootNode;
import ogm.icons.nodes.category.CategoryNode;
import ogm.icons.nodes.category.SubcategoryNode;
import ogm.icons.nodes.category.IconNode;
import ogm.TimedCache;

version(unittest) {
  import fluent.asserts;
  import crate.test.mock_query;
}

///
class CategoryTree {
  private {
    CrateListGetter iconListGetter;
    CrateGetter iconSetGetter;

    INode _root;

    IconCache iconCache;
    TimedCache!CategoryNode categoryCache;
    TimedCache!SubcategoryNode subcategoryCache;
    TimedCache!IconNode iconCategoryNodeCache;

    const string iconSetId;
  }

  ///
  this(CrateListGetter iconListGetter, CrateGetter iconSetGetter, IconCache iconCache, string iconSetId = "") {
    this.iconListGetter = iconListGetter;
    this.iconSetGetter = iconSetGetter;

    this.iconSetId = iconSetId;
    this.iconCache = iconCache;

    this._root = new RootNode(this);

    this.categoryCache = new TimedCache!CategoryNode(&this.createCategory);
    this.subcategoryCache = new TimedCache!SubcategoryNode(&this.createSubcategory);
    this.iconCategoryNodeCache = new TimedCache!IconNode(&this.createIconNode);
  }

  ///
  this(CrateListGetter iconListGetter, CrateGetter iconSetGetter, string iconSetId = "") {
    this(iconListGetter, iconSetGetter, new IconCache(iconListGetter, iconSetGetter), iconSetId);
  }

  private {
    SubcategoryNode createSubcategory(string key) {
      auto pieces = key.split(".");

      return new SubcategoryNode(this, pieces[0], pieces[1]);
    }

    CategoryNode createCategory(string key) {
      return new CategoryNode(this, key);
    }

    IconNode createIconNode(string id) {
      return new IconNode(this, id);
    }
  }

  INode root() {
    return this._root;
  }

  Json getIcon(string id) {
    return iconCache.get(id);
  }

  /// Get an icon property
  string getIcon(string field)(string iconId) {
    if(iconId == "") {
      return "";
    }

    auto icon = iconCache.get(iconId);

    if(icon.type != Json.Type.object || field !in icon) {
      return "";
    }

    return icon[field].to!string;
  }

  INode getSubcategoryNode(string category, string subcategory) {
    return subcategoryCache.get(category ~ "." ~ subcategory);
  }

  INode getCategoryNode(string category) {
    return categoryCache.get(category);
  }

  /// get a category icon node
  INode getIconNode(string id) {
    return iconCategoryNodeCache.get(id);
  }

  /// Get all subcategories of a category
  string[] getSubcategoryList(string name) {
    auto r = this.iconListGetter()
          .where("category").equal(name).and;

    if(iconSetId != "") {
      r = r.where("iconSet").equal(ObjectId.fromString(iconSetId)).and;
    }

    return r.withProjection(["subcategory"]).distinct("subcategory").map!(a => a.to!string).array;
  }

  /// Get all subcategories
  string[] getSubcategoryList() {
    auto r = this.iconListGetter();

    if(iconSetId != "") {
      r = r.where("iconSet").equal(ObjectId.fromString(iconSetId)).and;
    }

    return r.withProjection(["subcategory"]).distinct("subcategory").map!(a => a.to!string).array;
  }

  /// Get all categories
  string[] getCategoryList() {
    auto r = this.iconListGetter();

    if(iconSetId != "") {
      r = r.where("iconSet").equal(ObjectId.fromString(iconSetId)).and;
    }

    return r.withProjection(["category"]).distinct("category").map!(a => a.to!string).array;
  }

  /// Get all root icons id
  string[] getRootIconIdList() {
    auto r = this.iconListGetter()
          .where("parent").equal("").and;

    if(iconSetId != "") {
      r = r.where("iconSet").equal(ObjectId.fromString(iconSetId)).and;
    }

    return r.withProjection(["_id"]).distinct("_id").map!(a => a.to!string).array;
  }

  /// Get all subcategories of a category
  string[] getIconIdList(string category, string subcategory) {
    auto r = this.iconListGetter()
          .where("category").equal(category).and
          .where("subcategory").equal(subcategory).and
          .where("parent").equal("").and;

    if(iconSetId != "") {
      r = r.where("iconSet").equal(ObjectId.fromString(iconSetId)).and;
    }

    return r.withProjection(["_id"]).distinct("_id").map!(a => a.to!string).array;
  }

  string[] getIconIdListWithParent(string id) {
    auto r = this.iconListGetter().where("parent").equal(id).and;

    if(iconSetId != "") {
      r = r.where("iconSet").equal(ObjectId.fromString(iconSetId)).and;
    }

    return r.withProjection(["_id"]).distinct("_id").map!(a => a.to!string).array;
  }

  ///
  CategoryTree subset(string iconSetId) {
    return new CategoryTree(iconListGetter, iconSetGetter, iconCache, iconSetId);
  }
}

/// It should allow creating a subset by set id
unittest {
  auto iconsQuery = new MockQuery();

  IQuery getIcons() @safe {
    return iconsQuery;
  }

  IQuery getIconSet(string id) @safe {
    assert(false);
  }

  auto iconsTree = new CategoryTree(&getIcons, &getIconSet).subset("1");

  iconsQuery.log = [];
  iconsTree.getIconIdListWithParent("2");
  iconsQuery.log.join(" ").should.equal(
    "where parent equal 2 and where iconSet equal 000000000000000000000001 and withProjection (_id) distinct (_id)");

  iconsQuery.log = [];
  iconsTree.getIconIdList("1", "2");
  iconsQuery.log.join(" ").should.equal(
    "where category equal 1 and where subcategory equal 2 and where parent equal  and where iconSet equal 000000000000000000000001 and withProjection (_id) distinct (_id)");

  iconsQuery.log = [];
  iconsTree.getRootIconIdList;
  iconsQuery.log.join(" ").should.equal(
    "where parent equal  and where iconSet equal 000000000000000000000001 and withProjection (_id) distinct (_id)");

  iconsQuery.log = [];
  iconsTree.getCategoryList;
  iconsQuery.log.join(" ").should.equal(
    "where iconSet equal 000000000000000000000001 and withProjection (category) distinct (category)");

  iconsQuery.log = [];
  iconsTree.getSubcategoryList("test");
  iconsQuery.log.join(" ").should.equal(
    "where category equal test and where iconSet equal 000000000000000000000001 and withProjection (subcategory) distinct (subcategory)");
}

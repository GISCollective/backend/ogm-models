/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.files.configuration;

import vibe.service.configuration.general;

///
struct MicroFilesConfig {
  /// Configurations for all services
  GeneralConfig general;

  ///
  FilesConfiguration files;
}

struct FilesConfiguration {
  string location;
  string baseUrl;
}

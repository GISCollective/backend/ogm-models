/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.test.fixtures;

public import crate.collection.memory;
public import crate.http.router;
public import crate.resource.file;
public import crate.base;
public import geo.json;
public import crate.lazydata.base;
public import crate.auth.usercollection;

public import std.datetime;

public import std.conv;
public import std.stdio;
public import std.algorithm;
public import std.array;
public import std.range;
public import std.string;
public import ogm.auth;
public import ogm.crates.all;

public import ogm.models.modelinfo;
public import ogm.models.visibility;

public import vibe.data.json;
public import vibe.http.router;
public import vibeauth.data.token;
public import vibeauth.data.usermodel;

public import geo.json : GeoJsonGeometry;

import vibe.db.mongo.collection;
import vibe.db.mongo.mongo;

version(unittest) {
  shared static this() {
    import vibe.core.log;

    foreach(logger; getLoggers()) {
      deregisterLogger(logger);
    }
  }
}

OgmCrates crates;
UserCrateCollection userCollection;

Token administratorToken;
Token bearerToken;
Token bearerLeaderToken;
Token bearerMemberToken;
Token bearerGuestToken;
Token otherToken;

string[string] userTokenList;
string[string] userId;
string[string] userEmail;
enum userTypes = [ "guest", "member", "leader", "owner" ];

Team team1;
Team team2;
Team team3;
Team team4;

Map map1;
Map map2;
Map map3;
Map map4;

Campaign campaign1;
Campaign campaign2;
Campaign campaign3;
Campaign campaign4;

IconSet iconSet1;
IconSet iconSet2;
IconSet iconSet3;
IconSet iconSet4;

Picture cover;

Icon icon1;
Icon icon2;

void setupTestData() {
  Authentication.reset;
  createCrates();
  createPicture("1", "default", "@system");

  createUserData();
  createMapsWithSites();
}

auto createCrates() {
  crates = OgmCrates();

  crates.user = new MemoryCrate!User;
  immutable(string[]) accessList = [];
  userCollection = new UserCrateCollection(accessList, crates.user);

  crates.article = new MemoryCrate!Article;
  crates.baseMap = new MemoryCrate!BaseMap;
  crates.batchJob = new MemoryCrate!BatchJob;
  crates.campaign = new MemoryCrate!Campaign;
  crates.campaignAnswer = new MemoryCrate!CampaignAnswer;
  crates.changeSet = new MemoryCrate!ChangeSet;
  crates.feature = new MemoryCrate!Feature;
  crates.simpleFeature = new MemoryCrate!SimpleFeature;
  crates.fieldTranslation = new MemoryCrate!FieldTranslation;
  crates.icon = new MemoryCrate!Icon;
  crates.iconSet = new MemoryCrate!IconSet;
  crates.issue = new MemoryCrate!Issue;
  crates.legend = new LegendCrate(crates.feature, crates.icon);
  crates.log = new MemoryCrate!LogEntry;
  crates.map = new MemoryCrate!Map;
  crates.mapFile = new MemoryCrate!MapFile;
  crates.meta = new MemoryCrate!Meta;
  crates.metric = new MemoryCrate!Metric;
  crates.dataBinding = new MemoryCrate!DataBinding;
  crates.model = new ModelCrate();
  crates.page = new MemoryCrate!Page;
  crates.space = new MemoryCrate!Space;
  crates.presentation = new MemoryCrate!Presentation;
  crates.picture = new MemoryCrate!Picture;
  crates.preference = new MemoryCrate!Preference;
  crates.siteSubset = new MemoryCrate!SiteSubset;
  crates.sound = new MemoryCrate!Sound;
  crates.team = new MemoryCrate!Team;
  crates.tileLayer = new MemoryCrate!TileLayer;
  crates.translation = new MemoryCrate!Translation;
  crates.userProfile = new MemoryCrate!UserProfile;
  crates.stat = new MemoryCrate!Stat;
  crates.newsletter = new MemoryCrate!Newsletter;
  crates.newsletterEmail = new MemoryCrate!NewsletterEmail;
  crates.articleLink = new MemoryCrate!ArticleLink;
  crates.searchMeta = new MemoryCrate!SearchMeta;
  crates.event = new MemoryCrate!Event;
  crates.calendar = new MemoryCrate!Calendar;
  crates.message = new MemoryCrate!Message;

  GeocodingCrate.instance = new GeocodingCrate([]);
  crates.geocoding = GeocodingCrate.instance;

  crates.iconFiles = new MockGridFsFiles;
  crates.iconChunks = new MockGridFsChunks;

  crates.pictureFiles = new MockGridFsFiles;
  crates.pictureChunks = new MockGridFsChunks;

  crates.soundFiles = new MockGridFsFiles;
  crates.soundChunks = new MockGridFsChunks;

  crates.issueFiles = new MockGridFsFiles;
  crates.issueChunks = new MockGridFsChunks;

  crates.mapFiles = new MockGridFsFiles;
  crates.mapChunks = new MockGridFsChunks;

  crates.translationFiles = new MockGridFsFiles;
  crates.translationChunks = new MockGridFsChunks;

  PictureFileSettings.files = crates.pictureFiles;
  PictureFileSettings.chunks = crates.pictureChunks;

  SoundFileSettings.files = crates.pictureFiles;
  SoundFileSettings.chunks = crates.pictureChunks;

  MapFileSettings.files = crates.mapFiles;
  MapFileSettings.chunks = crates.mapChunks;

  TranslationFileSettings.files = crates.translationFiles;
  TranslationFileSettings.chunks = crates.translationChunks;

  return crates;
}

auto createTestDefaultSpace() {
  Space space;
  space.domain = "giscollective.com";
  space.visibility.isDefault = true;

  auto spaceJson = Lazy!Space.fromModel(space).to!Json;

  crates.space.addItem(spaceJson);

  return space;
}

auto createMap(string id, string name, Json description, bool isPublic, Polygon polygon, Team team, Picture cover, IconSet[] sets) {
  auto map = Map(ObjectId.fromString(id), name, description, Visibility(team, isPublic), polygon);
  map.cover = cover;
  map.iconSets.list = sets;

  return createMap(map);
}

auto createMap(Map map) {
  auto mapJson = Lazy!Map.fromModel(map).to!Json;

  crates.map.addItem(mapJson);

  return map;
}

auto createMetric(Metric metric) {
  auto metricJson = Lazy!Metric.fromModel(metric).to!Json;

  return crates.metric.addItem(metricJson);
}

auto createCampaignAnswer(CampaignAnswer answer) {
  auto answerJson = Lazy!CampaignAnswer.fromModel(answer).to!Json;

  return crates.campaignAnswer.addItem(answerJson);
}

auto createCampaign(Campaign campaign) {
  auto campaignJson = Lazy!Campaign.fromModel(campaign).to!Json;

  crates.campaign.addItem(campaignJson);

  return campaign;
}

auto createPicture(string id, string name = "", string owner = "") {
  Picture picture;
  picture.name = name;
  picture._id = ObjectId.fromString(id);
  picture.picture = new PictureFile;
  picture.owner = owner;

  auto jsonPicture = picture.serializeToJson;
  jsonPicture["picture"] = "";

  crates.picture.addItem(jsonPicture);

  return picture;
}

auto createPicture(Picture picture) {
  auto jsonPicture = Lazy!Picture.fromModel(picture).to!Json;

  crates.picture.addItem(jsonPicture);

  return jsonPicture;
}

auto createSound(Sound sound) {
  auto soundJson = Lazy!Sound.fromModel(sound).to!Json;

  crates.sound.addItem(soundJson);

  return soundJson;
}

auto createFeature(Feature feature) {
  feature.computedVisibility.isPublic = feature.visibility != VisibilityEnum.Private ? true : false;

  auto featureJson = Lazy!Feature.fromModel(feature).to!Json;

  if(featureJson["maps"].length) {
    featureJson["computedVisibility"]["team"] = featureJson["maps"][0];
  }

  featureJson["positionBox"] = featureJson["position"];

  crates.feature.addItem(featureJson);
  crates.siteSubset.addItem(featureJson);

  return featureJson;
}

auto createArticle(Article article) {
  auto articleJson = Lazy!Article.fromModel(article).to!Json;

  crates.article.addItem(articleJson);

  return articleJson;
}

auto createIssue(Issue issue) {
  auto issueJson = Lazy!Issue.fromModel(issue).to!Json;

  crates.issue.addItem(issueJson);

  return issueJson;
}

auto createIconSet(IconSet iconSet) {
  auto iconSetJson = Lazy!IconSet.fromModel(iconSet).to!Json;

  crates.iconSet.addItem(iconSetJson);

  return iconSetJson;
}

auto createIcon(Icon icon) {
  auto iconJson = Lazy!Icon.fromModel(icon).to!Json;

  crates.icon.addItem(iconJson);

  return iconJson;
}

auto createCalendar(Calendar calendar) {
  auto calendarJson = Lazy!Calendar.fromModel(calendar).to!Json;

  crates.calendar.addItem(calendarJson);

  return calendarJson;
}

void createUserData() {
  UserModel user1;
  user1._id = ObjectId.fromString("1").toString;
  user1.firstName = "John";
  user1.lastName = "Doe";
  user1.username = "test";
  user1.email = "leader@gmail.com";
  user1.isActive = true;
  userCollection.createUser(user1, "password");

  UserModel user2;
  user2._id = ObjectId.fromString("2").toString;
  user2.firstName = "John";
  user2.lastName = "Doe";
  user2.username = "test";
  user2.email = "member@gmail.com";
  user2.isActive = true;
  userCollection.createUser(user2, "password");

  UserModel user3;
  user3._id = ObjectId.fromString("3").toString;
  user3.firstName = "John";
  user3.lastName = "Doe";
  user3.username = "test";
  user3.email = "guest@gmail.com";
  user3.isActive = true;
  userCollection.createUser(user3, "password");

  UserModel user4;
  user4._id = ObjectId.fromString("4").toString;
  user4.firstName = "John";
  user4.lastName = "Doe";
  user4.username = "test";
  user4.email = "owner@gmail.com";
  user4.isActive = true;
  userCollection.createUser(user4, "password");

  UserModel user5;
  user5._id = ObjectId.fromString("5").toString;
  user5.firstName = "John";
  user5.lastName = "Doe";
  user5.username = "test";
  user5.email = "admin@gmail.com";
  user5.scopes = ["admin"];
  user5.isActive = true;
  userCollection.createUser(user5, "password");

  UserModel user6;
  user6._id = ObjectId.fromString("6").toString;
  user6.firstName = "John";
  user6.lastName = "Doe";
  user6.username = "test";
  user6.email = "other@gmail.com";
  user6.scopes = [];
  user6.isActive = true;
  userCollection.createUser(user6, "password");

  administratorToken = userCollection.createToken("admin@gmail.com", Clock.currTime + 3600.seconds, ["doStuff"], "Bearer");
  bearerToken = userCollection.createToken("owner@gmail.com", Clock.currTime + 3600.seconds, ["doStuff"], "Bearer");
  bearerLeaderToken = userCollection.createToken("leader@gmail.com", Clock.currTime + 3600.seconds, ["doStuff"], "Bearer");
  bearerMemberToken = userCollection.createToken("member@gmail.com", Clock.currTime + 3600.seconds, ["doStuff"], "Bearer");
  bearerGuestToken = userCollection.createToken("guest@gmail.com", Clock.currTime + 3600.seconds, ["doStuff"], "Bearer");
  otherToken = userCollection.createToken("other@gmail.com", Clock.currTime + 3600.seconds, ["doStuff"], "Bearer");

  userTokenList["administrator"] = administratorToken.name;
  userTokenList["owner"] = bearerToken.name;
  userTokenList["leader"] = bearerLeaderToken.name;
  userTokenList["member"] = bearerMemberToken.name;
  userTokenList["guest"] = bearerGuestToken.name;

  userId["administrator"] = ObjectId.fromString(user5._id).toString;
  userId["owner"] = ObjectId.fromString(user4._id).toString;
  userId["leader"] = ObjectId.fromString(user1._id).toString;
  userId["member"] = ObjectId.fromString(user2._id).toString;
  userId["guest"] = ObjectId.fromString(user3._id).toString;
  userId["other"] = ObjectId.fromString(user6._id).toString;

  userEmail["administrator"] = user5.email;
  userEmail["leader"] = user1.email;
  userEmail["member"] = user2.email;
  userEmail["guest"]  = user3.email;
  userEmail["owner"]  = user4.email;
}

Json createTeam(Team team) {
  auto jsonTeam = team.serializeToJson;
  jsonTeam["logo"] = jsonTeam["logo"]["_id"];

  crates.team.addItem(jsonTeam);

  return jsonTeam;
}

void createMapsWithSites() {
  cover._id = ObjectId.fromString("2");
  cover.name = "cover";
  cover.owner = "@system";
  cover.picture = new PictureFile;
  crates.picture.addItem(cover.serializeToJson);

  DefaultCrateFileSettings.path = "tmp-files/";

  team1 = Team(ObjectId.fromString("1"), "team1", Json(""),
    ["owner@gmail.com", "000000000000000000000004"],
    ["leader@gmail.com", "000000000000000000000001"],
    ["member@gmail.com", "000000000000000000000002"],
    ["guest@gmail.com", "000000000000000000000003"],
    true, cover);
  team1.contactEmail = "test1@gmail.com";

  team2 = Team(ObjectId.fromString("2"), "team2", Json(""),
    ["owner@gmail.com", "000000000000000000000004"],
    ["leader@gmail.com", "000000000000000000000001"],
    ["member@gmail.com", "000000000000000000000002"],
    ["guest@gmail.com", "000000000000000000000003"],
    false, cover);
  team3 = Team(ObjectId.fromString("3"), "team3", Json(""), [],[],[],[], true, cover);
  team4 = Team(ObjectId.fromString("4"), "team4", Json(""), [],[],[],[], false, cover);

  createTeam(team1);
  createTeam(team2);
  createTeam(team3);
  createTeam(team4);

  iconSet1._id = ObjectId.fromString("1");
  iconSet1.visibility = Visibility(team1, true, true);
  iconSet1.name = "set1";
  iconSet1.description = "description1";
  createIconSet(iconSet1);

  iconSet2._id = ObjectId.fromString("2");
  iconSet2.visibility = Visibility(team2, false);
  iconSet2.name = "set2";
  iconSet2.description = "description2";
  createIconSet(iconSet2);

  iconSet3._id = ObjectId.fromString("3");
  iconSet3.visibility = Visibility(team3, true);
  iconSet3.name = "set3";
  iconSet3.description = "description3";
  createIconSet(iconSet3);

  iconSet4._id = ObjectId.fromString("4");
  iconSet4.visibility = Visibility(team4, false);
  iconSet4.name = "set4";
  iconSet4.description = "description4";
  createIconSet(iconSet4);

  icon1._id = ObjectId.fromString("1");
  icon1.iconSet = iconSet1;
  icon1.category = "category";
  icon1.subcategory = "subcategory";
  icon1.name = "name1";
  icon1.description = "description1";
  icon1.otherNames = [ "other name 1" ];
  icon1.image = OptionalIconImage(false, new IconFile());
  icon1.attributes = [ IconAttribute("phone", "short text"), IconAttribute("program", "long text"),
                        IconAttribute("max number of people", "integer"), IconAttribute("price", "decimal"),
                        IconAttribute("kids-friendly","boolean"), IconAttribute("type of food","options","vegan,international,asian") ];
  icon1.visibility = iconSet1.visibility;
  createIcon(icon1);

  icon2._id = ObjectId.fromString("2");
  icon2.iconSet = iconSet2;
  icon2.category = "category2";
  icon2.subcategory = "subcategory2";
  icon2.name = "name2";
  icon2.description = "description2";
  icon2.image = OptionalIconImage(false, new IconFile);
  icon2.visibility = iconSet2.visibility;
  createIcon(icon2);

  Icon icon3;
  icon3._id = ObjectId.fromString("3");
  icon3.iconSet = iconSet3;
  icon3.category = "category3";
  icon3.subcategory = "subcategory3";
  icon3.name = "name3";
  icon3.description = "description3";
  icon3.image = OptionalIconImage(false, new IconFile);
  icon3.visibility = iconSet3.visibility;
  createIcon(icon3);

  Icon icon4;
  icon4._id = ObjectId.fromString("4");
  icon4.iconSet = iconSet4;
  icon4.category = "category4";
  icon4.subcategory = "subcategory4";
  icon4.name = "name4";
  icon4.description = "description4";
  icon4.image = OptionalIconImage(false, new IconFile);
  icon4.visibility = iconSet4.visibility;
  createIcon(icon4);

  map1 = createMap(Map(ObjectId.fromString("1"), "map1", Json(""),
    Visibility(team1, true), Polygon([[[-70, 30], [-70, 50], [-80, 50], [-80, 30], [-70,30]]]),
    cover, IconSets(true, [iconSet1]), BaseMaps(), "", SysTime.fromISOExtString("2015-01-01T00:00:00Z"), SysTime.fromISOExtString("2015-02-01T00:00:00Z"),
    ModelInfo(SysTime.fromISOExtString("2015-01-01T00:00:00Z"), SysTime.fromISOExtString("2015-01-01T00:00:00Z"))));

  map2 = createMap(Map(ObjectId.fromString("2"), "map2", Json(""),
    Visibility(team2, false), Polygon([[[-7, 3], [-7, 5], [-8, 5], [-8, 3], [-7,3]]]),
    cover, IconSets(true, [iconSet1]), BaseMaps(), "", SysTime.fromISOExtString("2015-01-01T00:00:00Z"), SysTime.fromISOExtString("2015-02-01T00:00:00Z"),
    ModelInfo(SysTime.fromISOExtString("2015-01-01T00:00:00Z"), SysTime.fromISOExtString("2015-01-01T00:00:00Z"))));

  map3 = createMap(Map(ObjectId.fromString("3"), "map3", Json(""),
    Visibility(team3, true), Polygon([[[-7, 3], [-7, 5], [-8, 5], [-8, 3], [-7,3]]]),
    cover, IconSets(true, [iconSet1]), BaseMaps(), "", SysTime.fromISOExtString("2015-01-01T00:00:00Z"), SysTime.fromISOExtString("2015-02-01T00:00:00Z"),
    ModelInfo(SysTime.fromISOExtString("2015-01-01T00:00:00Z"), SysTime.fromISOExtString("2015-01-01T00:00:00Z"))));

  map4 = createMap(Map(ObjectId.fromString("4"), "map4", Json(""),
    Visibility(team4, false), Polygon([[[-7, 3], [-7, 5], [-8, 5], [-8, 3], [-7,3]]]),
    cover, IconSets(true, [iconSet1]), BaseMaps(), "", SysTime.fromISOExtString("2015-01-01T00:00:00Z"), SysTime.fromISOExtString("2015-02-01T00:00:00Z"),
    ModelInfo(SysTime.fromISOExtString("2015-01-01T00:00:00Z"), SysTime.fromISOExtString("2015-01-01T00:00:00Z"))));

  createCampaign(Campaign(ObjectId.fromString("1"), "Campaign 1", Json(""), map1.visibility, map1.info, cover, [], [], OptionalMap(true, map1._id.toString)));
  createCampaign(Campaign(ObjectId.fromString("2"), "Campaign 2", Json(""), map2.visibility, map2.info, cover, [], [], OptionalMap(true, map2._id.toString)));
  createCampaign(Campaign(ObjectId.fromString("3"), "Campaign 3", Json(""), map3.visibility, map3.info, cover, [], [], OptionalMap(true, map3._id.toString)));
  createCampaign(Campaign(ObjectId.fromString("4"), "Campaign 4", Json(""), map4.visibility, map4.info, cover, [], [], OptionalMap(true, map4._id.toString)));

  createFeature(
    Feature(ObjectId.fromString("1"),
          [map1],
          "site1",
          Json("description of site1"),
          GeoJsonGeometry.fromJson(`{ "type": "Point", "coordinates": [1.5, 1.5] }`.parseJsonString),
          ModelInfo(
            SysTime.fromISOExtString("2015-01-01T00:00:00Z"),
            SysTime.fromISOExtString("2015-01-01T00:00:00Z"),
            0,"",
            "000000000000000000000001"),
          [],
          [],
          [icon1],
          VisibilityEnum.Public,
          [],
          ["name1": `{"phone": "123456", "program": "Luni-Vineri:9-18", "max number of people":"23", "price":"11.4", "kids-friendly":"true", "type of food":"international"}`.parseJsonString]));

  createFeature(
    Feature(ObjectId.fromString("2"),
          [map2],
          "site2",
          Json("description of site2"),
          GeoJsonGeometry.fromJson(`{ "type": "Point", "coordinates": [1.6, 1.6] }`.parseJsonString),
          ModelInfo(SysTime.fromISOExtString("2015-01-01T00:00:00Z"), SysTime.fromISOExtString("2015-01-01T00:00:00Z")),
          [],
          [],
          [icon1, icon2], VisibilityEnum.Private));

  createFeature(
    Feature(ObjectId.fromString("3"),
          [map3],
          "site3",
          Json("description of site3"),
          GeoJsonGeometry.fromJson(`{ "type": "Point", "coordinates": [1.7, 1.7] }`.parseJsonString),
          ModelInfo(SysTime.fromISOExtString("2015-01-01T00:00:00Z"), SysTime.fromISOExtString("2015-01-01T00:00:00Z")),
          [],
          [],
          [icon1, icon2], VisibilityEnum.Public));

  createFeature(
    Feature(ObjectId.fromString("4"),
          [map4],
          "site4",
          Json("description of site4"),
          GeoJsonGeometry.fromJson(`{ "type": "Point", "coordinates": [1.8, 1.8] }`.parseJsonString),
          ModelInfo(SysTime.fromISOExtString("2015-01-01T00:00:00Z"), SysTime.fromISOExtString("2015-01-01T00:00:00Z")),
          [],
          [],
          [icon1, icon2],
          VisibilityEnum.Private));

  auto publicCalendar = Calendar(ObjectId.fromString("1"), map1.visibility, map1.info, "Public Calendar", Json.emptyObject);
  createCalendar(publicCalendar);

  auto privateCalendar = Calendar(ObjectId.fromString("2"), map2.visibility, map2.info, "Private Calendar", Json.emptyObject);
  createCalendar(privateCalendar);

  auto publicCalendarOther = Calendar(ObjectId.fromString("3"), map3.visibility, map3.info, "Public Calendar Other Team", Json.emptyObject);
  createCalendar(publicCalendarOther);

  auto privateCalendarOther = Calendar(ObjectId.fromString("4"), map4.visibility, map4.info, "Private Calendar Other Team", Json.emptyObject);
  createCalendar(privateCalendarOther);
}

import crate.resource.gridfs;

class MockFSCursor : IFSCursor {
  InputRange!Bson range;

  @trusted:
    this(InputRange!Bson range) {
      this.range = range;
    }

    bool empty() {
      return range.empty;
    }

    Bson front() {
      return range.front;
    }

    void popFront() {
      range.popFront;
    }

    void sort(int[string]) {}
    size_t size() { return 0; }
}

class MockGridFsFiles : IGridFsCollection {
  @safe:
    size_t _count;
    static Bson metadata = Bson.emptyObject;

    size_t countDocuments(BsonObjectID[string]) {
      return _count;
    }

    MockFSCursor find(BsonObjectID[string]) {
      auto fileRecord = Bson.emptyObject;
      fileRecord["metadata"] = metadata;

      fileRecord["_id"] = BsonObjectID.generate();
      fileRecord["length"] = 0;
      fileRecord["chunkSize"] = 0;
      fileRecord["filename"] = "";
      fileRecord["metadata"] = metadata;

      return new MockFSCursor([fileRecord].inputRangeObject);
    }

    MockFSCursor find(Bson[string]) {
      BsonObjectID[string] empty;

      return this.find(empty);
    }

    void replaceOne(Bson[string], Bson) {}
    void insertOne(Bson) { _count++; }
    void deleteMany(BsonObjectID[string]) { _count--; }
    void deleteMany(Bson) { _count--; }

    MockFSCursor get() {
      Bson[] empty;
      return new MockFSCursor(empty.inputRangeObject);
    }
}

class MockEmptyGridFsFiles : IGridFsCollection {
  @safe:
    size_t countDocuments(BsonObjectID[string]) {
      return 0;
    }

    MockFSCursor find(BsonObjectID[string]) {
      Bson[] empty;
      return new MockFSCursor(empty.inputRangeObject);
    }

    MockFSCursor find(Bson[string]) {
      Bson[] empty;
      return new MockFSCursor(empty.inputRangeObject);
    }

    void replaceOne(Bson[string], Bson) {}
    void insertOne(Bson) {}
    void deleteMany(BsonObjectID[string]) {}
    void deleteMany(Bson) {}

    MockFSCursor get() {
      Bson[] empty;
      return new MockFSCursor(empty.inputRangeObject);
    }
}

class MockGridFsChunks : IGridFsCollection {
  @safe:
    static {
      Bson[] items;
    }

    Bson findOne(BsonObjectID[string]) {
      return Bson.emptyObject;
    }

    size_t countDocuments(BsonObjectID[string]) {
      return 1;
    }

    MockFSCursor find(BsonObjectID[string]) {
      return new MockFSCursor(items.inputRangeObject);
    }

    MockFSCursor find(Bson[string]) {
      return new MockFSCursor(items.inputRangeObject);
    }

    Bson updatedData;
    Bson updatedSelect;
    void replaceOne(Bson[string] selector, Bson data) {
      updatedData = data;
      updatedSelect = selector;
    }

    Bson insertedData;
    void insertOne(Bson data) {
      insertedData = data;
    }
    void deleteMany(BsonObjectID[string]) {}
    void deleteMany(Bson) {}

    MockFSCursor get() {
      return new MockFSCursor(items.inputRangeObject);
    }
}

void setMockFile(string type, string data) {
  MockGridFsFiles.metadata = Bson.fromJson(parseJsonString(`{ "mime": "` ~ type ~ `" }`));
  auto fileData = Bson.emptyObject;

  fileData["data"] = BsonBinData(BsonBinData.Type.binaryOld, data.representation);

  MockGridFsChunks.items = [fileData];
}

void setMockFile(string type, ubyte[] data) {
  MockGridFsFiles.metadata = Bson.fromJson(parseJsonString(`{ "mime": "` ~ type ~ `" }`));
  auto fileData = Bson.emptyObject;

  fileData["data"] = BsonBinData(BsonBinData.Type.binaryOld, data.idup);

  MockGridFsChunks.items = [fileData];
}

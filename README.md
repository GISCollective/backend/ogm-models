# GISCollective Models Library

GISCollective is an open source platform for easy map-making.

This repository contains the code for defining the GISCollective data models and some other classes that are commonly used in our DLang projects. It is based on the [vibe.d](https://vibed.org/) and [crate](https://gitlab.com/szabobogdan3/crate) library.

Each model has it's own dedicated file under `source/ogm/models` where all models are defined. For example the `map` model has the `source/ogm/models/map.d` file which configures the model as a struct. Here you can also find some static configs for example image sizes and formats.

The `ogm.crates.all` module contains the db initialization. The `setupOgmCrates` function connects to the db and creates the necessary indexes. For testing, the library defines a fixtures module `ogm.test.fixtures`, which initializes an in-memory storage to remove the dependency of a database while running the tests. You can call `createCrates();` from a test to setup the storage or, `setupTestData();` if you want to seed the storage with some test data.

You can read more about the architecture at [https://guide.giscollective.com/en/develop/architecture/](https://guide.giscollective.com/en/develop/architecture/) or find more about our platform at
 [https://giscollective.com](https://giscollective.com/).

## Prerequisites

You will need the following things properly installed on your computer.

* [Git](https://git-scm.com/)
* A [DLang](https://dlang.org/) compiler. We recommend using [DMD](https://dlang.org/download.html) for development and [LDC](https://github.com/ldc-developers/ldc#installation) for production releases.
* The `openssl`, `ImageMagick`, `libsqlite3`, `libevent` and `librsvg2-tools` libraries.

    For fedora/redhat:
    ```
      > dnf install -y openssl ImageMagick librsvg2-tools
    ```

    For ubuntu systems:
    ```
      > apt-get install -y git libssl-dev libevent-dev libsqlite3-dev
    ```
* The [Trial](http://trial.szabobogdan.com/) test runner
* A mongo db server running

## Installation

* `git clone --recurse-submodules -j8 <repository-url>` this repository with its submodules
* `cd ogm-server`
* create an app `config/configuration.js` file based on `config/configuration.model.js`
* create a db `config/db` folder based on `config/db.model`
* make sure `mongo` db is running
* `dub`

## Running / Development

Since this is a library, this can not be executed directly. To use it you can add the dependency in your project:

```
dub.json


...
   "dependencies": {

     ...

    "gis-collective-models": {
      "path": "./submodules/ogm-models"
    },

    ...

  },

...

```

Check the [dub](https://code.dlang.org) package manager docs for mode dedails.

### Writing Tests

We use [Trial](http://trial.szabobogdan.com/) for running tests and [FluentAsserts](http://fluentasserts.szabobogdan.com/) for writing assertions.

* `trial` to run all tests
* `trial -s "suite name"` to run just one suite
* `trial -t "test name"` to run tests that contain `test name` in the test name

### Building

We use [dub](https://dub.pm/commandline.html) for building the library:

* `dub` (development)
* `dub build --build release` (production)

## Contributing

You can find our contribution guide at [https://guide.giscollective.com/en/develop/CONTRIBUTING/](https://guide.giscollective.com/en/develop/CONTRIBUTING/)

## Code of conduct

You can find our code of conduct at [https://guide.giscollective.com/en/develop/CODE_OF_CONDUCT/](https://guide.giscollective.com/en/develop/CODE_OF_CONDUCT/)
